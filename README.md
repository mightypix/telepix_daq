DAQ for MightyPix prototype based on [Basil](https://github.com/SiLab-Bonn/basil) framework.

## Required packages

- Install [conda](http://conda.pydata.org) for python and needed packages (Note install this as user in your home directory to be able to change stuff fast):
```bash
curl https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -o miniconda.sh
bash miniconda.sh -b -p $HOME/miniconda
export PATH=$HOME/miniconda/bin:$PATH
conda update --yes conda
conda install --yes numpy bitarray pytest pyyaml numba mock matplotlib scipy pytables progressbar2 zmq
#load miniconda3 with:
source $HOME/miniconda/bin/activate

```

- Install [pySiLibUSB](https://github.com/SiLab-Bonn/pySiLibUSB) for USB support.
    * Be sure to allow read and write mode for USB with the udev rules
    * Reload with ```udevadm control --reload-rules && udevadm trigger```

- Download and install [Basil](https://github.com/SiLab-Bonn/basil) for data acquisition and generic firmware modules (tested with v2.4.4):
```bash
git clone https://github.com/SiLab-Bonn/basil
cd basil
python setup.py develop
cd ..
```

- Download and install [Online Monitor](https://github.com/SiLab-Bonn/online_monitor) for live monitoring:
```bash
git clone https://github.com/SiLab-Bonn/online_monitor.git
cd online_monitor
python setup.py develop
cd ..
```


- Download and setup [telepix_daq](https://gitlab.cern.ch/mightypix/telepix_daq)
```bash
git clone https://gitlab.cern.ch/mightypix/telepix_daq.git
cd telepix_daq
python setup.py develop
```

If you want to use Simulation install
```
pip install cocotb cocotb_bus
conda install -c conda-forge iverilog
```

## SiTCP
```bash
cd telepix_daq/telepix_daq
git clone git@github.com:BeeBeansTechnologies/SiTCP_Netlist_for_Kintex7.git
```
Be aware the following lines force the IP address to 192.168.10.16 in file telepix_daq/SiTCP_Netlist_for_Kintex7/WRAP_SiTCP_GMII_XC7K_32K.V.
```
  assign	MY_TCP_PORT[15:0]	= (~FORCE_DEFAULTn | (EXT_TCP_PORT[15:0]==16'd0)	? DEFAULT_TCP_PORT[15:0]	: EXT_TCP_PORT[15:0]	);
  assign	MY_RBCP_PORT[15:0]	= (~FORCE_DEFAULTn | (EXT_RBCP_PORT[15:0]==16'd0)	? DEFAULT_RBCP_PORT[15:0]	: EXT_RBCP_PORT[15:0]	);
  assign	MY_IP_ADDR[31:0]	= (~FORCE_DEFAULTn | (EXT_IP_ADDR[31:0]==32'd0) 	? DEFAULT_IP_ADDR[31:0]		: EXT_IP_ADDR[31:0]		);
```
Only use the EXT_ variables if you want to change it.
Set your local network to manual something like this:
```
cat /etc/sysconfig/network-scripts/ifcfg-Profile_1
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=none
IPADDR=192.168.10.100
PREFIX=24
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=no
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
IPV6_ADDR_GEN_MODE=stable-privacy
NAME=local_network_usb
UUID=CHANGE
ONBOOT=yes
HWADDR=CHANGE
DEVICE=YOUR_LOCAL_DEVICE  (eg. "enp0s20f0u3u3")
```

## Code formatting

For code formatting use pep8! For python automatic formatting [python black](https://github.com/psf/black) is used (--line-length=9999).


The code is based on the many developments [SiLab](https://github.com/SiLab-Bonn). Many thanks for all the work!

## Online Monitor:
To start the online monitor there are some depnedencies you have to install (checkout the docker!)
```
cd telepix_daq/telepix_daq/analysis/online_monitor
start_online_monitor online_monitor.yaml
```
# How to start?

Look at test_adapter_simple.py to see how to create, talk and readout the chip

Scans should go into the scans folder.