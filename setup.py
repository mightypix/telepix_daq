#!/usr/bin/env python

from setuptools import setup
from setuptools import find_packages
from platform import system

import numpy as np
import os
import telepix_daq

version = '0.0.1'


author = 'Klaas Padeken', 'Hannah Schmitz'
author_email = 'padeken@hiskp.uni-bonn.de'

# requirements for core functionality
install_requires = ['basil-daq>=3.2.0.dev0', 'bitarray>=0.8.1', 'matplotlib', 'numpy', 'pyyaml', 'scipy', 'ppretty']

setup(
    name='telepix_daq',
    version=version,
    description='DAQ for telepix',
    url='https://gitlab.cern.ch/mightypixtestbeam/FFFFF',
    license='',
    long_description='',
    author=author,
    maintainer=author,
    author_email=author_email,
    maintainer_email=author_email,
    install_requires=install_requires,
    packages=find_packages(),
    include_package_data=True,
    package_data={'': ['README.*', 'VERSION'], 'docs': ['*'], 'telepix_daq': ['*.yaml', '*.bit']},
    platforms='any'
)
