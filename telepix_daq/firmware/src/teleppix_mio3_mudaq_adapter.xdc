set_property PACKAGE_PIN P21 [get_ports rgmii_rx_ctl]
set_property PACKAGE_PIN P16 [get_ports {rgmii_rxd[0]}]
set_property PACKAGE_PIN N17 [get_ports {rgmii_rxd[1]}]
set_property PACKAGE_PIN R16 [get_ports {rgmii_rxd[2]}]
set_property PACKAGE_PIN R17 [get_ports {rgmii_rxd[3]}]
# constrains based on MIO ucf file
# ------ Constraints
# FCLK (100 MHz)
set_property PACKAGE_PIN AA3 [get_ports FCLK_IN]
set_property IOSTANDARD LVCMOS15 [get_ports FCLK_IN]


create_clock -period 10.000 -name FCLK_IN -add [get_ports FCLK_IN]
create_clock -period 8.000 -name rgmii_rxc -add [get_ports rgmii_rxc]

# Derived Clocks
# create_generated_clock -name clock_divider/conf_clk_div -source [get_pins PLLE2_BASE_inst_clk/CLKOUT0] -divide_by 80 [get_pins clock_divider/CLOCK_reg/Q]
# create_generated_clock -name clock_divider_x5/conf_clk_x5_div -source [get_pins PLLE2_BASE_inst_clk/CLKOUT0] -divide_by 16 [get_pins clock_divider_x5/CLOCK_reg/Q]
create_generated_clock -name mightypix_core/i_clock_divisor_spi/I2C_CLK -source [get_pins PLLE2_BASE_inst_comm/CLKOUT0] -divide_by 1500 [get_pins mightypix_core/i_clock_divisor_spi/CLOCK_reg/Q]
create_generated_clock -name rgmii_txc -source [get_pins rgmii/ODDR_inst/C] -divide_by 1 [get_ports rgmii_txc]
create_clock -period 8.000 -name CLK_MGT_REF -add [get_ports MGT_REFCLK1_P]
# create_clock -period 6.250 -name CLK_MGT_REF -add [get_ports MGT_REFCLK1_P]

# Exclude asynchronous clock domains from timing (handled by CDCs)
set_clock_groups -asynchronous -group BUS_CLK_PLL -group mightypix_core/i_clock_divisor_spi/I2C_CLK -group {CLK_IN CLK_RX CLK40_PLL CLK400 CLK_RX_OUT conf_clk_PLL} -group [get_clocks -include_generated_clocks rgmii_rxc] -group [get_clocks -include_generated_clocks CLK_MGT_REF]


# set_false_path -from [get_clocks BUS_CLK_PLL] -to [get_clocks clock_divider/conf_clk_div]
# set_false_path -from [get_clocks clock_divider/conf_clk_div] -to [get_clocks BUS_CLK_PLL]
set_false_path -from [get_clocks rgmii_rxc] -to [get_clocks BUS_CLK_PLL]


set_false_path -from [get_clocks rgmii_rxc] -to [get_clocks -of_objects [get_pins PLLE2_BASE_inst_comm/CLKOUT2]]

# set_false_path -from [get_clocks -of_objects [get_pins PLLE2_BASE_inst_comm/CLKOUT0]] -to [get_clocks -of_objects [get_pins PLLE2_BASE_inst_clk/CLKOUT1]]

# false path between BUS_CLK_PLL and CLK125PLLTX
set_false_path -from [get_clocks -of_objects [get_pins PLLE2_BASE_inst_comm/CLKOUT0]] -to [get_clocks -of_objects [get_pins PLLE2_BASE_inst_comm/CLKOUT2]]
set_false_path -from [get_clocks -of_objects [get_pins PLLE2_BASE_inst_comm/CLKOUT2]] -to [get_clocks -of_objects [get_pins PLLE2_BASE_inst_comm/CLKOUT0]]

#######SiTCP#######
#this separates the TCPIP logic from
set_input_delay -clock [get_clocks rgmii_rxc] -max -add_delay 10.000 [get_ports {rgmii_rxd[*]}]
set_input_delay -clock [get_clocks rgmii_rxc] -max -add_delay 10.000 [get_ports rgmii_rx_ctl]
# set_output_delay -clock [get_clocks rgmii_rxc] -max 10 [get_pins {rgmii_rxd[*]}]
# ------ LED
set_property PACKAGE_PIN M17 [get_ports {LED[0]}]
set_property PACKAGE_PIN L18 [get_ports {LED[1]}]
set_property PACKAGE_PIN L17 [get_ports {LED[2]}]
set_property PACKAGE_PIN K18 [get_ports {LED[3]}]
set_property PACKAGE_PIN P26 [get_ports {LED[4]}]
set_property PACKAGE_PIN M25 [get_ports {LED[5]}]
set_property PACKAGE_PIN L25 [get_ports {LED[6]}]
set_property PACKAGE_PIN P23 [get_ports {LED[7]}]
set_property IOSTANDARD LVCMOS25 [get_ports LED*]
set_property SLEW SLOW [get_ports LED*]

set_property IOSTANDARD LVCMOS25 [get_ports {LEMO_RX[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {LEMO_RX[0]}]

#CLK Mux
set_property PACKAGE_PIN K25 [get_ports MGT_REF_SEL]
set_property IOSTANDARD LVCMOS25 [get_ports MGT_REF_SEL]
set_property PULLUP true [get_ports MGT_REF_SEL]

# timing layer
set_property PACKAGE_PIN AB26 [get_ports TS_IN_P]
set_property PACKAGE_PIN AC26 [get_ports TS_IN_N]
set_property IOSTANDARD LVDS_25 [get_ports TS_IN_N]
set_property IOSTANDARD LVDS_25 [get_ports TS_IN_P]


# 200 MHz differential signal input
# set_property PACKAGE_PIN D6 [get_ports LVDS_IN_CLK1_P]
# set_property PACKAGE_PIN D5 [get_ports LVDS_IN_CLK1_N]
set_property PACKAGE_PIN F6 [get_ports MGT_REFCLK1_P]
set_property PACKAGE_PIN F5 [get_ports MGT_REFCLK1_N]

# ------ Button & Spare & more - omitted for now
set_property PACKAGE_PIN C18 [get_ports RESET_N]
set_property IOSTANDARD LVCMOS25 [get_ports RESET_N]
set_property PULLUP true [get_ports RESET_N]

# ------ I2C control signals
set_property PACKAGE_PIN P24 [get_ports SDA]
set_property IOSTANDARD LVCMOS25 [get_ports SDA]
set_property PACKAGE_PIN N24 [get_ports SCL]
set_property IOSTANDARD LVCMOS25 [get_ports SCL]

# ------ Trigger IOs - partial (MIO3 has fewer lemo than MIO)
# TX[0]
set_property PACKAGE_PIN AB21 [get_ports TLU_CLK]
set_property IOSTANDARD LVCMOS25 [get_ports TLU_CLK]
# TX[1]
set_property PACKAGE_PIN V23 [get_ports TLU_BUSY]
set_property IOSTANDARD LVCMOS25 [get_ports TLU_BUSY]

# ------ Async SRAM - omitted for now
# SRAM faked with SiTCP

# ------ RGMII
set_property SLEW FAST [get_ports mdio_phy_mdc]
set_property IOSTANDARD LVCMOS25 [get_ports mdio_phy_mdc]
set_property PACKAGE_PIN N16 [get_ports mdio_phy_mdc]

set_property SLEW FAST [get_ports mdio_phy_mdio]
set_property IOSTANDARD LVCMOS25 [get_ports mdio_phy_mdio]
set_property PACKAGE_PIN U16 [get_ports mdio_phy_mdio]

set_property SLEW FAST [get_ports phy_rst_n]
set_property IOSTANDARD LVCMOS25 [get_ports phy_rst_n]
set_property PACKAGE_PIN M20 [get_ports phy_rst_n]

set_property IOSTANDARD LVCMOS25 [get_ports rgmii_rxc]
set_property PACKAGE_PIN R21 [get_ports rgmii_rxc]

set_property IOSTANDARD LVCMOS25 [get_ports rgmii_rx_ctl]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_rxd[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_rxd[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_rxd[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_rxd[3]}]

set_property SLEW FAST [get_ports rgmii_txc]
set_property IOSTANDARD LVCMOS25 [get_ports rgmii_txc]
set_property PACKAGE_PIN R18 [get_ports rgmii_txc]

set_property SLEW FAST [get_ports rgmii_tx_ctl]
set_property IOSTANDARD LVCMOS25 [get_ports rgmii_tx_ctl]
set_property PACKAGE_PIN P18 [get_ports rgmii_tx_ctl]

set_property SLEW FAST [get_ports {rgmii_txd[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_txd[0]}]
set_property PACKAGE_PIN N18 [get_ports {rgmii_txd[0]}]
set_property SLEW FAST [get_ports {rgmii_txd[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_txd[1]}]
set_property PACKAGE_PIN M19 [get_ports {rgmii_txd[1]}]
set_property SLEW FAST [get_ports {rgmii_txd[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_txd[2]}]
set_property PACKAGE_PIN U17 [get_ports {rgmii_txd[2]}]
set_property SLEW FAST [get_ports {rgmii_txd[3]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_txd[3]}]
set_property PACKAGE_PIN T17 [get_ports {rgmii_txd[3]}]


#POWER ON Detection
set_property PACKAGE_PIN AE25 [get_ports POWER_ON]
set_property IOSTANDARD LVCMOS25 [get_ports POWER_ON]
set_property PULLDOWN true [get_ports POWER_ON]

# DOUT[0]
set_property PACKAGE_PIN A19 [get_ports Ctl_Load]
set_property IOSTANDARD LVCMOS25 [get_ports Ctl_Load]
set_property PULLDOWN true [get_ports Ctl_Load]

# DOUT[1]
set_property PACKAGE_PIN A18 [get_ports Ctl_SOut]
set_property IOSTANDARD LVCMOS25 [get_ports Ctl_SOut]
set_property PULLDOWN true [get_ports Ctl_SOut]

# DOUT[2]
set_property PACKAGE_PIN C16 [get_ports RST_CHIP]
set_property IOSTANDARD LVCMOS25 [get_ports RST_CHIP]
set_property PULLDOWN true [get_ports RST_CHIP]

# DOUT[3]
set_property PACKAGE_PIN B16 [get_ports Ctl_SIN]
set_property IOSTANDARD LVCMOS25 [get_ports Ctl_SIN]
set_property PULLDOWN true [get_ports Ctl_SIN]

# DIN[4]
set_property PACKAGE_PIN J20 [get_ports Ctl_Clk_1]
set_property IOSTANDARD LVCMOS25 [get_ports Ctl_Clk_1]
set_property PULLDOWN true [get_ports Ctl_Clk_1]

# DOUT[5] both MOSI & CSB are only dummies
set_property PACKAGE_PIN T24 [get_ports CSB]
set_property IOSTANDARD LVCMOS25 [get_ports CSB]
set_property PULLDOWN true [get_ports CSB]

set_property PACKAGE_PIN M24 [get_ports MOSI]
set_property IOSTANDARD LVCMOS25 [get_ports MOSI]
set_property PULLDOWN true [get_ports MOSI]

# DOUT[6]
set_property PACKAGE_PIN W23 [get_ports SCK]
set_property IOSTANDARD LVCMOS25 [get_ports SCK]
set_property PULLDOWN true [get_ports SCK]


# DOUT[19] LVDSOUT 0
set_property PACKAGE_PIN G19 [get_ports Clk_ext_LVDS_P]
set_property PACKAGE_PIN F20 [get_ports Clk_ext_LVDS_N]
set_property IOSTANDARD LVDS_25 [get_ports Clk_ext_LVDS_P]
set_property IOSTANDARD LVDS_25 [get_ports Clk_ext_LVDS_N]

# DOUT[18] LVDSOUT 1
set_property PACKAGE_PIN J18 [get_ports Clk_ref_P]
set_property PACKAGE_PIN J19 [get_ports Clk_ref_N]
set_property IOSTANDARD LVDS_25 [get_ports Clk_ref_N]
set_property IOSTANDARD LVDS_25 [get_ports Clk_ref_P]

# DOUT[17] LVDSOUT 2
set_property PACKAGE_PIN C13 [get_ports Ctl_Clk_2]
set_property IOSTANDARD LVCMOS25 [get_ports Ctl_Clk_2]
set_property PULLDOWN true [get_ports Ctl_Clk_2]

set_property PACKAGE_PIN D19 [get_ports Sync_res_P]
set_property PACKAGE_PIN D20 [get_ports Sync_res_N]
set_property IOSTANDARD LVDS_25 [get_ports Sync_res_N]
set_property IOSTANDARD LVDS_25 [get_ports Sync_res_P]
set_property PULLDOWN true [get_ports Sync_res_P]
set_property PULLDOWN true [get_ports Sync_res_N]



#DIN[1]
set_property PACKAGE_PIN T25 [get_ports MISO]
set_property IOSTANDARD LVCMOS25 [get_ports MISO]
set_property PULLDOWN true [get_ports MISO]

# DIN[8] (LVDS0)
#fast
# set_property PACKAGE_PIN G3 [get_ports DATA_LVDS_N]
# set_property PACKAGE_PIN G4 [get_ports DATA_LVDS_P]
#slow
set_property DIFF_TERM TRUE [get_ports DATA_LVDS_P]
set_property DIFF_TERM TRUE [get_ports DATA_LVDS_N]
set_property IOSTANDARD LVDS_25 [get_ports DATA_LVDS_P]
set_property IOSTANDARD LVDS_25 [get_ports DATA_LVDS_N]


# INJECTION connected to INJ_STRB
set_property PACKAGE_PIN A15 [get_ports INJECTION]
set_property IOSTANDARD LVCMOS25 [get_ports INJECTION]
set_property DRIVE 16 [get_ports INJECTION]
set_property SLEW FAST [get_ports INJECTION]

set_property PACKAGE_PIN V21 [get_ports TLU_RESET]
set_property PACKAGE_PIN Y25 [get_ports TLU_TRIGGER]
set_property IOSTANDARD LVCMOS25 [get_ports TLU_RESET]
set_property IOSTANDARD LVCMOS25 [get_ports TLU_TRIGGER]







#set_false_path -from [get_clocks -of_objects [get_pins mmcm_adv_inst/CLKOUT4]] -to [get_clocks -of_objects [get_pins mmcm_adv_inst/CLKOUT4]]
set_false_path -from [get_clocks rgmii_rxc] -to [get_clocks rgmii_rxc]


set_false_path -from [get_clocks -of_objects [get_pins PLLE2_BASE_inst_clk/CLKOUT3]] -to [get_clocks -of_objects [get_pins PLLE2_BASE_inst_clk/CLKOUT3]]





set_property PACKAGE_PIN C19 [get_ports DATA_LVDS_P]
set_property PACKAGE_PIN B19 [get_ports DATA_LVDS_N]




create_debug_core u_ila_0 ila
set_property ALL_PROBE_SAME_MU true [get_debug_cores u_ila_0]
set_property ALL_PROBE_SAME_MU_CNT 1 [get_debug_cores u_ila_0]
set_property C_ADV_TRIGGER false [get_debug_cores u_ila_0]
set_property C_DATA_DEPTH 1024 [get_debug_cores u_ila_0]
set_property C_EN_STRG_QUAL false [get_debug_cores u_ila_0]
set_property C_INPUT_PIPE_STAGES 0 [get_debug_cores u_ila_0]
set_property C_TRIGIN_EN false [get_debug_cores u_ila_0]
set_property C_TRIGOUT_EN false [get_debug_cores u_ila_0]
set_property port_width 1 [get_debug_ports u_ila_0/clk]
connect_debug_port u_ila_0/clk [get_nets [list CLK_RX_BUF]]
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe0]
set_property port_width 10 [get_debug_ports u_ila_0/probe0]
connect_debug_port u_ila_0/probe0 [get_nets [list {mightypix_core/mightypix_data_rx/core/data_to_dec[0]} {mightypix_core/mightypix_data_rx/core/data_to_dec[1]} {mightypix_core/mightypix_data_rx/core/data_to_dec[2]} {mightypix_core/mightypix_data_rx/core/data_to_dec[3]} {mightypix_core/mightypix_data_rx/core/data_to_dec[4]} {mightypix_core/mightypix_data_rx/core/data_to_dec[5]} {mightypix_core/mightypix_data_rx/core/data_to_dec[6]} {mightypix_core/mightypix_data_rx/core/data_to_dec[7]} {mightypix_core/mightypix_data_rx/core/data_to_dec[8]} {mightypix_core/mightypix_data_rx/core/data_to_dec[9]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe1]
set_property port_width 28 [get_debug_ports u_ila_0/probe1]
connect_debug_port u_ila_0/probe1 [get_nets [list {mightypix_core/mightypix_data_rx/core/wdata[0]} {mightypix_core/mightypix_data_rx/core/wdata[1]} {mightypix_core/mightypix_data_rx/core/wdata[2]} {mightypix_core/mightypix_data_rx/core/wdata[3]} {mightypix_core/mightypix_data_rx/core/wdata[4]} {mightypix_core/mightypix_data_rx/core/wdata[5]} {mightypix_core/mightypix_data_rx/core/wdata[6]} {mightypix_core/mightypix_data_rx/core/wdata[7]} {mightypix_core/mightypix_data_rx/core/wdata[8]} {mightypix_core/mightypix_data_rx/core/wdata[9]} {mightypix_core/mightypix_data_rx/core/wdata[10]} {mightypix_core/mightypix_data_rx/core/wdata[11]} {mightypix_core/mightypix_data_rx/core/wdata[12]} {mightypix_core/mightypix_data_rx/core/wdata[13]} {mightypix_core/mightypix_data_rx/core/wdata[14]} {mightypix_core/mightypix_data_rx/core/wdata[15]} {mightypix_core/mightypix_data_rx/core/wdata[16]} {mightypix_core/mightypix_data_rx/core/wdata[17]} {mightypix_core/mightypix_data_rx/core/wdata[18]} {mightypix_core/mightypix_data_rx/core/wdata[19]} {mightypix_core/mightypix_data_rx/core/wdata[20]} {mightypix_core/mightypix_data_rx/core/wdata[21]} {mightypix_core/mightypix_data_rx/core/wdata[22]} {mightypix_core/mightypix_data_rx/core/wdata[23]} {mightypix_core/mightypix_data_rx/core/wdata[24]} {mightypix_core/mightypix_data_rx/core/wdata[25]} {mightypix_core/mightypix_data_rx/core/wdata[26]} {mightypix_core/mightypix_data_rx/core/wdata[27]}]]
create_debug_core u_ila_1 ila
set_property ALL_PROBE_SAME_MU true [get_debug_cores u_ila_1]
set_property ALL_PROBE_SAME_MU_CNT 1 [get_debug_cores u_ila_1]
set_property C_ADV_TRIGGER false [get_debug_cores u_ila_1]
set_property C_DATA_DEPTH 1024 [get_debug_cores u_ila_1]
set_property C_EN_STRG_QUAL false [get_debug_cores u_ila_1]
set_property C_INPUT_PIPE_STAGES 0 [get_debug_cores u_ila_1]
set_property C_TRIGIN_EN false [get_debug_cores u_ila_1]
set_property C_TRIGOUT_EN false [get_debug_cores u_ila_1]
set_property port_width 1 [get_debug_ports u_ila_1/clk]
connect_debug_port u_ila_1/clk [get_nets [list BUS_CLK]]
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_1/probe0]
set_property port_width 28 [get_debug_ports u_ila_1/probe0]
connect_debug_port u_ila_1/probe0 [get_nets [list {mightypix_core/RX_FIFO_DATA[0]} {mightypix_core/RX_FIFO_DATA[1]} {mightypix_core/RX_FIFO_DATA[2]} {mightypix_core/RX_FIFO_DATA[3]} {mightypix_core/RX_FIFO_DATA[4]} {mightypix_core/RX_FIFO_DATA[5]} {mightypix_core/RX_FIFO_DATA[6]} {mightypix_core/RX_FIFO_DATA[7]} {mightypix_core/RX_FIFO_DATA[8]} {mightypix_core/RX_FIFO_DATA[9]} {mightypix_core/RX_FIFO_DATA[10]} {mightypix_core/RX_FIFO_DATA[11]} {mightypix_core/RX_FIFO_DATA[12]} {mightypix_core/RX_FIFO_DATA[13]} {mightypix_core/RX_FIFO_DATA[14]} {mightypix_core/RX_FIFO_DATA[15]} {mightypix_core/RX_FIFO_DATA[16]} {mightypix_core/RX_FIFO_DATA[17]} {mightypix_core/RX_FIFO_DATA[18]} {mightypix_core/RX_FIFO_DATA[19]} {mightypix_core/RX_FIFO_DATA[20]} {mightypix_core/RX_FIFO_DATA[21]} {mightypix_core/RX_FIFO_DATA[22]} {mightypix_core/RX_FIFO_DATA[23]} {mightypix_core/RX_FIFO_DATA[24]} {mightypix_core/RX_FIFO_DATA[25]} {mightypix_core/RX_FIFO_DATA[26]} {mightypix_core/RX_FIFO_DATA[27]}]]
create_debug_core u_ila_2 ila
set_property ALL_PROBE_SAME_MU true [get_debug_cores u_ila_2]
set_property ALL_PROBE_SAME_MU_CNT 1 [get_debug_cores u_ila_2]
set_property C_ADV_TRIGGER false [get_debug_cores u_ila_2]
set_property C_DATA_DEPTH 1024 [get_debug_cores u_ila_2]
set_property C_EN_STRG_QUAL false [get_debug_cores u_ila_2]
set_property C_INPUT_PIPE_STAGES 0 [get_debug_cores u_ila_2]
set_property C_TRIGIN_EN false [get_debug_cores u_ila_2]
set_property C_TRIGOUT_EN false [get_debug_cores u_ila_2]
set_property port_width 1 [get_debug_ports u_ila_2/clk]
connect_debug_port u_ila_2/clk [get_nets [list CLK_IN_BUF]]
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_2/probe0]
set_property port_width 48 [get_debug_ports u_ila_2/probe0]
connect_debug_port u_ila_2/probe0 [get_nets [list {mightypix_core/TIMESTAMP[0]} {mightypix_core/TIMESTAMP[1]} {mightypix_core/TIMESTAMP[2]} {mightypix_core/TIMESTAMP[3]} {mightypix_core/TIMESTAMP[4]} {mightypix_core/TIMESTAMP[5]} {mightypix_core/TIMESTAMP[6]} {mightypix_core/TIMESTAMP[7]} {mightypix_core/TIMESTAMP[8]} {mightypix_core/TIMESTAMP[9]} {mightypix_core/TIMESTAMP[10]} {mightypix_core/TIMESTAMP[11]} {mightypix_core/TIMESTAMP[12]} {mightypix_core/TIMESTAMP[13]} {mightypix_core/TIMESTAMP[14]} {mightypix_core/TIMESTAMP[15]} {mightypix_core/TIMESTAMP[16]} {mightypix_core/TIMESTAMP[17]} {mightypix_core/TIMESTAMP[18]} {mightypix_core/TIMESTAMP[19]} {mightypix_core/TIMESTAMP[20]} {mightypix_core/TIMESTAMP[21]} {mightypix_core/TIMESTAMP[22]} {mightypix_core/TIMESTAMP[23]} {mightypix_core/TIMESTAMP[24]} {mightypix_core/TIMESTAMP[25]} {mightypix_core/TIMESTAMP[26]} {mightypix_core/TIMESTAMP[27]} {mightypix_core/TIMESTAMP[28]} {mightypix_core/TIMESTAMP[29]} {mightypix_core/TIMESTAMP[30]} {mightypix_core/TIMESTAMP[31]} {mightypix_core/TIMESTAMP[32]} {mightypix_core/TIMESTAMP[33]} {mightypix_core/TIMESTAMP[34]} {mightypix_core/TIMESTAMP[35]} {mightypix_core/TIMESTAMP[36]} {mightypix_core/TIMESTAMP[37]} {mightypix_core/TIMESTAMP[38]} {mightypix_core/TIMESTAMP[39]} {mightypix_core/TIMESTAMP[40]} {mightypix_core/TIMESTAMP[41]} {mightypix_core/TIMESTAMP[42]} {mightypix_core/TIMESTAMP[43]} {mightypix_core/TIMESTAMP[44]} {mightypix_core/TIMESTAMP[45]} {mightypix_core/TIMESTAMP[46]} {mightypix_core/TIMESTAMP[47]}]]
set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
connect_debug_port dbg_hub/clk [get_nets CLK_IN_BUF]
