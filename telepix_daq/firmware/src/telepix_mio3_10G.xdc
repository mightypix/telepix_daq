# Clock domains
# CLK_SYS: 100 MHz, from xtal oscillator
# -> PLL1: main chip clocks
# CLK_MGT_REF: 160 MHz, from Si570 programmable oscilaltor
# ->       not used at the moment
# MGT_REFCLK1_P: 156.25 MHz, from 10G Ethernet
# ->       BUS_CLK: 156.25 MHz, main system clock


# constrains based on MIO ucf file
# ------ Constraints
# FCLK (100 MHz)
set_property PACKAGE_PIN AA3 [get_ports FCLK_IN]
set_property IOSTANDARD LVCMOS15 [get_ports FCLK_IN]


create_clock -period 10.000 -name FCLK_IN -add [get_ports FCLK_IN]
# create_clock -period 6.250 -name CLK_MGT_REF -waveform {0.000 3.125} [get_ports {MGT_REFCLK0_P}]


# Derived Clocks
create_generated_clock -name I2C_CLK -source MGT_REFCLK1_P -divide_by 1600 [get_pins mightypix_core/i_clock_divisor_spi/CLOCK_reg/Q]
# create_generated_clock -name mightypix_core/i_clock_divisor_spi/I2C_CLK -source [get_pins PLLE2_BASE_inst_comm/CLKOUT0] -divide_by 1500 [get_pins mightypix_core/i_clock_divisor_spi/CLOCK_reg/Q]

# Exclude asynchronous clock domains from timing (handled by CDCs)
# set_clock_groups -asynchronous -group BUS_CLK_PLL -group mightypix_core/i_clock_divisor_spi/I2C_CLK -group {CLK_IN CLK_RX CLK40_PLL CLK400 CLK_RX_OUT conf_clk_PLL}  -group [get_clocks -include_generated_clocks MGT_REFCLK1_P]
set_clock_groups -asynchronous -group {CLK_IN CLK_RX CLK40_PLL CLK400 CLK_RX_OUT conf_clk_PLL}  -group [get_clocks -include_generated_clocks MGT_REFCLK1_P]

set_false_path -from [get_clocks -of_objects [get_pins PLLE2_BASE_inst_clk/CLKOUT3]] -to [get_clocks -of_objects [get_pins PLLE2_BASE_inst_clk/CLKOUT3]]
set_false_path -from [get_clocks -of_objects [get_pins PLLE2_BASE_inst_clk/CLKOUT1]] -to [get_clocks -of_objects [get_pins PLLE2_BASE_inst_clk/CLKOUT1]]


# ------ LED
set_property PACKAGE_PIN M17 [get_ports {LED[0]}]
set_property PACKAGE_PIN L18 [get_ports {LED[1]}]
set_property PACKAGE_PIN L17 [get_ports {LED[2]}]
set_property PACKAGE_PIN K18 [get_ports {LED[3]}]
set_property PACKAGE_PIN P26 [get_ports {LED[4]}]
set_property PACKAGE_PIN M25 [get_ports {LED[5]}]
set_property PACKAGE_PIN L25 [get_ports {LED[6]}]
set_property PACKAGE_PIN P23 [get_ports {LED[7]}]
set_property IOSTANDARD LVCMOS25 [get_ports LED*]
set_property SLEW SLOW [get_ports LED*]

set_property IOSTANDARD LVCMOS25 [get_ports {LEMO_RX[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {LEMO_RX[0]}]

#CLK Mux
set_property PACKAGE_PIN K25 [get_ports MGT_REF_SEL]
set_property IOSTANDARD LVCMOS25 [get_ports MGT_REF_SEL]
set_property PULLUP true [get_ports MGT_REF_SEL]

# timing layer
set_property PACKAGE_PIN AB26 [get_ports TS_IN_P]
set_property PACKAGE_PIN AC26 [get_ports TS_IN_N]
set_property IOSTANDARD LVDS_25 [get_ports TS_IN_N]
set_property IOSTANDARD LVDS_25 [get_ports TS_IN_P]


# 200 MHz differential signal input
set_property PACKAGE_PIN G19 [get_ports LVDS_IN_CLK1_P]
set_property PACKAGE_PIN F20 [get_ports LVDS_IN_CLK1_N]
set_property DIFF_TERM TRUE [get_ports LVDS_IN_CLK1_P]
set_property DIFF_TERM TRUE [get_ports LVDS_IN_CLK1_N]
set_property IOSTANDARD LVDS_25 [get_ports LVDS_IN_CLK1_P]
set_property IOSTANDARD LVDS_25 [get_ports LVDS_IN_CLK1_N]
# set_property PACKAGE_PIN F6 [get_ports MGT_REFCLK1_P]
# set_property PACKAGE_PIN F5 [get_ports MGT_REFCLK1_N]

# ------ Button & Spare & more - omitted for now
set_property PACKAGE_PIN C18 [get_ports RESET_N]
set_property IOSTANDARD LVCMOS25 [get_ports RESET_N]
set_property PULLUP true [get_ports RESET_N]

# ------ I2C control signals
set_property PACKAGE_PIN P24 [get_ports SDA]
set_property IOSTANDARD LVCMOS25 [get_ports SDA]
set_property PACKAGE_PIN N24 [get_ports SCL]
set_property IOSTANDARD LVCMOS25 [get_ports SCL]

# ------ Trigger IOs - partial (MIO3 has fewer lemo than MIO)
# TX[0]
set_property PACKAGE_PIN AB21 [get_ports TLU_CLK]
set_property IOSTANDARD LVCMOS25 [get_ports TLU_CLK]
# TX[1]
set_property PACKAGE_PIN V23 [get_ports TLU_BUSY]
set_property IOSTANDARD LVCMOS25 [get_ports TLU_BUSY]

set_property PACKAGE_PIN Y25 [get_ports TLU_RESET]
set_property PACKAGE_PIN V21 [get_ports TLU_TRIGGER]
set_property IOSTANDARD LVCMOS25 [get_ports TLU_RESET]
set_property IOSTANDARD LVCMOS25 [get_ports TLU_TRIGGER]



# SFP+ related signals
# set_property PACKAGE_PIN G10 [get_ports MGT_REFCLK1_P]
# set_property PACKAGE_PIN G9 [get_ports MGT_REFCLK3_N]
# set_property PACKAGE_PIN C9 [get_ports SFP_RX_P]
# set_property PACKAGE_PIN B9 [get_ports SFP_TX_P]
# set_property PACKAGE_PIN AA24 [get_ports SFP_TX_DISABLE]
# set_property IOSTANDARD LVCMOS25 [get_ports SFP_TX_DISABLE]

set_property PACKAGE_PIN F6 [get_ports MGT_REFCLK1_P]
set_property PACKAGE_PIN F5 [get_ports MGT_REFCLK1_N]
set_property PACKAGE_PIN B6 [get_ports SFP_RX_P]
set_property PACKAGE_PIN A4 [get_ports SFP_TX_P]
set_property PACKAGE_PIN AB24 [get_ports SFP_TX_DISABLE]
set_property IOSTANDARD LVCMOS25 [get_ports SFP_TX_DISABLE]

#POWER ON Detection
set_property PACKAGE_PIN AE25 [get_ports POWER_ON]
set_property IOSTANDARD LVCMOS25 [get_ports POWER_ON]
set_property PULLDOWN true [get_ports POWER_ON]

# DIN0
set_property PACKAGE_PIN A19 [get_ports Ctl_SIN]
set_property IOSTANDARD LVCMOS25 [get_ports Ctl_SIN]
set_property PULLDOWN true [get_ports Ctl_SIN]

# Load is not used -> unsed PIN
set_property PACKAGE_PIN AA25 [get_ports Ctl_Load]
set_property IOSTANDARD LVCMOS25 [get_ports Ctl_Load]
set_property PULLDOWN true [get_ports Ctl_Load]

# DIN2
set_property PACKAGE_PIN C16 [get_ports Ctl_Clk_2]
set_property IOSTANDARD LVCMOS25 [get_ports Ctl_Clk_2]
set_property PULLDOWN true [get_ports Ctl_Clk_2]

# DIN1
set_property PACKAGE_PIN A18 [get_ports Ctl_Clk_1]
set_property IOSTANDARD LVCMOS25 [get_ports Ctl_Clk_1]
set_property PULLDOWN true [get_ports Ctl_Clk_1]

# Dummy
set_property PACKAGE_PIN D13 [get_ports MOSI]
set_property IOSTANDARD LVCMOS25 [get_ports MOSI]
set_property PULLDOWN true [get_ports MOSI]

# Dummy
set_property PACKAGE_PIN U25 [get_ports CSB]
set_property IOSTANDARD LVCMOS25 [get_ports CSB]
set_property PULLDOWN true [get_ports CSB]

# Dummy
set_property PACKAGE_PIN W23 [get_ports SCK]
set_property IOSTANDARD LVCMOS25 [get_ports SCK]
set_property PULLDOWN true [get_ports SCK]

# DIN4 rest to chip
set_property PACKAGE_PIN C13 [get_ports RST_CHIP]
set_property IOSTANDARD LVCMOS25 [get_ports RST_CHIP]
set_property PULLDOWN true [get_ports RST_CHIP]


# DOUT[19] LVDSOUT 0
set_property PACKAGE_PIN K20 [get_ports Clk_ext_LVDS_P]
set_property PACKAGE_PIN J20 [get_ports Clk_ext_LVDS_N]
set_property IOSTANDARD LVDS_25 [get_ports Clk_ext_LVDS_P]
set_property IOSTANDARD LVDS_25 [get_ports Clk_ext_LVDS_N]

# DOUT[18] LVDSOUT 1
set_property PACKAGE_PIN J18 [get_ports Clk_ref_P]
set_property PACKAGE_PIN J19 [get_ports Clk_ref_N]
set_property IOSTANDARD LVDS_25 [get_ports Clk_ref_N]
set_property IOSTANDARD LVDS_25 [get_ports Clk_ref_P]

# DOUT[17] LVDSOUT 2
set_property PACKAGE_PIN D19 [get_ports Sync_res_P]
set_property PACKAGE_PIN D20 [get_ports Sync_res_N]
set_property IOSTANDARD LVDS_25 [get_ports Sync_res_N]
set_property IOSTANDARD LVDS_25 [get_ports Sync_res_P]
set_property PULLDOWN true [get_ports Sync_res_P]
set_property PULLDOWN true [get_ports Sync_res_N]


# Dummy
set_property PACKAGE_PIN W26 [get_ports Ctl_SOut]
set_property IOSTANDARD LVCMOS25 [get_ports Ctl_SOut]
set_property PULLDOWN true [get_ports Ctl_SOut]

#Dummy
set_property PACKAGE_PIN T25 [get_ports MISO]
set_property IOSTANDARD LVCMOS25 [get_ports MISO]
set_property PULLDOWN true [get_ports MISO]


# DIN[8] (LVDS0)
# fast data lines
# set_property PACKAGE_PIN G3 [get_ports DATA_LVDS_N]
# set_property PACKAGE_PIN G4 [get_ports DATA_LVDS_P]
# # set_property PACKAGE_PIN E3 [get_ports DATA_LVDS_N]
# # set_property PACKAGE_PIN E4 [get_ports DATA_LVDS_P]
# slow data lines
set_property PACKAGE_PIN C19 [get_ports DATA_LVDS_P]
set_property PACKAGE_PIN B19 [get_ports DATA_LVDS_N]
set_property DIFF_TERM TRUE [get_ports DATA_LVDS_P]
set_property DIFF_TERM TRUE [get_ports DATA_LVDS_N]
set_property IOSTANDARD LVDS_25 [get_ports DATA_LVDS_P]
set_property IOSTANDARD LVDS_25 [get_ports DATA_LVDS_N]


# INJECTION connected to INJ_STRB
set_property PACKAGE_PIN A15 [get_ports INJECTION]
set_property IOSTANDARD LVCMOS25 [get_ports INJECTION]
set_property DRIVE 16 [get_ports INJECTION]
set_property SLEW FAST [get_ports INJECTION]


# SPI configuration flash
set_property CONFIG_MODE SPIx4 [current_design]
set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]
set_property BITSTREAM.CONFIG.CONFIGRATE 33 [current_design]







set_property PULLDOWN true [get_ports SCL]
set_property PULLDOWN true [get_ports SDA]
set_property SLEW FAST [get_ports SCL]
set_property SLEW FAST [get_ports SDA]
set_property DRIVE 12 [get_ports SCL]

