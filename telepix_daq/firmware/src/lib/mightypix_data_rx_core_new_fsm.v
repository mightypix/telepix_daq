/**
 * ------------------------------------------------------------
 * Copyright (c) All rights reserved
 * SiLab, Institute of Physics, University of Bonn
 * ------------------------------------------------------------
 */
`timescale 1ps/1ps
`default_nettype none

module mightypix_data_rx_core_slow_new #(
    parameter           DSIZE = 10,
    parameter           DATA_IDENTIFIER = 4'h1,
    parameter           ABUSWIDTH = 32
) (
    // Bus
    input wire BUS_CLK,
    input wire [ABUSWIDTH-1:0] BUS_ADD,
    input wire [7:0] BUS_DATA_IN,
    output reg [7:0] BUS_DATA_OUT,
    input wire BUS_RST,
    input wire BUS_WR,
    input wire BUS_RD,
    input wire TS_CLK,
    // clocks
    // input wire RX_CLK, // same as 1 bit rate
    input wire CLK_RX_OUT, // one bit alligns with full cycle
    // input wire CLK_RX_OUTX2, // one bit alligns with full cycle
    input wire RX_CLKW, // 1/10 of the 1 bit rate
    // chip
    input wire RX_DATA,
    // input wire [47:0] FPGA_TIMESTAMP,
    input wire FIFO_READ,
    output wire FIFO_EMPTY,
    output wire [31:0] FIFO_DATA,
    output wire RX_READY,
    output wire RX_ENABLED,

    // Fast receiver modules - not used here
    output wire [3:0] rxcharisk,
    output wire [3:0] rxdisperr,
    output wire [3:0] rxnotintable, 
    output wire TRACK_DATA_OUT,
    output wire rxusrclk,
    output wire rxusrclk2,
    
    // trigger TS if data
    output wire ts_data_trigger_sync

);

localparam VERSION = 1;

// writing to register 0 asserts soft reset
wire SOFT_RST;
assign SOFT_RST = (BUS_ADD == 0 && BUS_WR);

wire RST;
assign RST = BUS_RST | SOFT_RST; 

// writing to register 1 asserts reset RX only
wire RX_RST;
assign RX_RST = (BUS_ADD==1 && BUS_WR);

// assign RX_READY = rx_ready_bus_clk;
assign RX_READY = rx_ready_bus_clk;




// registers
// 0 - soft reset
// 1 - RX reset
// 2 - status
// 3-4 - fifo size
// 5 - 8b10b decoder error counter
// 6 - lost data counter
wire rx_ready_bus_clk;
wire [31:0] load_col2_cnt;
wire [15:0] load_pix2_cnt;
wire [12:0] fifo_size;     // BUS_ADD==3, 4
reg [7:0] decoder_err_cnt; // BUS_ADD==5
reg [7:0] lost_err_cnt; // BUS_ADD==8
reg [7:0] read_col2_cnt;    // BUS_ADD==7
reg [19:0] raw_data;
reg [4:0] CONF_DATA_DLY;
wire [4:0] DATA_DLY_OUT;


//--------------status register----------------------//
// status register 10x8b with different states
// only in write direction
reg [7:0] status_regs [9:0];

always @(posedge BUS_CLK) begin
    if(RX_RST) begin
        status_regs[2] <= 8'b0000_0000; // disable Rx by default
        status_regs[7] <= 8'b0000_0000;
        status_regs[8] <= 8'b0000_0000;
        status_regs[9] <= 8'b0000_0000;
    end
    else if(BUS_WR && BUS_ADD < 10)
        status_regs[BUS_ADD[3:0]] <= BUS_DATA_IN;
end


wire CONF_EN_RX; // BUS_ADD==2 BIT==2

`ifdef COCOTB_SIM
    assign CONF_EN_RX = 1'b1;
`else
    assign CONF_EN_RX = status_regs[2][2];
`endif

assign RX_ENABLED = CONF_EN_RX;





//----------------BUS DATA-----------------------//

// reset and switches sync (I am not so sure whether all are needed...)
reg [5:0] rst_cnt;
always @(posedge BUS_CLK) begin
    if(RST)
        rst_cnt <=1;
    else if (rst_cnt!=0)
        rst_cnt <= rst_cnt +1;
end


wire RST_LONG;
assign  RST_LONG = (rst_cnt)!=0;


`ifdef COCOTB_SIM
    assign rx_ready_bus_clk = 1'b1;
    always @(posedge BUS_CLK) begin
        assign CONF_EN = 1'b1;
        assign CONF_INVERT_RX =1'b0;
    end
`endif


// only in read direction
always @(posedge BUS_CLK) begin
    if(BUS_RD) begin
        if(BUS_ADD == 0)
            BUS_DATA_OUT <= VERSION;
        // else if(BUS_ADD == 2)
        // `ifdef COCOTB_SIM
        //     BUS_DATA_OUT <= {status_regs[2][7:1], 1'b1};
        // `else
        //     BUS_DATA_OUT <= {status_regs[2][7:1], rx_ready_bus_clk};
        // `endif
        else if(BUS_ADD == 1)
            BUS_DATA_OUT <= {CONF_DUMP_ALL, CONF_CAPTURE_RAW,
                             CONF_INVERT_RX, CONF_EN, rx_ready_bus_clk,
                             DECODER_ERR, READ_COL2, LOST_ERR}; //state
        else if(BUS_ADD == 2)
            BUS_DATA_OUT <= {CONF_EN_TS, CONF_SAMPLING_EDGE, CONF_DATA_DLY_WR, CONF_DATA_DLY};

        else if(BUS_ADD == 3)
            BUS_DATA_OUT <= fifo_size[7:0];
        else if(BUS_ADD == 4)
            BUS_DATA_OUT <= fifo_size[12:8];
        else if(BUS_ADD == 5)
            BUS_DATA_OUT <= decoder_err_cnt[7:0];
        else if(BUS_ADD == 6)
            BUS_DATA_OUT <= {3'b0, DATA_DLY_OUT};
        else if(BUS_ADD == 7)
            BUS_DATA_OUT <= read_col2_cnt;
        else if(BUS_ADD == 8)
            BUS_DATA_OUT <= lost_err_cnt;
        else if(BUS_ADD == 9)
            BUS_DATA_OUT <= load_col2_cnt[7:0];
        else if(BUS_ADD == 10)
            BUS_DATA_OUT <= load_pix2_cnt[7:0];
        else if(BUS_ADD == 11)
            BUS_DATA_OUT <= raw_data[7:0];
        else if(BUS_ADD == 12)
            BUS_DATA_OUT <= raw_data[15:8];
        else if(BUS_ADD == 13)
            BUS_DATA_OUT <= {4'b0,raw_data[19:16]};
        // else if(BUS_ADD == 14)
        //     BUS_DATA_OUT <= {3'b0, CONF_DLY_CNT};
        else
            BUS_DATA_OUT <= 8'b0;
    end
end



wire CONF_EN_RX_WCLK;
three_stage_synchronizer conf_en_rx_synchronizer_data_clk (
    .CLK(RX_CLKW),
    .IN(CONF_EN_RX),
    .OUT(CONF_EN_RX_WCLK)
);

three_stage_synchronizer rx_ready_synchronizer_bus_clk (
    .CLK(BUS_CLK),
    .IN(rec_sync_ready),
    .OUT(rx_ready_bus_clk)
);

wire CONF_INVERT_RX_SYNC;
three_stage_synchronizer sync_conf_invert_data (.CLK(RX_CLKW), .IN(CONF_INVERT_RX), .OUT(CONF_INVERT_RX_SYNC));
wire CONF_CAPTURE_RAW_SYNC;
three_stage_synchronizer sync_capture_raw (.CLK(RX_CLKW), .IN(CONF_CAPTURE_RAW), .OUT(CONF_CAPTURE_RAW_SYNC));
wire CONF_DUMP_ALL_SYNC;
three_stage_synchronizer sync_conf_dump_all (.CLK(RX_CLKW), .IN(CONF_DUMP_ALL), .OUT(CONF_DUMP_ALL_SYNC));
wire ts_data_trigger;
three_stage_synchronizer sync_ts_data (.CLK(RX_CLKW), .IN(ts_data_trigger), .OUT(ts_data_trigger_sync));
wire CONF_EN_SYNC;
three_stage_synchronizer sync_conf_en (.CLK(RX_CLKW), .IN(CONF_EN), .OUT(CONF_EN_SYNC));
wire CONF_RESET_CNT_SYNC;
three_stage_synchronizer sync_reset_cnt (.CLK(RX_CLKW), .IN(CONF_RESET_CNT), .OUT(CONF_RESET_CNT_SYNC));
wire RESET_WCLK;
cdc_reset_sync cdc_reset_sync_wclk (.clk_in(BUS_CLK), .pulse_in(RST), .clk_out(RX_CLKW), .pulse_out(RESET_WCLK));
wire RESET_FIFO;  //generate delayed and long reset (TH, this is not understood well...)
cdc_reset_sync cdc_reset_fifo (.clk_in(RX_CLKW), .pulse_in(RESET_WCLK), .clk_out(BUS_CLK), .pulse_out(RESET_FIFO));




//------------------ DATA START -------------------------------//
reg CONF_DUMP_ALL, CONF_INVERT_RX, CONF_EN, CONF_EN_TS;
reg CONF_SAMPLING_EDGE, CONF_CAPTURE_RAW, CONF_DATA_DLY_WR;
reg CONF_RESET_CNT;
wire DECODER_ERR, READ_COL2, LOST_ERR;

always @(posedge BUS_CLK) begin
    if(BUS_RST) begin
        CONF_INVERT_RX <= 0;
        CONF_EN <= 1'b0;
        CONF_SAMPLING_EDGE <= 0;
        CONF_CAPTURE_RAW <= 0;
        CONF_DUMP_ALL <= 1'b0;
        CONF_DATA_DLY_WR <=1'b0;
        CONF_EN_TS <= 1'b0;
        CONF_RESET_CNT <= 1'b0;
        // CONF_DLY_CNT <= 4'b0; 
    end
        else if(BUS_WR) begin
        if(BUS_ADD == 1)
            {CONF_DUMP_ALL, CONF_CAPTURE_RAW, CONF_INVERT_RX, CONF_EN} <= BUS_DATA_IN[7:4];
        else if(BUS_ADD == 2)
            // {CONF_RESET_CNT, CONF_SAMPLING_EDGE, CONF_DATA_DLY_WR, CONF_DATA_DLY} <= BUS_DATA_IN[3:0];
            {CONF_RESET_CNT} <= BUS_DATA_IN[0];
        // else if(BUS_ADD == 5)
        //     CONF_DLY_CNT <= BUS_DATA_IN[4:0];

    end

end

// status
assign DECODER_ERR = (decoder_err_cnt!=8'b0);
assign LOST_ERR = (lost_err_cnt!=8'b0);
assign READ_COL2 = (read_col2_cnt!=8'b0);



///////////////////////DDR without delay /////////////////////

wire RX_DDR;
wire RX_DLY;
assign DATA_DLY_OUT = 5'b0;
// TODO fill codes here!
assign RX_DLY = RX_DATA;
wire Q1, Q2;
IDDR IDDR_RX (
    .Q1(Q1),
    .Q2(Q2),
    .C(CLK_RX_OUT),
    .CE(1'b1),
    .D(RX_DLY),
    .R(1'b0),
    .S(1'b0)
);
// assign RX_DDR = Q1 ;
assign RX_DDR = CONF_SAMPLING_EDGE ? Q1 : Q2;  /// !!!
// assign RX_DDR = RX_DLY;
///////////////////////DDR without delay end/////////////////////


// (* mark_debug = "true" *) wire [47:0] FPGA_TIMESTAMP_BUF; 
// BUFG BUFG_inst_FPGATS( .O(FPGA_TIMESTAMP_BUF), .I(FPGA_TIMESTAMP) );

wire cdc_ts_fifo_empty, cdc_ts_fifo_full;
wire RESET_FIFO_TS, RESET_WCLK_TS;
// (* mark_debug = "true" *) wire [47:0] FPGA_TIMESTAMP_CLK; //TIMESTAMP in fifo clk domain
cdc_reset_sync rst_wclk_sync (.clk_in(BUS_CLK), .pulse_in(RST), .clk_out(TS_CLK), .pulse_out(RESET_WCLK_TS));
cdc_reset_sync rst_fclk_sync (.clk_in(BUS_CLK), .pulse_in(RST), .clk_out(RX_CLKW), .pulse_out(RESET_FIFO_TS));

// cdc_syncfifo #(
//     .DSIZE(48),
//     .ASIZE(2)
// ) cdc_syncfifo_ts (
//     .rdata(FPGA_TIMESTAMP_CLK),
//     .wfull(cdc_ts_fifo_full),
//     .rempty(cdc_ts_fifo_empty),
//     .wdata(FPGA_TIMESTAMP),
//     .winc(1'b1),
//     .wclk(TS_CLK),
//     .wrst(RESET_WCLK_TS),
//     .rinc(!cdc_ts_fifo_empty),
//     .rclk(RX_CLKW),
//     .rrst(RESET_FIFO_TS)
// );





// 10b data sync
(* mark_debug = "true" *)  wire [9:0] data_8b10b;
reg decoder_err;
wire rec_sync_ready;
wire aligned;
rec_sync rec_sync (
    .reset(RESET_WCLK),
    .datain(CONF_INVERT_RX_SYNC ? ~RX_DDR : RX_DDR),
    .data(data_8b10b),
    .WCLK(RX_CLKW),
    .FCLK(CLK_RX_OUT),
    .rec_sync_ready(rec_sync_ready), //output
    .aligned(aligned),
    .decoder_err(decoder_err)        //input
);


// 8b/10b decode
reg [9:0] data_to_dec;
integer i;
always @ (*) begin
    for (i=0; i<10; i=i+1)
        data_to_dec[(10-1)-i] = data_8b10b[i];
end
always @(posedge RX_CLKW)
    if (CONF_CAPTURE_RAW_SYNC)
        raw_data <= {raw_data[9:0], data_to_dec};

reg dispin;
wire dispout;
always@(posedge RX_CLKW) begin
    if(RESET_WCLK)
        dispin <= 1'b0;
    else
        dispin <= dispout;
end
(* mark_debug = "true" *) wire dec_k;
(* mark_debug = "true" *) wire [7:0] dec_data;
wire code_err;
wire disp_err;
decode_8b10b decode_8b10b (
    .datain(data_to_dec),
    .dispin(dispin),
    .dataout({dec_k, dec_data}), // control character, data out
    .dispout(dispout),
    .code_err(code_err),
    .disp_err(disp_err)
);
always @(posedge RX_CLKW) begin
    if(RESET_WCLK)
        decoder_err <= 1'b0;
    else
        decoder_err <= code_err | disp_err;
end
wire write_en;
assign write_en = (rec_sync_ready|CONF_CAPTURE_RAW_SYNC) & CONF_EN_SYNC;
always@(posedge RX_CLKW) begin
    // if(RESET_WCLK | CONF_RESET_CNT_SYNC)
    if(RESET_WCLK)
        decoder_err_cnt <= 0;
    else
        if(decoder_err && write_en && decoder_err_cnt != 8'hff)
        // if(decoder_err && write_en)
            decoder_err_cnt <= decoder_err_cnt + 1;
end



wire sof;
// assign sof = write_en && ((dec_k ==1 && dec_data==8'h1C) || (dec_k ==0 && dec_data==8'hAA));
assign sof = write_en && ((dec_k ==0 && dec_data==8'hC0));
assign ts_data_trigger = sof;
// assign ts_data_trigger = busy;
// wire eof;
// assign eof = write_en && dec_k && (dec_data==8'b010_11100 || dec_data==8'b011_11100);
wire idle;
// assign idle = write_en && dec_k && (dec_data==8'b101_11100);
assign idle = write_en && dec_k;
// assign idle = write_en && ((dec_k && ((dec_data==8'b101_11100) || dec_data==8'h1C ) ) || (dec_k ==0 && dec_data==8'hAA));
wire busy;
assign busy = write_en && !idle;
// assign busy = write_en && !idle  && !dec_k;

// reg [8:0] data_memory;
reg [2:0] byte_sel;
always@(posedge RX_CLKW) begin
    if(RESET_WCLK)
        byte_sel <= 0;
    // else if(busy  || sof) begin
    else if(busy) begin
        if (byte_sel == 2)
            byte_sel <= 0;
        else
            byte_sel <= byte_sel+1;
    end
    else
        byte_sel <= 0;
    // data_memory <= {dec_k, dec_data};
end

// wire is_lc2;
// assign is_lc2 = (({dec_k, dec_data} == 9'h0AA) && (data_memory== 9'h11C));

reg [8:0] data_dec_in [2:0];
always@(posedge RX_CLKW) begin
    if(RESET_WCLK) begin
        for (i=0; i<3; i=i+1)
            data_dec_in[i] <= 8'b0;
    end
    // else if ((busy && dec_k==1'b0 ) || sof) begin
    else if ((busy && dec_k==1'b0 ) || sof) begin
    // else if ((busy && dec_k==1'b0 &&  !is_lc2) || sof) begin
            data_dec_in[byte_sel] <= {dec_k, dec_data};
    end
    else if (idle && byte_sel==2)
        data_dec_in[2] <= {1'b1, 8'b010_11100};
    // else if (busy && is_lc2)
    //     data_dec_in[byte_sel] <= {1'b1, 8'b010_11100};
    
    else if (idle && byte_sel==1) begin
        data_dec_in[1] <= {1'b1, 8'b010_11100};        // 0x5c k28.2
        data_dec_in[2] <= {1'b1, 8'b010_11100};
    end
end


reg busy_ff;
always@(posedge RX_CLKW) begin
    busy_ff <= busy && (byte_sel==1 || byte_sel==0);
end


reg write_dec_in;
always@(*) begin
    if(RESET_WCLK) 
        write_dec_in <= 1'b0;
    else if (byte_sel==2)
        write_dec_in <= 1'b1;
    else if (idle && busy_ff)
        write_dec_in <= 1'b1;
    else
        write_dec_in <= 1'b0;
end

wire [27:0] wdata;

// localparam STATE_IDLE = 2'b00, STATE_TS_MSB = 2'b01, STATE_TS_LSB = 2'b10;
// reg [1:0] state = STATE_IDLE;
// reg write_cdc;
// reg [47:0] timestamp_buf;

// always @(RX_CLKW) begin
//     // case (state)
//         // STATE_TS_MSB: wdata <= {4'b1011, timestamp_buf[23:0]};
//         // STATE_TS_MSB: wdata <= {4'b1011, timestamp_buf[47:24]};
//         // STATE_TS_LSB: wdata <= {4'b1010, timestamp_buf[23:0]};
//         // idle
//     wdata <= {1'b0,data_dec_in[0][8],data_dec_in[1][8],data_dec_in[2][8]  ,data_dec_in[0][7:0], data_dec_in[1][7:0], data_dec_in[2][7:0]};
//     // endcase
// end

assign wdata={1'b0,data_dec_in[0][8],data_dec_in[1][8],data_dec_in[2][8]  ,data_dec_in[0][7:0], data_dec_in[1][7:0], data_dec_in[2][7:0]};
// assign wdata={1'b0,data_dec_in[0][8],data_dec_in[1][8],data_dec_in[2][8]  ,8'hc1, data_dec_in[1][7:0], data_dec_in[2][7:0]};

// reg has_seen_data;
// localparam ID_IDLE=8'hBC, ID_LC2=8'h1C, ID_LP2=8'hC0, ID_RC2=8'hC1, ID_CNT=8'h3C; //, ID_RC4=2'hd0
// // reg [3:0] ID = ID_IDLE;
// // reg [3:0] bit_packet;
// always @(posedge RX_CLKW) begin
//     if (RESET_WCLK)
//         // bit_packet<=4'b0000;
//         has_seen_data<=1'b0;
//     else if (busy)
//         begin
//         // full_dw<={full_dw[2:0],
//         if (dec_data == ID_RC2)
//             has_seen_data<= 1'b1;
//         else if (state == STATE_TS_LSB)
//             has_seen_data<= 1'b0;
//             // case(dec_data):
//             //     ID_IDLE: bit_packet[0]<=1'b1;
//             //     ID_LC2: bit_packet[0]<=1'b1;
//             //     ID_RC2: bit_packet[0]<=1'b1;
//             //     ID_CNT: bit_packet[0]<=1'b1;
//             // default:
//             //     has_seen_data<={bit_packet[2:0],0}
//             // endcase
//     end
// end
    
// reg has_seen_data;
// always @(posedge RX_CLKW) begin
//     if (RESET_WCLK)
//         state <= STATE_IDLE;
//     else
//         state <= state;

//      case (state)
//         STATE_IDLE: 
//             begin
//                 // if(sof) begin
//                 timestamp_buf <= FPGA_TIMESTAMP_CLK;
//                 state <= STATE_TS_MSB;
//             end
//         STATE_TS_MSB: begin
//             state <= STATE_TS_LSB;
//         end
//         STATE_TS_LSB: begin
//             state <= STATE_IDLE;
//         end
//     endcase
// end


wire dont_write;
assign dont_write = 
                    // (!CONF_EN_RX_WCLK) & (!CONF_DUMP_ALL_SYNC)
                    // & ((data[0]==9'h11c & data[1]==9'haa & data[2]==9'h11c) 
                    //  | (data[0]==9'haa & data[1]==9'h15c & data[2]==9'h11c)
                    //  | (data[0]==9'h1fc & data[1]==9'h15c & data[2]==9'h115)
                    //  )
                    (!CONF_DUMP_ALL_SYNC)
                    // & ((data_dec_in[0]==9'h11c & data_dec_in[1]==9'haa & data_dec_in[2]==9'h11c) 
                    //  | (data_dec_in[0]==9'haa & data_dec_in[1]==9'h15c & data_dec_in[2]==9'h15c & byte_sel==2)
                     &  ((data_dec_in[0]==9'h0aa & data_dec_in[1]==9'h15c & data_dec_in[2]==9'h15c)
                    //  | (data_dec_in[0]==9'haa & data_dec_in[1]==9'h15c & data_dec_in[2]==9'h11c)
                    //  | (data_dec_in[0]==9'haa & data_dec_in[1]==9'h13c & data_dec_in[2]==9'h11c )
                     )
                    // (!CONF_DUMP_ALL_SYNC)
                    // & ((data_dec_in[0]==9'h11c & data_dec_in[1]==9'haa & data_dec_in[2]==9'h11c & byte_sel==2) 
                    // //  | (data_dec_in[0]==9'haa & data_dec_in[1]==9'h15c & data_dec_in[2]==9'h15c & byte_sel==2)
                    //  | (data_dec_in[0]==9'h0aa & data_dec_in[1]==9'h0aa & data_dec_in[2]==9'h15c & byte_sel==2)
                    //  | (data_dec_in[0]==9'h1aa & data_dec_in[1]==9'h1aa & data_dec_in[2]==9'h15c & byte_sel==2)
                    //  | (data_dec_in[0]==9'h1aa & data_dec_in[1]==9'h15c & data_dec_in[2]==9'h11c & byte_sel==2)
                    //  | (data_dec_in[0]==9'h1aa & data_dec_in[1]==9'h13c & data_dec_in[2]==9'h11c & byte_sel==2)
                    //  )
                    ; 

wire cdc_fifo_full, cdc_fifo_empty;
always@(posedge RX_CLKW) begin
    if(RESET_WCLK | CONF_RESET_CNT_SYNC)
        lost_err_cnt <= 0;
    else if(cdc_fifo_full && write_dec_in && lost_err_cnt != 8'hff)
        lost_err_cnt <= lost_err_cnt + 1;
end
reg cdc_fifo_winc;
`ifdef COCOTB_SIM
    assign cdc_fifo_full = 1'b0;
`endif
// assign cdc_fifo_winc = (write_dec_in || (state==STATE_TS_LSB) || (state==STATE_TS_MSB)) & !cdc_fifo_full & !dont_write;
// assign cdc_fifo_winc = (write_dec_in) & !cdc_fifo_full & !dont_write;
always@(posedge RX_CLKW) begin
    // cdc_fifo_winc <= (write_dec_in) & !cdc_fifo_full;
    cdc_fifo_winc = (write_dec_in) & !cdc_fifo_full & !dont_write;
end
// assign cdc_fifo_winc = (write_dec_in) & !cdc_fifo_full;

// fifo
wire fifo_full;
(* mark_debug = "true" *) wire [27:0] cdc_data_out;
cdc_syncfifo #( .DSIZE(28),.ASIZE(2) ) cdc_syncfifo ( //sync to fifo_clock
    .wfull(cdc_fifo_full),
    .wdata(wdata),
    .winc(cdc_fifo_winc),
    .wclk(RX_CLKW),
    .wrst(RESET_WCLK),
    .rempty(cdc_fifo_empty),
    .rinc(!fifo_full),
    .rclk(BUS_CLK),
    .rrst(RESET_FIFO),
    .rdata(cdc_data_out)
);

// reg [111:0] buffered_data;
// always@(posedge !cdc_fifo_empty) begin
//     buffered_data={buffered_data[83:0],cdc_data_out};
// end

// wire write_fifo;
// assign write_fifo = ((buffered_data[111]==1) &&  (buffered_data[83]==1)) && !cdc_fifo_empty;


gerneric_fifo #( .DATA_SIZE(28), .DEPTH(1024*10) ) gerneric_fifo ( //actual fifo
    .clk(BUS_CLK),
    .reset(RESET_FIFO),
    .write(!cdc_fifo_empty),
    // .write(write_fifo),
    .read(FIFO_READ),
    .data_in(cdc_data_out),
    // .data_in(buffered_data[111:84]),
    .data_out(FIFO_DATA[27:0]),
    .full(fifo_full),
    .empty(FIFO_EMPTY),
    .size(fifo_size)
);


assign FIFO_DATA[31:28] = DATA_IDENTIFIER;

//------------------ DATA END -------------------------------//



endmodule
