/**
 * ------------------------------------------------------------
 * Copyright (c) All rights reserved
 * SiLab, Institute of Physics, University of Bonn
 * ------------------------------------------------------------
 */
`timescale 1ps/1ps
`default_nettype none

module rec_sync #(
    parameter                       DSIZE = 10
) (
    input wire                      reset,
    input wire                      datain,
    output reg      [DSIZE-1:0]     data,
    // input wire                      WCLK,
    input wire                      FCLK,
    output reg                      rec_sync_ready,
    input wire                      decoder_err
);


reg [DSIZE-1:0] shift_reg;
// (*mark_debug = "TRUE" *) reg [DSIZE-1:0] shift_reg;
reg [DSIZE*DSIZE-1:0] shift_reg_big;
reg [DSIZE-1:0] shift_reg_old;
reg [DSIZE-1:0] bit_cnt;
// (*mark_debug = "TRUE" *) reg [DSIZE-1:0]  locked_sync;
reg [DSIZE-1:0]  locked_sync;
initial bit_cnt = 1;
initial locked_sync=0;
initial rec_sync_ready=1'b0;
always @(posedge FCLK) begin
    shift_reg <= {datain,shift_reg[DSIZE-1:1]};
    // shift_reg <= {shift_reg[DSIZE-2:0], datain};
    shift_reg_big <= {shift_reg_big[DSIZE*DSIZE-DSIZE-1:0],shift_reg};
    shift_reg_old <= shift_reg_big[(DSIZE*DSIZE)-1-DSIZE:(DSIZE*DSIZE)-2*DSIZE];

    bit_cnt <= {bit_cnt[DSIZE-2:0],bit_cnt[DSIZE-1]};
    if (!rec_sync_ready && check)
        locked_sync<=bit_cnt;
    if (bit_cnt==locked_sync) begin
        data<=shift_reg;
        rec_sync_ready=1'b1;
    end
    if (reset) begin
    // if (reset || decoder_err) begin
        rec_sync_ready=1'b0;
        locked_sync=0;
        bit_cnt = 1;
    end
end

// localparam code_plus =  10'b11111010;//OFA simulation
// localparam code_minus = 10'b1100000101;//305 simulation
// localparam code_plus  = 10'b0101111100; // 17C simulation
// localparam code_minus = 10'b1010000011; //283

// parameter code_plus  = 10'b1001110100; //0x274
// parameter code_minus = 10'b1110001011; //0x18B

// localparam code_plus =  10'b1111101011;
// localparam code_minus =  10'b0000010100;

// from ap3 code:
// https://git.scc.kit.edu/adl_publicsoftware/atlaspix3_public/-/blob/hit-driven/GECCO_Firmware/GECCO_Firmware.src/fastreadout_lib/untriggered_data_alignment.v
localparam code_minus  = 10'b1010000011;
localparam code_plus = 10'b0101111100;



wire check;
assign check =(((shift_reg == code_plus) && (shift_reg_old == code_minus)) || ((shift_reg_old == code_plus) && (shift_reg == code_minus)));
// assign check =((shift_reg == code_plus) || (shift_reg == code_minus));

endmodule
