/**
 * ------------------------------------------------------------
 * Copyright (c) All rights reserved
 * SiLab, Institute of Physics, University of Bonn
 * ------------------------------------------------------------
 */
`timescale 1ps/1ps
`default_nettype none


module des_mightypix #(
    parameter CLKDV = 1
) (
    input wire CLK2X,
    input wire CLK,
    input wire WCLK,
    input wire IN,
    output reg [CLKDV*4-1:0] OUT
);

wire [3:0] DDRQ;
ISERDESE2 #(
    .DATA_RATE("DDR"),
    .DATA_WIDTH(4),
    .INTERFACE_TYPE("NETWORKING"),
    .DYN_CLKDIV_INV_EN ("FALSE"),
    .DYN_CLK_INV_EN    ("FALSE"),
    .OFB_USED          ("FALSE"),
    .IOBDELAY          ("NONE"), 
    .NUM_CE(1),
    .SERDES_MODE("MASTER")
) ISERDESE2_inst (
    .Q1(DDRQ[3]),   
    .Q2(DDRQ[2]),
    .Q3(DDRQ[1]),   
    .Q4(DDRQ[0]),   
    .CLK(CLK2X), // 1-bit clock input
    .CE1(1'b1),  // 1-bit clock enable input
    .D(IN), // 1-bit DDR data input
    .DDLY(1'b0),                 // 1-bit input: Serial data from IDELAYE2   
    .OFB(1'b0),                  // 1-bit input: Data feedback from OSERDESE2   
  