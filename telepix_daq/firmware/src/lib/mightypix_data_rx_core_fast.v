/**
 * ------------------------------------------------------------
 * Copyright (c) All rights reserved
 * SiLab, Institute of Physics, University of Bonn
 * ------------------------------------------------------------
 */
`timescale 1ps/1ps
`default_nettype none

module mightypix_data_rx_core_fast #(
    parameter           DSIZE = 10,
    parameter           DATA_IDENTIFIER = 4'h1,
    parameter           ABUSWIDTH = 32
) (
    // Bus
    input wire BUS_CLK,
    input wire [ABUSWIDTH-1:0] BUS_ADD,
    input wire [7:0] BUS_DATA_IN,
    output reg [7:0] BUS_DATA_OUT,
    input wire BUS_RST,
    input wire BUS_WR,
    input wire BUS_RD,
    // clocks
    input wire RX_CLK2x,
    input wire RX_CLKW,
    // chip
    input wire RX_DATA,
    input wire [47:0] FPGA_TIMESTAMP,
    input wire FIFO_READ,
    output wire FIFO_EMPTY,
    output wire [31:0] FIFO_DATA,
    output wire RX_READY,
    output wire RX_ENABLED,

    // Fast receiver modules - not used here
    output wire [3:0] rxcharisk,
    output wire [3:0] rxdisperr,
    output wire [3:0] rxnotintable, 
    output wire TRACK_DATA_OUT,
    output wire rxusrclk,
    output wire rxusrclk2

);

localparam VERSION = 1;


// writing to register 0 asserts soft reset
wire SOFT_RST;
assign SOFT_RST = (BUS_ADD == 0 && BUS_WR);

wire RST;
assign RST = BUS_RST | SOFT_RST; 

// writing to register 1 asserts reset RX only
wire RX_RST;
assign RX_RST = (BUS_ADD==1 && BUS_WR);

assign RX_READY = rx_ready_bus_clk;


// registers
// 0 - soft reset
// 1 - RX reset
// 2 - status
// 3-4 - fifo size
// 5 - 8b10b decoder error counter
// 6 - lost data counter
wire rx_ready_bus_clk;
wire [31:0] load_col2_cnt;
wire [15:0] load_pix2_cnt;
wire [12:0] fifo_size;     // BUS_ADD==3, 4
reg [7:0] decoder_err_cnt; // BUS_ADD==5
reg [7:0] lost_err_cnt; // BUS_ADD==8
reg [7:0] data_err_cnt;    // BUS_ADD==7
reg [19:0] raw_data;
reg [4:0] CONF_DATA_DLY;
wire [4:0] DATA_DLY_OUT;


//--------------status register----------------------//
// status register 10x8b with different states
// only in write direction
reg [7:0] status_regs [9:0];

always @(posedge BUS_CLK) begin
    if(RX_RST) begin
        status_regs[2] <= 8'b0000_0000; // disable Rx by default
        status_regs[7] <= 8'b0000_0000;
        status_regs[8] <= 8'b0000_0000;
        status_regs[9] <= 8'b0000_0000;
    end
    else if(BUS_WR && BUS_ADD < 10)
        status_regs[BUS_ADD[3:0]] <= BUS_DATA_IN;
end


wire CONF_EN_RX; // BUS_ADD==2 BIT==2
// wire RX_RESET; // BUS_ADD==0 
`ifdef COCOTB_SIM
assign CONF_EN_RX = 1'b1;
// assign RX_RESET = 1'b0;
`else
assign CONF_EN_RX = status_regs[2][2];
// assign RX_RESET= status_regs[0][0];
`endif
assign RX_ENABLED = CONF_EN_RX;
// assign  = RX_RESET;

wire CONF_FILTER_LOAD_COLUMN2; // BUS_ADD==2 BIT==3 negated
assign CONF_FILTER_LOAD_COLUMN2 = !status_regs[2][3]; // since we want to filter by default and 0 is default this is inverted

wire [4:0] CONF_RX_DATA_DLY;
assign CONF_RX_DATA_DLY = status_regs[7][4:0];



//----------------BUS DATA-----------------------//

// reset and switches sync (I am not so sure whether all are needed...)
reg [5:0] rst_cnt;
always @(posedge BUS_CLK) begin
    if(RST)
        rst_cnt <=1;
    else if (rst_cnt!=0)
        rst_cnt <= rst_cnt +1;
end


wire RST_LONG;
assign  RST_LONG = (rst_cnt)!=0;


// only in read direction
always @(posedge BUS_CLK) begin
    if(BUS_RD) begin
        if(BUS_ADD == 0)
            BUS_DATA_OUT <= VERSION;
        // else if(BUS_ADD == 2)
        // `ifdef COCOTB_SIM
        //     BUS_DATA_OUT <= {status_regs[2][7:1], 1'b1};
        // `else
        //     BUS_DATA_OUT <= {status_regs[2][7:1], rx_ready_bus_clk};
        // `endif
        else if(BUS_ADD == 1)
            BUS_DATA_OUT <= {CONF_DUMP_ALL, CONF_CAPTURE_RAW,
                             CONF_INVERT_RX, CONF_EN, rx_ready_bus_clk,
                             DECODER_ERR, DATA_ERR, LOST_ERR}; //state
        else if(BUS_ADD == 2)
            BUS_DATA_OUT <= {CONF_EN_TS, CONF_SAMPLING_EDGE, CONF_DATA_DLY_WR, CONF_DATA_DLY};

        else if(BUS_ADD == 3)
            BUS_DATA_OUT <= fifo_size[7:0];
        else if(BUS_ADD == 4)
            BUS_DATA_OUT <= fifo_size[12:8];
        else if(BUS_ADD == 5)
            BUS_DATA_OUT <= decoder_err_cnt[7:0];
        else if(BUS_ADD == 6)
            BUS_DATA_OUT <= {3'b0, DATA_DLY_OUT};
        else if(BUS_ADD == 7)
            BUS_DATA_OUT <= data_err_cnt;
        else if(BUS_ADD == 8)
            BUS_DATA_OUT <= lost_err_cnt;
        else if(BUS_ADD == 9)
            BUS_DATA_OUT <= load_col2_cnt[7:0];
        else if(BUS_ADD == 10)
            BUS_DATA_OUT <= load_pix2_cnt[7:0];
        else if(BUS_ADD == 11)
            BUS_DATA_OUT <= raw_data[7:0];
        else if(BUS_ADD == 12)
            BUS_DATA_OUT <= raw_data[15:8];
        else if(BUS_ADD == 13)
            BUS_DATA_OUT <= {4'b0,raw_data[19:16]};
        else if(BUS_ADD == 14)
            BUS_DATA_OUT <= {3'b0, CONF_DLY_CNT};
        else
            BUS_DATA_OUT <= 8'b0;
    end
end

wire CONF_EN_RX_WCLK;
three_stage_synchronizer conf_en_rx_synchronizer_data_clk (
    .CLK(RX_CLKW),
    .IN(CONF_EN_RX),
    .OUT(CONF_EN_RX_WCLK)
);

three_stage_synchronizer rx_ready_synchronizer_bus_clk (
    .CLK(BUS_CLK),
    .IN(rec_sync_ready),
    .OUT(rx_ready_bus_clk)
);

wire CONF_INVERT_RX_SYNC;
three_stage_synchronizer sync_conf_invert_data (.CLK(RX_CLKW), .IN(CONF_INVERT_RX), .OUT(CONF_INVERT_RX_SYNC));
wire CONF_CAPTURE_RAW_SYNC;
three_stage_synchronizer sync_capture_raw (.CLK(RX_CLKW), .IN(CONF_CAPTURE_RAW), .OUT(CONF_CAPTURE_RAW_SYNC));
wire CONF_DUMP_ALL_SYNC;
three_stage_synchronizer sync_conf_dump_all (.CLK(RX_CLKW), .IN(CONF_DUMP_ALL), .OUT(CONF_DUMP_ALL_SYNC));
wire CONF_EN_SYNC;
three_stage_synchronizer sync_conf_en (.CLK(RX_CLKW), .IN(CONF_EN), .OUT(CONF_EN_SYNC));
wire RESET_WCLK;
cdc_reset_sync cdc_reset_sync_wclk (.clk_in(BUS_CLK), .pulse_in(RST_LONG), .clk_out(RX_CLKW), .pulse_out(RESET_WCLK));
wire RESET_FIFO;  //generate delayed and long reset (TH, this is not understood well...)
cdc_reset_sync cdc_reset_fifo (.clk_in(RX_CLKW), .pulse_in(RESET_WCLK), .clk_out(BUS_CLK), .pulse_out(RESET_FIFO));



//------------------ DATA START -------------------------------//
reg CONF_DUMP_ALL, CONF_INVERT_RX, CONF_EN, CONF_EN_TS;
reg CONF_SAMPLING_EDGE, CONF_CAPTURE_RAW, CONF_DATA_DLY_WR;
wire DECODER_ERR, DATA_ERR, LOST_ERR;

always @(posedge BUS_CLK) begin
    if(BUS_RST) begin
        CONF_INVERT_RX <= 0;
        CONF_EN <= 1'b0;
        CONF_SAMPLING_EDGE <= 0;
        CONF_CAPTURE_RAW <= 0;
        CONF_DUMP_ALL <= 1'b0;
        CONF_DATA_DLY_WR <=1'b0;
        CONF_EN_TS <= 1'b0;
        CONF_DLY_CNT <= 4'b0; 
    end
        else if(BUS_WR) begin
        if(BUS_ADD == 1)
            {CONF_DUMP_ALL, CONF_CAPTURE_RAW, CONF_INVERT_RX, CONF_EN} <= BUS_DATA_IN[7:4];
        else if(BUS_ADD == 2)
            {CONF_SAMPLING_EDGE, CONF_DATA_DLY_WR, CONF_DATA_DLY} <= BUS_DATA_IN[6:0];
        else if(BUS_ADD == 5)
            CONF_DLY_CNT <= BUS_DATA_IN[4:0];

    end

end

// status
assign DECODER_ERR = (decoder_err_cnt!=8'b0);
assign LOST_ERR = (lost_err_cnt!=8'b0);
assign DATA_ERR = (data_err_cnt!=8'b0);


// error counts
assign decoder_err_cnt = rxnotintable;
assign data_err_cnt = rxdisperr;


// define data word consisting of comma word & data 
wire  [35:0] userk_data_to_cdc = {rxcharisk, RXDATA}; 

// inc statement for cdc fifo && filtering of incoming data
reg cdc_fifo_winc;
always @(posedge rxusrclk) begin
    if(RESET_usrclk)
        cdc_fifo_winc <= 0;
    else
        if(enable_rx && ({rxcharisk, RXDATA} != 36'b0) && rxdisperr == 2'b00 && rxnotintable == 2'b00 && cplllocked)
            cdc_fifo_winc <= 1;
        else
            cdc_fifo_winc <= 0;
end


wire [35:0] wdata;
assign wdata = {rxcharisk, RXDATA}


wire cdc_fifo_full, cdc_fifo_empty;
always@(posedge RX_CLKW) begin
    if(RESET_WCLK)
        lost_err_cnt <= 0;
    else if(cdc_fifo_full && write_wdata && lost_err_cnt != 8'hff)
        lost_err_cnt <= lost_err_cnt + 1;
end
wire cdc_fifo_winc;
assign cdc_fifo_winc = write_wdata & (~cdc_fifo_full) & (~dont_write);

// fifo
wire fifo_full;
cdc_syncfifo #( .DSIZE(36),.ASIZE(3) ) cdc_syncfifo ( //sync to fifo_clock
    .wfull(cdc_fifo_full),
    .wdata(wdata),
    .winc(cdc_fifo_winc),
    .wclk(RX_CLKW),
    .wrst(RESET_WCLK),
    .rempty(cdc_fifo_empty),
    .rinc(!fifo_full),
    .rclk(BUS_CLK),
    .rrst(RESET_FIFO),
    .rdata(cdc_data_out)
);
wire [26:0] data_out;
gerneric_fifo #( .DATA_SIZE(28), .DEPTH(1024*8) ) gerneric_fifo ( //actual fifo
    .clk(BUS_CLK),
    .reset(RESET_FIFO),
    .write(!cdc_fifo_empty),
    .read(FIFO_READ),
    .data_in(cdc_data_out),
    .data_out(FIFO_DATA[27:0]),
    .full(fifo_full),
    .empty(FIFO_EMPTY),
    .size(fifo_size)
);
assign FIFO_DATA[31:28] = DATA_IDENTIFIER;

endmodule
