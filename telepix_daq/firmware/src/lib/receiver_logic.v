/**
 * ------------------------------------------------------------
 * Copyright (c) All rights reserved
 * SiLab, Institute of Physics, University of Bonn
 * ------------------------------------------------------------
 */
`timescale 1ps / 1ps 
`default_nettype none

// (* DONT_TOUCH = "yes" *) 
module receiver_logic #(
    parameter DSIZE = 10
) (
    input  wire        RESET,
    input  wire        WCLK,
    input  wire        BUS_CLK,
    // input  wire        RX_DATA_P,
    // input  wire        RX_DATA_N,
    //RX INFO
    input  wire [ 3:0] rxdisperr,
    input  wire [ 3:0] rxnotintable,
    input  wire        rxusrclk,
    input  wire        rxusrclk2,
    input  wire [31:0] rxdata,
    // input  wire        cplllock,
    input  wire [ 3:0] rxcharisk,
    input wire         soft_reset_rx,
    input wire          USER_K_VALID,
    input wire cplllocked,


    input  wire           read,
    output wire    [23:0] data,
    output wire           empty,
    output reg            rx_fifo_full,     // full in monopix
    output wire           rec_sync_ready,   // used to sync fifo if ready

    output reg  [7:0]       lost_data_cnt,
    output reg  [23:0]      decoder_err_cnt,
    output reg  [23:0]      disp_err_cnt,
    output reg  [7:0]      load_col2_cnt,
    output reg  [7:0]      load_pix2_cnt,
    output wire     [15:0] fifo_size,
    input  wire            enable_rx,
    input  wire            FIFO_CLK,
    input wire             CONF_EN_REORDER_RX_DATA,
    input wire             CONF_FILTER_LOAD_COLUMN2
    // output wire           FIFO_WRITE,       //added to test fifo
    // output wire    [31:0] FIFO_DATA,        // added to test fifo
    // output wire    [15:0] gpio              // added to test fifo -> en_fake_data uses gpio hl
);

    wire RESET_WCLK;
    flag_domain_crossing reset_domain_crossing_wclk_inst (
        .CLK_A(BUS_CLK),
        .CLK_B(WCLK),
        .FLAG_IN_CLK_A(RESET),
        .FLAG_OUT_CLK_B(RESET_WCLK)
    );

    wire RESET_FIFO_CLK;
    flag_domain_crossing reset_domain_crossing_fifo_clk_inst (
        .CLK_A(BUS_CLK),
        .CLK_B(FIFO_CLK),
        .FLAG_IN_CLK_A(RESET),
        .FLAG_OUT_CLK_B(RESET_FIFO_CLK)
    );

    integer clk_counter = 0;
    wire RESET_usrclk;
    flag_domain_crossing reset_domain_crossing_usrclk_inst (
        .CLK_A(BUS_CLK),
        .CLK_B(rxusrclk),
        .FLAG_IN_CLK_A(RESET),
        .FLAG_OUT_CLK_B(RESET_usrclk)
    );
    
    parameter DATA_SIZE=22;
      // define data word consisting of comma word & data 
    reg  [DATA_SIZE-1:0] userk_data_to_cdc; // = {rxcharisk, rxdata}; 

    // inc statement for cdc fifo && filtering of incoming data
   reg cdc_fifo_write_input;
    wire [DATA_SIZE-1:0] ordered_data;
    wire [DATA_SIZE-1:0] ordered_data2;
    reg [DATA_SIZE-1:0] ordered_data_shift;
    // For the default fast_ip the bit order of the dataword is reversed!!
    // We need to invert the dataword order!
    assign ordered_data = (CONF_EN_REORDER_RX_DATA==0)?{  rxnotintable[0],rxnotintable[1]  ,rxdisperr[0],rxdisperr[1],   rxcharisk[0],rxcharisk[1], rxdata[7:0],rxdata[15:8]}: {rxnotintable[1:0],rxdisperr[1:0],rxcharisk[1:0], rxdata[15:0]};
    assign ordered_data2 = (CONF_EN_REORDER_RX_DATA==0)?{  rxnotintable[2],rxnotintable[3]  ,rxdisperr[2],rxdisperr[3],   rxcharisk[2],rxcharisk[3], rxdata[23:16],rxdata[31:24]}: {rxnotintable[3:2],rxdisperr[3:2],rxcharisk[3:2], rxdata[31:16]};
 
    // assign ordered_data = (CONF_EN_REORDER_RX_DATA==0)?{  4'b0000,   rxcharisk[0],rxcharisk[1], rxdata[7:0],rxdata[15:8]}: {rxnotintable[1:0],rxdisperr[1:0],rxcharisk[1:0], rxdata[15:0]};
    // assign ordered_data2 = (CONF_EN_REORDER_RX_DATA==0)?{  4'b0000,   rxcharisk[2],rxcharisk[3], rxdata[23:16],rxdata[31:24]}: {rxnotintable[3:2],rxdisperr[3:2],rxcharisk[3:2], rxdata[31:16]};
 
    reg flag_lc2;
//    wire flag_lc2_cond;
//    if ((ordered_data = 18'h3bc1c) || (ordered_data = 18'h21c55) || (ordered_data = 18'h1551c) || (ordered_data = 18'h155bc))
//        flag_lc2_cond =1;
//    assign flag_lc2=((ordered_data = 18'h3bc1c) || (ordered_data = 18'h21c55) || (ordered_data = 18'h1551c) || (ordered_data = 18'h155bc));
    // if you want to filter for LC2 there are several options, because the 16bit is not alligned with the output!
    wire write_8b10b;
    assign write_8b10b= enable_rx && ( ordered_data != 18'b0) && cplllocked;
    reg k_i;
    initial k_i=0;
    always @(posedge rxusrclk2) begin
        if(RESET_usrclk)begin
            cdc_fifo_write_input <= 0;
            k_i=0;
        end 
            else begin
            // if (CONF_FILTER_LOAD_COLUMN2) begin
            //     case ((ordered_data & 18'h3FFFF) ||(ordered_data2 & 18'h3FFFF))
            //         18'h3bc1c : flag_lc2<=1;
            //         18'h21c55 : flag_lc2<=1;
            //         18'h1551c : flag_lc2<=1;
            //         18'h155bc : flag_lc2<=1;
            //         default   : flag_lc2<=0;
            //        endcase
            // end 
            // else begin
            flag_lc2<=0;
            ordered_data_shift <= (k_i==0) ? ordered_data: ordered_data2;
            // ordered_data_shift<=ordered_data2;
            k_i<=!k_i;
            end
        // if(enable_rx && (ordered_data != 18'b000000000000000000) &&(ordered_data != 18'h3BCBC) && rxdisperr == 2'b00 && rxnotintable == 2'b00 && cplllocked)
        // ordered data is 20b but we only check the last 18. The first 2 are the errors
        if( !flag_lc2 && enable_rx && ((ordered_data_shift & 18'h3FFFF) != 18'b0) &&((ordered_data_shift & 18'h3FFFF) != 18'h3BCBC) && cplllocked) begin
            cdc_fifo_write_input <= 1;
            userk_data_to_cdc <= ordered_data_shift; 
        end
        else
                cdc_fifo_write_input <= 0;
    end
// end
    
    // Write to CDC fifo after applying the filter
    wire [DATA_SIZE-1:0] userk_cdc_data_out;
    wire userk_wfull,userk_cdc_fifo_empty;
    // wire userk_read_fifo_cdc = !userk_cdc_fifo_empty;
    wire cdc_fifo_empty;
    wire fifo_full;
//    wire select_event= dec_sync==0 && byte_sel==1 && ~(load_col2_flag) ;
//    wire cdc_fifo_write_input=( write_8b10b && select_event );
    // CDC fifo for the USER_K data
    cdc_syncfifo #(.DSIZE(24), .ASIZE(3)) userk_cdc_syncfifo_i
    (
        .rdata(userk_cdc_data_out),
        .wfull(userk_wfull),
        .rempty(cdc_fifo_empty),
        .wdata(userk_data_to_cdc),
        .winc(cdc_fifo_write_input), .wclk(rxusrclk), .wrst(RESET_usrclk),
        .rinc(!fifo_full), .rclk(BUS_CLK), .rrst(RESET)
    );

  always @(posedge rxusrclk) begin
    clk_counter <= clk_counter + 1;
  end


// Invalid symbols may or may not cause
// disparity errors depending on the symbol
// itself and the disparities of the previous and
// subsequent symbols. For this reason,
// DISP_ERR should always be combined
// with CODE_ERR to detect all errors.
always @(posedge rxusrclk) begin
    if(RESET_usrclk) begin
        decoder_err_cnt <= 0;
        disp_err_cnt <= 0;
    end
    else begin
        if(rxnotintable && write_8b10b)
            decoder_err_cnt <= decoder_err_cnt + 1;
        if(rxdisperr && write_8b10b)
            disp_err_cnt <= disp_err_cnt + 1;
    end
end

assign rec_sync_ready = (rxdisperr == 4'b0000 && rxnotintable == 4'b0000 && cplllocked);
// assign rec_sync_ready = (rxdisperr == 2'b00 && rxnotintable == 2'b00);

  // Sync DataOut <= {8'hBC, 8'hBC, 8'hBC, 8'hBC}; 
  // CommaOut <= {1'b1,1'b1,1'b1,1'b1}; 
  // Send counter 1 DataOut <= {K_28_1,timestamp_bin_counter[7:0] , timestamp_bin_counter[15:8], 8'd0}; 
  // CommaOut <= 4'b1_0_0_0; 
  // Load column 2 DataOut <= {8'h1C, 8'hAA, 8'h1C, 8'hAA}; 
  // CommaOut <= {1'b1,1'b0,1'b1,1'b0}; 
  // Load pixel 2 DataOut <= {8'hC0, timestamp_bin_counter[15:8], timestamp_bin_counter[7:0], timestamp_gray_value[7:0]}; 
  // CommaOut <= 4'b0_0_0_0;
  // Read column 2 DataOut <= {8'hC1,2'd0, ColAddFromDet[4:0], RowAddFromDet[9:0], TS3FromDet[6:0]}; 
  // CommaOut <= {1'b0,1'b0,1'b0,1'b0}; 
  // Read column 4 DataOut <= {2'd0, TSFromDet[19:0], TS2FromDet[9:0]}; 
  // CommaOut <= {1'b0,1'b0,1'b0,1'b0}; 
  // In all other cases, the data out register is filled with following pattern 
  // DataOut <= {8'hBC, 8'hBC, 8'hBC, 8'hBC}; 
  // CommaOut <= {1'b1,1'b1,1'b1,1'b1};

    //Realign data if wrong 16bits are chosen:
    // wire  [19:0] data_select;
    // wire  [19:0] data_buffer[1:0];
    // reg [37:0] shift_reg;
    // assign data_buffer[0]=shift_reg[37:20];
    // assign data_buffer[1]={shift_reg[36],shift_reg[17],shift_reg[27:20] ,shift_reg[15:8]};
    // reg k;
    // initial k=0;
    // assign data_select=data_buffer[k];
    // // reg [1:0] valid_data;
    // // initial valid_data=2'b01;
    // reg cdc_fifo_empty_shifted;
    // reg cdc_fifo_empty_shifted_2;
    // reg cdc_fifo_empty_shifted_3;
    // reg cdc_fifo_empty_shifted_4;
    // always @(posedge BUS_CLK) begin
    //     cdc_fifo_empty_shifted<=cdc_fifo_empty;
    //     // cdc_fifo_empty_shifted_2<=cdc_fifo_empty_shifted_3;
    //     // cdc_fifo_empty_shifted_4<=cdc_fifo_empty_shifted_2;
    //     // cdc_fifo_empty_shifted<=cdc_fifo_empty_shifted_4;
    //     if (RESET) begin
    //          k<=0;
    //     //     shift_reg <= 0;
    //     end
    //     else begin
    //         if (!cdc_fifo_empty_shifted) begin
    //             if (data_select == 20'h1551c) begin
    //                 k<= !k;
    //             end   
    //             else
    //                 k<=0;
    //         end
    //         if (!cdc_fifo_empty) begin
    //             shift_reg<={shift_reg[19:0],userk_cdc_data_out};
    //             // valid_data<= {valid_data[0],valid_data[1]};
    //             cdc_f   ifo_empty_shifted<=0;
    //         end else begin
    //             cdc_fifo_empty_shifted<=1;
    //         end
    //         // data_select<=data_buffer[k];
    //     end
    // end


    



   wire load_col2_flag;
   wire load_pix2_flag;
   assign load_col2_flag = {userk_cdc_data_out} == 16'h1CAA;
   assign load_pix2_flag = {4'b0000,userk_cdc_data_out[17:8]} == 12'h0C0;
   always @(posedge BUS_CLK) begin
       if (RESET) begin
           load_col2_cnt <= 0;
           load_pix2_cnt <= 0;
       end
       else begin
       if (load_pix2_flag && !cdc_fifo_empty) load_pix2_cnt <= load_pix2_cnt + 1;
       if (load_col2_flag && !cdc_fifo_empty) load_col2_cnt <=  load_col2_cnt + 1;
       end
   end

// //  here starts FIFO stuff
   reg cdc_fifo_write;


// wire [23:0] wdata;
// localparam PSIZE = 4;
// reg [23:0] pattern[3:0];
// initial begin
//      pattern[0] = {6'b0, 2'b11, 16'hFFF1};
//      pattern[1] = {6'b0, 2'b11, 16'hF0F1};
//      pattern[2] = {6'b0, 2'b11, 16'hFF01};
//      pattern[3] = {6'b0, 2'b11, 16'hA0F1};
// end


// integer wcounter = 0;
// always@(posedge WCLK)
//     if (wcounter < PSIZE-1)
//         wcounter <= wcounter +1;
//     else
//         wcounter <= 0;

// assign wdata = pattern[wcounter];


  wire wfull;
  wire fifo_full2;


  gerneric_fifo #(
      .DATA_SIZE(24),
      .DEPTH(8*1024)
  ) fifo_i2 (
      .clk(BUS_CLK), //   same clk as cdc output
      .reset(RESET),
      .write(!cdc_fifo_empty),
      .read(read),
      .data_in(userk_cdc_data_out),
      .full(fifo_full),
      .empty(empty),
      .data_out(data),
      .size(fifo_size)
  );

  always @(posedge WCLK) begin
    
      if (fifo_full && !cdc_fifo_empty) begin  // write when FIFO full
          rx_fifo_full <= 1'b1;
      end else if (!fifo_full && !cdc_fifo_empty) begin  // write when FIFO not full
          rx_fifo_full <= 1'b0;
      end
  end

  always @(posedge WCLK) begin
      if (RESET_WCLK)
          lost_data_cnt <= 0;
      else
          if (fifo_full && !cdc_fifo_empty && lost_data_cnt != 8'b1111_1111)
              lost_data_cnt <= lost_data_cnt + 1;
  end

//   always @(posedge BUS_CLK) begin
//       fifo_size <= {5'b0, fifo_size_int};
//   end




endmodule


    
    // assign write_8b10b = (rec_sync_ready && enable_rx);



    // Generic FIFO for handling RX bursts
//     wire write_out_fifo, fifo_full;
//    (*mark_debug = "TRUE" *)  wire [17:0] cdc_data;
//     wire [17:0] FIFO_DATA;
//     reg  [17:0] cdc_data_delayed;
//     assign cdc_data = userk_cdc_data_out;
//     // assign write_out_fifo = byte_cnt_prev != 0;//(byte_cnt != 0 || byte_cnt_prev != 0);

//     always@(posedge BUS_CLK) begin
//         cdc_data_delayed <= cdc_data;
//     end

    // gerneric_fifo #(.DATA_SIZE(18), .DEPTH(1024))  fifo_i
    // (   .clk(BUS_CLK), .reset(RESET),
    //     .write(write_out_fifo),
    //     .read(FIFO_READ),
    //     .data_in(cdc_data_delayed),
    //     .full(fifo_full),
    //     .empty(FIFO_EMPTY),
    //     .data_out(FIFO_DATA),
    //     .size()
    //     );



//   assign rec_sync_ready = (cplllock && rxdisperr == 4'b0000 && rxnotintable == 4'b0000 && locked);
//   assign rec_sync_ready = (rxdisperr == 4'b0000 && rxnotintable == 4'b0000 && locked);
  
//   wire [7:0] rx_data8b[1:0];
//   assign rx_data8b[0] = userk_cdc_data_out[7:0];
//   assign rx_data8b[1] = userk_cdc_data_out[15:8];
//   wire [1:0] rx_kdata= userk_cdc_data_out[17:16];
//   wire dec_sync;
//   assign dec_sync = (rxdata == 16'hbcbc && rxcharisk == 2'b11);

//   wire load_col2_flag = (write_8b10b && rec_sync_ready && cdc_data_delayed[15:0] == {8'hAA, 8'h1C} && cdc_data_delayed[17:16] == {1'b0,1'b1});
//   wire load_pix2_flag = (write_8b10b && rec_sync_ready && cdc_data_delayed[15:8] == 8'hC0 && cdc_data_delayed[17] == 1'b1);


//   always @(posedge rxusrclk) begin
//     if (RESET_usrclk) begin
//       load_col2_cnt <= 0;
//       load_pix2_cnt <= 0;
//     end
//     else begin
//     //   if (!dec_sync) load_col2_cnt <=  load_col2_cnt + 1;
//     //   if (!dec_sync ) load_pix2_cnt <= load_pix2_cnt + 1;
//       if (load_col2_flag) load_col2_cnt <=  load_col2_cnt + 1;
//     //   if (!load_col2_flag ) load_pix2_cnt <= load_pix2_cnt + 1;
//     end
//   end


//   reg byte_sel;
//   always @(posedge WCLK) begin
//       if(RESET_WCLK)
//           byte_sel <= 0;
//       else
//           byte_sel <= ~byte_sel;
//   end

// //   //select the right events
//   wire [15:0] rx_data16b[1:0];
//   assign rx_data16b[0] = rxdata[15:0];
//   assign rx_data16b[1] = rxdata[31:16];
//   wire [1:0] rx_comma2b[1:0];
//   assign rx_comma2b[0] =  rxcharisk[1:0];
//   assign rx_comma2b[1] = rxcharisk[3:2];


// wire [23:0] wdata;
// // assign wdata = {6'b0, rx_comma2b[byte_sel], rx_data16b[byte_sel]};
// // // assign wdata = {6'b0, rxcharisk, rxdata};
// // assign wdata = {6'b0, rx_kdata, rx_data8b[0]};
// assign wdata = {6'b0, userk_cdc_data_out};
// // assign wdata = {6'b0, 2'b11,16'hFFF1};
// // assign wdata = {6'b0,cdc_data_delayed};

// localparam PSIZE = 4;

// reg [23:0] pattern[3:0];
// initial begin
//      pattern[0] = {6'b0, 2'b11, 16'hFFF1};
//      pattern[1] = {6'b0, 2'b11, 16'hF0F1};
//      pattern[2] = {6'b0, 2'b11, 16'hFF01};
//      pattern[3] = {6'b0, 2'b11, 16'hA0F1};
// end


// integer wcounter = 0;
// always@(posedge WCLK)
//     if (wcounter < PSIZE-1)
//         wcounter <= wcounter +1;
//     else
//         wcounter <= 0;

// // assign wdata = pattern[wcounter];

// //   wire select_event= dec_sync==0 && ~(load_col2_flag);
//   always @(posedge WCLK) begin
//       if(RESET_WCLK)
//           cdc_fifo_write <= 0;
//       else
//         //   cdc_fifo_write <= 1;
//         //   if( write_8b10b && select_event ) begin
//           if( write_8b10b ) begin
//               cdc_fifo_write <= 1;
//           end else
//               cdc_fifo_write <= 0;
//   end

  //   wire [23:0] cdc_data_out;
   

// start of decoding of rxdata

// wire RX_DATA_SYNC; //, USEAOUT, USEBOUT, USECOUT, USEDOUT;
// sync_master sync_master_inst(
//     .clk(rxusrclk),             // clock input
//     .clk_2x(rxusrclk2),        // clock 90 input
//     .datain(rxdata),       // data inputs
//     .rst(RESET_usrclk),       // reset input
//     .useaout(),             // useA output for cascade
//     .usebout(),             // useB output for cascade
//     .usecout(),             // useC output for cascade
//     .usedout(),             // useD output for cascade
//     .ctrlout(),             // ctrl outputs for cascade
//     .sdataout(RX_DATA_SYNC)
// );

// reg RX_DATA_SYNC_BUF, RX_DATA_SYNC_BUF2;
// always @(posedge rxusrclk) begin
//     RX_DATA_SYNC_BUF <= RX_DATA_SYNC;
//     RX_DATA_SYNC_BUF2 <= RX_DATA_SYNC_BUF;
// end
// // 8b/10b record sync
// wire [9:0] data_8b10b;
// wire auto_resync;
// assign auto_resync=decoder_err_cnt>24'd100000;
// reg decoder_err;
// rec_sync #(
//     .DSIZE(DSIZE)
// ) rec_sync_inst (
//     .reset(RESET_usrclk),
//     .datain(~rxdata),
//     .data(data_8b10b),
//     .FCLK(rxusrclk),
//     .rec_sync_ready(rec_sync_ready),
//     .decoder_err(auto_resync) 
// );


// wire write_8b10b;
// `ifdef COCOTB_SIM //for simulation
//     assign write_8b10b = 1;
// `else
//     assign write_8b10b = rec_sync_ready && enable_rx;
// `endif


// reg [9:0] data_to_dec;

// always @(posedge rxusrclk2) begin
//     data_to_dec<=data_8b10b;
// end

// reg dispin;
// wire dispout;
// always @(posedge rxusrclk2) begin
//     if(RESET_usrclk)
//         dispin <= 1'b0;
//     else
//         dispin <= dispout;
// end

// wire dec_k;
// wire [7:0] dec_data;
// wire [8:0] data_9bit;
// wire code_err, disp_err;
// decode_8b10b decode_8b10b_inst (
//     .datain(data_to_dec),
//     .dispin(dispin),
//     .dataout(data_9bit), // control character, data out
//     .dispout(dispout),
//     .code_err(code_err),
//     .disp_err(disp_err)
// );

// assign dec_data = data_9bit[7:0];
// assign dec_k = data_9bit[8];

// always @(posedge rxusrclk2) begin
//     if(RESET_usrclk)
//         decoder_err <= 1'b0;
//     else
//         decoder_err <= code_err | disp_err;
// end


// always @(posedge rxusrclk) begin
//     if(RESET_usrclk) begin
//         decoder_err_cnt <= 0;
//         disp_err_cnt <= 0;
//     end
//     else begin
//         // if(decoder_err)
//         if(code_err && write_8b10b)
//         // if(decoder_err && write_8b10b)
//             decoder_err_cnt <= decoder_err_cnt + 1;
//         if(disp_err && write_8b10b)
//         // if(decoder_err && write_8b10b)
//             disp_err_cnt <= disp_err_cnt + 1;
//         // else if(decoder_err && write_8b10b && decoder_err_cnt == 8'hff)
//         // decoder_err_cnt <= 0;
//     end
// end
// end of decoding data 


// start of building data word. Either 16b or 32b. 
// First, detect idle pattern bc 
// reg byte_sel;
// reg [1:0] byte_sel_full_dw;
// wire dec_sync = (dec_k == 1 && full_dw[byte_sel_full_dw] == 8'hbc); //if 1: sync
// always @(posedge rxusrclk2) begin
//     if(RESET_usrclk || (write_8b10b && dec_sync) || (write_8b10b && dec_sync==0 && byte_sel==1))
//         byte_sel <= 0;
//     else if(write_8b10b)
//         byte_sel <= 1;
//     if(RESET_usrclk || (write_8b10b && dec_sync) || (write_8b10b && dec_sync==0 && byte_sel_full_dw==3))
//         byte_sel_full_dw <= 0;
//     else if(write_8b10b)
//         byte_sel_full_dw <= byte_sel_full_dw + 1;
// end

// //just to have the full 32 bit:
// reg [7:0] full_dw [3:0];
// reg full_k [3:0];

// reg [7:0] data_dec_in [1:0];
// reg data_dec_k_in [1:0];
// always @(posedge rxusrclk2) begin
//     if(write_8b10b && dec_sync==0) begin
//         data_dec_in[byte_sel] <= dec_data;
//         data_dec_k_in[byte_sel] <= dec_k;
//         full_dw[byte_sel_full_dw] <= dec_data;
//         full_k[byte_sel_full_dw] <= dec_k;
//     end else begin
//         data_dec_in[0] <= 8'hbc;
//         data_dec_k_in[0] <= 1'b1;
//         data_dec_in[1] <= 8'hbc;
//         data_dec_k_in[1] <= 1'b1;
//         full_dw[0] <= 8'hbc;
//         full_dw[1] <= 8'hbc;
//         full_dw[2] <= 8'hbc;
//         full_dw[3] <= 8'hbc;
//         full_k[0] <= 1'b1;
//         full_k[1] <= 1'b1;
//         full_k[2] <= 1'b1;
//         full_k[3] <= 1'b1;
//     end
// end

// wire [23:0] wdata;
// assign wdata = {6'b0, data_dec_k_in[0], data_dec_k_in[1], data_dec_in[0], data_dec_in[1]};
// end of building full dw



// wire load_col2_flag;
// assign load_col2_flag=( 
//     (
//         data_dec_in[~byte_sel]==8'h1C && data_dec_k_in[~byte_sel]==1'b1 // this is what it should be
//         // && dec_data==8'hAA && dec_k==1'b0
//         && (dec_data==8'h55 || dec_data==8'hAA ) && dec_k==1'b0 // this should be AA, but with the chip scope we see 0x55!!
//     ) 
// );
