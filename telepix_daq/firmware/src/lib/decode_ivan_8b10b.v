
`timescale 1ps/1ps
`default_nettype none


module decode_ivan_8b10b(
    input  wire clock,
    input  wire reset,
    input  wire [9:0] datain,
    output reg  [8:0] dataout,
    input wire        dispin,
    output wire       dispout,
    output wire       code_err,
    output wire       disp_err,
);

parameter numdecoders = 4;
parameter addrwidth   = 2;

reg  [9:0] encoded[numdecoders-1:0];
wire [8:0] decoded[numdecoders-1:0];

reg [addrwidth-1:0] phase;

integer i;
always @(negedge clock) begin
    if(reset) begin
        phase <= 0;
        for(i = 0; i < numdecoders; i = i+1) begin
            encoded[i] <= 10'b0;
        end
    end
    else begin
        if(phase < numdecoders-1)
            phase <= phase + {addrwidth{1'b1}};
        else
            phase <= {addrwidth{1'b0}};
        
        dataout        <= decoded[phase];
        encoded[phase] <= datain;
    end
end

genvar j;
generate
    for(j = 0; j < numdecoders; j = j + 1) begin
        decode_8b10b decode_8b10b_inst(
            .datain(encoded[j]),
            .dispin(dispin), 
            .dataout(decoded[j]),  
            .dispout(dispout),
            .code_err(code_err),
            .disp_err(disp_err)
        ); 
    end
endgenerate

endmodule
