`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: KIT-ADL
// Engineer: Nicolas Striebig
//
// Create Date: 04.01.2023 11:44:41
// Design Name:
// Module Name: align_mightypix
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


/*
    Align Dataout bitstram to MightyPix words with Starbits 2'b10 controlled by FSM

    TRY: Performs bitslip if not aligned, if it seems to be aligned try 2^10 samples,
    if it fails try again

    LOCKED: If sucessfully aligned, count non-aligned words.
    If treshold is crossed, go back to try state. If only few words cannot be aligned from transmission errors, stay in locked state.

*/

module align_mightypix #(
        parameter STARTSTOP = 2'b10
)(
        input wire res,
        input wire clk_in,    //320MHz
        input wire clk_div_in,
        input wire [3:0] data,
        //input wire data,   //TODO add ifdef?
        input wire [1:0] speed_mode,
        input wire is_fixed_bitslip,
        input wire [4:0] fixed_bitslip_value,
        input wire [9:0] align_threshold,

        output reg aligned,
        output wire [4:0] bitslip,
        output reg [31:0] data_out,
        output wire [63:0] debug64
);

    reg [63:0] data_reg;
    reg [31:0] data_reg_slice;

    always @(posedge clk_in) begin
        //if(speed_mode==2'b11)  //TODO add ifdef?
        //data_reg <= {data_reg[62:0], data};
        //else if (speed_mode==2'b01)
        //    data_reg <= {data_reg[61:0], data[1:0]};
        //else
            data_reg <= {data_reg[59:0], data[3:0]};
    end
assign debug64 = data_reg;

reg [9:0] count_aligned;
reg [4:0] auto_bitslip;
always @(posedge clk_div_in) begin
    if ( res ) begin
        aligned <= 1'b0;
        auto_bitslip <= 5'b0;
        count_aligned <= 10'h000;
    end
    else if (aligned) begin
        data_out <= data_reg_slice;
        if (data_reg_slice[31:30] != STARTSTOP) begin
            count_aligned <= 10'h000;
            aligned <= 1'b0;
        end
    end
    else begin
        if(data_reg_slice[31:30] == STARTSTOP) begin
            if (count_aligned < align_threshold)
                count_aligned <= count_aligned + 1;
            else
                aligned <= 1'b1;
        end
        else begin
            auto_bitslip <= auto_bitslip + 1;
            count_aligned <= 10'h000;
        end
    end
end

assign bitslip = (is_fixed_bitslip)? fixed_bitslip_value:auto_bitslip;

always @(*) begin
    data_reg_slice = data_reg >> bitslip;
end
endmodule
