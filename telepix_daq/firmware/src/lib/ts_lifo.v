module ts_lifo #(
    parameter IDENTIFIER = 3
)(

    input wire DI,
    input wire clk,
    input wire BUS_CLK,
    input wire rst,
    input wire pop,
    input wire block,
    output wire [31:0] data_out
);


    reg [63:0] curr_timestamp;
    always @(posedge clk) begin
        if(rst)
            curr_timestamp <= 0;
        else
            curr_timestamp <= curr_timestamp + 1;
    end

    reg [63:0] timestamp_out;
    reg [3:0] bit_cnt;
    reg used_ts;

    always @(posedge clk) begin
        if(rst) begin
            timestamp_out <= 0;
            used_ts <= 0;
        end
        else if(DI & ~used_ts & ~pop) begin // this should just prevent that we don't use the first edge
            timestamp_out <= curr_timestamp;
            used_ts <=1;
        end
        else if (~DI & used_ts) begin
            used_ts <=0;
        end
    end
    
    reg [1:0] byte2_cnt;
    always @(posedge BUS_CLK)
    if(rst)
        byte2_cnt <= 0;
    else if(byte2_cnt == 0 & pop)
        byte2_cnt <= 2;
    else if (byte2_cnt != 0 & pop)
        byte2_cnt <= byte2_cnt - 1;


    wire [31:0] fifo_write_data_byte [2:0];
    assign fifo_write_data_byte[0] = {IDENTIFIER,4'b0001,timestamp_out[23:0]};
    // assign fifo_write_data_byte[1] = {IDENTIFIER,4'b0010,timestamp_out[23:0]};
    assign fifo_write_data_byte[1] = {IDENTIFIER,4'b0010,timestamp_out[47:24]};
    assign fifo_write_data_byte[2] = {IDENTIFIER,4'b0011,8'b0,timestamp_out[63:48]};
    assign data_out = fifo_write_data_byte[byte2_cnt];

endmodule