/**
 * ------------------------------------------------------------
 * Copyright (c) All rights reserved
 * SiLab, Institute of Physics, University of Bonn
 * ------------------------------------------------------------
 */
`timescale 1ps/1ps
`default_nettype none

module mightypix_data_rx_core_slow #(
    parameter           DSIZE = 10,
    parameter           DATA_IDENTIFIER = 4'h1,
    parameter           ABUSWIDTH = 32
) (
    // Bus
    input wire BUS_CLK,
    input wire [ABUSWIDTH-1:0] BUS_ADD,
    input wire [7:0] BUS_DATA_IN,
    output reg [7:0] BUS_DATA_OUT,
    input wire BUS_RST,
    input wire BUS_WR,
    input wire BUS_RD,
    // clocks
    // input wire RX_CLK, // same as 1 bit rate
    input wire CLK_RX_OUT, // one bit alligns with full cycle
    // input wire CLK_RX_OUTX2, // one bit alligns with full cycle
    input wire RX_CLKW, // 1/10 of the 1 bit rate
    // chip
    input wire RX_DATA,
    input wire [47:0] FPGA_TIMESTAMP,
    input wire FIFO_READ,
    output wire FIFO_EMPTY,
    output wire [31:0] FIFO_DATA,
    output wire RX_READY,
    output wire RX_ENABLED,

    // Fast receiver modules - not used here
    output wire [3:0] rxcharisk,
    output wire [3:0] rxdisperr,
    output wire [3:0] rxnotintable, 
    output wire TRACK_DATA_OUT,
    output wire rxusrclk,
    output wire rxusrclk2

);

localparam VERSION = 1;

// writing to register 0 asserts soft reset
wire SOFT_RST;
assign SOFT_RST = (BUS_ADD == 0 && BUS_WR);

wire RST;
assign RST = BUS_RST | SOFT_RST; 

// writing to register 1 asserts reset RX only
wire RX_RST;
assign RX_RST = (BUS_ADD==1 && BUS_WR);

assign RX_READY = rx_ready_bus_clk;




// registers
// 0 - soft reset
// 1 - RX reset
// 2 - status
// 3-4 - fifo size
// 5 - 8b10b decoder error counter
// 6 - lost data counter
wire rx_ready_bus_clk;
wire [31:0] load_col2_cnt;
wire [15:0] load_pix2_cnt;
wire [12:0] fifo_size;     // BUS_ADD==3, 4
reg [7:0] decoder_err_cnt; // BUS_ADD==5
reg [7:0] lost_err_cnt; // BUS_ADD==8
reg [7:0] read_col2_cnt;    // BUS_ADD==7
reg [19:0] raw_data;
reg [4:0] CONF_DATA_DLY;
wire [4:0] DATA_DLY_OUT;


//--------------status register----------------------//
// status register 10x8b with different states
// only in write direction
reg [7:0] status_regs [9:0];

always @(posedge BUS_CLK) begin
    if(RX_RST) begin
        status_regs[2] <= 8'b0000_0000; // disable Rx by default
        status_regs[7] <= 8'b0000_0000;
        status_regs[8] <= 8'b0000_0000;
        status_regs[9] <= 8'b0000_0000;
    end
    else if(BUS_WR && BUS_ADD < 10)
        status_regs[BUS_ADD[3:0]] <= BUS_DATA_IN;
end


wire CONF_EN_RX; // BUS_ADD==2 BIT==2
// wire RX_RESET; // BUS_ADD==0 
`ifdef COCOTB_SIM
assign CONF_EN_RX = 1'b1;
// assign RX_RESET = 1'b0;
`else
assign CONF_EN_RX = status_regs[2][2];
// assign RX_RESET= status_regs[0][0];
`endif
assign RX_ENABLED = CONF_EN_RX;
// assign  = RX_RESET;

wire CONF_FILTER_LOAD_COLUMN2; // BUS_ADD==2 BIT==3 negated
assign CONF_FILTER_LOAD_COLUMN2 = !status_regs[2][3]; // since we want to filter by default and 0 is default this is inverted

wire [4:0] CONF_RX_DATA_DLY;
assign CONF_RX_DATA_DLY = status_regs[7][4:0];



//----------------BUS DATA-----------------------//

// reset and switches sync (I am not so sure whether all are needed...)
reg [5:0] rst_cnt;
always @(posedge BUS_CLK) begin
    if(RST)
        rst_cnt <=1;
    else if (rst_cnt!=0)
        rst_cnt <= rst_cnt +1;
end


wire RST_LONG;
assign  RST_LONG = (rst_cnt)!=0;


// only in read direction
always @(posedge BUS_CLK) begin
    if(BUS_RD) begin
        if(BUS_ADD == 0)
            BUS_DATA_OUT <= VERSION;
        // else if(BUS_ADD == 2)
        // `ifdef COCOTB_SIM
        //     BUS_DATA_OUT <= {status_regs[2][7:1], 1'b1};
        // `else
        //     BUS_DATA_OUT <= {status_regs[2][7:1], rx_ready_bus_clk};
        // `endif
        else if(BUS_ADD == 1)
            BUS_DATA_OUT <= {CONF_DUMP_ALL, CONF_CAPTURE_RAW,
                             CONF_INVERT_RX, CONF_EN, rx_ready_bus_clk,
                             DECODER_ERR, READ_COL2, LOST_ERR}; //state
        else if(BUS_ADD == 2)
            BUS_DATA_OUT <= {CONF_EN_TS, CONF_SAMPLING_EDGE, CONF_DATA_DLY_WR, CONF_DATA_DLY};

        else if(BUS_ADD == 3)
            BUS_DATA_OUT <= fifo_size[7:0];
        else if(BUS_ADD == 4)
            BUS_DATA_OUT <= fifo_size[12:8];
        else if(BUS_ADD == 5)
            BUS_DATA_OUT <= decoder_err_cnt[7:0];
        else if(BUS_ADD == 6)
            BUS_DATA_OUT <= {3'b0, DATA_DLY_OUT};
        else if(BUS_ADD == 7)
            BUS_DATA_OUT <= read_col2_cnt;
        else if(BUS_ADD == 8)
            BUS_DATA_OUT <= lost_err_cnt;
        else if(BUS_ADD == 9)
            BUS_DATA_OUT <= load_col2_cnt[7:0];
        else if(BUS_ADD == 10)
            BUS_DATA_OUT <= load_pix2_cnt[7:0];
        else if(BUS_ADD == 11)
            BUS_DATA_OUT <= raw_data[7:0];
        else if(BUS_ADD == 12)
            BUS_DATA_OUT <= raw_data[15:8];
        else if(BUS_ADD == 13)
            BUS_DATA_OUT <= {4'b0,raw_data[19:16]};
        // else if(BUS_ADD == 14)
        //     BUS_DATA_OUT <= {3'b0, CONF_DLY_CNT};
        else
            BUS_DATA_OUT <= 8'b0;
    end
end



wire CONF_EN_RX_WCLK;
three_stage_synchronizer conf_en_rx_synchronizer_data_clk (
    .CLK(RX_CLKW),
    .IN(CONF_EN_RX),
    .OUT(CONF_EN_RX_WCLK)
);

three_stage_synchronizer rx_ready_synchronizer_bus_clk (
    .CLK(BUS_CLK),
    .IN(rec_sync_ready),
    .OUT(rx_ready_bus_clk)
);

wire CONF_INVERT_RX_SYNC;
three_stage_synchronizer sync_conf_invert_data (.CLK(RX_CLKW), .IN(CONF_INVERT_RX), .OUT(CONF_INVERT_RX_SYNC));
wire CONF_CAPTURE_RAW_SYNC;
three_stage_synchronizer sync_capture_raw (.CLK(RX_CLKW), .IN(CONF_CAPTURE_RAW), .OUT(CONF_CAPTURE_RAW_SYNC));
wire CONF_DUMP_ALL_SYNC;
three_stage_synchronizer sync_conf_dump_all (.CLK(RX_CLKW), .IN(CONF_DUMP_ALL), .OUT(CONF_DUMP_ALL_SYNC));
wire CONF_EN_SYNC;
three_stage_synchronizer sync_conf_en (.CLK(RX_CLKW), .IN(CONF_EN), .OUT(CONF_EN_SYNC));
wire CONF_RESET_CNT_SYNC;
three_stage_synchronizer sync_reset_cnt (.CLK(RX_CLKW), .IN(CONF_RESET_CNT), .OUT(CONF_RESET_CNT_SYNC));
wire RESET_WCLK;
cdc_reset_sync cdc_reset_sync_wclk (.clk_in(BUS_CLK), .pulse_in(RST), .clk_out(RX_CLKW), .pulse_out(RESET_WCLK));
wire RESET_FIFO;  //generate delayed and long reset (TH, this is not understood well...)
cdc_reset_sync cdc_reset_fifo (.clk_in(RX_CLKW), .pulse_in(RESET_WCLK), .clk_out(BUS_CLK), .pulse_out(RESET_FIFO));



//------------------ DATA START -------------------------------//
reg CONF_DUMP_ALL, CONF_INVERT_RX, CONF_EN, CONF_EN_TS;
reg CONF_SAMPLING_EDGE, CONF_CAPTURE_RAW, CONF_DATA_DLY_WR;
(* mark_debug = "true" *)reg CONF_RESET_CNT;
wire DECODER_ERR, READ_COL2, LOST_ERR;

always @(posedge BUS_CLK) begin
    if(BUS_RST) begin
        CONF_INVERT_RX <= 0;
        CONF_EN <= 1'b0;
        CONF_SAMPLING_EDGE <= 0;
        CONF_CAPTURE_RAW <= 0;
        CONF_DUMP_ALL <= 1'b0;
        CONF_DATA_DLY_WR <=1'b0;
        CONF_EN_TS <= 1'b1;
        CONF_RESET_CNT <= 1'b0;
        // CONF_DLY_CNT <= 4'b0; 
    end
        else if(BUS_WR) begin
        if(BUS_ADD == 1)
            {CONF_DUMP_ALL, CONF_CAPTURE_RAW, CONF_INVERT_RX, CONF_EN} <= BUS_DATA_IN[7:4];
        else if(BUS_ADD == 2)
            // {CONF_RESET_CNT, CONF_SAMPLING_EDGE, CONF_DATA_DLY_WR, CONF_DATA_DLY} <= BUS_DATA_IN[3:0];
            {CONF_RESET_CNT} <= BUS_DATA_IN[0];
        // else if(BUS_ADD == 5)
        //     CONF_DLY_CNT <= BUS_DATA_IN[4:0];

    end

end

// status
assign DECODER_ERR = (decoder_err_cnt!=8'b0);
assign LOST_ERR = (lost_err_cnt!=8'b0);
assign READ_COL2 = (read_col2_cnt!=8'b0);



///////////////////////DDR without delay /////////////////////

wire RX_DDR;
(* mark_debug = "true" *) wire RX_DLY;
assign DATA_DLY_OUT = 5'b0;
// TODO fill codes here!
assign RX_DLY = RX_DATA;
wire Q1, Q2;
IDDR IDDR_RX (
    .Q1(Q1),
    .Q2(Q2),
    .C(CLK_RX_OUT),
    .CE(1'b1),
    // .D(RX_DATA_DLY),
    .D(RX_DLY),
    .R(1'b0),
    .S(1'b0)
);
// assign RX_DDR = Q1 ;
//assign RX_DDR = CONF_SAMPLING_EDGE ? Q1 : Q2;  /// !!!
assign RX_DDR = RX_DLY;

///////////////////////DDR without delay end/////////////////////




///////////////////////DDR with delay/////////////////////
//TODO: add delay
// reg [4:0] CONF_DLY_CNT;
// wire RX_DATA_DLY;
// wire [4:0] DLY_CNT_OUT;
// reg [3:0] des;
// wire LD_DLY;
//  IDELAYE2 #(
//          .CINVCTRL_SEL("FALSE"),
//          .DELAY_SRC("IDATAIN"),
//          .HIGH_PERFORMANCE_MODE("TRUE"), 
//          .IDELAY_TYPE("VAR_LOAD"), 
//          .IDELAY_VALUE(0),
//          .REFCLK_FREQUENCY(200.0),
//          .SIGNAL_PATTERN("DATA")
//  ) IDELAYE2 (
//      .C(BUS_CLK),
//      .REGRST(1'b0),
//      .LD(LD_DLY),
//      .CE(1'b0),
//      .INC(1'b0),
//      .CINVCTRL(1'b0),
//      .CNTVALUEIN(CONF_DLY_CNT[4:0]),
//      .IDATAIN(RX_DATA),
//      .DATAIN(1'b0),
//      .LDPIPEEN(1'b0), //input 1bit
//      .CNTVALUEOUT(DLY_CNT_OUT),
//      .DATAOUT(RX_DATA_DLY)
//  );


///////////////////////DDR with delay end/////////////////////


// 10b data sync
(* mark_debug = "true" *)  wire [9:0] data_8b10b;
reg decoder_err;
wire rec_sync_ready;
wire aligned;
rec_sync rec_sync (
    .reset(RESET_WCLK),
    .datain(CONF_INVERT_RX_SYNC ? ~RX_DDR : RX_DDR),
    .data(data_8b10b),
    .WCLK(RX_CLKW),
    .FCLK(CLK_RX_OUT),
    .rec_sync_ready(rec_sync_ready), //output
    .aligned(aligned),
    .decoder_err(decoder_err)        //input
);


// 8b/10b decode
(* mark_debug = "true" *) reg [9:0] data_to_dec;
integer i;
always @ (*) begin
    for (i=0; i<10; i=i+1)
        data_to_dec[(10-1)-i] = data_8b10b[i];
end
always @(posedge RX_CLKW)
    if (CONF_CAPTURE_RAW_SYNC)
        raw_data <= {raw_data[9:0], data_to_dec};

reg dispin;
wire dispout;
always@(posedge RX_CLKW) begin
    if(RESET_WCLK)
        dispin <= 1'b0;
    else
        dispin <= dispout;
end
wire dec_k;
(* mark_debug = "true" *) wire [7:0] dec_data;
wire code_err;
wire disp_err;
decode_8b10b decode_8b10b (
    .datain(data_to_dec),
    .dispin(dispin),
    .dataout({dec_k, dec_data}), // control character, data out
    .dispout(dispout),
    .code_err(code_err),
    .disp_err(disp_err)
);
always @(posedge RX_CLKW) begin
    if(RESET_WCLK)
        decoder_err <= 1'b0;
    else
        decoder_err <= code_err | disp_err;
end
wire write_en;
assign write_en = (rec_sync_ready|CONF_CAPTURE_RAW_SYNC) & CONF_EN_SYNC;
always@(posedge RX_CLKW) begin
    // if(RESET_WCLK | CONF_RESET_CNT_SYNC)
    if(RESET_WCLK)
        decoder_err_cnt <= 0;
    else
        if(decoder_err && write_en && decoder_err_cnt != 8'hff)
        // if(decoder_err && write_en)
            decoder_err_cnt <= decoder_err_cnt + 1;
end

// get data
wire idle = write_en && dec_k && (dec_data==8'b101_11100); //0x1BC(K28.5)   > 5C
wire busy = write_en && (!idle | CONF_DUMP_ALL_SYNC);
wire head = write_en && dec_k && (dec_data==8'b000_11100); //0x11C(K28.0)(L_cl_2),
wire cnt_head = write_en && dec_k && dec_data==8'b001_11100; //0x13C(K28.1)
reg [2:0] byte_sel;
always@(posedge RX_CLKW) begin
    if(RESET_WCLK)
        byte_sel <= 0;
    else if(busy) begin
        if (byte_sel == 2)
            byte_sel <= 0;
        else
            byte_sel <= byte_sel+1;
    end
    else
        byte_sel <= 0;
end
reg [2:0] busy_ff;
always@(posedge RX_CLKW) begin
    busy_ff <= {busy_ff[1:0], busy && (byte_sel==1 | byte_sel==0)};
end
always@(posedge RX_CLKW) begin
    if(RESET_WCLK | CONF_RESET_CNT_SYNC | read_col2_cnt ==8'hFF)
        read_col2_cnt <= 8'h00;
    // lets count the ReadColum2 signals
    else if (write_en && busy && ((data[0]==9'h0C1 & data[1][7:6]==2'b00) | (data[1]==9'h0C1 & data[2][7:6]==2'b00)  | (data[2]==9'h0C1) ) )
        read_col2_cnt <= read_col2_cnt + 1;
end

// align data for fifo, 3 x 9bits data
reg [3:0] wcnt;
always@(posedge RX_CLKW) begin
    if(RESET_WCLK)
        wcnt <= 4'hd;
    else if(head | wcnt==4'hd)
        wcnt <= 4'h0;
    else if ((byte_sel==2 | (idle & busy_ff[0])))
        wcnt <= wcnt + 1;
    else if (busy_ff[1] & idle)
        wcnt <= 4'he;
    else if (busy_ff[2] & idle)
        wcnt <= 4'hf;
    else if (idle)
        wcnt <= 4'hd;
end
reg [8:0] data [2:0];
reg [3:0] data_info;
always@(posedge RX_CLKW) begin
    if(RESET_WCLK) begin
        data[0] <= 9'b0;
        data[1] <= 9'b0;
        data[2] <= 9'b0;
    end
    else if (busy) begin
        data[byte_sel] <= {dec_k, dec_data};
        data_info[2-byte_sel] <=dec_k;
        data_info[3] <=0;
    end
    else if (busy_ff[0] & byte_sel==2 & idle) begin
        data[2] <= 9'b1_010_11100;
        data_info[0] <=1;
        data_info[3] <=1;
    end
    else if (busy_ff[0] & byte_sel==1 & idle) begin
        data[1] <= 9'b1_010_11100;
        data_info[1] <=1;
        data_info[3] <=1;
        //why?
        if (data[2]!=9'h11c) begin
            data[2] <= 9'b1_010_11100; //K28.2 0x5C
            data_info[0] <=1;
            data_info[3] <=1;
        end
    end
    else if (busy_ff[1] & idle) begin
        data[0]<={1'b0,FPGA_TIMESTAMP[7:0]};
        data[1]<={1'b0,FPGA_TIMESTAMP[15:8]};
        data[2]<={1'b0,FPGA_TIMESTAMP[23:16]};
        data_info <= {4'b0110};
    end
    // else if (busy_ff[2] & idle & FPGA_TIMESTAMP[31:24] != 0 ) begin
    else if (busy_ff[2] & idle) begin
        data[0]<={1'b0,FPGA_TIMESTAMP[31:24]};
        data[1]<={1'b0,FPGA_TIMESTAMP[39:32]};
        data[2]<={1'b0,FPGA_TIMESTAMP[47:40]};
        data_info <= {4'b0111};
    end
end
wire [27:0] cdc_data_out;
wire [27:0] wdata;
// wire [3:0] data_info;
// assign data_info = CONF_DUMP_ALL_SYNC ? {1'b0, data[0][8], data[1][8], data[2][8]} : wcnt;
assign wdata = {data_info, data[0][7:0], data[1][7:0], data[2][7:0]};
// assign wdata = {1'b0, data[0][8], data[1][8], data[2][8], data[0][7:0], data[1][7:0], data[2][7:0]};
wire dont_write;
// 0x151caa1c
// 0x1baa5c1c to be checked where should the 5C come from?
// new ones:
// 0x1ffc5c5c
//0x151caa1c
// 0x1ffc5c5c
assign dont_write = 
                    // (!CONF_EN_RX_WCLK) & (!CONF_DUMP_ALL_SYNC)
                    // & ((data[0]==9'h11c & data[1]==9'haa & data[2]==9'h11c) 
                    //  | (data[0]==9'haa & data[1]==9'h15c & data[2]==9'h11c)
                    //  | (data[0]==9'h1fc & data[1]==9'h15c & data[2]==9'h115)
                    //  )
                    (!CONF_DUMP_ALL_SYNC)
                    & ((data[0]==9'h11c & data[1]==9'haa & data[2]==9'h11c & wcnt==0) 
                     | (data[0]==9'haa & data[1]==9'h15c & data[2]==9'h11c & wcnt==1)
                    //  | data_info[3]
                     ) 
                    ; 

// prepare signals for fifo
reg write_wdata;
always@(posedge RX_CLKW) begin
    if(RESET_WCLK) 
        write_wdata <= 1'b0;
    else if (byte_sel==2)
        write_wdata <= 1'b1;
    else if (idle & busy_ff[0])
        write_wdata <= 1'b1;
    else if (idle & busy_ff[2:1]!=2'b00 & CONF_EN_TS)
        write_wdata <= 1'b1;
    else
        write_wdata <= 1'b0;
end
wire cdc_fifo_full, cdc_fifo_empty;
always@(posedge RX_CLKW) begin
    if(RESET_WCLK | CONF_RESET_CNT_SYNC)
        lost_err_cnt <= 0;
    else if(cdc_fifo_full && write_wdata && (~dont_write) && lost_err_cnt != 8'hff)
    // else if(cdc_fifo_full && write_wdata && lost_err_cnt != 8'hff)
        lost_err_cnt <= lost_err_cnt + 1;
end
wire cdc_fifo_winc;
assign cdc_fifo_winc = write_wdata & (~cdc_fifo_full) & (~dont_write);

// fifo
wire fifo_full;
cdc_syncfifo #( .DSIZE(28),.ASIZE(6) ) cdc_syncfifo ( //sync to fifo_clock
    .wfull(cdc_fifo_full),
    .wdata(wdata),
    .winc(cdc_fifo_winc),
    .wclk(RX_CLKW),
    .wrst(RESET_WCLK),
    .rempty(cdc_fifo_empty),
    .rinc(!fifo_full),
    .rclk(BUS_CLK),
    .rrst(RESET_FIFO),
    .rdata(cdc_data_out)
);
wire [26:0] data_out;
gerneric_fifo #( .DATA_SIZE(28), .DEPTH(1024*10) ) gerneric_fifo ( //actual fifo
    .clk(BUS_CLK),
    .reset(RESET_FIFO),
    .write(!cdc_fifo_empty),
    .read(FIFO_READ),
    .data_in(cdc_data_out),
    .data_out(FIFO_DATA[27:0]),
    .full(fifo_full),
    .empty(FIFO_EMPTY),
    .size(fifo_size)
);
assign FIFO_DATA[31:28] = DATA_IDENTIFIER;

//------------------ DATA END -------------------------------//



endmodule
