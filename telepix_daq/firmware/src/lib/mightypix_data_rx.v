/**
 * ------------------------------------------------------------
 * Copyright (c) All rights reserved
 * SiLab, Institute of Physics, University of Bonn
 * ------------------------------------------------------------
 */
`timescale 1ps / 1ps 
`default_nettype none

module mightypix_data_rx #(
    parameter BASEADDR = 32'h0000,
    parameter HIGHADDR = 32'h0000,
    parameter DSIZE = 10,
    parameter DATA_IDENTIFIER = 4'h1,
    parameter ABUSWIDTH = 32,
    parameter USE_FIFO_CLK = 0
) (
    // clocks
    input  wire RX_CLK,
    input  wire CLK_RX_OUT,
    input wire TS_CLK,


    // // Fast receiver module
    // output wire soft_reset_rx,
    // input wire [3:0] rxdisperr,
    // input wire [3:0] rxnotintable,
    // input wire rxusrclk,
    // input wire rxusrclk2,
    // input wire [31:0] rxdata,
    // input wire [3:0] rxcharisk,
    // input wire resetdone,
    // input wire track_data_out,
    // input wire cplllocked,


    // chip
    output wire RX_READY,
    input wire RX_DATA,
    // input wire [47:0] FPGA_TIMESTAMP,
    input wire FIFO_READ,
    output wire FIFO_EMPTY,
    output wire [31:0] FIFO_DATA,
    output wire RX_ENABLED,
    output wire ts_data_trigger,

    // Bus
    input wire                 BUS_CLK,
    input wire                 BUS_RST,
    input wire [ABUSWIDTH-1:0] BUS_ADD,
    inout wire [          7:0] BUS_DATA,
    input wire                 BUS_RD,
    input wire                 BUS_WR


);


  wire IP_RD, IP_WR;
  wire [ABUSWIDTH-1:0] IP_ADD;
  wire [7:0] IP_DATA_IN;
  wire [7:0] IP_DATA_OUT;

  bus_to_ip #(
      .BASEADDR (BASEADDR),
      .HIGHADDR (HIGHADDR),
      .ABUSWIDTH(ABUSWIDTH)
  ) bus_to_ip (
      .BUS_RD  (BUS_RD),
      .BUS_WR  (BUS_WR),
      .BUS_ADD (BUS_ADD),
      .BUS_DATA(BUS_DATA),

      .IP_RD(IP_RD),
      .IP_WR(IP_WR),
      .IP_ADD(IP_ADD),
      .IP_DATA_IN(IP_DATA_IN),
      .IP_DATA_OUT(IP_DATA_OUT)
  );

  wire FIFO_CLK_INT;
  assign FIFO_CLK_INT = BUS_CLK;



// `ifdef SLOW

  mightypix_data_rx_core_slow_new#(
      .DSIZE(DSIZE),
      .DATA_IDENTIFIER(DATA_IDENTIFIER),
      .ABUSWIDTH(ABUSWIDTH)
  ) core (
    // Bus
      .BUS_CLK(BUS_CLK),
      .BUS_RST(BUS_RST),
      .BUS_ADD(IP_ADD),
      .BUS_DATA_IN(IP_DATA_IN),
      .BUS_RD(IP_RD),
      .BUS_WR(IP_WR),
      .BUS_DATA_OUT(IP_DATA_OUT),
      .TS_CLK(TS_CLK),

    // clocks
      .RX_CLKW(RX_CLK),
      .CLK_RX_OUT(CLK_RX_OUT),

    // chip
      .RX_DATA(RX_DATA),
      .RX_READY(RX_READY), 
      // .FPGA_TIMESTAMP(FPGA_TIMESTAMP),
      .FIFO_READ(FIFO_READ),
      .FIFO_EMPTY(FIFO_EMPTY),
      .FIFO_DATA(FIFO_DATA),
      .RX_ENABLED(RX_ENABLED),

      .ts_data_trigger_sync(ts_data_trigger)
     
  );


// // fast receiver core module

// `else
//   mightypix_data_rx_core_fast #(
//       .DSIZE(DSIZE),
//       .DATA_IDENTIFIER(DATA_IDENTIFIER),
//       .ABUSWIDTH(ABUSWIDTH)
//   ) core (
//       .BUS_CLK(BUS_CLK),
//       .BUS_RST(BUS_RST),
//       .BUS_ADD(IP_ADD),
//       .BUS_DATA_IN(IP_DATA_IN),
//       .BUS_RD(IP_RD),
//       .BUS_WR(IP_WR),
//       .BUS_DATA_OUT(IP_DATA_OUT),

//       .TS_CLK(TS_CLK)
//       .RX_CLK(RX_CLK),
//       .RX_CLK2X(RX_CLK2X),b1
//       .DATA_CLK(DATA_CLK),
//       .DATA_CLK_x5(DATA_CLK_x5),
//       .RX_DATA_P(RX_DATA_P),
//       .RX_DATA_N(RX_DATA_N),
//       .RX_READY(RX_READY),
//       .RX_8B10B_DECODER_ERR(RX_8B10B_DECODER_ERR),
//       .RX_FIFO_OVERFLOW_ERR(RX_FIFO_OVERFLOW_ERR),

//        //RX INFO
//       .soft_reset_rx(soft_reset_rx),
//       .rxdisperr(rxdisperr),
//       .rxnotintable(rxnotintable),
//       .rxusrclk(rxusrclk),
//       .RX_DATA(rxdata),
//       .rxcharisk(rxcharisk),
//       .resetdone(resetdone),
//       .track_data_out(track_data_out),
//       .cplllocked(cplllocked),

//       .FIFO_CLK(FIFO_CLK_INT),
//       .FIFO_READ(FIFO_READ),
//       .FIFO_EMPTY(FIFO_EMPTY),
//       .FIFO_DATA(FIFO_DATA),
//       .EN_FAKE_Data(EN_FAKE_Data),

//       .RX_FIFO_FULL(RX_FIFO_FULL),
//       .RX_ENABLED  (RX_ENABLED)
//   );
// `endif




endmodule