/**
 * ------------------------------------------------------------
 * Copyright (c) All rights reserved
 * SiLab, Institute of Physics, University of Bonn
 * ------------------------------------------------------------
 */
`timescale 1ps/1ps
`default_nettype none

module mighty_rx_core_DDR #(
    parameter           DSIZE = 10,
    parameter           ABUSWIDTH = 16,
    parameter           IDENTIFIER = 3'b000
) (
    input wire BUS_CLK,
    input wire [ABUSWIDTH-1:0] BUS_ADD,
    input wire [7:0] BUS_DATA_IN,
    output reg [7:0] BUS_DATA_OUT,
    input wire BUS_RST,
    input wire BUS_WR,
    input wire BUS_RD,

    input wire RX_CLK2x,
    input wire RX_CLK,
    input wire RX_CLKW_FAST2x,
    input wire RX_CLKW_FAST,
    input wire RX_CLKW_SLOW,
    input wire RX_CLKW_SLOWEST,
    input wire RX_DATA_FAST,
    input wire RX_DATA_SLOWEST,

    input wire FIFO_READ,
    output wire FIFO_EMPTY,
    output wire [31:0] FIFO_DATA
);

localparam VERSION = 1;

////////////////////////////////////////
// Resets
wire SOFT_RST;
assign SOFT_RST = (BUS_ADD==0 && BUS_WR); // reset sync and registers when write to addr = 0 then reset
wire LD_DLY;
assign LD_DLY = (BUS_ADD==24 && BUS_WR);
wire RST;
assign RST = BUS_RST | SOFT_RST;

reg [7:0] sync_cnt;
always @(posedge BUS_CLK) begin
    if(RST)
        sync_cnt <= 120;
    else if(sync_cnt != 100)
        sync_cnt <= sync_cnt +1;
end
wire RST_LONG;
assign RST_LONG = sync_cnt[7];
////////////////////////////////////////
// Local Bus parameters
reg CONF_EN;
reg CONF_DESCRAMBLE;
reg CONF_INVERT_DATA;
reg CONF_SAMPLING_EDGE;
reg [1:0] CONF_SPEED_MODE;
reg CONF_DUMP_ALL;

reg [9:0] CONF_ALIGN_THRESHOLD;
reg [4:0] CONF_DLY_CNT;
reg [4:0] CONF_FIXED_BITSLIP_VALUE;
reg CONF_IS_FIXED_BITSLIP;

always @(posedge BUS_CLK) begin
    if(RST) begin
        CONF_EN <= 1'b0;
        CONF_DESCRAMBLE <= 1'b0;
        CONF_INVERT_DATA <= 1'b1;
        CONF_SAMPLING_EDGE <= 1'b0;
        CONF_DUMP_ALL <= 1'b0;
        CONF_SPEED_MODE <= 2'b0;
        CONF_ALIGN_THRESHOLD <= 8'd2;
        CONF_DLY_CNT <= 4'b0; 
        CONF_IS_FIXED_BITSLIP <= 1'b0;
        CONF_FIXED_BITSLIP_VALUE <= 5'h15;
    end
    else if(BUS_WR) begin
        if(BUS_ADD == 2) begin
            CONF_EN <= BUS_DATA_IN[0];
            CONF_DESCRAMBLE <= BUS_DATA_IN[1];
            CONF_INVERT_DATA <= BUS_DATA_IN[2];
            CONF_SAMPLING_EDGE <= BUS_DATA_IN[3];
            CONF_DUMP_ALL <= BUS_DATA_IN[4];
            CONF_SPEED_MODE <= BUS_DATA_IN[6:5];
        end
        else if(BUS_ADD == 3)
            CONF_ALIGN_THRESHOLD[7:0] <= BUS_DATA_IN;
        else if(BUS_ADD == 4) begin
            CONF_ALIGN_THRESHOLD[9:8] <= BUS_DATA_IN[1:0]; 
            CONF_FIXED_BITSLIP_VALUE <= BUS_DATA_IN[6:2];
            CONF_IS_FIXED_BITSLIP <= BUS_DATA_IN[7];
        end
        else if(BUS_ADD == 5)
            CONF_DLY_CNT <= BUS_DATA_IN[4:0];
    end
end

reg [15:0] fifo_size;
reg [15:0] LOST_COUNT;
wire LOST_ERR;
reg [63:0] CURRENT_DATA;
reg [63:0] AlIGNED_DATA;
wire ALIGNED;
wire [4:0] BITSLIP;
wire IDLE;
wire IS_BIN_COUNTER;
wire [4:0] DLY_CNT_OUT;
wire IDLE_PATTERN;
always @ (posedge BUS_CLK) begin  //TODO clean address
    if(BUS_RD) begin
        if(BUS_ADD == 0)
            BUS_DATA_OUT <= VERSION;
        else if(BUS_ADD == 1)
            BUS_DATA_OUT <= {LOST_ERR, IDLE, ALIGNED, BITSLIP};
        else if(BUS_ADD == 2)
            BUS_DATA_OUT <= {1'b0, CONF_SPEED_MODE, CONF_DUMP_ALL,
                             CONF_SAMPLING_EDGE, CONF_INVERT_DATA, CONF_DESCRAMBLE, CONF_EN};
        else if(BUS_ADD == 3)
            BUS_DATA_OUT <= CONF_ALIGN_THRESHOLD[7:0];
        else if(BUS_ADD == 4)
            BUS_DATA_OUT <= {CONF_IS_FIXED_BITSLIP, CONF_FIXED_BITSLIP_VALUE, CONF_ALIGN_THRESHOLD[9:8]};
        else if(BUS_ADD == 5)
            BUS_DATA_OUT <= {3'b0, CONF_DLY_CNT};
        else if(BUS_ADD == 6)
            BUS_DATA_OUT <= LOST_COUNT[7:0];
        else if(BUS_ADD == 7)
            BUS_DATA_OUT <= LOST_COUNT[15:8];
        else if(BUS_ADD == 8)
            BUS_DATA_OUT <= AlIGNED_DATA[7:0];  //for debug
        else if(BUS_ADD == 9)
            BUS_DATA_OUT <= AlIGNED_DATA[15:8];
        else if(BUS_ADD == 10)
            BUS_DATA_OUT <= AlIGNED_DATA[23:16];
        else if(BUS_ADD == 11)
            BUS_DATA_OUT <= AlIGNED_DATA[31:24];
        else if(BUS_ADD == 12)
            BUS_DATA_OUT <= AlIGNED_DATA[39:32];
        else if(BUS_ADD == 13)
            BUS_DATA_OUT <= AlIGNED_DATA[47:40];
        else if(BUS_ADD == 14)
            BUS_DATA_OUT <= AlIGNED_DATA[55:40];
        else if(BUS_ADD == 15)
            BUS_DATA_OUT <= AlIGNED_DATA[63:56];
        else if(BUS_ADD == 16)
            BUS_DATA_OUT <= CURRENT_DATA[7:0];  //for debug
        else if(BUS_ADD == 17)
            BUS_DATA_OUT <= CURRENT_DATA[15:8];
        else if(BUS_ADD == 18)
            BUS_DATA_OUT <= CURRENT_DATA[23:16];
        else if(BUS_ADD == 19)
            BUS_DATA_OUT <= CURRENT_DATA[31:24];
        else if(BUS_ADD == 20)
            BUS_DATA_OUT <= CURRENT_DATA[39:32];
        else if(BUS_ADD == 21)
            BUS_DATA_OUT <= CURRENT_DATA[47:40];
        else if(BUS_ADD == 22)
            BUS_DATA_OUT <= CURRENT_DATA[55:40];
        else if(BUS_ADD == 23)
            BUS_DATA_OUT <= CURRENT_DATA[63:56];
        else if(BUS_ADD == 24)
            BUS_DATA_OUT <= {1'b0, IDLE_PATTERN, IS_BIN_COUNTER, DLY_CNT_OUT[4:0]};
        else
            BUS_DATA_OUT <= 8'b0;
    end
end
wire RX_CLKW;
//assign CLKW = CONF_SPEED_MODE_SYNC[0]? (CONF_SPEED_MODE_SYNC[1]? RX_CLKW_SLOWEST:RX_CLKW_SLOW):RX_FAST;
assign RX_CLKW = RX_CLKW_FAST;
wire RX_CLKW2x;
//assign RX_CLKW2x= CONF_SPEED_MODE[1] ? RX_CLKW_SLOW:RX_CLKW_FAST;
assign RX_CLKW2x = RX_CLKW_FAST2x;
wire RST_SYNC;
cdc_pulse_sync rst_pulse_sync (.clk_in(BUS_CLK), .pulse_in(RST), .clk_out(RX_CLKW), .pulse_out(RST_SYNC));
wire CONF_DUMP_ALL_SYNC;
three_stage_synchronizer conf_dump_all_sync( .CLK(RX_CLKW), .IN(CONF_DUMP_ALL), .OUT(CONF_DUMP_ALL_SYNC) );
wire CONF_EN_SYNC;
three_stage_synchronizer conf_en_sync( .CLK(RX_CLKW), .IN(CONF_EN), .OUT(CONF_EN_SYNC));
////////////////////////////////////////
// recieve data
//TODO: add delay
wire RX_DATA_DLY;
 IDELAYE2 #(
         .CINVCTRL_SEL("FALSE"),
         .DELAY_SRC("IDATAIN"),
         .HIGH_PERFORMANCE_MODE("TRUE"), 
         .IDELAY_TYPE("VAR_LOAD"), 
         .IDELAY_VALUE(0),
         .REFCLK_FREQUENCY(200.0),
         .SIGNAL_PATTERN("DATA")
 ) IDELAYE2 (
     .C(BUS_CLK),
     .REGRST(1'b0),
     .LD(LD_DLY),
     .CE(1'b0),
     .INC(1'b0),
     .CINVCTRL(1'b0),
     .CNTVALUEIN(CONF_DLY_CNT[4:0]),
     .IDATAIN(RX_DATA_FAST),
     .DATAIN(1'b0),
     .LDPIPEEN(1'b0), //input 1bit
     .CNTVALUEOUT(DLY_CNT_OUT),
     .DATAOUT(RX_DATA_DLY)
 );
// IDELAYE2 #(
reg [3:0] des;
/*ISERDESE2 #(
     .DATA_WIDTH         (4),
     .DATA_RATE          ("DDR"),
     .SERDES_MODE        ("MASTER"),
     .IOBDELAY           ("IFD"),  //IFD, NONE
     .INTERFACE_TYPE     ("NETWORKING")
) ISERDESE2_i (
     .D               (RX_DATA_FAST),
     .DDLY            (RX_DATA_DLY),
     .CE1             (1'b1),
     .CE2             (1'b1),
     .CLK             (RX_CLK2x),
     .CLKB            (~RX_CLK2x),
     .RST             (RST_SYNC),
     .CLKDIV          (RX_CLK),
     .CLKDIVP         (1'b0),
     .OCLK            (1'b0),
     .OCLKB           (1'b0),
     .DYNCLKSEL       (1'b0),
     .DYNCLKDIVSEL    (1'b0),
     .SHIFTIN1        (1'b0),
     .SHIFTIN2        (1'b0),
     .BITSLIP         (1'b0),
     .O(),
     .Q4              (des[3]),
     .Q3              (des[2]),
     .Q2              (des[1]),
     .Q1              (des[0]),
     .SHIFTOUT1       (),
     .SHIFTOUT2       ()
);*/
//         .CINVCTRL_SEL("FALSE"),
wire [1:0] DDRQ;
IDDR # (
   .DDR_CLK_EDGE("SAME_EDGE_PIPELINED")
) IDDR_inst (
    .Q1(DDRQ[1]), // 1-bit output for positive edge of clock
    .Q2(DDRQ[0]), // 1-bit output for negative edge of clock
    .C(RX_CLK2x), // 1-bit clock input
    .CE(1'b1), // 1-bit clock enable input
    .D(RX_DATA_DLY), // 1-bit DDR data input
    .R(1'b0), // 1-bit reset
    .S(1'b0)  // 1-bit set
);
//         .DELAY_SRC("IDATAIN"),
reg [1:0] DDRQ_DLY;
always @(posedge RX_CLK2x)
    DDRQ_DLY[1:0] <= DDRQ[1:0];
//         .HIGH_PERFORMANCE_MODE("TRUE"), 
reg [3:0] DDRQ_DATA;
always @(posedge RX_CLK2x)
    DDRQ_DATA[3:0] <= {DDRQ_DLY[1:0], DDRQ[1:0]};
//         .IDELAY_TYPE("VAR_LOAD"), 
 reg [3:0] DDRQ_DATA_BUF;
always @(posedge RX_CLK2x)
    DDRQ_DATA_BUF[3:0] <= DDRQ_DATA[3:0];
//         .IDELAY_VALUE(0),
always @(posedge RX_CLK)
    des[3:0] <= DDRQ_DATA_BUF[3:0];

wire [3:0] RX_DATA;
assign RX_DATA = CONF_SAMPLING_EDGE? {~des[2],~des[3],~des[0],~des[1]}:~des; //?? useless

wire [31:0] DATA_ALIGNED;
wire [63:0] debug64;
align_mightypix align_mightypix (
    .res(RST_SYNC),
    .clk_in(RX_CLK),
    .clk_div_in(RX_CLKW),
    .data(RX_DATA),
    .speed_mode(CONF_SPEED_MODE),
    .align_threshold(CONF_ALIGN_THRESHOLD),
    .is_fixed_bitslip(CONF_IS_FIXED_BITSLIP),
    .fixed_bitslip_value(CONF_FIXED_BITSLIP_VALUE),
    .aligned(ALIGNED),
    .bitslip(BITSLIP),
    .data_out(DATA_ALIGNED),

    .debug64(debug64)
);

wire [31:0] DATA_DESCR;
descrambler_mightypix descrambler_mightypix(
        .RST(RST_SYNC),
        .dataIn(DATA_ALIGNED),
        .clk_w_2x(RX_CLKW2x),
        .clk_w(RX_CLKW),
        .scrambleEnable(CONF_DESCRAMBLE),
        .dataOut(DATA_DESCR)
);
// TODO add error counter
always@(posedge RX_CLKW)
    CURRENT_DATA <= {CURRENT_DATA[31:0], DATA_DESCR}; // just for debug
always@(posedge RX_CLKW)
    AlIGNED_DATA <={AlIGNED_DATA[31:0], DATA_ALIGNED};

// mask IDLE data   
reg [15:0] cal_counter;
always@(posedge RX_CLKW) begin
    if (RST_SYNC) begin
        cal_counter <= 16'b0;
    end
    else begin
        cal_counter <= DATA_DESCR[25:10] + 1;
     end
end
assign IS_BIN_COUNTER = (cal_counter==DATA_DESCR[25:10]);
assign IDLE_PATTERN = (DATA_DESCR[31:26] == 6'b101101) && (DATA_DESCR[9:0] == 10'b0);
// mask IDLE data
assign IDLE = (DATA_DESCR[31:26] == 6'b101101) && (DATA_DESCR[9:0] == 10'b0) && (!CONF_DUMP_ALL_SYNC);

reg cdc_winc;
reg [80:0] wdata;
reg [1:0] word_cnt;
wire wfull;
always@(posedge RX_CLKW) begin
    if(RST_SYNC) begin
        word_cnt <= 0;
        wdata <= 80'b0;
        cdc_winc <= 1'b0;
    end
    else if (IDLE) begin
        wdata[80] <= IS_BIN_COUNTER;
        wdata[79:64] <= DATA_DESCR[25:10];
        word_cnt <= 0;
        cdc_winc <= 1'b0;
    end
    else if(word_cnt==0 || word_cnt==2) begin
        wdata[80] <= IS_BIN_COUNTER;
        wdata[78] <= IDLE_PATTERN;
        wdata[31:0] = DATA_DESCR;
        word_cnt <= 1;
        cdc_winc <= 1'b0;
    end
    else if(word_cnt==1) begin
        wdata[79] <= IS_BIN_COUNTER;
        wdata[77] <= IDLE_PATTERN;
        wdata[63:32] = DATA_DESCR;
        word_cnt <= 2;
        cdc_winc <= CONF_EN_SYNC;
    end
end

// lost counter
always @(posedge RX_CLKW) begin
    if(RST_SYNC)
        LOST_COUNT <= 0;
    else if (wfull && cdc_winc && LOST_COUNT != 8'b1111_1111)
        LOST_COUNT <= LOST_COUNT + 1;
end
assign LOST_ERR = (LOST_COUNT!=16'b0);

// send to fifo
wire cdc_fifo_empty;
wire cdc_fifo_full;
wire [80:0] cdc_data_out;
wire fifo_full;
wire cdc_fifo_read;
cdc_syncfifo #(
    .DSIZE(81), .ASIZE(8)
) cdc_syncfifo_i (
    .wfull(wfull),
    .rempty(cdc_fifo_empty),

    .winc(cdc_winc),
    .wclk(RX_CLKW),
    .wrst(RST_LONG),
    .wdata(wdata),

    .rinc(cdc_fifo_read),
    .rclk(BUS_CLK),
    .rrst(RST_LONG),
    .rdata(cdc_data_out)
);

reg [1:0] byte2_cnt, byte2_cnt_prev;
always @(posedge BUS_CLK)
    byte2_cnt_prev <= byte2_cnt;
assign cdc_fifo_read = (byte2_cnt_prev==0 & byte2_cnt!=0);
wire fifo_write;
assign fifo_write = byte2_cnt_prev != 0;

always @(posedge BUS_CLK)
    if(RST)
        byte2_cnt <= 0;
    else if(!cdc_fifo_empty && !fifo_full && byte2_cnt == 0)
        byte2_cnt <= 3;
    else if (!fifo_full & byte2_cnt != 0)
        byte2_cnt <= byte2_cnt - 1;

reg [80:0] data_buf;
always @(posedge BUS_CLK)
    if(cdc_fifo_read)
        data_buf <= cdc_data_out;

wire [28:0] fifo_write_data_byte [3:0];
assign fifo_write_data_byte[3] = 29'b0; // dummy data
assign fifo_write_data_byte[2] = {2'b01, data_buf[26:0]};
assign fifo_write_data_byte[1] = {2'b10, data_buf[53:27]};
assign fifo_write_data_byte[0] = {2'b11, data_buf[80:54]};
wire [28:0] fifo_data_in;
assign fifo_data_in = fifo_write_data_byte[byte2_cnt];

gerneric_fifo #(
    .DATA_SIZE(29),
    .DEPTH(1024)
) fifo_i (
    .clk(BUS_CLK),
    .reset(RST_LONG | BUS_RST),
    .write(fifo_write),
    .read(FIFO_READ),
    .data_in(fifo_data_in),
    .full(fifo_full),
    .empty(FIFO_EMPTY),
    .data_out(FIFO_DATA[28:0]),
    .size()
);
assign FIFO_DATA[31:29] = IDENTIFIER[2:0];

endmodule
