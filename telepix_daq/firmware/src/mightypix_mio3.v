
`timescale 1ns / 1ps
`default_nettype none

`define CODE_FOR_MIO3
`define SLOW 1
// `define 10G 1

`ifndef 10G
    `include "utils/fifo_32_to_8.v"

    `include "SiTCP/TIMER.v"
    `include "SiTCP/SiTCP_XC7K_32K_BBT_V110.V"
    `include "SiTCP/WRAP_SiTCP_GMII_XC7K_32K.V"
    `include "utils/rgmii_io.v"
    // `include "WRAP_SiTCP_GMII_XC7K_32K.V"
    // `include "SiTCP_XC7K_32K_BBT_V110.V"
    // `include "TIMER.v"


`endif

`include "clk_gen.v"
`include "mightypix_core.v"

`include "utils/bus_to_ip.v"

`include "utils/cdc_syncfifo.v"
`include "utils/generic_fifo.v"
`include "utils/cdc_pulse_sync.v"

`include "utils/reset_gen.v"
`include "utils/CG_MOD_pos.v"

`include "spi/blk_mem_gen_8_to_1_2k.v"
`include "two_phase_spi/two_phase_spi_core.v"
`include "two_phase_spi/two_phase_spi.v"

`include "shift_register_external/shift_reg_ext_core.v"
`include "shift_register_external/shift_reg_ext.v"

`include "timestamp_div/timestamp_div_core.v"
`include "timestamp_div/timestamp_div.v"

`include "gpio/gpio.v"
`include "gpio/gpio_core.v"

`include "rrp_arbiter/rrp_arbiter.v"

`include "tlu/tlu_controller.v"
`include "tlu/tlu_controller_core.v"
`include "tlu/tlu_controller_fsm.v"

// `include "pulse_gen/pulse_gen.v"
// `include "pulse_gen/pulse_gen_core.v"
`include "pulse_gen_rising.v"

`include "timestamp_ext_rst/timestamp.v"
`include "timestamp_ext_rst/timestamp_core.v"

`include "utils/fx2_to_bus.v"
`include "utils/clock_divider.v"

`include "pulse_gen/pulse_gen.v"
`include "pulse_gen/pulse_gen_core.v"

`include "sram_fifo/sram_fifo_core.v"
`include "sram_fifo/sram_fifo.v"

`include "utils/3_stage_synchronizer.v"
`include "utils/flag_domain_crossing.v"
`include "utils/ddr_des.v"

`include "i2c/i2c.v"
`include "i2c/i2c_core.v"
`include "utils/rbcp_to_bus.v"


`include "lib/decode_8b10b.v"
// `include "lib/untriggered_data_alignment.v"
`include "lib/mightypix_data_rx.v"
`include "lib/receiver_logic.v"
`include "lib/sync_master.v"

`include "lib/ts_lifo.v"
// `include "lib/lifo.v"

`ifdef SLOW
// `include "lib/slow_data_input_exdes.v"
`include "lib/rec_sync.v"
// `include "lib/mightypix_data_rx_core_slow.v"
`include "lib/mightypix_data_rx_core_new_fsm.v"

`else
`include "../IP/fast_data_input_exdes.v"
`include "../IP/fast_data_input_gt_frame_check.v"
`include "lib/rec_sync_scratch.v"
`include "lib/des_mightypix.v"
`include "lib/align_mightypix.v"
`include "lib/mightypix_data_rx_core_DDR.v"
// `include "lib/mightypix_data_rx_core.v"
`endif

`include "tlu_slave/tlu_slave.v"
`include "tlu_slave/tlu_slave_core.v"
`include "tlu_slave/tlu_slave_fsm.v"
`include "timestamp640/timestamp640.v"
`include "timestamp640/timestamp640_core.v"

`include "utils/cdc_reset_sync.v"

// `include "utils/cdc_reset_sync.v"

`ifdef COCOTB_SIM //for simulation
   `include "utils/ODDR_sim.v"
   `include "utils/IDDR_sim.v"
  //  `include "utils/IBUF_sim.v"
  //  `include "utils/IOBUF_sim.v"
  //  `include "utils/PLLE2_BASE_sim.v"
   `include "utils/RAMB16_S1_S9_sim.v"
  //  `include "utils/DCM_sim.v"
  //  `include "utils/clock_multiplier.v"
   `include "utils/BUFG_sim.v"

  //  `include "utils/RAMB16_S1_S9_sim.v"
// `else
//    `include "utils/IDDR_s3.v"
//    `include "utils/ODDR_s3.v"
`endif

module mightypix_mio3 (

    // FPGA clk
    input wire FCLK_IN, // 100MHz
    //LED, Triggers
    output wire [7:0] LED,
    input wire [1:0] LEMO_RX,
    output wire TLU_CLK,        //TX0 and RJ45
    output wire TLU_BUSY,       //TX1 and RJ45
    input wire TLU_RESET,       //RJ45
    input wire TLU_TRIGGER,     //RJ45

    output wire SCK,    //DIN6
    output wire CSB,    //DIN5
    output wire MOSI,   //DIN4
    input wire  MISO,    //DOUT1
    output wire RST_CHIP,   //DIN7
    output wire Ctl_Clk_1,    //DIN3
    output wire Ctl_Clk_2,    //DIN2
    output wire Ctl_Load,     //DIN1 TODO fix conncection
    output wire Ctl_SIN,     //DIN0
    input wire  Ctl_SOut,      //DOUT0

    output wire INJECTION,
    input wire DATA_LVDS_P,     //LVDA_OUT0
    input wire DATA_LVDS_N,     //LVDA_OUT0
    output wire Clk_ext_LVDS_P, //LVDA_IN0
    output wire Clk_ext_LVDS_N, //LVDA_IN0
    output wire Clk_ref_P,      //LVDA_IN1
    output wire Clk_ref_N,      //LVDA_IN1
    output wire Sync_res_P,     //LVDA_IN2
    output wire Sync_res_N,     //LVDA_IN2


   input wire LVDS_IN_CLK1_P,
   input wire LVDS_IN_CLK1_N,

    // I2C
    inout wire SDA,
    inout wire SCL,

    //power detection
    input wire POWER_ON,
    input wire CLK_ENABLE,

    // declarations below are for MIO3 only
    input wire RESET_N,
    output wire MGT_REF_SEL,

   // Ethernet
  `ifdef 10G
      // SFP+
          output wire       SFP_TX_DISABLE,
          input wire        SFP_RX_N,
          input wire        SFP_RX_P,
          output wire       SFP_TX_N,
          output wire       SFP_TX_P,
          input wire        MGT_REFCLK1_P,  // Si511 156.25 MHz oscillator
          input wire        MGT_REFCLK1_N,  // Si511 156.25 MHz oscillator

  `else
          output wire [3:0] rgmii_txd,
          output wire       rgmii_tx_ctl,
          output wire       rgmii_txc,
          input  wire [3:0] rgmii_rxd,
          input  wire       rgmii_rx_ctl,
          input  wire       rgmii_rxc,
          output wire       mdio_phy_mdc,
          inout  wire       mdio_phy_mdio,
          output wire       phy_rst_n,
  `endif

    //reference clk layer
    input wire TS_IN_P, 
    input wire TS_IN_N
  );

   assign MGT_REF_SEL = 0;
  // assign SDA = 1'bz;
  // assign SCL = 1'bz;

  // ------- RESRT/CLOCK  ------- //
  wire RST;
  wire BUS_CLK_PLL, CLK250PLL, CLK125PLLTX, CLK125PLLTX90, CLK125PLLRX;
  wire PLL_FEEDBACK, LOCKED;

  wire CLK200_DLY, CLK200_DLY_PLL;
  IDELAYCTRL IDELAYCTRL_inst (
      .RDY   (         ), // 1-bit Ready output
      .REFCLK( CLK200_DLY  ), // 1-beit Reference clock input
      .RST   ( ~LOCKED )  // 1-bit Reset input
  );

  // Clock signals only needed for the 10G interface 
  `ifdef 10G
      wire TEN_GIG_REF_CLK_P, TEN_GIG_REF_CLK_N;
        assign TEN_GIG_REF_CLK_P = MGT_REFCLK1_P;
        assign TEN_GIG_REF_CLK_N = MGT_REFCLK1_N;
  `endif


  // -------  PLL for communication with FPGA  ------- //
  PLLE2_BASE #(
               .BANDWIDTH("OPTIMIZED"),  // OPTIMIZED, HIGH, LOW
               .CLKFBOUT_MULT(10),       // Multiply value for all CLKOUT, (2-64)
               .CLKFBOUT_PHASE(0.0),     // Phase offset in degrees of CLKFB, (-360.000-360.000).
               .CLKIN1_PERIOD(10.000),      // Input clock period in ns to ps resolution (i.e. 33.333 is 30 MHz).
               .DIVCLK_DIVIDE(1),        // Master division value, (1-56)
               .REF_JITTER1(0.0),        // Reference input jitter in UI, (0.000-0.999).
               .STARTUP_WAIT("FALSE"),     // Delay DONE until PLL Locks, ("TRUE"/"FALSE")

               .CLKOUT0_DIVIDE(7),     // Divide amount for CLKOUT0 (1-128)
               .CLKOUT0_DUTY_CYCLE(0.5), // Duty cycle for CLKOUT0 (0.001-0.999).
               .CLKOUT0_PHASE(0.0),      // Phase offset for CLKOUT0 (-360.000-360.000).

               .CLKOUT1_DIVIDE(4),     // Divide amount for CLKOUT0 (1-128)
               .CLKOUT1_DUTY_CYCLE(0.5), // Duty cycle for CLKOUT0 (0.001-0.999).
               .CLKOUT1_PHASE(0.0),      // Phase offset for CLKOUT0 (-360.000-360.000).

               .CLKOUT2_DIVIDE(8),     // Divide amount for CLKOUT0 (1-128)
               .CLKOUT2_DUTY_CYCLE(0.5), // Duty cycle for CLKOUT0 (0.001-0.999).
               .CLKOUT2_PHASE(0.0),      // Phase offset for CLKOUT0 (-360.000-360.000).

               .CLKOUT3_DIVIDE(8),     // Divide amount for CLKOUT0 (1-128)
               .CLKOUT3_DUTY_CYCLE(0.5), // Duty cycle for CLKOUT0 (0.001-0.999).
               .CLKOUT3_PHASE(90.0),      // Phase offset for CLKOUT0 (-360.000-360.000).

               .CLKOUT4_DIVIDE(8),     // Divide amount for CLKOUT0 (1-128)
               .CLKOUT4_DUTY_CYCLE(0.5), // Duty cycle for CLKOUT0 (0.001-0.999).
               .CLKOUT4_PHASE(0),      // Phase offset for CLKOUT0 (-360.000-360.000).
               //-65 -> 0?; - 45 -> 39;  -25 -> 100; -5 -> 0;

              .CLKOUT5_DIVIDE(5),       // Divide amount for CLKOUT0 (1-128)
              .CLKOUT5_DUTY_CYCLE(0.5), // Duty cycle for CLKOUT0 (0.001-0.999).
              .CLKOUT5_PHASE(0) 

             )
             PLLE2_BASE_inst_comm (
               .CLKOUT0(BUS_CLK_PLL),
               //  .CLKOUT0(),
               .CLKOUT1(CLK250PLL),
               .CLKOUT2(CLK125PLLTX),
               .CLKOUT3(CLK125PLLTX90),
               .CLKOUT4(CLK125PLLRX),
               .CLKOUT5(CLK200_DLY_PLL),

               .CLKFBOUT(PLL_FEEDBACK),

               .LOCKED(LOCKED),     // 1-bit output: LOCK

               // Input 100 MHz clock
               .CLKIN1(FCLK_IN),

               // Control Ports
               .PWRDWN(0),
               .RST(!RESET_N),

               // Feedback
               .CLKFBIN(PLL_FEEDBACK)
             );

  wire CLK125TX, CLK125TX90, CLK125RX;
  BUFG BUFG_inst_CLK200DLY(  .O(CLK200_DLY),  .I(CLK200_DLY_PLL) );

`ifdef 10G
    assign BUS_CLK = CLK156M;
`else
    BUFG BUFG_inst_BUS_CKL (.O(BUS_CLK), .I(BUS_CLK_PLL) );
    BUFG BUFG_inst_CLK125TX (  .O(CLK125TX),  .I(CLK125PLLTX) );
    BUFG BUFG_inst_CLK125TX90 (  .O(CLK125TX90),  .I(CLK125PLLTX90) );
    BUFG BUFG_inst_CLK125RX (  .O(CLK125RX),  .I(rgmii_rxc) );
`endif



  // -------  Power on detection  ------- //
  wire POWER_ON_BUF;
  IBUF #(
   .IBUF_LOW_PWR("TRUE"),  // Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
   .IOSTANDARD("DEFAULT")  // Specify the input I/O standard
  ) IBUF_inst (
    .O(POWER_ON_BUF),     // Buffer output
    .I(POWER_ON)      // Buffer input (connect directly to top-level port)
  );


  // -------  PLL for clk synthesis  ------- //
wire CLK_RX_OUT_BUF;
wire CLK_RX_BUF;
wire CLK_IN_BUF;
wire CLK400_BUF;
wire CLK_REF_IN_BUF;
wire BUS_CLK;
wire conf_clk;
wire conf_clk_x5;
wire CLK_REF_IN; 
wire spi_clk;
wire CLK_1GHZ_BUF;


  //This allows the clocks to be swiched off, when the power is off! FCLK: 100MHz & CLK125: 125MHz
  wire swichable_FCLK_IN;
  // assign swichable_FCLK_IN = (POWER_ON_BUF)? CLK125RX :0;
  assign swichable_FCLK_IN = FCLK_IN;
  // assign swichable_FCLK_IN = CLK125RX;


    // localparam PLL_MUL                = 10;
    localparam PLL_MUL                = 10;
    localparam PLL_DIV_MAIN           = 1;
    localparam PLL_DIV_CLK_IN         = 10; // used for internal pll
    localparam PLL_DIV_CLK_RX         = 10; //used as WCLK in receiver  -> data clk
    localparam PLL_DIV_CLK_RX_OUT     = 1; // -> data read clk (2x rx)
    localparam PLL_DIV_CLK_REF_IN     = 2; // used as external PLL
    localparam PLL_DIV_CLK400         = 2; // used for external timestamp
    // localparam PLL_DIV_CLK_RX_OUTX2   = 1; // used for external timestamp
    localparam PLL_DIV_CLK_1GHZ  = 1; // used for external timestamp


  wire LOCKED2, FEEDBACK2, FEEDBACK2_PLL;
  wire CLK400, CLK_IN, CLK_RX, CLK_RX_OUT, CLK_1GHZ;

  PLLE2_BASE #(//800Mhz at Pll because of 1.25ns periode &  have 100MHz(input) * 8 
               .BANDWIDTH("OPTIMIZED"),  // OPTIMIZED, HIGH, LOW
              //  .CLKFBOUT_MULT(48),       // Multiply value for all CLKOUT, (2-64)
               .CLKFBOUT_MULT(PLL_MUL),       // Multiply value for all CLKOUT, (2-64)
               .CLKFBOUT_PHASE(0.0),     // Phase offset in degrees of CLKFB, (-360.000-360.000).
               .CLKIN1_PERIOD(10),      // Input clock period in ns to ps resolution (i.e. 33.333 is 30 MHz).
               .DIVCLK_DIVIDE(PLL_DIV_MAIN),        // Master division value, (1-56)
              //  .DIVCLK_DIVIDE(5),        // Master division value, (1-56)
               .REF_JITTER1(0.0),        // Reference input jitter in UI, (0.000-0.999).
               .STARTUP_WAIT("FALSE"),     // Delay DONE until PLL Locks, ("TRUE"/"FALSE")
              //  CLK_IN
               .CLKOUT0_DIVIDE(PLL_DIV_CLK_IN),     // Divide amount for CLKOUT0 (1-128)
               .CLKOUT0_DUTY_CYCLE(0.5), // Duty cycle for CLKOUT0 (0.001-0.999).
               .CLKOUT0_PHASE(0.0),      // Phase offset for CLKOUT0 (-360.000-360.000).
               //CLK_RX
               .CLKOUT1_DIVIDE(PLL_DIV_CLK_RX),     // Divide amount for CLKOUT0 (1-128)
               .CLKOUT1_DUTY_CYCLE(0.5), // Duty cycle for CLKOUT0 (0.001-0.999).
               .CLKOUT1_PHASE(0.0),      // Phase offset for CLKOUT0 (-360.000-360.000).
               //CLK_Rx_Out
               .CLKOUT2_DIVIDE(PLL_DIV_CLK_RX_OUT),     // Divide amount for CLKOUT0 (1-128)
               .CLKOUT2_DUTY_CYCLE(0.5), // Duty cycle for CLKOUT0 (0.001-0.999).
               .CLKOUT2_PHASE(0.0),      // Phase offset for CLKOUT0 (-360.000-360.000).
               //CLK_400
               .CLKOUT3_DIVIDE(PLL_DIV_CLK400),     // Divide amount for CLKOUT0 (1-128)
               .CLKOUT3_DUTY_CYCLE(0.5), //FCLK Duty cycle for CLKOUT0 (0.001-0.999).
               .CLKOUT3_PHASE(0.0),      // Phase offset for CLKOUT0 (-360.000-360.000).
               // CLK_Ref_IN
               .CLKOUT4_DIVIDE(PLL_DIV_CLK_REF_IN),     // Divide amount for CLKOUT0 (1-128)
               .CLKOUT4_DUTY_CYCLE(0.5), // Duty cycle for CLKOUT0 (0.001-0.999).
               .CLKOUT4_PHASE(0.0),      // Phase offset for CLKOUT0 (-360.000-360.000).
               //CLK_200
               .CLKOUT5_DIVIDE(PLL_DIV_CLK_1GHZ),     // Divide amount for CLKOUT0 (1-128)
               .CLKOUT5_DUTY_CYCLE(0.5), // Duty cycle for CLKOUT0 (0.001-0.999).
               .CLKOUT5_PHASE(0.0)     // Phase offset for CLKOUT0 (-360.000-360.000).
  


             )
             PLLE2_BASE_inst_clk (

               .CLKOUT0(CLK_IN), // rx divided by 10
               .CLKOUT1(CLK_RX), // rx divided by 20
               .CLKOUT2(CLK_RX_OUT), //input clk -> 40Mhz input
               .CLKOUT3(CLK400), // output has to be 2 times rx clk
               .CLKOUT4(CLK_REF_IN), // output has to be 5 times ref clk
               .CLKOUT5(CLK_1GHZ),
               .CLKFBOUT(FEEDBACK2_PLL),

               .LOCKED(LOCKED2),     // 1-bit output: LOCK

               // Input 100 MHz clock
               .CLKIN1(swichable_FCLK_IN),

               // Control Ports
               .PWRDWN(0),
               .RST(!RESET_N),

               // Feedback
               .CLKFBIN(FEEDBACK2_PLL)
             );




  wire conf_clk_div;
  clock_divider #(.DIVISOR(80)) clock_divider (
                  .CLK(CLK_IN_BUF),
                  .RESET(),
                  .CE(), // for sequential logic driven by CLK
                  .CLOCK(conf_clk_div) // only for combinatorial logic, does not waste bufg
                );

  wire spi_clk_div;
clock_divider #(.DIVISOR(700)) spi_clock_divider (
                  .CLK(CLK_IN_BUF),
                  .RESET(),
                  .CE(), // for sequential logic driven by CLK
                  .CLOCK(spi_clk_div) // only for combinatorial logic, does not waste bufg
                );

  BUFG BUFG_inst_FEEDBACK2 ( .O(FEEDBACK2), .I(FEEDBACK2_PLL) );
  // BUFG BUFG_inst_BUS_CKL (.O(BUS_CLK), .I(BUS_CLK_PLL) );
  BUFG BUFG_inst_CLK_1HGZ_BUF(  .O(CLK_1GHZ_BUF),  .I(CLK_1GHZ) );
  BUFG BUFG_inst_CLK_400 (  .O(CLK400_BUF),  .I(CLK400) );
  BUFG BUFG_inst_CLK_IN_BUF (  .O(CLK_IN_BUF),  .I(CLK_IN) );
  BUFG BUFG_inst_CLK_RX_BUF (  .O(CLK_RX_BUF),  .I(CLK_RX) );
  BUFG BUFG_inst_CLK_RX_OUT_BUF (  .O(CLK_RX_OUT_BUF),  .I(CLK_RX_OUT) );
  BUFG BUFG_inst_CLK_REF_IN_BUF (  .O(CLK_REF_IN_BUF),  .I(CLK_REF_IN) );
  BUFG BUFG_inst_conf_clk (  .O(conf_clk),  .I(conf_clk_div) );
  BUFG BUFG_inst_spi_clk (  .O(spi_clk),  .I(spi_clk_div) );


`ifdef 10G
    //------------------------------------------------------------------------------
    //	10GbE SiTCP interface
    //------------------------------------------------------------------------------
    wire			SiTCPXG_OPEN_REQ;
    wire			SiTCPXG_ESTABLISHED;
    wire			SiTCPXG_CLOSE_REQ;
    wire			SiTCPXG_CLOSE_ACK;
    wire			SiTCPXG_TX_AFULL;
    wire	[63:0]	SiTCPXG_TX_D;
    wire	[3:0]	SiTCPXG_TX_B;
    wire	[15:0]	SiTCPXG_RX_SIZE;
    wire			SiTCPXG_RX_CLR_ENB;
    wire			SiTCPXG_RX_CLR_REQ;
    wire	[15:0]	SiTCPXG_RX_RADR;
    wire	[15:0]	SiTCPXG_RX_WADR;
    wire	[ 7:0]	SiTCPXG_RX_WENB;
    wire	[63:0]	SiTCPXG_RX_WDAT;

    wire	[63:0]	xgmii_rxd;
    wire	[7:0]	xgmii_rxc;
    wire	[7:0]	xgmii_txc;
    wire	[63:0]	xgmii_txd;

    wire            intSfpTxDisable;
    assign	SFP_TX_DISABLE	= ~intSfpTxDisable;

    wire			drp_req;
    wire	[15:0]	drp_daddr;
    wire			drp_den;
    wire	[15:0]	drp_di;
    wire	[15:0]	drp_drpdo;
    wire			drp_drdy;
    wire			drp_dwe;

    wire            CLK156M;
    wire            SiTCP_RST;
    wire            RBCP_ACK, RBCP_ACT, RBCP_WE, RBCP_RE;
    wire [7:0]      RBCP_WD, RBCP_RD;
    wire [31:0]     RBCP_ADDR;
    wire            RST_EEPROM;

    //    wire BUS_RST_CLK156M;
    //    cdc_pulse_sync start_pulse_sync (.clk_in(BUS_CLK), .pulse_in(BUS_RST), .clk_out(CLK156M), .pulse_out(BUS_RST_CLK156M));

    ten_gig_eth_pcs_pma ten_gig_eth_pcs_pma	(
        .refclk_p					(TEN_GIG_REF_CLK_P          ),  // input wire refclk_p
        .refclk_n					(TEN_GIG_REF_CLK_N          ),  // input wire refclk_n
        .reset						(0                 			),	// input wire reset
        .coreclk_out				(CLK156M					),	// output wire coreclk_out
        .txp						(SFP_TX_P					),	// output wire txp
        .txn						(SFP_TX_N					),	// output wire txn
        .rxp						(SFP_RX_P					),	// input wire rxp
        .rxn						(SFP_RX_N					),	// input wire rxn
        .xgmii_txd					(xgmii_txd[63:0]			),	// input wire [63 : 0] xgmii_txd
        .xgmii_txc					(xgmii_txc[7:0]				),	// input wire [7 : 0] xgmii_txc
        .xgmii_rxd					(xgmii_rxd[63:0]			),	// output wire [63 : 0] xgmii_rxd
        .xgmii_rxc					(xgmii_rxc[7:0]				),	// output wire [7 : 0] xgmii_rxc
    // MDIO
        .mdc						(1'b1						),	// input wire mdc
        .mdio_in					(1'b1						),	// input wire mdio_in
        .mdio_out					(							),	// output wire mdio_out
        .mdio_tri					(							),	// output wire mdio_tri
        .prtad						(5'd0						),	// input wire [4 : 0] prtad
    // SPF+ I/F
        .signal_detect				(1'b1						),	// input wire signal_detect
        .tx_fault					(1'b0						),	// input wire tx_fault
        .tx_disable					(intSfpTxDisable			),	// output wire tx_disable
    // drp_interface_ports
        .dclk						(CLK156M					),	// input wire dclk	<DRP clock 156MHz>
        .drp_req					(drp_req					),	// output wire drp_req
        .drp_gnt					(drp_req					),	// input wire drp_gnt
    // core_gt_drp_interface
        .drp_daddr_i				(drp_daddr[15:0]			),	// input wire [15 : 0] drp_daddr_i
        .drp_den_i					(drp_den					),	// input wire drp_den_i
        .drp_di_i					(drp_di[15:0]				),	// input wire [15 : 0] drp_di_i
        .drp_drpdo_o				(drp_drpdo[15:0]			),	// output wire [15 : 0] drp_drpdo_o
        .drp_drdy_o					(drp_drdy					),	// output wire drp_drdy_o
        .drp_dwe_i					(drp_dwe					),	// input wire drp_dwe_i
    // core_gt_drp_interface
        .drp_daddr_o				(drp_daddr[15:0]			),	// output wire [15 : 0] drp_daddr_o
        .drp_den_o					(drp_den					),	// output wire drp_den_o
        .drp_di_o					(drp_di[15:0]				),	// output wire [15 : 0] drp_di_o
        .drp_drpdo_i				(drp_drpdo[15:0]			),	// input wire [15 : 0] drp_drpdo_i
        .drp_drdy_i					(drp_drdy					),	// input wire drp_drdy_i
        .drp_dwe_o					(drp_dwe					),	// output wire drp_dwe_o
    //Miscellaneous port
        .core_status				(							),	// output wire [7 : 0] core_status
        .sim_speedup_control		(1'b0						),	// input wire sim_speedup_control
        .pma_pmd_type				(3'b111						),	// input wire [2 : 0] pma_pmd_type
        .resetdone_out				(							),	// output wire resetdone_out
        .rxrecclk_out				(							),	// output wire rxrecclk_out
        .txusrclk_out				(							),	// output wire txusrclk_out
        .txusrclk2_out				(							),	// output wire txusrclk2_out
        .areset_datapathclk_out		(							),	// output wire areset_datapathclk_out
        .gttxreset_out				(							),	// output wire gttxreset_out
        .gtrxreset_out				(							),	// output wire gtrxreset_out
        .txuserrdy_out				(							),	// output wire txuserrdy_out
        .reset_counter_done_out		(							),	// output wire reset_counter_done_out
        .qplllock_out				(							),	// output wire qplllock_out
        .qplloutclk_out				(							),	// output wire qplloutclk_out
        .qplloutrefclk_out			(							)	// output wire qplloutrefclk_out
    );
    assign SiTCPXG_RX_SIZE = 16'hffff;
    WRAP_SiTCPXG_XC7K_128K	#(
        .RxBufferSize				("LongLong"					)	// "Byte":8bit width ,"Word":16bit width ,"LongWord":32bit width , "LongLong":64bit width
    ) WRAP_SiTCPXG_XC7K_128K (
        .REG_FPGA_VER				(0                          ),  // in   : User logic Version(For example, the synthesized date)
        .REG_FPGA_ID				(0                          ),  // in   : User logic ID (We recommend using the lower 4 bytes of the MAC address.)
        //		==== System I/F ====
        .FORCE_DEFAULTn				(0                          ),  // in   : Force to set default values
        .XGMII_CLOCK				(CLK156M					),	// in	: XGMII Clock 156.25MHz
        .RSTs						(0                			),	// in	: System reset (Sync.)
        //		==== XGMII I/F ====
        .XGMII_RXC					(xgmii_rxc[7:0]				),	// in	: Rx control[7:0]
        .XGMII_RXD					(xgmii_rxd[63:0]			),	// in	: Rx data[63:0]
        .XGMII_TXC					(xgmii_txc[7:0]				),	// out  : Control bits[7:0]
        .XGMII_TXD					(xgmii_txd[63:0]			), 	// out  : Data[63:0]
        //		==== 93C46 I/F ====
        `ifdef BDAQ53
        .EEPROM_CS					(EEPROM_CS					),	// out	: Chip select
        .EEPROM_SK					(EEPROM_SK	    			),	// out	: Serial data clock
        .EEPROM_DI					(EEPROM_DI  				),	// out	: Serial write data
        .EEPROM_DO					(EEPROM_DO					),	// in	: Serial read data
        `endif
        //		==== User I/F ====
        .SiTCP_RESET_OUT			(SiTCP_RST  				),	// out	: System reset for user's module
        //			--- RBCP ---
        .RBCP_ACT					(RBCP_ACT					),	// out	: Indicates that bus access is active.
        .RBCP_ADDR					(RBCP_ADDR[31:0]			),	// out	: Address[31:0]
        .RBCP_WE					(RBCP_WE					),	// out	: Write enable
        .RBCP_WD					(RBCP_WD[7:0]				),	// out	: Data[7:0]
        .RBCP_RE					(RBCP_RE					),	// out	: Read enable
        .RBCP_ACK					(RBCP_ACK					),	// in	: Access acknowledge
        .RBCP_RD					(RBCP_RD[7:0]				),	// in	: Read data[7:0]
        //			--- TCP ---
        .USER_SESSION_OPEN_REQ		(SiTCPXG_OPEN_REQ			),	// in	: Request for opening the new session
        .USER_SESSION_ESTABLISHED	(SiTCPXG_ESTABLISHED		),	// out	: Establish of a session
        .USER_SESSION_CLOSE_REQ		(SiTCPXG_CLOSE_REQ			),	// out	: Request for closing session.
        .USER_SESSION_CLOSE_ACK		(SiTCPXG_CLOSE_ACK			),	// in	: Acknowledge for USER_SESSION_CLOSE_REQ.
        .USER_TX_D					(SiTCPXG_TX_D[63:0]			),	// in	: Write data
        .USER_TX_B					(SiTCPXG_TX_B[3:0]			),	// in	: Byte length of USER_TX_DATA(Set to 0 if not written)
        .USER_TX_AFULL				(SiTCPXG_TX_AFULL			),	// out	: Request to stop TX
        .USER_RX_SIZE				(SiTCPXG_RX_SIZE[15:0]		),	// in	: Receive buffer size(byte) caution:Set a value of 4000 or more and (memory size-16) or less
        .USER_RX_CLR_ENB			(SiTCPXG_RX_CLR_ENB			),	// out	: Receive buffer Clear Enable
        .USER_RX_CLR_REQ			(SiTCPXG_RX_CLR_REQ			),	// in	: Receive buffer Clear Request
        .USER_RX_RADR				(SiTCPXG_RX_RADR[15:0]		),	// in	: Receive buffer read address in bytes (unused upper bits are set to 0)
        .USER_RX_WADR				(SiTCPXG_RX_WADR[15:0]		),	// out	: Receive buffer write address in bytes (lower 3 bits are not connected to memory)
        .USER_RX_WENB				(SiTCPXG_RX_WENB[ 7:0]		),	// out	: Receive buffer byte write enable (big endian)
        .USER_RX_WDAT				(SiTCPXG_RX_WDAT[63:0]		)	// out	: Receive buffer write data (big endian)
    );
  `else  // 1G SiTCP interface
    assign RST = !RESET_N | !LOCKED;
    // -------  MDIO interface  ------- //
        wire   mdio_gem_mdc;
        wire   mdio_gem_i;
        wire   mdio_gem_o;
        wire   mdio_gem_t;
        wire   link_status;
        wire  [1:0] clock_speed;
        wire   duplex_status;

        // -------  RGMII interface  ------- //
        wire   gmii_tx_clk;
        wire   gmii_tx_en;
        wire  [7:0] gmii_txd;
        wire   gmii_tx_er;
        wire   gmii_crs;
        wire   gmii_col;
        wire   gmii_rx_clk;
        wire   gmii_rx_dv;
        wire  [7:0] gmii_rxd;
        wire   gmii_rx_er;

        rgmii_io rgmii
        (
            .rgmii_txd(rgmii_txd),
            .rgmii_tx_ctl(rgmii_tx_ctl),
            .rgmii_txc(rgmii_txc),

            .rgmii_rxd(rgmii_rxd),
            .rgmii_rx_ctl(rgmii_rx_ctl),

            .gmii_txd_int(gmii_txd),        // Internal gmii_txd signal.
            .gmii_tx_en_int(gmii_tx_en),
            .gmii_tx_er_int(gmii_tx_er),
            .gmii_col_int(gmii_col),
            .gmii_crs_int(gmii_crs),
            .gmii_rxd_reg(gmii_rxd),        // RGMII double data rate data valid.
            .gmii_rx_dv_reg(gmii_rx_dv),    // gmii_rx_dv_ibuf registered in IOBs.
            .gmii_rx_er_reg(gmii_rx_er),    // gmii_rx_er_ibuf registered in IOBs.

            .eth_link_status(link_status),
            .eth_clock_speed(clock_speed),
            .eth_duplex_status(duplex_status),

            // FOllowing are generated by DCMs
            .tx_rgmii_clk_int(CLK125TX),    // Internal RGMII transmitter clock.
            .tx_rgmii_clk90_int(CLK125TX90),// Internal RGMII transmitter clock w/ 90 deg phase
            .rx_rgmii_clk_int(CLK125RX),    // Internal RGMII receiver clock

            .reset(!phy_rst_n)
        );

    // Instantiate tri-state buffer for MDIO
        IOBUF i_iobuf_mdio(
            .O(mdio_gem_i),
            .IO(mdio_phy_mdio),
            .I(mdio_gem_o),
            .T(mdio_gem_t));


    // -------  SiTCP module  ------- //
        wire TCP_OPEN_ACK, TCP_CLOSE_REQ;
        wire TCP_RX_WR, TCP_TX_WR;
        wire TCP_TX_FULL, TCP_ERROR;
        wire [7:0] TCP_RX_DATA, TCP_TX_DATA;
        wire [15:0] TCP_RX_WC;
        wire RBCP_ACK, RBCP_ACT, RBCP_WE, RBCP_RE;
        wire [7:0] RBCP_WD, RBCP_RD;
        wire [31:0] RBCP_ADDR;
        wire SiTCP_RST;
        wire EEPROM_CS_int, EEPROM_SK_int, EEPROM_DI_int, EEPROM_DO_int;

        localparam CONST_phy_addr = 5'd3;

    // // connect the physical EEPROM pins only for the BDAQ53
    // `ifdef BDAQ53
    //     `ifndef _KX1
    //     assign EEPROM_CS = EEPROM_CS_int;
    //     assign EEPROM_SK = EEPROM_SK_int;
    //     assign EEPROM_DI = EEPROM_DI_int;
    //     assign EEPROM_DO_int = EEPROM_DO;
    //     `endif
    // `endif

    // use PMOD[3:0] to set the IP address
        wire [3:0] IP_ADDR_SEL;
        wire FORCE_DEFAULTn;
        assign IP_ADDR_SEL = 4'h0;
        assign FORCE_DEFAULTn = 1'b0;
    // `endif

    WRAP_SiTCP_GMII_XC7K_32K #(
        .TIM_PERIOD (8'd142)
    ) sitcp (
        .CLK(BUS_CLK)               ,    // in    : System Clock >129MHz
        .RST(RST)                   ,    // in    : System reset
        // Configuration parameters
        .FORCE_DEFAULTn(FORCE_DEFAULTn), // in    : Load default parameters
        .EXT_IP_ADDR({8'd192, 8'd168, |{IP_ADDR_SEL} ? 8'd10 + IP_ADDR_SEL : 8'd10, 8'd19})  ,    //IP address[31:0] default: 192.168.10.19.
        .EXT_TCP_PORT(16'd24)       ,    // in    : TCP port #[15:0]
        .EXT_RBCP_PORT(16'd4660)    ,    // in    : RBCP port #[15:0]
        .PHY_ADDR(CONST_phy_addr)   ,    // in    : PHY-device MIF address[4:0]
        // EEPROM
        .EEPROM_CS(EEPROM_CS_int)   ,    // out    : Chip select
        .EEPROM_SK(EEPROM_SK_int)   ,    // out    : Serial data clock
        .EEPROM_DI(EEPROM_DI_int)   ,    // out    : Serial write data
        .EEPROM_DO(EEPROM_DO_int)   ,    // in     : Serial read data
        // user data, initial values are stored in the EEPROM, 0xFFFF_FC3C-3F
        .USR_REG_X3C()              ,    // out    : Stored at 0xFFFF_FF3C
        .USR_REG_X3D()              ,    // out    : Stored at 0xFFFF_FF3D
        .USR_REG_X3E()              ,    // out    : Stored at 0xFFFF_FF3E
        .USR_REG_X3F()              ,    // out    : Stored at 0xFFFF_FF3F
        // MII interface
        .GMII_RSTn(phy_rst_n)       ,    // out    : PHY reset
        .GMII_1000M(1'b1)           ,    // in    : GMII mode (0:MII, 1:GMII)
        // TX
        .GMII_TX_CLK(CLK125TX)      ,    // in    : Tx clock
        .GMII_TX_EN(gmii_tx_en)     ,    // out    : Tx enable
        .GMII_TXD(gmii_txd)         ,    // out    : Tx data[7:0]
        .GMII_TX_ER(gmii_tx_er)     ,    // out    : TX error
        // RX
        .GMII_RX_CLK(CLK125RX)      ,    // in    : Rx clock
        .GMII_RX_DV(gmii_rx_dv)     ,    // in    : Rx data valid
        .GMII_RXD(gmii_rxd)         ,    // in    : Rx data[7:0]
        .GMII_RX_ER(gmii_rx_er)     ,    // in    : Rx error
        .GMII_CRS(gmii_crs)         ,    // in    : Carrier sense
        .GMII_COL(gmii_col)         ,    // in    : Collision detected
        // Management IF
        .GMII_MDC(mdio_phy_mdc)     ,    // out    : Clock for MDIO
        .GMII_MDIO_IN(mdio_gem_i)   ,    // in    : Data
        .GMII_MDIO_OUT(mdio_gem_o)  ,    // out    : Data
        .GMII_MDIO_OE(mdio_gem_t)   ,    // out    : MDIO output enable
        // User I/F
        .SiTCP_RST(SiTCP_RST)       ,    // out    : Reset for SiTCP and related circuits
        // TCP connection control
        .TCP_OPEN_REQ(1'b0)         ,    // in    : Reserved input, shoud be 0
        .TCP_OPEN_ACK(TCP_OPEN_ACK) ,    // out    : Acknowledge for open (=Socket busy)
        .TCP_ERROR(TCP_ERROR)       ,    // out    : TCP error, its active period is equal to MSL
        .TCP_CLOSE_REQ(TCP_CLOSE_REQ)   ,    // out    : Connection close request
        .TCP_CLOSE_ACK(TCP_CLOSE_REQ)   ,    // in    : Acknowledge for closing
        // FIFO I/F
        .TCP_RX_WC(TCP_RX_WC)       ,    // in    : Rx FIFO write count[15:0] (Unused bits should be set 1)
        .TCP_RX_WR(TCP_RX_WR)       ,    // out   : Write enable
        .TCP_RX_DATA(TCP_RX_DATA)   ,    // out   : Write data[7:0]
        .TCP_TX_FULL(TCP_TX_FULL)   ,    // out   : Almost full flag
        .TCP_TX_WR(TCP_TX_WR)       ,    // in    : Write enable
        .TCP_TX_DATA(TCP_TX_DATA)   ,    // in    : Write data[7:0]
        // RBCP
        .RBCP_ACT(RBCP_ACT)         ,    // out   : RBCP active
        .RBCP_ADDR(RBCP_ADDR)       ,    // out   : Address[31:0]
        .RBCP_WD(RBCP_WD)           ,    // out   : Data[7:0]
        .RBCP_WE(RBCP_WE)           ,    // out   : Write enable
        .RBCP_RE(RBCP_RE)           ,    // out   : Read enable
        .RBCP_ACK(RBCP_ACK)         ,    // in    : Access acknowledge
        .RBCP_RD(RBCP_RD)                // in    : Read data[7:0]
    );
`endif
                 


  // -------  BUS SYGNALING  ------- //
  wire BUS_WR, BUS_RD, BUS_RST;
  wire [31:0] BUS_ADD;
  wire [7:0] BUS_DATA;
  assign BUS_RST = SiTCP_RST;

  rbcp_to_bus irbcp_to_bus(
                .BUS_RST(BUS_RST),
                .BUS_CLK(BUS_CLK),

                .RBCP_ACT(RBCP_ACT),
                .RBCP_ADDR(RBCP_ADDR),
                .RBCP_WD(RBCP_WD),
                .RBCP_WE(RBCP_WE),
                .RBCP_RE(RBCP_RE),
                .RBCP_ACK(RBCP_ACK),
                .RBCP_RD(RBCP_RD),

                .BUS_WR(BUS_WR),
                .BUS_RD(BUS_RD),
                .BUS_ADD(BUS_ADD),
                .BUS_DATA(BUS_DATA)
              );


  // -------  MODULES for fast data readout(FIFO) - cdc_fifo is for timing reasons
  wire ARB_READY_OUT,ARB_WRITE_OUT;
  wire [31:0]ARB_DATA_OUT;
  wire FIFO_FULL,FIFO_NEAR_FULL;
  wire [31:0] cdc_data_out;
  wire tcp_fifo_full;
  wire cdc_fifo_empty;

  cdc_syncfifo #(.DSIZE(32), .ASIZE(3)) cdc_syncfifo_i
  (
      .rdata(cdc_data_out),
      .wfull(FIFO_FULL),
      .rempty(cdc_fifo_empty),
      .wdata(ARB_DATA_OUT),
      .winc(ARB_WRITE_OUT), .wclk(BUS_CLK), .wrst(BUS_RST),
      .rinc(!tcp_fifo_full), .rclk(BUS_CLK), .rrst(BUS_RST)
  );
  assign ARB_READY_OUT = !FIFO_FULL;

`ifdef 10G
    wire [31:0] TCP10G_TX_DATA_LOW;
    reg [63:0] TCP10G_TX_DATA;
    wire TCP_TX_WR;
    reg TCP10G_WORD_CNT = 0;
    wire TCP_TX_WR_ready;
    reg SiTCPXG_TX_VALID = 0;

    gerneric_fifo #(
        .DATA_SIZE(32),
        .DEPTH(128*1024)
    )  fifo_10G (
        .clk(CLK156M),
        .reset(BUS_RST),
        .write(!cdc_fifo_empty),
        .read(TCP_TX_WR),
        .data_in(cdc_data_out),
        .full(tcp_fifo_full),
        .empty(FIFO_EMPTY),
        .data_out(TCP10G_TX_DATA_LOW),
        .size()
        );

    assign TCP_TX_WR = SiTCPXG_ESTABLISHED & !SiTCPXG_TX_AFULL & !FIFO_EMPTY;

    `define sitcp_32bit

    `ifndef sitcp_32bit
        always @ (posedge CLK156M) begin
            if (TCP_TX_WR) begin
                if (TCP10G_WORD_CNT == 0)
                    TCP10G_TX_DATA[63:32] <= TCP10G_TX_DATA_LOW;
                else begin
                    TCP10G_TX_DATA[31:0] <= TCP10G_TX_DATA_LOW;
                    SiTCPXG_TX_VALID <= 1;
                end
                TCP10G_WORD_CNT <= ~TCP10G_WORD_CNT;
            end
            else
                SiTCPXG_TX_VALID <= 0;
        end

        // 64 bit bus
        assign TCP_TX_WR_ready = SiTCPXG_ESTABLISHED & !SiTCPXG_TX_AFULL & !FIFO_EMPTY;
//        assign SiTCPXG_TX_B = (TCP_TX_WR & TCP10G_WORD_CNT) ? 4'd8 : 4'd0;
        assign SiTCPXG_TX_B = SiTCPXG_TX_VALID ? 4'd8 : 4'd0;
        assign SiTCPXG_TX_D = {TCP10G_TX_DATA[7:0], TCP10G_TX_DATA[15:8], TCP10G_TX_DATA[23:16], TCP10G_TX_DATA[31:24],
                            TCP10G_TX_DATA[39:32], TCP10G_TX_DATA[47:40], TCP10G_TX_DATA[55:48], TCP10G_TX_DATA[63:56]};  // byte swapping
    `else
    /*
        always @ (posedge CLK156M) begin
            if (TCP_TX_WR) begin
                TCP10G_TX_DATA <= TCP10G_TX_DATA_LOW;
                SiTCPXG_TX_VALID <= 1;
                end
            else
                SiTCPXG_TX_VALID <= 0;
        end
    */

        // 32 bit bus
//        assign SiTCPXG_TX_B = SiTCPXG_TX_VALID ? 4'd4 : 4'd0;
//        assign SiTCPXG_TX_D = {TCP10G_TX_DATA[7:0], TCP10G_TX_DATA[15:8], TCP10G_TX_DATA[23:16], TCP10G_TX_DATA[31:24], 32'h00000000};
        assign SiTCPXG_TX_B = TCP_TX_WR ? 4'd4 : 4'd0;
        assign SiTCPXG_TX_D = {TCP10G_TX_DATA_LOW[7:0], TCP10G_TX_DATA_LOW[15:8], TCP10G_TX_DATA_LOW[23:16], TCP10G_TX_DATA_LOW[31:24], 32'h00000000};
    `endif 

    // TCP connection
    assign SiTCPXG_CLOSE_ACK = SiTCPXG_CLOSE_REQ;  // close  the TCP connection uppon request
//    assign SiTCPXG_OPEN_REQ = ~SiTCPXG_ESTABLISHED & ~SiTCPXG_CLOSE_REQ;   // open and hold a connection as long as it is not actively closed
//    assign SiTCPXG_OPEN_REQ = (~SiTCPXG_ESTABLISHED | SiTCPXG_OPEN_REQ) & ~SiTCPXG_CLOSE_REQ;   // open and hold a connection as long as it is not actively closed

`else
    fifo_32_to_8 #(
        .DEPTH(128*1024)
    ) i_data_fifo (
        .RST(BUS_RST),
        .CLK(BUS_CLK),

        .WRITE(!cdc_fifo_empty),
        .READ(TCP_TX_WR),
        .DATA_IN(cdc_data_out),
        .FULL(tcp_fifo_full),
        .EMPTY(FIFO_EMPTY),
        .DATA_OUT(TCP_TX_DATA)
        );
    assign TCP_TX_WR = !TCP_TX_FULL & !FIFO_EMPTY;
`endif


// -------  USER CORE ------- 
  wire FIFO_EMPTY;



  // 100 MHz from FPGA to DUT as external clk(PLL input). Numbers from GECCO
  OBUFTDS OBUFDS_CLK_REF(
    .O(Clk_ref_P),
    .OB(Clk_ref_N),
    .I(CLK_IN_BUF)
  );
  
  wire Sync_res;
  OBUFTDS OBUFDS_CLK_REF2(
    .O(Sync_res_P),
    .OB(Sync_res_N),
    .I(Sync_res)
  );

  // wire Clk_ext_LVDS;
  OBUFTDS OBUFDS_Clk_ext_LVDS(
    .O(Clk_ext_LVDS_P),
    .OB(Clk_ext_LVDS_N),
    .I(CLK_REF_IN_BUF)
  );

  wire TS_IN;
  IBUFDS IBUFDS_TS_IN_inst
      (
        .I  (TS_IN_P),
        .IB (TS_IN_N),
        .O  (TS_IN)
      );


  // `ifdef SLOW
  wire DATA_IN;
  IBUFDS IBUFDS_DATA_IN_inst
     (
        .I  (DATA_LVDS_P),
        .IB (DATA_LVDS_N),
        .O  (DATA_IN)
     );


// mightypix core module
  wire RX_READY,FIFO_EMPTY_IN;
  mightypix_core mightypix_core
                 (
                   //local bus
                   .BUS_CLK(BUS_CLK),
                   .BUS_DATA(BUS_DATA),
                   .BUS_ADD(BUS_ADD),
                   .BUS_RD(BUS_RD),
                   .BUS_WR(BUS_WR),
                   .BUS_RST(BUS_RST),

                   //fifo
                  .ARB_READY_OUT(ARB_READY_OUT),
                  .ARB_WRITE_OUT(ARB_WRITE_OUT),
                  .ARB_DATA_OUT(ARB_DATA_OUT),
                  .FIFO_FULL(FIFO_FULL),
                  .FIFO_NEAR_FULL(FIFO_NEAR_FULL),

                  //TLU
                  .TLU_CLK(TLU_CLK),
                  .TLU_BUSY(TLU_BUSY),
                  .TLU_RESET(TLU_RESET),
                  .TLU_TRIGGER(TLU_TRIGGER),

                   //clocks
                   .conf_clk(conf_clk),
                   .spi_clk(spi_clk),
                   .CLK_RX_x0p1(conf_clk),
                   .CLK_IN(CLK_IN_BUF),
                   .CLK_RX(CLK_RX_BUF),
                   .CLK_RX_OUT(CLK_RX_OUT_BUF),
                   .CLK400(CLK400_BUF),
                   .CLK_1GHZ(CLK_1GHZ),

                    //RX INFO
                    // .soft_reset_rx(soft_reset_rx),
                    // .rxdisperr(rxdisperr_i),
                    // .rxnotintable(rxnotintable_i),
                    // .rxusrclk(rxusrclk_i),
                    // .rxusrclk2(rxusrclk2_i),
                    // .rxdata(recieved_data),
                    // .rxcharisk(rxcharisk_i),
                    // .resetdone(resetdone),
                    // .track_data_out(track_data_out),
                    // .cplllocked(cplllocked),

                   //data_out here!!!
                   .SDA(SDA),
                   .SCL(SCL),
                   .SCK(SCK),    //DIN6
                   .CSB(CSB),    //DIN5
                   .MOSI(MOSI),   //DIN4
                   .MISO(MISO),    //DOUT1
                   .RST_CHIP(RST_CHIP),   //DIN7
                   .Ctl_Clk_1(Ctl_Clk_1),    //DIN3
                   .Ctl_Clk_2(Ctl_Clk_2),    //DIN2
                   .Ctl_Load(Ctl_Load),     //DIN1
                   .Ctl_SIN(Ctl_SIN),     //DIN0
                   .Ctl_SOut(Ctl_SOut),      //DOUT0

                   .INJECTION(INJECTION),
                   .Sync_res(Sync_res),  
                   .RX_READY(RX_READY),
                   .DATA(DATA_IN),
                   .CLK_ENABLE(CLK_ENABLE),
                  //  .CLK_EXT(CLK_REF_IN_BUF),


                  // external TS
                    .TS_IN(TS_IN)
                 );

  // LED assignments
  assign LED[0] = 1;
  assign LED[1] = 0;
  assign LED[2] = 0;
  assign LED[3] = RX_READY;
  assign LED[4] = 0;

endmodule
