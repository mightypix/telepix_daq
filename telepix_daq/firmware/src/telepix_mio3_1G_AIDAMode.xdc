set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets TLU_CLK]
set_property PACKAGE_PIN P21 [get_ports rgmii_rx_ctl]
set_property PACKAGE_PIN P16 [get_ports {rgmii_rxd[0]}]
set_property PACKAGE_PIN N17 [get_ports {rgmii_rxd[1]}]
set_property PACKAGE_PIN R16 [get_ports {rgmii_rxd[2]}]
set_property PACKAGE_PIN R17 [get_ports {rgmii_rxd[3]}]
# constrains based on MIO ucf file
# ------ Constraints
# FCLK (100 MHz)
set_property PACKAGE_PIN AA3 [get_ports FCLK_IN]
set_property IOSTANDARD LVCMOS15 [get_ports FCLK_IN]

set_property PACKAGE_PIN AB21 [get_ports TLU_CLK_P]
set_property PACKAGE_PIN AC21 [get_ports TLU_CLK_N]
set_property IOSTANDARD LVDS_25 [get_ports TLU_CLK_P]
set_property IOSTANDARD LVDS_25 [get_ports TLU_CLK_N]
set_property DIFF_TERM TRUE [get_ports TLU_CLK_P]
set_property DIFF_TERM TRUE [get_ports TLU_CLK_N]

create_clock -period 10.000 -name FCLK_IN -add [get_ports FCLK_IN]
create_clock -period 8.000 -name rgmii_rxc -add [get_ports rgmii_rxc]
create_clock -period 25.000 -name TLU_CLK -add [get_ports TLU_CLK_P]

# Derived Clocks
# create_generated_clock -name clock_divider/conf_clk_div -source [get_pins PLLE2_BASE_inst_clk/CLKOUT0] -divide_by 80 [get_pins clock_divider/CLOCK_reg/Q]
# create_generated_clock -name clock_divider_x5/conf_clk_x5_div -source [get_pins PLLE2_BASE_inst_clk/CLKOUT0] -divide_by 16 [get_pins clock_divider_x5/CLOCK_reg/Q]
create_generated_clock -name mightypix_core_AIDAMode/i_clock_divisor_spi/I2C_CLK -source [get_pins PLLE2_BASE_inst_comm/CLKOUT0] -divide_by 1500 [get_pins mightypix_core_AIDAMode/i_clock_divisor_spi/CLOCK_reg/Q]
create_generated_clock -name rgmii_txc -source [get_pins rgmii/ODDR_inst/C] -divide_by 1 [get_ports rgmii_txc]
# create_clock -period 8.000 -name CLK_MGT_REF -add [get_ports MGT_REFCLK1_P]
# create_clock -period 6.250 -name CLK_MGT_REF -add [get_ports MGT_REFCLK1_P]

# Exclude asynchronous clock domains from timing (handled by CDCs)
set_clock_groups -asynchronous -group BUS_CLK_PLL -group mightypix_core_AIDAMode/i_clock_divisor_spi/I2C_CLK -group {CLK_IN CLK_RX CLK400 CLK_RX_OUT} -group [get_clocks -include_generated_clocks rgmii_rxc]


set_false_path -from [get_clocks rgmii_rxc] -to [get_clocks BUS_CLK_PLL]
set_false_path -from [get_clocks rgmii_rxc] -to [get_clocks -of_objects [get_pins PLLE2_BASE_inst_comm/CLKOUT2]]


set_false_path -from [get_clocks -of_objects [get_pins PLLE2_BASE_inst_clk/CLKOUT1]] -to [get_clocks -of_objects [get_pins PLLE2_BASE_inst_clk/CLKOUT2]]
set_false_path -from [get_clocks -of_objects [get_pins PLLE2_BASE_inst_clk/CLKOUT2]] -to [get_clocks -of_objects [get_pins PLLE2_BASE_inst_clk/CLKOUT1]]

# false path between BUS_CLK_PLL and CLK125PLLTX
set_false_path -from [get_clocks -of_objects [get_pins PLLE2_BASE_inst_comm/CLKOUT0]] -to [get_clocks -of_objects [get_pins PLLE2_BASE_inst_comm/CLKOUT2]]
set_false_path -from [get_clocks -of_objects [get_pins PLLE2_BASE_inst_comm/CLKOUT2]] -to [get_clocks -of_objects [get_pins PLLE2_BASE_inst_comm/CLKOUT0]]

#######SiTCP#######
#this separates the TCPIP logic from
set_input_delay -clock [get_clocks rgmii_rxc] -max -add_delay 10.000 [get_ports {rgmii_rxd[*]}]
set_input_delay -clock [get_clocks rgmii_rxc] -max -add_delay 10.000 [get_ports rgmii_rx_ctl]
# set_output_delay -clock [get_clocks rgmii_rxc] -max 10 [get_pins {rgmii_rxd[*]}]
# ------ LED
set_property PACKAGE_PIN M17 [get_ports {LED[0]}]
set_property PACKAGE_PIN L18 [get_ports {LED[1]}]
set_property PACKAGE_PIN L17 [get_ports {LED[2]}]
set_property PACKAGE_PIN K18 [get_ports {LED[3]}]
set_property PACKAGE_PIN P26 [get_ports {LED[4]}]
set_property PACKAGE_PIN M25 [get_ports {LED[5]}]
set_property PACKAGE_PIN L25 [get_ports {LED[6]}]
set_property PACKAGE_PIN P23 [get_ports {LED[7]}]
set_property IOSTANDARD LVCMOS25 [get_ports LED*]
set_property SLEW SLOW [get_ports LED*]

set_property IOSTANDARD LVCMOS25 [get_ports {LEMO_RX[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {LEMO_RX[0]}]


#SW_0 Button
set_property PACKAGE_PIN AF22 [get_ports sw_button]
set_property IOSTANDARD LVCMOS25 [get_ports sw_button]

#CLK Mux
set_property PACKAGE_PIN K25 [get_ports MGT_REF_SEL]
set_property IOSTANDARD LVCMOS25 [get_ports MGT_REF_SEL]
set_property PULLTYPE PULLUP [get_ports MGT_REF_SEL]

# timing layer
set_property PACKAGE_PIN AB26 [get_ports TS_IN_P]
set_property PACKAGE_PIN AC26 [get_ports TS_IN_N]
set_property IOSTANDARD LVDS_25 [get_ports TS_IN_N]
set_property IOSTANDARD LVDS_25 [get_ports TS_IN_P]


# 200 MHz differential signal input
set_property PACKAGE_PIN G19 [get_ports LVDS_IN_CLK1_P]
set_property PACKAGE_PIN F20 [get_ports LVDS_IN_CLK1_N]
set_property DIFF_TERM TRUE [get_ports LVDS_IN_CLK1_P]
set_property DIFF_TERM TRUE [get_ports LVDS_IN_CLK1_N]
set_property IOSTANDARD LVDS_25 [get_ports LVDS_IN_CLK1_P]
set_property IOSTANDARD LVDS_25 [get_ports LVDS_IN_CLK1_N]
# set_property PACKAGE_PIN F6 [get_ports MGT_REFCLK1_P]
# set_property PACKAGE_PIN F5 [get_ports MGT_REFCLK1_N]

# ------ Button & Spare & more - omitted for now
set_property PACKAGE_PIN C18 [get_ports RESET_N]
set_property IOSTANDARD LVCMOS25 [get_ports RESET_N]
set_property PULLTYPE PULLUP [get_ports RESET_N]

# ------ I2C control signals
set_property PACKAGE_PIN P24 [get_ports SDA]
set_property IOSTANDARD LVCMOS25 [get_ports SDA]
set_property PACKAGE_PIN N24 [get_ports SCL]
set_property IOSTANDARD LVCMOS25 [get_ports SCL]

# ------ Trigger IOs - partial (MIO3 has fewer lemo than MIO)
# # TX[0]
# set_property PACKAGE_PIN AB21 [get_ports TLU_CLK]
# set_property IOSTANDARD LVCMOS25 [get_ports TLU_CLK]
# TX[1]


set_property PACKAGE_PIN Y22 [get_ports TLU_BUSY_P]
set_property PACKAGE_PIN AA22 [get_ports TLU_BUSY_N]
set_property IOSTANDARD LVDS_25 [get_ports TLU_BUSY_P]
set_property IOSTANDARD LVDS_25 [get_ports TLU_BUSY_N]

set_property PACKAGE_PIN V21 [get_ports TLU_TRIGGER_P]
set_property IOSTANDARD LVDS_25 [get_ports TLU_TRIGGER_P]


set_property PACKAGE_PIN Y25 [get_ports TLU_RESET_P]
set_property IOSTANDARD LVDS_25 [get_ports TLU_RESET_P]

# ------ Async SRAM - omitted for now
# SRAM faked with SiTCP

# ------ RGMII
set_property SLEW FAST [get_ports mdio_phy_mdc]
set_property IOSTANDARD LVCMOS25 [get_ports mdio_phy_mdc]
set_property PACKAGE_PIN N16 [get_ports mdio_phy_mdc]

set_property SLEW FAST [get_ports mdio_phy_mdio]
set_property IOSTANDARD LVCMOS25 [get_ports mdio_phy_mdio]
set_property PACKAGE_PIN U16 [get_ports mdio_phy_mdio]

set_property SLEW FAST [get_ports phy_rst_n]
set_property IOSTANDARD LVCMOS25 [get_ports phy_rst_n]
set_property PACKAGE_PIN M20 [get_ports phy_rst_n]

set_property IOSTANDARD LVCMOS25 [get_ports rgmii_rxc]
set_property PACKAGE_PIN R21 [get_ports rgmii_rxc]

set_property IOSTANDARD LVCMOS25 [get_ports rgmii_rx_ctl]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_rxd[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_rxd[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_rxd[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_rxd[3]}]

set_property SLEW FAST [get_ports rgmii_txc]
set_property IOSTANDARD LVCMOS25 [get_ports rgmii_txc]
set_property PACKAGE_PIN R18 [get_ports rgmii_txc]

set_property SLEW FAST [get_ports rgmii_tx_ctl]
set_property IOSTANDARD LVCMOS25 [get_ports rgmii_tx_ctl]
set_property PACKAGE_PIN P18 [get_ports rgmii_tx_ctl]

set_property SLEW FAST [get_ports {rgmii_txd[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_txd[0]}]
set_property PACKAGE_PIN N18 [get_ports {rgmii_txd[0]}]
set_property SLEW FAST [get_ports {rgmii_txd[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_txd[1]}]
set_property PACKAGE_PIN M19 [get_ports {rgmii_txd[1]}]
set_property SLEW FAST [get_ports {rgmii_txd[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_txd[2]}]
set_property PACKAGE_PIN U17 [get_ports {rgmii_txd[2]}]
set_property SLEW FAST [get_ports {rgmii_txd[3]}]
set_property IOSTANDARD LVCMOS25 [get_ports {rgmii_txd[3]}]
set_property PACKAGE_PIN T17 [get_ports {rgmii_txd[3]}]


#POWER ON Detection
set_property PACKAGE_PIN AE25 [get_ports POWER_ON]
set_property IOSTANDARD LVCMOS25 [get_ports POWER_ON]
set_property PULLTYPE PULLDOWN [get_ports POWER_ON]

# DIN0
set_property PACKAGE_PIN A19 [get_ports Ctl_SIN]
set_property IOSTANDARD LVCMOS25 [get_ports Ctl_SIN]
set_property PULLTYPE PULLDOWN [get_ports Ctl_SIN]

# Load is not used -> unsed PIN
set_property PACKAGE_PIN AA25 [get_ports Ctl_Load]
set_property IOSTANDARD LVCMOS25 [get_ports Ctl_Load]
set_property PULLTYPE PULLDOWN [get_ports Ctl_Load]

# DIN2
set_property PACKAGE_PIN C16 [get_ports Ctl_Clk_2]
set_property IOSTANDARD LVCMOS25 [get_ports Ctl_Clk_2]
set_property PULLTYPE PULLDOWN [get_ports Ctl_Clk_2]

# DIN1
set_property PACKAGE_PIN A18 [get_ports Ctl_Clk_1]
set_property IOSTANDARD LVCMOS25 [get_ports Ctl_Clk_1]
set_property PULLTYPE PULLDOWN [get_ports Ctl_Clk_1]

# Dummy
set_property PACKAGE_PIN D13 [get_ports MOSI]
set_property IOSTANDARD LVCMOS25 [get_ports MOSI]
set_property PULLTYPE PULLDOWN [get_ports MOSI]

# Dummy
set_property PACKAGE_PIN U25 [get_ports CSB]
set_property IOSTANDARD LVCMOS25 [get_ports CSB]
set_property PULLTYPE PULLDOWN [get_ports CSB]

# Dummy
set_property PACKAGE_PIN W23 [get_ports SCK]
set_property IOSTANDARD LVCMOS25 [get_ports SCK]
set_property PULLTYPE PULLDOWN [get_ports SCK]

# DIN4 rest to chip
set_property PACKAGE_PIN C13 [get_ports RST_CHIP]
set_property IOSTANDARD LVCMOS25 [get_ports RST_CHIP]
set_property PULLTYPE PULLDOWN [get_ports RST_CHIP]


# DOUT[19] LVDSOUT 0
set_property PACKAGE_PIN K20 [get_ports Clk_ext_LVDS_P]
set_property PACKAGE_PIN J20 [get_ports Clk_ext_LVDS_N]
set_property IOSTANDARD LVDS_25 [get_ports Clk_ext_LVDS_P]
set_property IOSTANDARD LVDS_25 [get_ports Clk_ext_LVDS_N]

# DOUT[18] LVDSOUT 1
set_property PACKAGE_PIN J18 [get_ports Clk_ref_P]
set_property PACKAGE_PIN J19 [get_ports Clk_ref_N]
set_property IOSTANDARD LVDS_25 [get_ports Clk_ref_N]
set_property IOSTANDARD LVDS_25 [get_ports Clk_ref_P]

# DOUT[17] LVDSOUT 2
set_property PACKAGE_PIN D19 [get_ports Sync_res_P]
set_property PACKAGE_PIN D20 [get_ports Sync_res_N]
set_property IOSTANDARD LVDS_25 [get_ports Sync_res_N]
set_property IOSTANDARD LVDS_25 [get_ports Sync_res_P]
set_property PULLTYPE PULLDOWN [get_ports Sync_res_P]
set_property PULLTYPE PULLDOWN [get_ports Sync_res_N]


# Dummy
set_property PACKAGE_PIN W26 [get_ports Ctl_SOut]
set_property IOSTANDARD LVCMOS25 [get_ports Ctl_SOut]
set_property PULLTYPE PULLDOWN [get_ports Ctl_SOut]

#Dummy
set_property PACKAGE_PIN T25 [get_ports MISO]
set_property IOSTANDARD LVCMOS25 [get_ports MISO]
set_property PULLTYPE PULLDOWN [get_ports MISO]


# DIN[8] (LVDS0)
# fast data lines
# set_property PACKAGE_PIN G3 [get_ports DATA_LVDS_N]
# set_property PACKAGE_PIN G4 [get_ports DATA_LVDS_P]
# # set_property PACKAGE_PIN E3 [get_ports DATA_LVDS_N]
# # set_property PACKAGE_PIN E4 [get_ports DATA_LVDS_P]
# slow data lines
set_property PACKAGE_PIN C19 [get_ports DATA_LVDS_P]
set_property PACKAGE_PIN B19 [get_ports DATA_LVDS_N]
set_property DIFF_TERM TRUE [get_ports DATA_LVDS_P]
set_property DIFF_TERM TRUE [get_ports DATA_LVDS_N]
set_property IOSTANDARD LVDS_25 [get_ports DATA_LVDS_P]
set_property IOSTANDARD LVDS_25 [get_ports DATA_LVDS_N]


# INJECTION connected to INJ_STRB
set_property PACKAGE_PIN A15 [get_ports INJECTION]
set_property IOSTANDARD LVCMOS25 [get_ports INJECTION]
set_property DRIVE 16 [get_ports INJECTION]
set_property SLEW FAST [get_ports INJECTION]




# SPI configuration flash
set_property CONFIG_MODE SPIx4 [current_design]
set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]
set_property BITSTREAM.CONFIG.CONFIGRATE 33 [current_design]


#set_false_path -from [get_clocks -of_objects [get_pins mmcm_adv_inst/CLKOUT4]] -to [get_clocks -of_objects [get_pins mmcm_adv_inst/CLKOUT4]]
set_false_path -from [get_clocks rgmii_rxc] -to [get_clocks rgmii_rxc]


# set_false_path -from [get_clocks -of_objects [get_pins PLLE2_BASE_inst_clk/CLKOUT3]] -to [get_clocks -of_objects [get_pins PLLE2_BASE_inst_clk/CLKOUT3]]


set_property PULLTYPE PULLDOWN [get_ports SCL]
set_property PULLTYPE PULLDOWN [get_ports SDA]
set_property SLEW FAST [get_ports SCL]
set_property SLEW FAST [get_ports SDA]
set_property DRIVE 12 [get_ports SCL]


set_false_path -from [get_clocks -of_objects [get_pins PLLE2_BASE_inst_clk/CLKOUT1]] -to [get_clocks -of_objects [get_pins PLLE2_BASE_inst_clk/CLKOUT1]]




set_property IOSTANDARD LVCMOS25 [get_ports CLK_ENABLE]
# create_clock -period 25.000 -name TLU_CLK_P -waveform {0.000 12.500} [get_ports TLU_CLK_P]

set_false_path -from [get_clocks -of_objects [get_pins PLLE2_BASE_inst_comm/CLKOUT0]] -to [get_clocks TLU_CLK]

# set_property OFFCHIP_TERM NONE [get_ports TLU_BUSY_P]
# set_property PULLTYPE PULLDOWN [get_ports TLU_BUSY_P]
