
# ---------------------------------------------------------------
# Copyright (c) SILAB ,  Institute of Physics, University of Bonn
# ---------------------------------------------------------------
#
#   This script creates Vivado projects and bitfiles for the supported hardware platforms.
#
#   Start Vivado in tcl/batch mode by executing:
#       vivado -mode batch -source run.tcl
#
#   NOTE: This will build firmware versions for every supported hardware. See the section "Create projects and bitfiles" below.
#   Alternatively, a list of 8 arguments can be used to build only the specified firmware.
#   The arguments have to be in the correct order. Just copy&paste from the "Create projects and bitfiles" section and remove all but one space in between the args. Empty arguments must be set to "".
#       vivado -mode tcl -source run.tcl -tclargs xc7k160tffg676-2 BDAQ53 "" ../src/bdaq53.xdc 64 "" _1LANE _RX640
#



# Use current environment python instead of vivado included python
if {[info exists ::env(PYTHONPATH)]} {
    unset ::env(PYTHONPATH)
}
if {[info exists ::env(PYTHONHOME)]} {
    unset ::env(PYTHONHOME)
}
# Get rid of Vivado python (since Vivado 2021) in PATH and use python from calling shell
set env(PATH) [join [lsearch -inline -all -not -regexp [split $::env(PATH) ":"] (.*)lnx64\/python(.*)] ":"]

# set telepix_daq_dir [exec python -c "print('\n'.join([i for i in sys.path if 'TelePixDAQ' in i]))"]
# set vivado_dir $telepix_daq_dir/telepix_daq/firmware/vivado/
# set vivado_dir [exec pwd]
set basil_dir [exec python -c "import basil, os; print(str(os.path.dirname(os.path.dirname(basil.__file__))))"]
set firmware_dir [exec python -c "import os; print(os.path.dirname(os.getcwd()))"]
# set include_dirs [list $vivado_dir/../src $vivado_dir/../../SiTCP_Netlist_for_Kintex7 $basil_dir/basil/firmware/modules $basil_dir/basil/firmware/modules/utils]
set include_dirs [list $basil_dir/basil/firmware/modules $basil_dir/basil/firmware/modules/utils $firmware_dir/src/ $firmware_dir  $firmware_dir/IP/synth $firmware_dir/IP]


file mkdir ../bit reports

proc read_design_files {part ethernet} {
    global firmware_dir
    if {$ethernet == "10G"} {
        generate_ip_cores "ten_gig_eth_pcs_pma" $part
        add_files -norecurse $firmware_dir/IP/ten_gig_eth_pcs_pma.xci

        read_edif $firmware_dir/SiTCP_10G/SiTCPXG_XC7K_128K_V4.edf
        read_verilog $firmware_dir/SiTCP_10G/SiTCPXG_XC7K_128K_V4.v
        read_verilog $firmware_dir/SiTCP_10G/TIMER_SiTCPXG.v
        read_verilog $firmware_dir/SiTCP_10G/WRAP_SiTCPXG_XC7K_128K.v
    } else {
        read_edif $firmware_dir/SiTCP/SiTCP_XC7K_32K_BBT_V110.ngc
        # read_edif $firmware_dir/../SiTCP_Netlist_for_Kintex7/SiTCP_XC7K_32K_BBT_V110.ngc
    }
    read_verilog $firmware_dir/src/mightypix_mio3_AIDAMode.v
    read_verilog -sv $firmware_dir/src/mightypix_core_AIDAMode.v
}

proc generate_ip_cores {name part} {
    global firmware_dir
    set ipname $name
    set xci_file $firmware_dir/IP/$name.xci

    create_project -force ipcore -part $part
    read_ip $xci_file
    upgrade_ip [get_ips *]
    generate_target -verbose -force all [get_ips]
    create_ip_run [get_files $xci_file]
    launch_runs $ipname\_synth_1 -jobs 12
    wait_on_run $ipname\_synth_1
    close_project
}


proc run_bit {part board connector xdc_file size option lanes speed ethernet TLU} {
    global firmware_dir
    global include_dirs

    set branch [if [catch {exec python -c "from telepix_daq import utils; print(utils.get_software_version())"}] {puts "none"} else {exec python -c "from telepix_daq import utils; print(utils.get_software_version().split('@')[0])"}]
    set version [exec python -c "from importlib.metadata import version; print(version('telepix_daq'))"]
    lindex [split $version '.'] 
    set version_major [lindex [split $version '.'] 0]
    set version_minor [lindex [split $version '.'] 1]
    set version_patch [lindex [split $version '.'] 2]

    create_project -force -part $part $board$option$connector$lanes$speed\_$ethernet\_$TLU designs

    read_design_files $part $ethernet
    read_xdc $firmware_dir/src/$xdc_file

    if {$ethernet == "10G"} {
        read_xdc $firmware_dir/src/SiTCP10G.xdc
    } else {
        read_xdc $firmware_dir/src/SITCP.xdc
    }

    synth_design -top mightypix_mio3_AIDAMode -include_dirs $include_dirs -verilog_define "$board=1" -verilog_define "$connector=1" -verilog_define "SYNTHESIS=1" -verilog_define "$option=1" -verilog_define "$lanes=1" -verilog_define "$speed=1" -verilog_define $ethernet -verilog_define $TLU -generic VERSION_MAJOR=8'd$version_major -generic VERSION_MINOR=8'd$version_minor -generic VERSION_PATCH=8'd$version_patch
    opt_design
    place_design
    phys_opt_design
    route_design
    report_utilization -file "reports/report_utilization_$board$option$connector$lanes$speed\_$ethernet\_$TLU.log"
    report_timing_summary -file "reports/report_timing_$board$option$connector$lanes$speed\_$ethernet\_$TLU.log"
    write_bitstream -force -file $firmware_dir/bit/$board$option$connector$lanes$speed\_$ethernet\_$TLU
    write_cfgmem -format mcs -size $size -interface SPIx4 -loadbit "up 0x0 $firmware_dir/bit/$board$option$connector$lanes$speed\_$ethernet\_$TLU.bit" -force -file $firmware_dir/bit/$board$option$connector$lanes$speed\_$ethernet\_$TLU
    close_project

    # exec tar -C ./output -cvzf output/$version\_$branch\_$board$option$connector$lanes$speed\_$ethernet.tar.gz $board$option$connector$lanes$speed\_$ethernet.bit $board$option$connector$lanes$speed\_$ethernet.mcs
}


#
# Create projects and bitfiles
#

if {$argc == 0} {
    # By default, all firmware versions are generated. You can comment the ones you don't need.
#       FPGA type           board name  connector   constraints file                    flash   option  lanes   speed  eth TLU
    run_bit xc7k160tffg676-1    MIO3      ""        telepix_mio3_1G_AIDAMode.xdc          64      ""      ""   ""     1G    AIDA
    # run_bit xc7k160tfbg676-3    MIO3      ""        telepix_mio3_10G.xdc          64      ""      ""   ""     10G

# In order to build only one specific firmware version, the tun.tcl can be executed with arguments
} else {
    if {$argc == 9} {
        run_bit {*}$argv
    } else {
        puts "ERROR: Invalid args"
    }
}
exit
