# Use current environment python instead of vivado included python
if {[info exists ::env(PYTHONPATH)]} {
    unset ::env(PYTHONPATH)
}
if {[info exists ::env(PYTHONHOME)]} {
    unset ::env(PYTHONHOME)
}

# set telepix_daq_dir [exec pip show telepix_daq |grep Location | cut -d " " -f2]
# set telepix_daq_dir [exec python -c "import sys; print(''.join([i_var for i_var in sys.path if 'TelePixDAQ' in i_var]))"]
set vivado_dir [exec pwd]
set bit_dir $vivado_dir/../bit/
# set bit_dir $telepix_daq_dir/telepix_daq/firmware/bit/

open_hw_manager
# connect_hw_server -allow_non_jtag
connect_hw_server 
open_hw_target

current_hw_device [lindex [get_hw_devices] 0]
set devPart [get_property PART [current_hw_device]]
# set_property PROGRAM.FILE {../bit/MIO3_1G.bit} [current_hw_device]

# report_property [current_hw_device] -regexp REGISTER.BOOT_STATUS.*
# report_property [current_hw_device] -regexp REGISTER.CONFIG_STATUS.*

# program_hw_devices [current_hw_device]

# current_hw_device [get_hw_devices xc7k160t_0]
# refresh_hw_device -update_hw_probes false [lindex [get_hw_devices xc7k160t_0] 0]
# report_property [current_hw_device] -regexp REGISTER.BOOT_STATUS.*
# report_property [current_hw_device] -regexp REGISTER.CONFIG_STATUS.*
# set_property PROBES.FILE {} [get_hw_devices xc7k160t_0]
# set_property FULL_PROBES.FILE {} [get_hw_devices xc7k160t_0]
# ## old 
# set_property PROGRAM.FILE {../bit/telepix_mio3.bit} [get_hw_devices xc7k160t_0] 
# ## 1G
set_property PROGRAM.FILE {../bit/MIO3_1G.bit} [get_hw_devices xc7k160t_0]
# set_property PROGRAM.FILE {../bit/MIO3_1G_AIDA.bit} [get_hw_devices xc7k160t_0]
# ## 10G
# set_property PROGRAM.FILE {../bit/MIO3_10G.bit} [get_hw_devices xc7k160t_0]
# program_hw_devices [get_hw_devices xc7k160t_0]
# program_hw_devices [get_hw_devices xc7k160t_0]
program_hw_devices [get_hw_devices xc7k160t_0]
refresh_hw_device [lindex [get_hw_devices xc7k160t_0] 0]
exit