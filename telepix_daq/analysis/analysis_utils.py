#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import ast
import logging
import multiprocessing as mp
import warnings
from functools import partial

import numpy as np
from scipy.optimize import OptimizeWarning, curve_fit
from scipy.special import erf
from tqdm import tqdm
import matplotlib.pyplot as plt
import time
import numba

import gc

#brute force to silence pescy root output
import sys
import os

from memory_profiler import profile

logger = logging.getLogger('Analysis')

ts_dtype =np.dtype([("event_number", "<u8"), 
            ("timestamp", "<u8"), 
            ('scan_param_id', '<u4')
            ])
trigger_dtype =np.dtype([("event_number", "<u8"), 
            ("timestamp", "<u8"), 
            ('scan_param_id', '<u4')
            ])
hit_dtype = np.dtype([
                ("event_number", "<u8"), 
                ("col", "<u1"),
                ("row", "<u2"), 
                ("ts1", "<u4"),
                ("ts1_n", "<u2"),
                ("ts2", "<u2"), 
                ("ts3", "<u1"),
                # ("tdiff", "<u4"),
                ("timestamp", "<u8"), 
                ("fpga", "<u8"), 
                ("timestamp_trigger", "<u8"), 
                ("trigger_i", "<u4"), 
                # ("tdiff_trigger_ts1", "<u4"), 
                ("tot", "<u2"),
                ('scan_param_id', '<u4')
                ])

event_dtype = np.dtype([
    ("event_number", "<u8"),
    ("trigger_number", "<u4"),
    ("frame", "<u1"),
    ("column", "<u2"),
    ("row", "<u2"),
    ("charge", "<u2"),
    ("timestamp", "<i8"),
])

matched_hit_dtype =np.dtype([
                ("event_number", "<u8"), 
                ("col", "<u4"),
                ("row", "<u4"), 
                ("tot", "<f8"),
                ("timestamp", "<u8"), 
                ("timestamp_trigger", "<u8"), 
                ("tdiff_trigger", "<f8"), 
                ('scan_param_id', '<u4')
                ])

# Word defines
TRIGGER_HEADER = 0x80000000

# Data Masks
TRG_MASK = 0x7FFFFFFF  # Trigger data (number and/or time stamp)

class ConfigDict(dict):
    ''' Dictionary with different value data types:
        str / int / float / list / tuple depending on value

        key can be string or byte-array. Contructor can
        be called with all data types.

        If cast of object to known type is not possible the
        string representation is returned as fallback
    '''

    def __init__(self, *args):
        for key, value in dict(*args).items():
            self.__setitem__(key, value)

    def __setitem__(self, key, val):
        # Change types on value setting
        key, val = self._type_cast(key, val)
        dict.__setitem__(self, key, val)

    def _type_cast(self, key, val):
        ''' Return python objects '''
        # Some data is in binary array representation (e.g. pytable data)
        # These must be convertet to string
        if isinstance(key, (bytes, bytearray)):
            key = key.decode()
        if isinstance(val, (bytes, bytearray)):
            val = val.decode()
        if 'chip_sn' in key:
            return key, val
        try:
            if isinstance(val, np.generic):
                return key, val.item()
            return key, ast.literal_eval(val)
        except (ValueError, SyntaxError):  # fallback to return the object
            return key, val


def block_print():
    sys.stdout = open(os.devnull, 'w')

def enable_print():
    sys.stdout = sys.__stdout__

def scurve(x, A, mu, sigma, b = 0):
    # print(f"scurve fit {A, mu, sigma}")
    return 0.5 * A * erf((x - mu) / (np.sqrt(2) * sigma)) + 0.5 * A + b


def zcurve(x, A, mu, sigma, b = 0):
    return -0.5 * A * erf((x - mu) / (np.sqrt(2) * sigma)) + 0.5 * A + b


def gauss(x, A, mu, sigma):
    return A * np.exp(-(x - mu) * (x - mu) / (2 * sigma * sigma))

def DoubleGaussExp(x, mean, sgma, kl, kh):
    middle = np.exp(-0.5*((x-mean)/sgma)**2)
    high = np.exp(kh**2/2-kh*((x-mean)/sgma))
    low = np.exp(kl**2/2+kl*((x-mean)/sgma))

    l_m = np.where((x-mean) <= -kl * sgma, low, middle)
    l_m_h = np.where((x-mean) > kh * sgma, high, l_m)

    return l_m_h

# @profile
def imap_bar(func, args, n_processes=None, unit='it', unit_scale=False):
    ''' Apply function (func) to interable (args) with progressbar
    '''
    p = mp.Pool(n_processes)
    res_list = []
    pbar = tqdm(total=len(args), unit=unit, unit_scale=unit_scale)
    for _, res in enumerate(p.imap(func, args)):
        # pass
        pbar.update()
        res_list.append(res)
    pbar.close()
    p.close()
    p.join()
    return res_list


def match_ts_hit_trigger(hit_table):
    '''
    discard events with trigger = 0 at beginning
    cut ts to same length
    convert to ns
    match:
        build list of events with same trigger ts
        select closest hit ts from list
    output:
        list of tdiffs, list of matched events
    '''
    tdiffs = []
    matched_events = []
    matching_list = []
    for i in range(len(hit_table[:]['event_number'])):
        if hit_table[:]['timestamp_trigger'] == 0:
            pass
        else:
            # assuming ts_hit runs at 200MHz and ts_trigger at 1 GHz
            event = hit_table[i:]['event_number']
            ts_hit = (hit_table[i:]['ts1']*5) % 2**19
            ts_trigger = (hit_table[i:]['timestamp_trigger']) % 2**19

    for i in range(len(event)-1):
        if ts_trigger[i] == ts_trigger[i+1]:
            matching_list.append(i)
            pass
        matching_list.append(i)
        diffs = np.abs(ts_trigger[matching_list] - ts_hit[matching_list])

    return tdiffs, matched_events



def get_threshold(x, y, n_injections):
    ''' Fit less approximation of threshold from s-curve.

        From: https://doi.org/10.1016/j.nima.2013.10.022

        Parameters
        ----------
        x, y : numpy array like
            Data in x and y
        n_injections: integer
            Number of injections
    '''

    # Sum over last dimension to support 1D and 2D hists
    M = y.sum(axis=len(y.shape) - 1)  # is total number of hits
    d = np.diff(x)[0]  # Delta x
    if not np.all(np.diff(x) == d):
        raise NotImplementedError('Threshold can only be calculated for equidistant x values!')
    # print(f"threshold:   {x.max() - (d * M).astype(float) / n_injections}")
    return x.max() - (d * M).astype(float) / n_injections


def fit_fancy_time_resolution(self, data, xliml = -10, xlimr = 10, NBOOTSTRAP = 1):


    #ToDo: silence root
    data = data[0:100000]
    try:
        import ROOT
        # ROOT.gROOT.ProcessLine("gErrorIgnoreLevel = 6001;")
        # ROOT.gErrorIgnoreLevel = ROOT.kInfo
        # ROOT.gErrorIgnoreLevel = ROOT.kFatal
        # ROOT.gErrorIgnoreLevel = ROOT.kPrint
        # ROOT.gErrorIgnoreLevel = ROOT.kError
        # ROOT.gErrorIgnoreLevel = ROOT.kBreak
        # ROOT.gErrorIgnoreLevel = ROOT.kSysError
        
        from scipy.interpolate import UnivariateSpline
        # ROOT.gROOT.ProcessLine("gErrorIgnoreLevel = 1001;")
        # ROOT.gROOT.ProcessLine("gErrorIgnoreLevel = 2001;")
        # warnings.filterwarnings("ignore", category=UserWarning, module="ROOT")
        # ROOT.gSystem.RedirectOutput('output.txt', 'w')
        # ROOT.gROOT.Process()
        
    except Exception as e:
        logger.info(f"Could not import ROOT. Skipping fancy time resolution. Did you source it correctly? {e}")
        return
    # data = data *10**9 #convert times to ns (for pretty plots)
    params_temp = []
    perr_temp = []

    # block_print()

    xliml = min(data)
    xlimr = max(data)

    x = ROOT.RooRealVar("x", "x", xliml, xlimr)
    mean = ROOT.RooRealVar("mean", "mean of gaussian", np.mean(data), xliml, xlimr)
    sigma = ROOT.RooRealVar("sigma", "width of gaussian", 1, 0.01, 100)
    k_l = ROOT.RooRealVar("k_l", "lower transition", 1, -10, 10)
    k_h = ROOT.RooRealVar("k_h", "upper transition", 1, -10, 10)
    root_data = ROOT.RooDataSet.from_numpy({"x": data}, [x])

    xframe = x.frame(Title="Fitted ExpGaussExp")
    root_data.plotOn(xframe)

    binwidth = xframe.getFitRangeBinW()

    ExpGaussExp_string = '(x-mean) <= -k_l * sigma ? exp(k_l**2/2+k_l*((x-mean)/sigma)) : ((x-mean) <= k_h * sigma ? exp(-0.5*((x-mean)/sigma)**2) : exp(k_h**2/2-k_h*((x-mean)/sigma)))'

    args = ROOT.RooArgList(x, mean, sigma, k_l, k_h)
    model = ROOT.RooGenericPdf('ExpGaussExp', 'Exp', ExpGaussExp_string , args)

    fit_results = model.fitTo(root_data, ROOT.RooFit.Range("fit"), ROOT.RooFit.Save())
    # model.plotOn(xframe, ROOT.RooFit.Range("fit"), ROOT.RooFit.NormRange("fit"))
    # model.paramOn(xframe)
    params_from_root = fit_results.floatParsFinal()

    # Create a RooRealVar for the integral
    integral_var = ROOT.RooRealVar("integral_var", "Integral", 0.0)

    # Create the integral
    integral = model.createIntegral(ROOT.RooArgSet(x), ROOT.RooFit.Range("fit"))

    # Set the value of the integral_var to the result of the integral
    integral_var.setVal(integral.getVal())

    for i in range(params_from_root.getSize()):
        param = params_from_root[i]
        params_temp.append(params_from_root[i].getVal())
        perr_temp.append(params_from_root[i].getError())
        # print(f"Parameter {param.GetName()}: Value = {param.getVal()}, Error = {param.getError()}")
    
    params = [params_temp[2], params_temp[3], params_temp[1], params_temp[0]]
    perr = [perr_temp[2], perr_temp[3], perr_temp[1], perr_temp[0]]

    mean.setVal(params[0])
    sigma.setVal(params[1])
    k_l.setVal(params[2])
    k_h.setVal(params[3])


    #gauss.plotOn(xframe)
    # Draw RooFit plot on a canvas.
    #c = ROOT.TCanvas("rf409_NumPyPandasToRooFit", "rf409_NumPyPandasToRooFit", 800, 400)
    #xframe.Draw()
    #c.Show()

    nbins = int((xlimr-xliml)/binwidth)
    counts, bin_edges = np.histogram(data, bins=nbins, range=(xliml, xlimr))
    bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2

    amplitude = sum(binwidth*counts) / integral_var.getVal()

    
    #calculate scaling factor for matplotlib fit display
    #amplitude, pamplitude = curve_fit(lambda x, amplitude: scalingCalc(x, amplitude,params_temp[2], params_temp[3], params_temp[1], params_temp[0]), bin_centers, counts)#, p0=initial_params, maxfev=10000, bounds=bounds
    

    bootstrap_params = []
    bootstrap_fail_counter = 0
    seed = 0
    for _ in tqdm(list(range(NBOOTSTRAP))):
        np.random.seed(seed)
        seed += 123
        bootstrap_sample = np.random.choice(data, size=len(data), replace=True)
        boot_params_temp = []
        boot_root_data = ROOT.RooDataSet.from_numpy({"x": bootstrap_sample}, [x])
        
        try:
            boot_fit_results = model.fitTo(boot_root_data, ROOT.RooFit.Range("fit"), ROOT.RooFit.Save())
            boot_params_from_root = boot_fit_results.floatParsFinal()
            for i in range(boot_params_from_root.getSize()):
                #boot_param = boot_params_from_root[i]
                boot_params_temp.append(boot_params_from_root[i].getVal())
                #boot_perr_temp.append(boot_params_from_root[i].getError())
                #print(f"Parameter {param.GetName()}: Value = {param.getVal()}, Error = {param.getError()}")
    
            boot_params = [boot_params_temp[2], boot_params_temp[3], boot_params_temp[1], boot_params_temp[0]]
            bootstrap_params.append(boot_params)
            #print(p)
            #plt.plot(np.linspace(xliml, xlimr,1000), amplitude*DoubleGaussExp(np.linspace(xliml, xlimr,1000), *boot_params), 'r-', label='GaussExp Fit')
            #plt.show()
            #plot(bootstrap_sample, xliml, xlimr, nbins, 'bootstrap')
        except:
            bootstrap_fail_counter += 1
            print('Bootstrap iteration failed!')

    bootstrap_params = np.array(bootstrap_params)
    # bootstrapping_fails.append(bootstrap_fail_counter)

    def calc_fwhm(params):
        fitted_data = amplitude*DoubleGaussExp(bin_centers, *params)
        deviation = counts - fitted_data
        err_counts = np.sqrt(counts)
        for i in range(len(err_counts)):
            if err_counts[i] <= 1:
                err_counts[i] = 1
        #print(err_counts)
        residuals = deviation/err_counts
        #reduced_chi_sq = np.sum(((counts - fitted_data) / err_counts) ** 2 /(len(counts) - len(params)))
        reduced_chi_sq = np.sum(residuals ** 2 /(len(counts) - len(params)))
        #print(reduced_chi_sq)
        fitted_data = DoubleGaussExp(np.linspace(xliml, xlimr,10000), *params)
        spline = UnivariateSpline(np.linspace(xliml, xlimr,10000), fitted_data-np.max(fitted_data)/2, s=0)
        try:
            r1, r2 = spline.roots() # find the roots
            fwhm = abs(r2-r1)
        except:
            fwhm = -1
            r1 = r2 = 0
            print("derp")
        return fwhm, residuals, reduced_chi_sq, r1, r2, err_counts
    fwhm, residuals, reduced_chi_sq, r1, r2, err_counts = calc_fwhm(params)

    bootstrap_fwhm = np.array([calc_fwhm(p)[0] for p in bootstrap_params])
    err_fwhm = np.std(bootstrap_fwhm[bootstrap_fwhm > 0])

    premature_plotting = False
    # premature_plotting = True
    if premature_plotting:
        # Plot the histogram and the fitted data
        fig = plt.figure(figsize=(8, 6))
        gs = fig.add_gridspec(2, hspace=0.3)
        axs = gs.subplots(sharex = False)
        #plt.figure(figsize=(8, 6))
        axs[0].hist(data, bins=nbins, alpha=0, density = False)
        axs[0].errorbar(bin_centers, counts, yerr = err_counts,fmt = ' ', marker='.', color = 'black', label='Data', ecolor = 'blue', capsize = 3)
        axs[0].plot(np.linspace(xliml, xlimr,1000), amplitude*DoubleGaussExp(np.linspace(xliml, xlimr,1000), *params), 'r-', label='ExpGaussExp Fit')
        axs[0].set_ylim(0,1.1*(max(counts)+max(err_counts)))
        axs[0].set_xlim(xliml,xlimr)
        axs[0].set_xlabel(r'$\Delta \mathrm{t}$ [ns]', fontsize = 16)
        axs[0].set_ylabel('Counts / ' + str(binwidth) + ' ns', fontsize = 16)
        axs[0].set_position([0.1, 0.35, 0.8, 0.55])
        #axs[0].set_title(title)
        # plt.show()
        description = ""\
        + r"$\mu$ = "+f'{params[0]:.3f}'+ r"$\pm$" +f'{perr[0]:.3f}' + 'ns'+ "\n"\
        + r"$\sigma$ = "+f'{params[1]:.3f}'+ r"$\pm$" + f'{perr[1]:.3f}' + 'ns'+ "\n"\
        + r"$k_L$ = "+f'{params[2]:.3f}' + r"$\pm$" + f'{perr[2]:.3f}' + "\n"\
        + r"$k_H$ = "+f'{params[3]:.3f}' + r"$\pm$" + f'{perr[3]:.3f}' + "\n"\
        + r'$\chi^2_{red} = $'+f'{reduced_chi_sq:.3f}' + "\n"\
        + r'FWHM =' + f'{fwhm:.3f}' + r"$\pm$" + f'{err_fwhm:.3f}' + ' ns'\
        # + r"$\epsilon$ = "+f'{efficiency:.4f}\n'+"n = "+ n + "\n"\
        #+ r"$A$ = "+f'{params[4]:.3f}'+ r"$\pm$" + f'{perr[4]:.3f}' + "\n"\
        axs[0].text(1.1, 5.3, description, transform=plt.gca().transAxes, ha='right', va='top', fontsize=12, bbox=dict(facecolor='white', edgecolor='black', boxstyle='round,pad=0.5'))

        axs[1].scatter(bin_centers,residuals, marker='+')
        axs[1].set_xlim(xliml,xlimr)
        axs[1].set_xlabel(r'$\Delta \mathrm{t}$ [ns]', fontsize = 16)
        axs[1].set_ylabel('Pulls', fontsize = 16)
        axs[1].set_position([0.1, 0.1, 0.8, 0.15])
        axs[1].axhline(y=0, color='black', linestyle='--', linewidth=1)
        axs[0].axvspan(r1, r2, facecolor='g', alpha=0.5)
        plt.show()

    # ROOT.gSystem.RedirectOutput(0)

    return [nbins], bin_centers, [amplitude], [params[0]], [params[1]], [params[2]], [params[3]], [fwhm], [err_fwhm], [reduced_chi_sq], [binwidth], [perr[0]], [perr[1]], [perr[2]], [perr[3]]






def get_noise(x, y, n_injections):
    ''' Fit less approximation of noise from s-curve.

        From: https://doi.org/10.1016/j.nima.2013.10.022

        Parameters
        ----------
        x, y : numpy array like
            Data in x and y
        n_injections: integer
            Number of injections
    '''

    mu = get_threshold(x, y, n_injections)
    d = np.abs(np.diff(x)[0])

    mu1 = y[x < mu].sum()
    mu2 = (n_injections - y[x > mu]).sum()
    # print(mu1, mu2)
    return d * (mu1 + mu2).astype(float) / n_injections * np.sqrt(np.pi / 2.)

# @profile
def fit_scurve(scurve_data, scan_params, n_injections, sigma_0, upper_chi2_ndf=5):
    '''
        Fit one pixel data with Scurve.
        Has to be global function for the multiprocessing module.

        Returns:
            (mu, sigma, chi2/ndf)
    '''
    # Typecast to working types
    scurve_data = np.array(scurve_data, dtype=float)
    # Scipy bug: fit does not work on float32 values, without any error message
    scan_params = np.array(scan_params, dtype=float)

    # Deselect masked values (== nan)
    # print(scurve_data)
    # print(np.isnan(scurve_data))
    x = scan_params[~np.isnan(scurve_data)]
    y = scurve_data[~np.isnan(scurve_data)]

    # Only fit data that is fittable
    if np.all(y == 0) or np.all(np.isnan(y)) or x.shape[0] < 3:
        return (0., 0., 0., 0., 0.)
    if y.max() < 0.2 * n_injections:
        return (0., 0., 0., 0., 0.)

    # Calculate data errors, Binomial errors
    min_err = np.sqrt(0.5 - 0.5 / n_injections)  # Set arbitrarly to error of 0.5 injections, needed for fit minimizers
    yerr = np.full_like(y, min_err, dtype=float)
    yerr[y <= n_injections] = np.sqrt(y[y <= n_injections] * (1. - y[y <= n_injections].astype(float) / n_injections))  # Binomial errors
    yerr[yerr < min_err] = min_err
    # Additional hits not following fit model set high error
    sel_bad = y > n_injections
    yerr[sel_bad] = (y - n_injections)[sel_bad]

    # Calculate threshold start value:
    mu = get_threshold(x=x, y=y, n_injections=n_injections)

    # Set fit start values
    # p0 = [mu, sigma_0]
    midpoint_slice = np.argmin(np.abs(y-n_injections/2))
    midpoint = int(x[midpoint_slice])
    # print(midpoint)
    p0 = [max(y),midpoint, 1, min(y)]#sigma_0
    # print(p0)



    # Bounds makes the optimizer 5 times slower and are therefore deactivated.
    # TODO: Maybe we find a better package?
    # bounds = [[x.min() - 5 * np.diff(x)[0], 0.05 * np.min(np.diff(x))],
    #           [x.max() + 5 * np.diff(x)[0], x.max() - x.min()]]

    # Special case: step function --> omit fit, set to result
    if not np.any(np.logical_and(y != 0, y != n_injections)):
        # All at n_inj or 0 --> set to mean between extrema
        return (n_injections, mu + np.min(np.diff(x)) / 2., 0.01 * np.min(np.diff(x)), 1e-6, 0.)

    # Special case: Nothing to optimize --> set very good start values
    # Will trigger OptimizeWarning
    is_scurve = False
    # print(np.count_nonzero(np.logical_and(y != 0, y != n_injections)))
    # if np.count_nonzero(np.logical_and(y != 0, y != n_injections)) == 1:
    if np.count_nonzero(np.logical_and(y != 0, y != n_injections)) >= 1:
        # Only one not at n_inj or 0 --> set mean to the point
        idx = np.ravel(np.where(np.logical_and(y != 0, y != n_injections)))[0]
        #p0 = (x[idx], 0.1 * np.min(np.diff(x)))
        p0 = (max(y),x[idx], 0.1 * np.min(np.diff(x)), min(y))
        p0 = (100,155,1,0)
        is_scurve = False
        if len(y)>0:
            ##TODO: this is to weak --> need a FIXME
            # print(np.mean(y[:midpoint_slice]), np.mean(y[midpoint_slice:]))
            if np.mean(y[:midpoint_slice]) > np.mean(y[midpoint_slice:]):
                # print("here3")
                is_scurve = True

    try:
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", OptimizeWarning)
            bounds = ([np.min((max(y)*0.8,np.mean(y)*1.2)),min(x),0,-np.inf],[max(y)*1.1,max(x),10,np.inf])
            bounds = ([0,0,0,0],[np.inf,np.inf,np.inf, np.inf])
            if is_scurve:
                # print("!!!!!!!!!!!!!!!!!!!!!!!!!!")
                popt = curve_fit(scurve,
                                xdata=x, ydata=y, p0=p0, sigma=yerr, bounds=bounds,
                                absolute_sigma=True if np.any(yerr) else False)[0]
                chi2 = np.sum(((y - scurve(x, *popt)/yerr)) ** 2/(len(y) - len(popt)))
            else:
                popt = curve_fit(zcurve,
                                xdata=x, ydata=y, p0=p0, sigma=yerr, bounds=bounds,
                                absolute_sigma=True if np.any(yerr) else False)[0]
                chi2 = np.sum(((y - zcurve(x, *popt))/yerr) ** 2/(len(y) - len(popt)))
            # if is_scurve:
            #     popt = curve_fit(f=lambda x, mu, sigma: scurve(x, n_injections, mu, sigma),
            #                     xdata=x, ydata=y, p0=p0, sigma=yerr,
            #                     absolute_sigma=True if np.any(yerr) else False)[0]
            #     chi2 = np.sum(((y - scurve(x, n_injections, popt[0], popt[1])/yerr)) ** 2/(len(y) - len(popt)))
            # else:
            #     popt = curve_fit(f=lambda x, mu, sigma: zcurve(x, n_injections, mu, sigma),
            #                     xdata=x, ydata=y, p0=p0, sigma=yerr,# bounds=bounds,
            #                     absolute_sigma=True if np.any(yerr) else False)[0]
            #     chi2 = np.sum(((y - zcurve(x, n_injections, popt[0], popt[1]))/yerr) ** 2/(len(y) - len(popt)))
            plot = False
            # plot = True
            if plot:
                # print(popt)
                plt.scatter(x,y)
                plt.plot(x, zcurve(x, *popt), label = str(popt))
                plt.plot(x, zcurve(x, *p0), label =str(p0))
                # # plt.plot(x, zcurve(x, n_injections, 150,1), label = '150,1, fix')
                plt.legend()
                plt.show()#block=False
                # # plt.pause(3)
                # plt.close() 
            # print(f"chi2 {chi2}")
                
    except RuntimeError:  # fit failed
        return (0., 0., 0., 0., 0.)

    # Treat data that does not follow an S-Curve, every fit result is possible here but not meaningful
    max_threshold = x.max() + 5. * np.abs(popt[1])
    min_threshold = x.min() - 5. * np.abs(popt[1])
    # if chi2 / (y.shape[0] - 3 - 1) > upper_chi2_ndf:
    if popt[1] <= 0 or not min_threshold < popt[0] < max_threshold:
        return (0., 0., 0., 0., 0.)
    # print(popt[0], popt[1], chi2 / (y.shape[0] - 3 - 1))
    del scurve_data
    del scan_params
    return (popt[0], popt[1], popt[2], popt[3],  chi2) # / (y.shape[0] - 3 - 1))#the shape is not needed?


def _mask_bad_data(scurve, n_injections):
    ''' This function tries to find the maximum value that is described by an S-Curve
        and maskes all values above.

        Multiple methods are used and the likelyhood that a bad S-Curve can happen
        by chance is valued. Especially these cases are treated:
        1. Additional noisy data
                       *
                      *
        n_inj-     ***
                  *
                 *
          0  - **
        2. Very noisy pixel leading to stuck pixels that see less hits
        n_inj-
                  *
                 * *
          0  - **   *
        3. Double S-Curve
                     *
        n_inj-     **     ***
                  *      *
                 *    * *
          0  - **      *
        4. Noisy data that looks bad but is ok (statistically possible)
        n_inj-          ***
                  * * *
                 * *
          0  - **

        Returns:
        --------
        numpy boolean array as a mask for good settings, True for bad settings
    '''

    scurve_mask = np.ones_like(scurve, dtype=np.bool)

    # Speedup, nothing to do if no slope
    if not np.any(scurve) or np.all(scurve == n_injections):
        return scurve_mask

    # Initialize result to best case (complete range can be used)
    idx_stop = scurve.shape[0]

    # Step 1: Find good maximum setting to restrict the range
    if np.any(scurve == n_injections):  # There is at least one setting seeing all injections
        idcs_stop = np.ravel(np.argwhere(scurve == n_injections))  # setting indices with all injections
        if len(idcs_stop) > 1:  # Several indexes
            # Find last index of the first region at n_injections
            if np.argmin(np.diff(idcs_stop) != 1) != 0:
                idx_stop = idcs_stop[np.argmin(np.diff(idcs_stop) != 1)] + 1
            else:  # Only one settled region, take last index
                idx_stop = idcs_stop[-1] + 1
        else:
            idx_stop = idcs_stop[-1] + 1
        scurve_cut = scurve[:idx_stop]
    elif scurve.max() > n_injections:  # Noisy pixels; no good maximum value; take latest non-noisy setting
        idx_stop = np.ravel(np.argwhere(scurve > n_injections))[0]
        scurve_cut = scurve[:idx_stop]
    else:  # n_injections not reached; scurve not fully recorded or pixel very noisy to have less hits
        scurve_cut = scurve

    # First measurement already with too many hits; no reasonable fit possible
    if idx_stop == 0:
        return scurve_mask

    # Check if first measurement is already noisy (> n_injections or more hits then following stuck settings)
    # Return if very noisy since no fit meaningful possible
    y_idx_sorted = scurve_cut.argsort()  # sort y value indeces to check for monotony
    if y_idx_sorted[0] != 0 and (scurve[0] > n_injections or (scurve[0] - scurve[1]) > 2 * np.sqrt(scurve[0] * (1. - float(scurve[0]) / n_injections))):
        return scurve_mask

    # Step 2: Find first local maximum
    sel = np.r_[True, scurve_cut[1:] >= scurve_cut[:-1]] & np.r_[scurve_cut[:-1] > scurve_cut[1:], True]  # Select local maximum; select last index if flat maximum, flat maximum expected for scurve
    y_max_idcs = np.arange(scurve_cut.shape[0])[sel]
    if np.any(y_max_idcs):  # Check for a maxima
        # Loop over maxima
        for y_max_idx in y_max_idcs:
            y_max = scurve_cut[y_max_idx]
            y_diff = np.diff(scurve_cut.astype(np.int))
            y_dist = (y_max.astype(np.int) - scurve_cut.astype(np.int)).astype(np.int)
            y_dist[y_max_idx + 1:] *= -1
            y_err = np.sqrt(scurve_cut * (1. - scurve_cut.astype(float) / n_injections))
            min_err = np.sqrt(0.5 - 0.5 / n_injections)
            y_err[y_err < min_err] = min_err
            # Only select settings where the slope cannot be explained by statistical fluctuations
            try:
                if np.any(y_diff < -2 * y_err[1:]):
                    idx_stop_diff = np.ravel(np.where(y_diff < -2 * y_err[1:]))[0]
                else:
                    idx_stop_diff = idx_stop
                idx_stop_dist = np.ravel(np.where(y_dist < -2 * y_err))[0]
                idx_stop = min(idx_stop_diff + 1, idx_stop_dist)
                break
            except IndexError:  # No maximum found
                pass

    scurve_mask[:idx_stop] = False

    return scurve_mask

# @profile
def fit_scurves_multithread(scurves, scan_params, n_injections=None, invert_x=False, optimize_fit_range=False, upper_chi2_ndf=50):
    ''' Fit Scurves on all available cores in parallel.

        Parameters
        ----------
        scurves: numpy array like
            Histogram with S-Curves. Channel index in the first and data in the second dimension.
        scan_params: array like
            Values used durig S-Curve scanning.
        n_injections: integer
            Number of injections
        invert_x: boolean
            True when x-axis inverted
        optimize_fit_range: boolean
            Reduce fit range of each S-curve independently to the S-Curve like range. Take full
            range if false
    '''
    gc.collect()

    scan_params = np.array(scan_params)  # Make sure it is numpy array

    if invert_x:
        scan_params *= -1

    if optimize_fit_range:
        scurve_mask = np.ones_like(scurves, dtype=np.bool)  # Mask to specify fit range for all scurves
        for i, scurve in enumerate(scurves):
            scurve_mask[i] = _mask_bad_data(scurve, n_injections)
        scurves_masked = np.ma.masked_array(scurves, scurve_mask)
    else:
        scurves_masked = np.ma.masked_array(scurves)

    # Calculate noise median for better fit start value
    logger.info("Calculate S-curve fit start parameters")
    sigmas = []

    for curve in tqdm(scurves_masked, unit=' S-curves', unit_scale=True):
        # Calculate from pixels with valid data (maximum = n_injections)
        # print("curve max", curve.max())
        tolerance = 10
        valid_values = [value for value in curve if n_injections - tolerance <= value <= n_injections + tolerance]
        if len(valid_values) > 0:
        # if curve.max() == n_injections:
            # print("valid")
            if np.all(curve.mask == np.ma.nomask):
                x = scan_params
            else:
                x = scan_params[~curve.mask]

            sigma = get_noise(x=x, y=curve.compressed(), n_injections=n_injections)
            sigmas.append(sigma)

    sigma_0 = np.median(sigmas)
    sigma_0 = np.max([sigma_0, np.diff(scan_params).min() * 0.01])  # Prevent sigma = 0

    logger.info("Start S-curve fit on %d CPU core(s)", mp.cpu_count())
    # print(scurves_masked.tolist())
    partialfit_scurve = partial(fit_scurve,
                                scan_params=scan_params,
                                n_injections=n_injections,
                                sigma_0=sigma_0,
                                upper_chi2_ndf=5)
    result_list = imap_bar(partialfit_scurve, scurves_masked.tolist(),n_processes=4, unit=' Fits', unit_scale=True)  # Masked array entries to list leads to NaNs
    result_array = np.array(result_list)
    logger.info("S-curve fit finished")
 
    thr = result_array[:, 1]
    # print(thr)
    if invert_x:
        thr *= -1
    amplitude = result_array[:, 0]
    sig = result_array[:, 2]
    bkngd = result_array[:, 3]
    chi2ndf = result_array[:, 4]
    amplitude2D = np.reshape(amplitude, (120, 400))
    thr2D = np.reshape(thr, (120, 400))
    sig2D = np.reshape(sig, (120, 400))
    bkgnd2D = np.reshape(bkngd, (120, 400))
    chi2ndf2D = np.reshape(chi2ndf, (120, 400))
    # print("here", thr2D, sig2D, chi2ndf2D)
    del scurves_masked
    del partialfit_scurve
    del result_array
    return amplitude2D, thr2D, sig2D,bkgnd2D, chi2ndf2D


@numba.njit(locals={'cluster_shape': numba.int64})
def calc_cluster_shape(cluster_array):
    '''Boolean 8x8 array to number.
    '''
    cluster_shape = 0
    indices_x, indices_y = np.nonzero(cluster_array)
    for index in np.arange(indices_x.size):
        cluster_shape += 2**xy2d_morton(indices_x[index], indices_y[index])
    return cluster_shape


@numba.njit(numba.int64(numba.uint32, numba.uint32))
def xy2d_morton(x, y):
    ''' Tuple to number.

    See: https://stackoverflow.com/questions/30539347/
         2d-morton-code-encode-decode-64bits
    '''
    x = (x | (x << 16)) & 0x0000FFFF0000FFFF
    x = (x | (x << 8)) & 0x00FF00FF00FF00FF
    x = (x | (x << 4)) & 0x0F0F0F0F0F0F0F0F
    x = (x | (x << 2)) & 0x3333333333333333
    x = (x | (x << 1)) & 0x5555555555555555

    y = (y | (y << 16)) & 0x0000FFFF0000FFFF
    y = (y | (y << 8)) & 0x00FF00FF00FF00FF
    y = (y | (y << 4)) & 0x0F0F0F0F0F0F0F0F
    y = (y | (y << 2)) & 0x3333333333333333
    y = (y | (y << 1)) & 0x5555555555555555

    return x | (y << 1)


# @numba.njit
# def create_matched_hits(hit_dat,cluster_dat,matched_buffer):
    
#     for icluser,cluster in enumerate(cluster_dat):
#         # Note that the following query is different for each client.
#         for hit in hit_dat:
#             if cluster["seed_col"] == hit["col"] and  cluster["seed_row"] == hit["row"] and hit['timestamp'] == cluster["event_number"]:
#                 matched_buffer[icluser]["event_number"]=hit["event_number"]
#                 matched_buffer[icluser]["col"]=hit["col"]
#                 matched_buffer[icluser]["row"]=hit["row"]
#                 matched_buffer[icluser]["tot"]=hit["tot"]
#                 matched_buffer[icluser]["timestamp"]=hit["timestamp"]
#                 matched_buffer[icluser]["timestamp_trigger"]=hit["timestamp_trigger"]
#                 # matched_hists[icluser]["tdiff_trigger"]=  hit["tdiff_trigger_ts1"]
#                 matched_buffer[icluser]["tdiff_trigger"]=  numba.float64(hit["timestamp"])*10.- numba.float64(hit["timestamp_trigger"])
#                 matched_buffer[icluser]["scan_param_id"]=hit["scan_param_id"]
#                 break

#     return matched_buffer


@numba.njit
def cast32(a):
    return a&0xFFFFFFFFFFFFFFFF
    # return a&0xFFFFF

@numba.njit
def subtract(x, y):
                    if (y == 0):
                        return x
                    return subtract(x ^ y, (~x & y) << 1)

@numba.njit
def create_matched_hits(hit_dat, cluster_dat, matched_buffer, trigger_dat):
    # Create a dictionary for fast lookups
    hit_dict = {}
    for hit in hit_dat:
        key = (np.uint16(hit['col']), np.uint16(hit['row']), np.uint64(hit['timestamp']))
        hit_dict[key] = hit
    offset=1e20
    timestamp_old = np.float64(0)
    for icluser, cluster in enumerate(cluster_dat):
        key = (np.uint16(cluster["seed_col"]), np.uint16(cluster["seed_row"]), np.uint64(cluster["event_number"]))
        if key in hit_dict:
            # if np.uint64(cluster["size"]) > np.uint64(6):
                # continue
            hit = hit_dict[key]
            matched_buffer[icluser]["event_number"] = hit["event_number"]
            matched_buffer[icluser]["col"] = hit["col"] 
            matched_buffer[icluser]["row"] = hit["row"]
            matched_buffer[icluser]["tot"] = np.float64(hit["tot"])
            
            matched_buffer[icluser]["timestamp"] = hit["timestamp"]
            if timestamp_old == hit["timestamp_trigger"]:
                matched_buffer[icluser]["timestamp_trigger"] = np.float64(1000000000000)
                timestamp_trigger =  np.float64(1000000000000)
                tdiff = np.float64(1000000000000)
            else:
                matched_buffer[icluser]["timestamp_trigger"] = (hit["timestamp_trigger"])
                timestamp_old = hit["timestamp_trigger"]
                timestamp_trigger =  np.float64((hit["timestamp_trigger"])*np.float64(2./1.))
                timestamp = np.float64((hit["timestamp"])*np.float64(5./1.))
                tdiff = (np.float64(timestamp -timestamp_trigger))
            
           
            # timestamp = np.int64(cast32(hit["timestamp"])) 
            # timestamp_trigger =  np.int64(cast32(hit["timestamp_trigger"])) 

            # timestamp = np.float64(hit["ts1"])*np.float64(10./1.5)
            # timestamp_trigger =  np.float64(hit["timestamp_trigger"])*np.float64(2./1.5)



            # mask=np.int64(0xFFFFF)
            # if((timestamp&mask)<= np.int64(timestamp_trigger)-np.int64(5e5)):
            #     timestamp+=(mask+np.int64(1))


            # tdiff = (np.float64(timestamp_trigger -timestamp))

            # tdiff = np.int64((np.int64(timestamp -timestamp_trigger)& cut) -offset) 
            # if tdiff>= np.int64(0xFFFFF):
            #     tdiff-=np.int64(0xFFFFF)


            # min_diff =np.int64(1000)
            # for i_trigger in trigger_dat[hit["trigger_i"]-20:hit["trigger_i"]+20]:
            #     # timestamp = np.float64(hit["timestamp"])*np.float64(10./1.5)
            #     timestamp_trigger =  np.float64(hit["timestamp_trigger"])*np.float64(2./1.5)
            #     tdiff = np.int64(timestamp_trigger -timestamp) 
            #     if np.abs(tdiff)<= np.abs(min_diff):
            #         min_diff=tdiff
            #         matched_buffer[icluser]["timestamp_trigger"]= i_trigger["timestamp"]
                    # if np.abs(min_diff) < 10:
                        # break


            matched_buffer[icluser]["tdiff_trigger"] = tdiff
            matched_buffer[icluser]["scan_param_id"] = hit["scan_param_id"]
            
    return matched_buffer