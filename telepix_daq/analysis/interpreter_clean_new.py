import numpy as np
import numba

## not used at all
class_spec = [
    ("chunk_size", numba.uint32),
    ("wcnt", numba.uint8),
    ("used_cnt", numba.int32),
    ("mightypix_data_flag", numba.uint8),
    ("mightypix_token_timestamp", numba.int64),
    # ("rx_timestamp_flag", numba.uint8),
    # ("rx_timestamp", numba.int64),
    ("error_cnt", numba.uint32),
    # ("error_cnt_order", numba.uint32),
    ("col", numba.uint8),
    ("row", numba.uint16),
    ("ts", numba.uint32),
    # ("ts_gray", numba.uint16),
    ("ts_decoded", numba.uint16),
    ("ts2", numba.uint16),
    ("ts3", numba.uint16),
    # ("ts3_gray", numba.uint16),
    ("ts1", numba.uint32),
    # ("ts1_n", numba.uint16),
    # ("ts12", numba.uint16),
    ("tot", numba.uint16),
    ("timestamp", numba.uint64),
    ("ts_bin_cnt", numba.uint16),
    # ("meta_idx", numba.uint32),
    ("raw_idx", numba.uint32),
    ("last32bits", numba.uint32),
    ("last_comma", numba.uint32),
    ("last32bits_old", numba.uint32),
    ("hit_index", numba.int64),
    ("t_i", numba.int64),
    ("lastcommas", numba.uint8),
    ("used_info", numba.uint8),
    # ("raw4x32", numba.uint32[:]),
    ("event_number", numba.int64),
    ('hist_occ', numba.uint32[:, :, :]),
    ('hist_tot', numba.uint16[:, :, :, :]),
    ('hist_id_value', numba.uint32[:, :]),
    ('n_scan_params', numba.int32),
]

# data fromat from FPGA
#  0    1    2    3   4   5   6    7
# C0, cnt, cnt, cnt,
# C1, col, row, ts3, ts, ts, ts, ts2
# More precisely,
# {K_28_0, 8'hAA, K_28_0, 8'hAA}
# {8'hC0, timestamp_bin_counter[15:8], timestamp_bin_counter[7:0], timestamp_gray_value[7:0]};
# {8'hC1,2'd0, ColAddFromDet[4:0], RowAddFromDet[9:0], TS3FromDet[6:0]};
# {2'd0, TS12FromDet[10:0],TS11FromDet[10:0], TS2FromDet[9:0]};

"""
First we separate the data into the dataword, and the comma information to check for errors:
"""


@numba.njit
def swap_bit_order(value, n_bits):
    return_value = numba.uint8(0)
    count = 0
    while n_bits > 0:
        n_bits = n_bits - 1
        if n_bits >= count:
            return_value = return_value | ((value & 1 << n_bits) >> (n_bits - count))
        else:
            return_value = return_value | ((value & 1 << n_bits) << (count - n_bits))
        count += 1
    return return_value


@numba.njit
def get_raw_dataword(word, iraw):
    # get either the first or the second dataword
    # print("This is the raw data word", 0xFF & (word >> 8 * iraw))
    return 0xFF & (word >> 8 * iraw)




@numba.njit
def get_raw_dataword_8b(word):
    # get 8b data words out of 32 if word looks like this x = 0xa1a2a3a4
    a4 = word & 0xff
    a3 = (word >> 8) & 0xff
    a2 = (word >> 16) & 0xff
    a1 = word >> 24
    not_in_table_err=(word >> 18) & 0x3
    disp_err=(word >> 20) & 0x3
    # print(hex(word), 0xFF & a1)
    return (a1,a2,a3,a4,disp_err,not_in_table_err)

# @numba.njit
# def get_raw_commaword(word, iraw):
#     # get the comma words just to check
#     return ( ((word >> 16) & 0x3) >> iraw) & 0x1

@numba.njit
def get_raw_commaword(word,iraw):
    # get the comma words just to check
    return (((word >> 24) & 0x3) >> iraw) & 0x1

@numba.njit
def get_wcnt(word):
    # get the wcnt for the current dw
    return ((word >> 24) & 0xF)
"""
There are states where the chip sends out data:
lc2 (Load column 2)
lp2 (Load pixel 2)
rc2 (Read column 2)
rc4 (Read column 4)
"""


@numba.njit
def check_last_commas(comma):
    # this is mainly in a function to use njit!
    return comma != 0x0


@numba.njit
def check_last_commas_first_bit(comma):
    # this is mainly in a function to use njit!
    return comma == 0x8


@numba.njit
def is_mightypix_data_lc2(word):
    # Load column 2
    # this should be filtered out by the fpga!
    return (word >> 24 & 0xFF) == 0x1C


@numba.njit
def is_mightypix_data_lp2(word):
    # Load pixel 2
    # print("lp2",hex(((word  >> 24 )& 0xFF) ))
    return ((word  >> 24 )& 0xFF) == 0xC0


@numba.njit
def is_mightypix_data_rc2(word):
    # Read column 2
    # print("rc2",hex(((word  >> 24 )& 0xFF)) )
    return (word >> 24 & 0xFF) == 0xC1


@numba.njit
def is_mightypix_data_rc4(word):
    # Read column 4
    # print("rc4",hex((word >> 30) & 0x3) )
    return (word >> 30) & 0x3 == 0x0


@numba.njit
def is_mightypix_data_sc1(word):
    # Send Counter 1
    return (word >> 24) & 0xFF == 0x3C


@numba.njit
def get_ts_bin_cnt(word):
    return (word >> 8) & 0xFFFF


@numba.njit
def get_ts_bin_cnt_sc1(word):
    word = get_ts_bin_cnt(word)
    return (word >> 8) & 0xFF + (word & 0xFF << 8)


@numba.njit
def get_ts_gray(word):
    return word & 0xFF


@numba.njit
def get_col(word):
    col =  (word >> 17) & 0x7F
    return 0x7F & (~col)


@numba.njit
def get_row(word):
    # Read column 2 DataOut <= {8'hC1,2'd0, ColAddFromDet[4:0], RowAddFromDet[9:0], TS3FromDet[6:0]};
    row =  (word >> 7) & 0x01FF
    return 0x01FF & (~row)


@numba.njit
def get_ts3(word):
    return word & 0x7F


# @numba.njit
# def get_ts11(word):
#     return (word & 10) & 0x3FF

# @numba.njit
# def get_ts12(word):
#     return (word >> 20) & 0x3FF

@numba.njit
def get_ts1(word):
    # print("data", word) 12 bit
    ts=(word >> 10) & 0x7FFFF
    nedge = word >> 29 & 0x1
    return (ts<<1) | nedge
    # return (word >> 12) & 0xFFF

@numba.njit
def get_ts1_n(word):
    # print("data", word)
    # return (word >> 10) & 0xFFFFF
    return (word >> 4) & 0xFFF


@numba.njit
def get_ts2(word):
    return word & 0x3FF


@numba.njit
def get_tot(word):
    return (((word >> 9) & 0x3F) - ((word >> 15) & 0x3F)) & 0x3F


@numba.njit
def get_noise_flag(word):
    return (word) & 0x3


@numba.njit
def get_token_TS_right(word):
    return (word) & 0x1FFFFFC


@numba.njit
def get_token_TS_left(word):
    return (word) & 0x1FFFFFF


"""
Functions to identify metadata
"""

@numba.njit
def is_tlu_data(word):
    return  (word>>31) == 0x1
    # return (word >> 23) == 0x1

@numba.njit
def is_FPGA_timestamp(word):
    return  ((word>>24) &0xF == 0xB) or ((word>>24) &0xF == 0xA)
    # return (word >> 23) == 0x1

@numba.njit
def is_FPGA_ts_first(word):
    return  ((word>>24) &0xF == 0xB)

@numba.njit
def get_timestamp_22bits(word):
    return word & 0x3FFFFF


@numba.njit
def is_tlu(word):
    return (word) & 0x80000000 == 0x80000000


@numba.njit
def get_tlu_number(word):
    return word & 0xFFFF


@numba.njit
def get_tlu_timestamp(word):
    return word & 0x7FFF0000


# @numba.njit
# def gray_decode(i):
#     ret = numba.uint8(0)
#     while i != 0:
#         ret = ret ^ i
#         i = i >> 1
#     return ret



@numba.njit
def decode_gray(gray, nbits):
    binary = gray & (1 << (nbits - 1))
    for i in range(nbits - 2, -1, -1):
        binary = binary | ((gray ^ (binary >> 1)) & (1 << i))
    return binary


@numba.experimental.jitclass(class_spec)
class RawDataInterpreter(object):
    def __init__(self, n_scan_params=1):
        self.error_cnt = 0
        # self.error_cnt_order = 0
        self.raw_idx = -1
        # self.meta_idx = 0
        # self.raw4x32 = np.zeros(4, dtype=np.uint32)
        self.used_cnt=0
        self.t_i = 0
        self.row=0
        self.col=0
        self.ts3=0
        # self.event_number=0

        self.n_scan_params = n_scan_params
        self.hist_occ = np.zeros((120, 400, self.n_scan_params), dtype=np.uint32)
        # self.hist_tot = 1
        self.hist_tot = np.zeros((120, 400, self.n_scan_params, 1024), dtype=np.uint16)
        self.hist_id_value = np.zeros((4, int(16777216/100000)), dtype=np.uint32)


        self.reset()

    def interpret(self, raw_data, hit_data, ts_data, scan_param_id=0):
        # hit_index = 0

        """
        The data comes from the fpga in two 32bit words that contain one 32bit data word and additional information:
        Original data send:
        Sync DataOut <= {8'hBC, 8'hBC, 8'hBC, 8'hBC};
        CommaOut <= {1'b1,1'b1,1'b1,1'b1};
        Send counter 1 DataOut <= {K_28_1,timestamp_bin_counter[7:0] , timestamp_bin_counter[15:8], 8'd0};
        CommaOut <= 4'b1_0_0_0;
        Load column 2 DataOut <= {8'h1C, 8'hAA, 8'h1C, 8'hAA};
        CommaOut <= {1'b1,1'b0,1'b1,1'b0};
        Load pixel 2 DataOut <= {8'hC0, timestamp_bin_counter[15:8], timestamp_bin_counter[7:0], timestamp_gray_value[7:0]};
        CommaOut <= 4'b0_0_0_0;
        Read column 2 DataOut <= {8'hC1,2'd0, ColAddFromDet[4:0], RowAddFromDet[9:0], TS3FromDet[6:0]};
        CommaOut <= {1'b0,1'b0,1'b0,1'b0};
        Read column 4 DataOut <= {2'd0, TSFromDet[19:0], TS2FromDet[9:0];
        CommaOut <= {1'b0,1'b0,1'b0,1'b0};
        In all other cases, the data out register is filled with following pattern
        DataOut <= {8'hBC, 8'hBC, 8'hBC, 8'hBC};
        CommaOut <= {1'b1,1'b1,1'b1,1'b1};

        this will be send in chunks:
        {5'b0, last_dw_lc2, data_dec_k_in[0], data_dec_k_in[1], data_dec_in[0], data_dec_in[1]};
        or
        {8'b meta, 8'b dw0 , 8'bdw1}
        the 8b meta data is: 5'b0, bit if last dw was a load column 2, comma word for dw0, comma word for dw1

        or it will send a 23bit timesamp from the fpga:
        {1'b1,23'b fpga_clock_counter_buf}
        """

        # hit_index = np.dtype('<u8')
        self.hit_index = 0
        self.t_i = 0
        self.used_cnt=0
        debug=True
        debug=False
        for raw_data_word in raw_data:

            self._fill_debug_hist(0, 0)
            self.raw_idx += 1
            if debug:
                input()
                print("raw dw:",hex(raw_data_word),"  ",hex(raw_data_word& 0xFF_FFFF))
            
            ##################
            # FPGA timestamp DATA #
            ##################
            if is_FPGA_timestamp(raw_data_word):
                if debug:
                    print("is ts")
                if is_FPGA_ts_first(raw_data_word):
                    # self._fill_debug_hist(1, np.uint32(raw_data_word & 0x00FF_FFFF))
                    if debug:
                        print("is ts first")
                    # if self.t_i>0:
                        # ts_data[self.t_i]['timestamp'] = ts_data[self.t_i-1]['timestamp']
                    # ts_data[self.t_i]['timestamp'] = np.uint64(raw_data_word & 0x00FF_FFFF)
                    ts_data[self.t_i]['timestamp'] = np.int64(raw_data_word & 0x00FF_FFFF) << 24
                    ts_data[self.t_i]['event_number'] = self.hit_index
                    ts_data[self.t_i]['scan_param_id'] = scan_param_id
                    self.t_i = self.t_i + 1
                else:
                    # self._fill_debug_hist(2, np.uint32(raw_data_word & 0x00FF_FFFF))
                    if debug:
                        print("is ts second")
                    ts_data[self.t_i]['timestamp'] = np.uint64(raw_data_word & 0x00FF_FFFF)


            ##################
            # DUT DATA #
            ##################
            else:
                if debug:
                    print("is data")
                # self._fill_debug_hist(3, np.uint32(raw_data_word & 0x00FF_FFFF))
                # since the important data can start either at the first or second dataword lets make a for loop for that.
                # This might be not good for timing, but something for the future to investigate
                # 32b contain 4b info, 24b data word (3x8b), 4b data identifier
                self.wcnt=get_wcnt(raw_data_word)

                # print("wcnt", self.wcnt)
                self.last32bits_old  = 0
                
                for iraw in range(2, -1, -1):
                    #this line shifts the variable last32bits by 8 places and adds the new data word by bitwise OR
                    self.last32bits = 0xFFFFFFFF & ( get_raw_dataword(raw_data_word, iraw) | self.last32bits << 8 )
                    self.last_comma = 0xF & (get_raw_commaword(raw_data_word,iraw) | self.last_comma<<1)
                    
                    if self.last32bits == 0:
                        continue
                    self.used_cnt=self.used_cnt-1
                    if self.used_cnt>0:
                        continue
                    if self.last32bits_old == self.last32bits:
                        continue
                    if self.last_comma:
                        continue
                    else:
                        
                        # check word cnter
                        # info = get_wcnt(raw_data_word)
                        # if info == 0xE:
                        #     info == 0
                        # check counter mode 
                        if is_mightypix_data_sc1(self.last32bits):
                            if debug:
                                print("Sc1")
                            self.ts_bin_cnt = get_ts_bin_cnt_sc1(self.last32bits)
                            self.used_info = self.used_info | 0x8
                            # self.raw4x32[3] = self.last32bits
                            self.used_cnt=4
                        elif is_mightypix_data_lp2(self.last32bits) and not self.last_comma:
                            # Load pixel 2 DataOut <= {8'hC0, timestamp_bin_counter[15:8], timestamp_bin_counter[7:0], timestamp_gray_value[7:0]};
                            # CommaOut <= 4'b0_0_0_0;
                            # if self.mightypix_data_flag != 0 and self.mightypix_data_flag != 3:
                            #     self.reset()
                            #     self.error_cnt += 1
                            #     continue
                            if debug:
                                print("lp2")
                            self.ts_bin_cnt = get_ts_bin_cnt(self.last32bits)
                            # self.ts_gray = get_ts_gray(self.last32bits)
                            self.ts_decoded = decode_gray(get_ts_gray(self.last32bits), 8)
                            self.mightypix_data_flag = 1
                            # self.raw4x32[0] = self.last32bits
                            self.used_info = self.used_info | 0x1
                            self.used_cnt=4

                        elif is_mightypix_data_rc2(self.last32bits) and not self.last_comma:
                            if debug:
                                print("lc2")
                            # Read column 2 DataOut <= {8'hC1,2'd0, ColAddFromDet[4:0], RowAddFromDet[9:0], TS3FromDet[6:0]};
                            # CommaOut <= {1'b0,1'b0,1'b0,1'b0};
                            # if self.mightypix_data_flag != 1 and self.mightypix_data_flag != 3:
                            #     self.reset()
                            #     self.error_cnt += 1
                            #     continue
                            self.col = get_col(self.last32bits)
                            self.row = get_row(self.last32bits)
                            # self.ts3_gray = get_ts3(self.last32bits)
                            self.ts3 = decode_gray(get_ts3(self.last32bits), 7)
                            self.mightypix_data_flag = 2
                            # self.raw4x32[1] = self.last32bits
                            self.used_info = self.used_info | 0x2
                            self.used_cnt=4

                        elif is_mightypix_data_rc4(self.last32bits) and not self.last_comma:
                            if debug:
                                print("rc4!!!!!!!!!!!!!", hex(self.last32bits))
                            # Read column 4 DataOut <= {2'd0, TSFromDet[19:0], TS2FromDet[9:0]}
                            # CommaOut <= {1'b0,1'b0,1'b0,1'b0};
                            # if self.mightypix_data_flag != 2:
                            #     self.reset()
                            #     self.error_cnt += 1
                            #     continue

                            self.ts1 = decode_gray(get_ts1(self.last32bits), 20)
                            # self.ts1_n = decode_gray(get_ts1_n(self.last32bits), 12)
                            self.ts2 = decode_gray(get_ts2(self.last32bits), 10)
                            self.tot = (self.ts2 - ((self.ts1>>1)& 0x3FF) ) & 0x3FF
                            # print("ts1")
                            # print(get_ts1(self.last32bits))
                            # print(self.ts1)
                            # print("ts2")
                            # print(get_ts2(self.last32bits))
                            # print(self.ts2)
                            self.used_info = self.used_info | 0x4
                            # self.raw4x32[2] = self.last32bits
                            self.event_number = self.hit_index

                            if self.row == 0 and self.col == 0 and self.event_number == 0:
                                # print( "zeros detected", self.event_number, self.col, self.row)
                                continue

                            hit_data[self.hit_index]["event_number"] = self.hit_index
                            hit_data[self.hit_index]["col"] = self.col
                            # if self.col == 128:
                            #     print(self.last32bits_old , self.last32bits)
                            hit_data[self.hit_index]["row"] = self.row
                            # hit_data[self.hit_index]["ts11"] = self.ts11
                            # hit_data[self.hit_index]["ts12"] = self.ts12
                            hit_data[self.hit_index]["ts1"] = self.ts1
                            hit_data[self.hit_index]["ts2"] = self.ts2
                            hit_data[self.hit_index]["ts3"] = self.ts3
                            hit_data[self.hit_index]["tot"] = self.tot
                            hit_data[self.hit_index]['timestamp'] = ts_data[self.t_i]['timestamp']
                            # hit_data[self.hit_index]['err'] = info 
                            hit_data[self.hit_index]["scan_param_id"] = scan_param_id
                        
                        
                            self.hit_index += 1
                            self.used_info = 0x0
                        
                           
                            self.mightypix_data_flag = 3
                            self.used_cnt=4
                            # print(self.ts2 - self.ts1)
                            # print(self.col, self.row, self.tot, scan_param_id)
                            self._fill_hist(self.col, self.row, self.tot, scan_param_id)

                        # else:
                        #     print(f"could not interpret data word: {hex(self.last32bits)}, comma word {bin(self.last_comma)}")
                        self.last32bits_old = self.last32bits
        
        # Trim hit_data buffer to interpreted data hits
        hit_data = hit_data[:self.hit_index]
        if self.t_i<0:
            self.t_i=0
        # ts_data = ts_data[:self.t_i]
        ts_data = ts_data[:self.t_i]
        return hit_data, ts_data

    def get_histograms(self):
        return self.hist_occ, self.hist_tot

    # def get_n_triggers(self):
    #     return self.n_triggers

    def get_n_tdc(self):
        return self.n_tdc

    def reset(self):
        
        # these are the temporary storage binaries to build that 32 bits that we need
        self.last32bits = 0
        self.last_comma = 0

        # Data from the chip
        self.mightypix_data_flag = 0
        self.mightypix_token_timestamp = 0

        # Timestamp from RX1 sampled in FPGA
        # self.rx_timestamp = 0
        self.used_info = 0
        self.used_cnt=0


    def get_error_count(self):
        return self.error_cnt
    
    def _fill_debug_hist(self,id,value):
        self.hist_id_value[id,int(value/100000)] +=1

    def _fill_hist(self, col, row, tot, scan_param_id):
        # print(col, row, scan_param_id)
        if col< 120 and row< 400:
            self.hist_occ[col, row, scan_param_id] += 1
            self.hist_tot[col, row, scan_param_id, tot] += 1

    def reset_hist(self):
        self.hist_occ = np.zeros((120, 400, self.n_scan_params), dtype=np.uint32)
        self.hist_tot = np.zeros((120, 400, self.n_scan_params, 1024), dtype=np.uint16)