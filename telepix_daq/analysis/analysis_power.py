import h5py
import numpy as np
from collections import defaultdict
from matplotlib.figure import Figure

'''
sorted data i a dict with following structure
 {scan_id: [[vd, d_vd], [va, d_va], [id, del_id], [ia_del_ia], [power, del_power]]}
'''

def cal_power(data):
    vd = data[0][0]
    del_vd = data[0][1]
    va = data[1][0]
    del_va = data[1][1]
    id = data[2][0]
    del_id = data[2][1]
    ia = data[3][0]
    del_ia =  data[3][1]


    power= va*ia + vd*id
    dpower = np.sqrt((del_va*ia)**2 + (del_vd*id)**2+(va*del_ia)**2 + (vd*del_id)**2)

    return power, dpower


def get_results(temp_table):

    param_id = np.array(temp_table[:]['scan_param_id'])
    vddd = np.array(temp_table[:]['voltage_channel_1'])
    vdda = np.array(temp_table[:]['voltage_channel_2'])
    iddd = np.array(temp_table[:]['current_channel_1'])
    idda = np.array(temp_table[:]['current_channel_2'])
    hv_current = np.array(temp_table[:]['currentHV_channel_2'])


    #summarize values with same scan id
    cut_indices = np.where(param_id[:-1] != param_id[1:])[0] + 1
    id_split = np.split(param_id, cut_indices)
    # print(id_split)
    vddd_split = np.split(vddd, cut_indices)
    vdda_split = np.split(vdda, cut_indices)
    iddd_split = np.split(iddd, cut_indices)
    idda_split = np.split(idda, cut_indices)
    hv_split = np.split(hv_current, cut_indices)


    # mean & std dev of all values corresponding to a unique scan id
    def calc_mean_and_std_dev(data_values):
        mean_list = [] 
        std_dev_list = []
        id_data = {}  

        for section_id, section in enumerate(id_split):
            unique_ids = np.unique(section)
            for id in unique_ids:
                # data_values = data_values_all[section_id][section == id]
                mean = np.mean(data_values[id])  
                std_dev = np.std(data_values[id])
                mean_list.append(mean)
                std_dev_list.append(std_dev)
                id_data[id] = [mean, std_dev]
 
        return id_data


    # calculate mean, std, sort data, plotting
    vd = calc_mean_and_std_dev(vddd_split)
    va = calc_mean_and_std_dev(vdda_split)
    id = calc_mean_and_std_dev(iddd_split)
    ia = calc_mean_and_std_dev(idda_split)
    i_hv = calc_mean_and_std_dev(hv_split)

    power_dac_data = [vd, va, id, ia, i_hv]   
    sorted_dac_data = defaultdict(list)
    for id in power_dac_data:
        for key, value in id.items():
            sorted_dac_data[key].append(value)

    sorted_dac_data = dict(sorted_dac_data)

    for scan_id in sorted_dac_data:
        power, del_power = cal_power(sorted_dac_data[scan_id])
        sorted_dac_data[scan_id].append([power, del_power])

    return sorted_dac_data
