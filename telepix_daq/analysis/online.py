#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Online data analysis functions
'''

import ctypes
import logging
import multiprocessing
import time
import queue
from functools import reduce

import numpy as np
import numba

from telepix_daq.analysis import analysis_utils as au
from telepix_daq.analysis.interpreter_clean import get_raw_dataword, get_raw_commaword, is_mightypix_data_rc2, is_mightypix_data_rc4, get_col, get_row

logger = logging.getLogger('OnlineAnalysis')


@numba.njit(cache=True, fastmath=True)
def gray2bin(gray):
    b6 = gray & 0x40
    b5 = (gray & 0x20) ^ (b6 >> 1)
    b4 = (gray & 0x10) ^ (b5 >> 1)
    b3 = (gray & 0x08) ^ (b4 >> 1)
    b2 = (gray & 0x04) ^ (b3 >> 1)
    b1 = (gray & 0x02) ^ (b2 >> 1)
    b0 = (gray & 0x01) ^ (b1 >> 1)
    return b6 + b5 + b4 + b3 + b2 + b1 + b0


@numba.njit(cache=True, fastmath=True)
def histogram(raw_data, hist, hit_data):
    ''' Raw data to 2D occupancy histogram '''
    mightypix_data_flag = 0
    last32bits = 0
    last_comma = 0

    for word in raw_data:
        for iraw in range(2, -1, -1):
            #this line shifts the variable last32bits by 8 places and adds the new data word by bitwise OR
            last32bits = 0xFFFFFFFF & ( get_raw_dataword(word, iraw) | last32bits << 8 )
            last_comma = 0xF & (get_raw_commaword(word,iraw) | last_comma<<1)
            if last32bits == 0:
                continue

            "check only for rc2 & rc4 flag because we are only interested in col & row"
            if is_mightypix_data_rc2(last32bits) and not last_comma:
                mightypix_data_flag = 2
                col = get_row(last32bits)
                row = get_row(last32bits)
            elif is_mightypix_data_rc4(last32bits) and not last_comma:
                if mightypix_data_flag != 2:
                    continue
                print(hit_data)
                print(hit_data[0])
                print(type(hit_data[0]["col"]), type(col), col)
                hit_data[0]["col"] = col
                hit_data[0]["row"] = row

                # Hit is complete, add to histogram
                if hit_data[0]['col'] < 120 and hit_data[0]['row'] < 400:
                    hist[hit_data[0]['col'], hit_data[0]['row']] += 1
        
    return raw_data, hist, hit_data

    # interpreter = RawDataInterpreter(n_scan_params=1)
    # hit_buffer = np.zeros(shape=len(raw_data), dtype=au.hit_dtype)
    # ts_buffer = np.zeros(shape=len(raw_data), dtype=au.ts_dtype)
    # hit_dat, ts_dat = interpreter.interpret(
    #     raw_data,
    #     hit_buffer,
    #     ts_buffer,
    #     0
    # )
    # print(hit_dat)
    # hist = interpreter.get_histograms() 
    # return raw_data, hist, hit_dat


class OnlineHistogrammingBase():
    ''' Base class to do online analysis with raw data from chip.

        The output data is a histogram of a given shape.
    '''
    _queue_timeout = 0.01  # max blocking time to delete object [s]

    def __init__(self, shape):
        self._raw_data_queue = multiprocessing.Queue()
        self.stop = multiprocessing.Event()
        self.lock = multiprocessing.Lock()
        self.last_add = None  # time of last add to queue
        self.shape = shape
        self.analysis_function_kwargs = {}
        self.p = None  # process

    def init(self):
        # Create shared memory 32 bit unsigned int numpy array
        n_values = reduce(lambda x, y: x * y, self.shape)
        shared_array_base = multiprocessing.Array(ctypes.c_uint, n_values)
        shared_array = np.ctypeslib.as_array(shared_array_base.get_obj())
        self.hist = shared_array.reshape(*self.shape)
        self.idle_worker = multiprocessing.Event()
        self.p = multiprocessing.Process(target=self.worker,
                                         args=(self._raw_data_queue, shared_array_base,
                                               self.lock, self.stop, self.idle_worker))
        self.p.start()
        logger.info('Starting process %d', self.p.pid)

    def analysis_function(self, raw_data, hist, *args):
        raise NotImplementedError("You have to implement the analysis_funtion")

    def add(self, raw_data, meta_data=None):
        ''' Add raw data to be histogrammed '''
        self.last_add = time.time()  # time of last add to queue
        self.idle_worker.clear()  # after addding data worker cannot be idle
        if meta_data is None:
            self._raw_data_queue.put(raw_data)
        else:
            self._raw_data_queue.put([raw_data, meta_data])

    def _reset_hist(self):
        with self.lock:
            self.hist = self.hist.reshape(-1)
            for i in range(self.hist.shape[0]):
                self.hist[i] = 0
            self.hist = self.hist.reshape(self.shape)

    def reset(self, wait=True, timeout=0.5):
        ''' Reset histogram '''
        if not wait:
            if not self._raw_data_queue.empty() or not self.idle_worker.is_set():
                logger.warning('Resetting histogram while filling data')
        else:
            if not self.idle_worker.wait(timeout):
                logger.warning('Resetting histogram while filling data')
        self._reset_hist()

    def get(self, wait=True, timeout=None, reset=True):
        ''' Get the result histogram '''
        if not wait:
            if not self._raw_data_queue.empty() or not self.idle_worker.is_set():
                logger.warning('Getting histogram while analyzing data')
        else:
            if not self.idle_worker.wait(timeout):
                logger.warning('Getting histogram while analyzing data. Consider increasing the timeout.')

        if reset:
            hist = self.hist.copy()
            # No overwrite with a new zero array due to shared memory
            self._reset_hist()
            return hist
        else:
            return self.hist

    def worker(self, raw_data_queue, shared_array_base, lock, stop, idle):
        ''' Histogramming in seperate process '''
        hist = np.ctypeslib.as_array(shared_array_base.get_obj()).reshape(self.shape)
        while not stop.is_set():
            try:
                data = raw_data_queue.get(timeout=self._queue_timeout)
                idle.clear()
                with lock:
                    return_values = self.analysis_function(data, hist, **self.analysis_function_kwargs)
                    self.analysis_function_kwargs.update(zip(self.analysis_function_kwargs, return_values))
            except queue.Empty:
                idle.set()
                continue
            except KeyboardInterrupt:  # Need to catch KeyboardInterrupt from main process
                stop.set()
        idle.set()

    def close(self):
        ''' Close process and wait till done. Likely needed to give access to pytable file handle.'''
        logger.info('Stopping process %d', self.p.pid)
        self._raw_data_queue.close()
        self._raw_data_queue.join_thread()  # Needed otherwise IOError: [Errno 232] The pipe is being closed
        self.stop.set()
        self.p.join()
        del self.p  # explicit delete required to free memory
        self.p = None

    def __del__(self):
        if self.p and self.p.is_alive():
            logger.warning('Process still running. Was close() called?')
            self.close()


class OccupancyHistogramming(OnlineHistogrammingBase):
    ''' Fast histogramming of raw data to a 2D hit histogramm

        No event building.
    '''

    def __init__(self):
        super().__init__(shape=(120, 400))
        # self.analysis_function_kwargs = {'hit_data': np.zeros(1, dtype=au.hit_dtype), 'is_sof': -1, 'is_eof': -1, 'tj_data_flag': 0}
        self.analysis_function_kwargs = {'hit_data': np.zeros(1, dtype=au.hit_dtype)}

        # def analysis_function(self, raw_data, hist, hit_data, is_sof, is_eof, tj_data_flag):
        #     return histogram(raw_data, hist, hit_data, is_sof, is_eof, tj_data_flag)
        def analysis_function(self, raw_data, hist, hit_data):
            return histogram(raw_data, hist, hit_data)
        setattr(OccupancyHistogramming, 'analysis_function', analysis_function)

        self.init()