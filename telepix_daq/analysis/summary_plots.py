import numpy as np 
import matplotlib.pyplot as plt 
from scipy.optimize import curve_fit
import tables as tb

from collections import OrderedDict
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas

from telepix_daq.analysis import analysis_power as ap
from telepix_daq.system import logger
from telepix_daq.analysis import analysis_utils as au
import matplotlib.colors as mcolors



SCURVE_CHI2_red_UPPER_LIMIT = 5
TITLE_COLOR = '#07529a'
OVERTEXT_COLOR = '#07529a'

DACS = {'TelePixV2': ['ThPix', ]
        }

TABLEAU_COLOR = []
for i in mcolors.TABLEAU_COLORS:
    TABLEAU_COLOR.append(mcolors.TABLEAU_COLORS[i])


class Comparison_plots(object):
    def __init__(self, analyzed_data_files, pdf_file=None, comparison_type = None, level='preliminary', mask_noisy_pixels=False, internal=False, save_single_pdf=False, save_png=False):
        self.log = logger.setup_derived_logger('Comparison_plots')

        self.plot_cnt = 0
        self.save_png = save_png
        self.save_single_pdf = save_single_pdf

        self.internal = internal
        self._module_type = None
        self.level = level

        self.mask_noisy_pixels = mask_noisy_pixels
        self.analyzed_data_files = analyzed_data_files
        self.n_data_files = len(analyzed_data_files)
        if comparison_type != None:
            if len(comparison_type) != len(analyzed_data_files):
                self.comparison_type = []
                self.log.warning('Size of comparison type and analyzed data files not matched!')
                for i in range(self.n_data_files):
                    self.comparison_type.append(f'Chip_{i}')
            else:
                self.comparison_type = comparison_type
        else:
            for i in range(self.n_data_files):
                self.comparison_type.append(f'Chip_{i}')



        
        
        
        if pdf_file is None:
            self.filename = ''
            for i,analyzed_df in enumerate(self.analyzed_data_files):
                if i == 0:
                    for word in analyzed_df.split('/')[1:-1]:
                        self.filename += '/' + word
                    self.filename += '/'
                if i == self.n_data_files -1:
                    self.filename += analyzed_df.split('/')[-1]
                    self.filename =self.filename.split('.')[:-1][0] + '.pdf'
                else:
                    for word in analyzed_df.split('/')[-1].split('_')[:2]:
                        self.filename += word + '_'
                    self.filename +='_'
                

        else:
            self.filename = pdf_file
        self.out_file = PdfPages(self.filename)




        self.in_file = []
        self.root = []
        try:
            for data_file in analyzed_data_files:
                if isinstance(data_file,str):
                    self.in_file.append(tb.open_file(data_file, 'r'))
                    self.root.append(self.in_file[-1].root)
                else:
                    self.root.append(data_file)
        except IOError:
            self.log.warning('Interpreted data file does not exist!')



        self.cols = 120
        self.rows = 400
        self.num_pix = self.rows * self.cols
        self.plot_box_bounds = [0.5, self.cols + 0.5, self.rows + 0.5, 0.5]

        self.scan_config = []
        self.run_config = []
        self.scan_params = []
        self.HistOcc = []
        self.registers = []
        for root in self.root:
            self.scan_config.append(au.ConfigDict(root.configuration_in.scan.scan_config[:]))
            self.run_config.append(au.ConfigDict(root.configuration_in.scan.run_config[:]))
            self.HistOcc.append(root.HistOcc[:])
            self.registers.append(au.ConfigDict(root.configuration_in.chip.ChipRegisters[:]))
            try:
                self.scan_params.append(root.configuration_in.scan.scan_params[:])
            except tb.NoSuchNodeError:
                self.scan_params.append(None)


        self.enable_mask = []
        self.n_enabled_pixels = []
        for i,run_conf in enumerate(self.run_config):
            if run_conf['scan_id']:
                self.enable_mask.append(self.root[i].configuration_in.chip.masks.enable[:])
                self.n_enabled_pixels.append(np.count_nonzero(self.enable_mask[-1]))
                





        self.scan_params_range = []
        for i,scan_configs in enumerate(self.scan_config):
            self.scan_params_range.append(np.arange(scan_configs['thr_high_START'], 
                                                    scan_configs['thr_high_STOP'] + scan_configs['thr_high_STEP'],
                                                    scan_configs['thr_high_STEP']))

        


        
        if self.mask_noisy_pixels:
            noisy_pixels = []
            for histocc in self.HistOcc:
                noisy_pixels.append(np.where(histocc > self.mask_noisy_pixels))
            for i,noisy_pix in enumerate(noisy_pixels):
                for n in range(len(noisy_pix[0])):
                    self.enable_mask[i][noisy_pix[0][n] , noisy_pix[1][n]] = False
                    self.n_enabled_pixels[i] = len(self.enable_mask[self.enable_mask])
                    self.log.warning('Disabled {} noisy pixels in total.'.format(len(noisy_pixels[i][0])))




        self.AmplitudeMap = []
        self.ThresholdMap = []
        self.BkgndMap = []
        self.Chi2Map = []
        self.NoiseMap = []
        self.Chi2Sel = []
        self.n_failed_scurves = []

        for i,run_conf in enumerate(self.run_config):
            if run_conf['scan_id'] in ['threshold_scan', 'fast_threshold_scan', 'global_threshold_tuning', 'in_time_threshold_scan', 'autorange_threshold_scan', 'crosstalk_scan', "pixel_thr_scan"]:
                self.AmplitudeMap.append(self.root[i].AmplitudeMap[: , :]) 
                self.ThresholdMap.append(self.root[i].ThresholdMap[: , :]) 
                self.BkgndMap.append(self.root[i].BkgndMap[: , :]) 
                self.Chi2Map.append(self.root[i].Chi2Map[: , :]) 
                self.NoiseMap.append(self.root[i].NoiseMap[:]) 
                self.Chi2Sel.append((self.Chi2Map[-1] > 0) & (self.Chi2Map[-1] < SCURVE_CHI2_red_UPPER_LIMIT) & (self.enable_mask[i]))
                self.n_failed_scurves.append(self.n_enabled_pixels[i] - len(self.Chi2Map[-1][self.Chi2Sel[-1]]))                






        try:
            for in_file in self.in_file:
                in_file.close()
        except Exception:
            pass

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if self.out_file is not None and isinstance(self.out_file, PdfPages):
            self.log.info('Closing output PDF file: {0}'.format(self.out_file._file.fh.name))
            self.out_file.close()
            # shutil.copyfile(self.filename, os.path.join(os.path.split(self.filename)[0], 'last_scan.pdf'))

    def create_standard_plots(self):
        self.log.info('Creating standard plots')
        self.create_parameter_page()
        self.create_threshold_plot()
        self.create_noise_plot()


    def create_parameter_page(self):
        try:
            for n in range(self.n_data_files):
                self._plot_parameter_page(n)
        except Exception as e:
            self.log.error(f'Could not create parameter page!\n{e}')
    
    def create_threshold_plot(self, logscale=False, scan_parameter_name='Scan parameter'):
        try:
            n_enabled_pixels = ''
            for n in self.n_enabled_pixels:
                n_enabled_pixels += f'{n}' +'/'
            n_enabled_pixels = n_enabled_pixels[:-1]
            title = f'Threshold distribution for {n_enabled_pixels} enabled pixels'
            all_same_len = True
            len_range = len(self.scan_params_range[0])
            for nrange in self.scan_params_range:
                if len(nrange) != len_range:
                    all_same_len = False
            all_same = True
            if all_same_len:
                for i in range(self.n_data_files -1):
                    if False in self.scan_params_range[i] == self.scan_params_range[i+1]:
                        all_same = False
            if all_same_len == False or all_same == False: 
                plot_range = None
                diff_range = True
            else:
                plot_range = self.scan_params_range[0]
                diff_range = False
                if(plot_range[0]>plot_range[1]):
                    plot_range = np.flip(plot_range)
            scan_parameter_name = 'Threshold/V'
            save_values = False
            plot_data = []
            for i in range(self.n_data_files):
                plot_data.append(self.ThresholdMap[i][self.Chi2Sel[i]].T)
            self._plot_distributions(plot_data,
                                    plot_range=plot_range,
                                    diff_range=diff_range,
                                    # electron_axis=electron_axis,
                                    x_axis_title=scan_parameter_name,
                                    title=title,
                                    log_y=logscale,
                                    y_axis_title='# of pixels',
                                    print_failed_fits=False,
                                    suffix='threshold_distribution',
                                    save_values= save_values)
        except Exception as e:
            self.log.error('Could not create threshold plot! ({0})'.format(e))


    def create_noise_plot(self, logscale=False, scan_parameter_name='Scan parameter'):
        try:
            n_enabled_pixels = ''
            for n in self.n_enabled_pixels:
                n_enabled_pixels += f'{n}' +'/'
            n_enabled_pixels = n_enabled_pixels[:-1]
            title = f'Noise distribution for {n_enabled_pixels} enabled pixels'
            n_enabled_pixels = n_enabled_pixels[:-1]
            mask = []
            for enable_mask in self.enable_mask:
                mask.append(enable_mask.copy())
            sel = []
            for chi2map in self.Chi2Map:
                sel.append(chi2map[:] > 0.)
            for i in range(self.n_data_files):
                mask[i][~sel[i]] = True
            plot_range = None
            scan_parameter_name = 'Amplifier Noise/V'
            plot_data = []
            for i in range(self.n_data_files):
                plot_data.append(np.ma.masked_array(self.NoiseMap[i], ~mask[i]).T)

            self._plot_distributions(plot_data,
                                    title=title,
                                    plot_range=plot_range,
                                    # electron_axis=electron_axis,
                                    use_electron_offset=False,
                                    x_axis_title=scan_parameter_name,
                                    y_axis_title='# of pixels',
                                    log_y=logscale,
                                    print_failed_fits=True,
                                    suffix='noise_distribution'
            )
        except Exception as e:
            self.log.error(f'Could not create noise plot!{e}')

    

    def _plot_distributions(self, data, plot_range=None,diff_range = False,  x_axis_title=None, electron_axis=False, use_electron_offset=True, y_axis_title='# of hits', log_y=False, align='edge', title=None, print_failed_fits=False, fit_gauss=True, plot_legend=True, suffix=None, save_values = False):
        if plot_range is None and diff_range == False:
                max_values = []
                min_values = []
                mask_zero = []
                medians = []
                for dat in data:
                    max_values.append(np.amax(dat))
                    min_values.append(np.amin(dat))
                    mask_zero.append(dat > 0)
                    medians.append(np.mean(np.ma.median(dat[mask_zero[-1]])))
                diff = np.max(max_values) - np.min(min_values)
                median = np.max(medians)
                max_d = np.max(max_values)
                min_d = max_d
                for i in range(self.n_data_files):
                    if np.amin(data[i][mask_zero[i]]) < min_d:
                        min_d = np.amin(data[i][mask_zero[i]])


                if max_d > 5*median:
                    plot_range = np.arange(min_d, median * 2 , median /100)
                else:
                    plot_range = np.arange(min_d, max_d + diff /100, diff/100)
            
        hist = []
        bins = []
        for i,dat in enumerate(data):
            if diff_range:
                if i == 0:
                    tick_size = np.diff(self.scan_params_range[i])[0]
                else:
                    if tick_size < np.diff(self.scan_params_range[i])[0]:
                        tick_size = np.diff(self.scan_params_range[i])[0]
                h,b = np.histogram(np.ravel(dat), bins= np.flip(self.scan_params_range[i]))
            else:
                tick_size = np.diff(plot_range)[0]
                h,b = np.histogram(np.ravel(dat), bins= plot_range)
            hist.append(h)
            bins.append(b)
        bin_centers = []
        p0 = []
        for i in range(self.n_data_files):
            bin_centers.append([(bins[i][:-1] + bins[i][1:]) / 2][0])
            if diff_range:
                p0.append([np.amax(hist[i]), np.nanmean(bins[i]),  (max(self.scan_params_range[i]) - min(self.scan_params_range[i])) / 3])
            else:
                p0.append([np.amax(hist[i]), np.nanmean(bins[i]),  (max(plot_range) - min(plot_range)) / 3])
        coeff = []
        if fit_gauss:
            for i in range(self.n_data_files):
                try:
                    coef,_ = curve_fit(au.gauss, bin_centers[i], hist[i], p0= p0[i])
                    coeff.append(coef)
                except Exception as e:
                    coeff.append(None)
                    self.log.warning(f'Gauss fit failed!: {e}')
        else:
           for i in range(self.n_data_files):
                    coeff.append(None)
        if diff_range:
            points = []
            for i in range(self.n_data_files):
                points.append(np.linspace(min(self.scan_params_range[i]), max(self.scan_params_range[i]), 500))
        else:
            points = np.linspace(min(plot_range), max(plot_range), 500)
        gau = []
        for i,coef in enumerate(coeff):
            if coef is not None:
                if diff_range:
                    gau.append(au.gauss(points[i], *coef))
                else:
                    gau.append(au.gauss(points, *coef))

        fig = Figure()
        FigureCanvas(fig)
        ax = fig.add_subplot(111)
        self._add_text(fig) 
        for i in range(self.n_data_files):
            if coeff[i] is not None:
                ax.bar(bins[i][:-1], hist[i], width=tick_size, align=align, alpha = 0.5 , label = f'{self.comparison_type[i]}: '+ '$\\mu={0:1.3f}\\; \\sigma={1:1.3f}$'.format(abs(coeff[i][1]) , abs(coeff[i][2])))
            else:
                ax.bar(bins[i][:-1], hist[i], width=tick_size, align=align, alpha = 0.5, label = f'{self.comparison_type[i]}: Fit failed')
            if coeff[i] is not None:
                if diff_range:
                    ax.plot(points[i], gau[i],color = TABLEAU_COLOR[i])
                else:
                    ax.plot(points, gau[i],color = TABLEAU_COLOR[i])
        

        if log_y:
            if title is not None:
                title += ' (logscale)'
            ax.set_yscale('log')
        
        if diff_range:
            max_d = 0 
            min_d = 10000 # range is normaly <1.8
            for prange in self.scan_params_range:
                if max_d < np.amax(prange):
                    max_d = np.amax(prange)
                if min_d > np.amin(prange):
                    min_d = np.amin(prange)   
            ax.set_xlim(min_d, max_d)
        else:
            ax.set_xlim(min(plot_range), max(plot_range))
        ax.set_title(title, color=TITLE_COLOR)
        if x_axis_title is not None:
            ax.set_xlabel(x_axis_title)
        if y_axis_title is not None:
            ax.set_ylabel(y_axis_title)
        ax.grid(True)
        if plot_legend:
            ax.legend(loc = 'upper left',framealpha = 0.5, fontsize = 8, facecolor = 'wheat', edgecolor = 'black')
        if save_values:
            content = ''
            for i in range(self.n_data_files):
                if coeff[i] is not None:
                    content += f'{self.comparison_type[i]}: '+ 'mu={0:1.3f} sigma={1:1.3f}'.format(abs(coeff[i][1]) , abs(coeff[i][2]))
                else:
                    content += f'{self.comparison_type[i]}: Fit failed'
                if i != self.n_data_files -1:
                    content += '\n'
            self._save_text(content)
        self._save_plots(fig, suffix)

    def _save_text(self, content):
        file_dir = self.filename.replace(".pdf", ".txt")
        print(file_dir)
        with open(f"{file_dir}","w+") as f:
            f.write(f"{content}\n")
        f.close()


    def _plot_parameter_page(self, n):
        try:
            fig = Figure()
            FigureCanvas(fig)
            ax = fig.add_subplot(111)
            ax.axis('off')
            
            scan_id = self.run_config[n]['scan_id']
            run_name = self.run_config[n]['run_name']
            if self._module_type is None:
                file_type = 'chip'
                chip_sn = self.run_config[n]['chip_sn']
            else:
                file_type = 'module'
                chip_sn = self.run_config[n]['module']

            sw_ver = self.run_config[n]['software_version']
            
            
            # timestamp = datetime.datetime.strptime(' '.join(run_name.split('_')[:2]), '%Y%m%d %H%M%S')  # FIXME: Workaround while there is no timestamp saved in h5 file

            text = 'This is a TelePixV2 {0} for {1} {2} ({3}).\nRun {4}'.format(scan_id, file_type, chip_sn, self.comparison_type[n], run_name)
            # text = 'This is a TelePixV2 {0} for {1} {2}.\nRun {3} was started {4}.'.format(scan_id, file_type, chip_sn, run_name, timestamp)

            ax.text(0.01, 0.9, text, fontsize=10)
            ax.text(-0.1, -0.11, 'Software version: {0}'.format(sw_ver), fontsize=3)

            if 'maskfile' in self.scan_config[n].keys() and self.scan_config[n]['maskfile'] is not None and not self.scan_config[n]['maskfile'] == 'None':
                ax.text(0.01, -0.05, 'Maskfile:\n{0}'.format(self.scan_config[n]['maskfile']), fontsize=6)

            scan_config_dict = OrderedDict()
            dac_dict = OrderedDict()
            scan_config_trg_tdc_dict = OrderedDict()

            exclude_run_conf_items = ['scan_id', 'run_name', 'timestamp', 'chip_sn', 'software_version', 'maskfile', 'TDAC']
            run_conf_trg_tdc = ['TRIGGER_MODE', 'TRIGGER_SELECT', 'TRIGGER_INVERT', 'TRIGGER_LOW_TIMEOUT', 'TRIGGER_VETO_SELECT',
                                'TRIGGER_HANDSHAKE_ACCEPT_WAIT_CYCLES', 'DATA_FORMAT', 'EN_TLU_VETO', 'TRIGGER_DATA_DELAY',
                                'EN_WRITE_TIMESTAMP', 'EN_TRIGGER_DIST', 'EN_NO_WRITE_TRIG_ERR', 'EN_INVERT_TDC', 'EN_INVERT_TRIGGER']

            for key, value in sorted(self.scan_config[n].items()):
                if key not in (exclude_run_conf_items + run_conf_trg_tdc):
                    if key == 'module' and value.startswith('module_'):  # Nice formatting
                        value = value.split('module_')[1]
                    if key == 'trigger_pattern':  # Nice formatting
                        value = hex(value)
                    scan_config_dict[key] = value
                if key in run_conf_trg_tdc:
                    scan_config_trg_tdc_dict[key] = value
            for flavor in DACS.keys():
                dac_dict[flavor] = OrderedDict()
                for reg, value in self.registers[n].items():
                    if any(reg.startswith(dac) for dac in DACS[flavor]):
                        dac_dict[flavor][reg] = value
            tb_list = []
            
            for i in range(max(len(scan_config_dict), len(dac_dict['TelePixV2']), len(scan_config_dict))):
                try:
                    key1 = list(scan_config_dict.keys())[i]
                    value1 = scan_config_dict[key1]
                except IndexError:
                    key1 = ''
                    value1 = ''
                try:
                    key2 = list(dac_dict['TelePixV2'].keys())[i]
                    value2 = dac_dict['TelePixV2'][key2]
                except IndexError:
                    key2 = ''
                    value2 = ''

                tb_list.append([key1, value1, '', key2, value2, ''])

            widths = [0.18, 0.10, 0.03, 0.18, 0.10, 0.03]
            labels = ['Scan config', 'Value', '', 'TelePixV2 config', 'Value', '']

            table = ax.table(cellText=tb_list, colWidths=widths, colLabels=labels, cellLoc='left', loc='center')
            table.scale(0.8, 0.8)
            table.auto_set_font_size(False)
            

            for key, cell in table.get_celld().items():
                cell.set_fontsize(3.5)
                row, col = key
                if row == 0:
                    cell.set_color('#ffb300')
                    cell.set_fontsize(5)
                if col in [2, 5]:
                    cell.set_color('white')
                if col in [1, 4, 7, 10, 13]:
                    cell._loc = 'center'
            self._save_plots(fig, suffix=f'{self.comparison_type[n]}: parameter_page')
        except Exception as e:
            print(e)

    
    def _save_plots(self, fig, suffix=None, tight=False):
        increase_count = False
        bbox_inches = 'tight' if tight else ''
        if suffix is None:
            suffix = str(self.plot_cnt)
        self.out_file.savefig(fig, bbox_inches=bbox_inches)
        if self.save_png:
            fig.savefig(self.filename[:-4] + '_' + suffix + '.png', bbox_inches=bbox_inches)
            increase_count = True
        if self.save_single_pdf:
            fig.savefig(self.filename[:-4] + '_' + suffix + '.pdf', bbox_inches=bbox_inches)
            increase_count = True
        if increase_count:
            self.plot_cnt += 1
    

    def _add_text(self, fig):
        fig.subplots_adjust(top=0.85)
        y_coord = 0.92
        fig.text(0.1, y_coord, '{0} {1}'.format("TelePixV2", self.level), fontsize=12, color=OVERTEXT_COLOR, transform=fig.transFigure)
        # if self._module_type is None:
            # for i,run_config in enumerate(self.run_config):
                # print(i)
                # if i is not self.n_data_files-1:
                    # if i == 0:
                        # module_text = 'Chip S/N: {0}\n'.format(run_config['chip_sn'])
                    # else:
                        # module_text += 'Chip S/N: {0}\n'.format(run_config['chip_sn'])
                # else:
                    # module_text += 'Chip S/N: {0}'.format(run_config['chip_sn'])
        # else:
            # module_text = 'Module: {0}'.format(self.module_settings['identifier'])
        # fig.text(0.7, y_coord, module_text, fontsize=12, color=OVERTEXT_COLOR, transform=fig.transFigure)
        # if self.internal:
            # fig.text(0.1, 1, 'Internal', fontsize=16, color='r', rotation=45, bbox=dict(boxstyle='round', facecolor='white', edgecolor='red', alpha=0.7), transform=fig.transFigure)

        