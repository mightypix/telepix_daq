import h5py
import os
from telepix_daq.analysis import plotting
from telepix_daq.analysis import analysis
import numpy as np
import tables

##FIXME: Does not work as I would like...
'''
    Need a better search within h5 file because there are also subfolders containing again different data formats -> this is not catched at the moment
'''

def merge_or_concatenate_datasets(source, target):
    for key, item in source.items():
        if isinstance(item, h5py.Dataset):
            try:
                if key in target:
                    target_item = target[key]

                    # Check if the dataset is chunked or compressed (CArray-like behavior)
                    if item.chunks or item.compression:  # Treat this like a CArray
                        combined_data = np.concatenate((target_item[:], item[:]), axis=0)
                        
                        # Resize and update the dataset
                        target_item.resize(combined_data.shape)
                        target_item[:] = combined_data
                        # print(f"Concatenated chunked/compressed dataset '{key}' from source to target.")
                    
                    elif item.dtype.names:  # Compound data type
                        combined_data = {}
                        for field in item.dtype.names:
                            combined_data[field] = np.concatenate((target_item[field][:], item[field][:]), axis=0)
                        
                        # Resize and update the dataset
                        target_item.resize((len(combined_data[item.dtype.names[0]]),))
                        for field in item.dtype.names:
                            target_item[field] = combined_data[field]

                    else:  # Regular non-compound, non-chunked dataset
                        combined_data = np.concatenate((target_item[:], item[:]), axis=0)
                        target_item.resize(combined_data.shape)
                        target_item[:] = combined_data
                    # print(f"Concatenated dataset '{key}' from source to target.")
                else:
                    # print(f"Copying dataset '{key}' to target.")
                    target.copy(item, key)

            except Exception as e:
                print(f"Error accessing dataset '{key}': {e}")
                
        elif isinstance(item, h5py.Group):
            # Recursively handle groups
            if key not in target:
                # print(f"Creating group '{key}' in target.")
                target.create_group(key)
            
            # Recursively merge datasets inside nested groups
            merge_or_concatenate_datasets(item, target[key])
            # print(f"Finished handling group '{key}'.")
            
        else:
            print(f"Skipping non-dataset item '{key}'.")


def combine_h5_files(file_list, outputdir, output_file):
    output_path = os.path.join(outputdir, output_file)

    with h5py.File(output_path, 'w') as h5_out:
        for file in file_list:
            with h5py.File(file, 'r') as h5_in:
                merge_or_concatenate_datasets(h5_in, h5_out)

if __name__ == "__main__":
    # List of input HDF5 files you want to combine
    h5_files = [
        '/home/lhcb/daq/telepix_daq/telepix_daq/output_data/module_0/chip_09/2024_September/120V/cool_box/0degC/thr_scan/module_0/chip_09/AllPixel/20240919_212850_pixel_thr_scan.h5',
        '/home/lhcb/daq/telepix_daq/telepix_daq/output_data/module_0/chip_09/2024_September/120V/cool_box/0degC/thr_scan/module_0/chip_09/AllPixel/20240919_212243_pixel_thr_scan.h5',
        # Add paths to other files here
    ]
    # h5_files = [
    #     '/home/lhcb/daq/telepix_daq/telepix_daq/output_data/module_0/chip_09/2024_September/120V/cool_box/0degC/thr_scan/module_0/chip_09/AllPixel/20240919_212850_pixel_thr_scan_interpreted.h5',
    #     '/home/lhcb/daq/telepix_daq/telepix_daq/output_data/module_0/chip_09/2024_September/120V/cool_box/0degC/thr_scan/module_0/chip_09/AllPixel/20240919_212243_pixel_thr_scan_interpreted.h5',
    #     # Add paths to other files here
    # ]
    
    # Output combined HDF5 file
    output_dir = '/home/lhcb/daq/telepix_daq/telepix_daq/output_data/module_0/chip_09/2024_September/120V/cool_box/0degC/thr_scan/module_0/chip_09/AllPixel/'
    output_file = 'h5_all_pixel_thr_scan.h5'

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)  # Create the directory if it doesn't exist

    # Combine HDF5 files
    combine_h5_files(h5_files, output_dir, output_file)
    print(f"Combined HDF5 file saved as {os.path.join(output_dir, output_file)}")

    h5_combi = os.path.join(output_dir, output_file)

    with analysis.Analysis(raw_data_file=h5_combi, analysis_timestamp = True, cluster_hits = False) as a:
        a.analyze_data()

    with h5py.File(h5_combi, 'r') as f:
        configuration = f["configuration_out"]
        create_pdf=False
        for i,ival in configuration['bench']['analysis']:
            if i == b'create_pdf':
                create_pdf=(ival==b'True')
        if create_pdf:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
                p.create_standard_plots()


    # with h5py.File(h5_combi, 'r') as f:
    #         configuration = f["configuration_out"]
    #         create_pdf=False
    #         for i,ival in configuration['bench']['analysis']:
    #             if i == b'create_pdf':
    #                 create_pdf=(ival==b'True')
    #         if create_pdf:
    #             with plotting.Plotting(analyzed_data_file=h5_combi) as p:
    #                 p.create_standard_plots()
