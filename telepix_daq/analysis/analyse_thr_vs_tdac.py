import os
import re
import matplotlib.pyplot as plt
import numpy as np
import glob

'''
    This analysis script can be used to analyze TDAC vs THR vs VNDAC vs VMINUS vs IPDAC.
    It processes files with and without "0tdacs" in their names and saves the results 
    in corresponding subfolders.
    Check the fixme!!
'''

def find_latest_results_files(directory, pattern):
    file_pattern = os.path.join(directory, pattern)
    files = glob.glob(file_pattern)

    latest_files = {}
    for file in files:
        pattern_tdac = r'_tdac_(\d+)_'
        match_tdac = re.search(pattern_tdac, file)
        if match_tdac:
            tdac_value = float(match_tdac.group(1))
        
        # Extract vminus, vndac, and ipdac values
        pattern_vminus_vndac = r'vndac([+-]?\d*\.\d+|\d+)_vminus_([+-]?\d*\.\d+|\d+)'
        match_vminus_vndac = re.search(pattern_vminus_vndac, file)
        if match_vminus_vndac:
            vndac = float(match_vminus_vndac.group(1))
            vminus = float(match_vminus_vndac.group(2))
            key = (vminus, vndac)

            if key not in latest_files:
                latest_files[key] = {}

            if tdac_value in latest_files[key]:
                current_file_timestamp = os.path.getmtime(file)
                existing_file_timestamp = os.path.getmtime(latest_files[key][tdac_value])

                if current_file_timestamp > existing_file_timestamp:
                    latest_files[key][tdac_value] = file
            else:
                latest_files[key][tdac_value] = file

    return latest_files


def analyze_and_plot(directory_results, latest_results_files):

    all_data = []  # combined data
    grouped_by_vndac = {}
    grouped_by_vminus = {}

    for (vminus, vndac), files_dict in latest_results_files.items():
        tdac_thr = {}
        for tdac_value, file_path in files_dict.items():
            with open(file_path, 'r') as file:
                lines = file.readlines()
                for line in lines:
                    if "mu" in line:
                        mu_match = re.search(r'mu=([\d\.\+-]+)', line)
                        sigma_match = re.search(r'sigma=([\d\.\+-]+)', line)
                        
                        if mu_match:
                            mu = float(mu_match.group(1))
                        if sigma_match:
                            sigma = float(sigma_match.group(1))
                        
                        tdac_thr[tdac_value] = (mu, sigma)
    
        ordered_tdac_thr = dict(sorted(tdac_thr.items()))

        # Prepare lists to store TDAC, THR, and THR_ERR, avoiding invalid data
        tdacs = []
        thr = []
        thr_err = []

        prev_mu = None
        
        for tdac_value, (mu, sigma) in reversed(ordered_tdac_thr.items()):
            if mu >= 180:
                continue
            ### FIXME: IMPORTANT lines to check. With, clean plots and without messy but you can see all. TODO: Make this cleverer!!
            # if prev_mu is not None:
            #     if mu == thr[-1]:
            #         continue

            #     # Check if the THR value is strictly increasing
            #     if mu >= prev_mu:
            #         continue

            tdacs.append(tdac_value)
            thr.append(mu)
            thr_err.append(sigma)
            prev_mu = mu

        if len(tdacs) > 1:  # Ensure there are enough points to fit
            coefficients = np.polyfit(tdacs, thr, 1)
            linear_fit = np.polyval(coefficients, tdacs)
            plt.plot(tdacs, linear_fit, color='b', linestyle='--')

        plt.errorbar(tdacs, thr, yerr=thr_err, marker="o", 
                     label=f"VMinus = {vminus} V, VNDAC = {int(vndac)}")
        plt.ylabel('Thr [DAC]')
        plt.xlabel('TDAC')
        plt.grid(True)
        plt.tight_layout()
        plt.legend()
        plt.savefig(f"{directory_results}tdac_vs_thr_vndac_{vndac}_vminus_{vminus}.pdf")
        plt.savefig(f"{directory_results}tdac_vs_thr_vndac_{vndac}_vminus_{vminus}.png")
        plt.close()

        all_data.append((tdacs, thr, thr_err, vminus, vndac))

        if vndac not in grouped_by_vndac:
            grouped_by_vndac[vndac] = []
        grouped_by_vndac[vndac].append((tdacs, thr, thr_err, vminus))


        if vminus not in grouped_by_vminus:
            grouped_by_vminus[vminus] = []
        grouped_by_vminus[vminus].append((tdacs, thr, thr_err, vndac))

    plt.figure()

    for tdacs, thr, thr_err, vminus, vndac in all_data:

        if len(tdacs) > 1:  # Ensure there are enough points to fit
            coefficients = np.polyfit(tdacs, thr, 1)
            linear_fit = np.polyval(coefficients, tdacs)
            plt.plot(tdacs, linear_fit, color='b', linestyle='--')

        plt.errorbar(tdacs, thr, yerr=thr_err, marker="o", 
                     label=f"VMinus = {vminus} V, VNDAC = {int(vndac)}")

    plt.ylabel('Thr/V')
    plt.xlabel('TDAC')
    plt.grid(True)
    plt.tight_layout()
    plt.legend()
    plt.savefig(f"{directory_results}combined_tdac_vs_thr.pdf")
    plt.savefig(f"{directory_results}combined_tdac_vs_thr.png")
    plt.close()

    # Create additional plots for the same vminus with different vndac
    for vminus, data_list in grouped_by_vminus.items():
        plt.figure()
        for tdacs, thr, thr_err, vndac in data_list:

            if len(tdacs) > 1:  # Ensure there are enough points to fit
                coefficients = np.polyfit(tdacs, thr, 1)
                linear_fit = np.polyval(coefficients, tdacs)
                plt.plot(tdacs, linear_fit, color='b', linestyle='--')

            plt.errorbar(tdacs, thr, yerr=thr_err, marker="o", 
                         label=f"VNDAC = {int(vndac)}")
        plt.ylabel('Thr [DAC]')
        plt.xlabel('TDAC')
        plt.grid(True)
        plt.tight_layout()
        plt.legend()
        plt.savefig(f"{directory_results}combined_tdac_vs_thr_vminus_{vminus}.pdf")
        plt.savefig(f"{directory_results}combined_tdac_vs_thr_vminus_{vminus}.png")
        plt.close()


   ## Slope (thr change) vs vndac
    slopes_by_vminus = {}
    slope_errors_by_vminus = {}

    # First loop through each vminus to calculate the slopes and their errors
    for vminus, data_list in grouped_by_vminus.items():
        slopes = []  # Store the slopes for each VNDAC value
        slope_errors = []  # Store the slope errors for each VNDAC value
        vndac_values = []  # Store corresponding VNDAC values
        
        for tdacs, thr, thr_err, vndac in data_list:
            if len(tdacs) > 1:  # Ensure there are enough points to fit
                # Perform linear fit and also get covariance matrix to calculate errors
                coefficients, cov_matrix = np.polyfit(tdacs, thr, 1, cov=True)
                slope = coefficients[0]
                slope_error = np.sqrt(cov_matrix[0][0])  # Standard error of the slope
                
                slopes.append(slope)
                slope_errors.append(slope_error)
                vndac_values.append(vndac)  # Store VNDAC value for the slope
        
        # Save slopes and their errors for this vminus
        slopes_by_vminus[vminus] = (vndac_values, slopes, slope_errors)

    # Plot slope vs VNDAC including the errors
    for vminus, (vndac_values, slopes, slope_errors) in slopes_by_vminus.items():
        fig, ax1 = plt.subplots()

        # Scatter plot of VNDAC vs slopes with error bars
        ax1.errorbar(vndac_values, slopes, yerr=slope_errors, marker='o', color='r', label='Data', fmt='o')
        
        # Primary axis (original slope values)
        ax1.set_xlabel('VNDAC')
        ax1.set_ylabel('Threshold change [DAC/step]', color='r')
        ax1.grid(True)

        # Perform linear fit for the slope vs VNDAC
        if len(vndac_values) > 1:  # Ensure there are enough points to fit
            fit_coefficients = np.polyfit(vndac_values, slopes, 1)  # Linear fit
            fit_line = np.polyval(fit_coefficients, vndac_values)  # Calculate fit values
            
            # Plot the linear fit
            ax1.plot(vndac_values, fit_line, linestyle='--', color='grey', label=f"Fit: m={fit_coefficients[0]:.3f}*x + {fit_coefficients[1]:.3f}")

        ax1.legend(loc='upper right')

        # Secondary y-axis (converted values)
        ax2 = ax1.twinx()
        ax2.set_ylabel('Threshold change [mV/step]', color='b')
        
      
        # Link the second axis to the first, multiplying by 7
        ax2.set_ylim(np.array(ax1.get_ylim()) * 7)

        ax2.tick_params(axis='y', labelcolor='b')

        # Save the figure
        plt.savefig(f"{directory_results}thrchange_vminus_{vminus}.pdf")
        plt.savefig(f"{directory_results}thrchange_vminus_{vminus}.png")
        plt.close()




def main():
    directory = '/home/lhcb/daq/telepix_daq/telepix_daq/output_data/module_0/chip_09/2024_September/120V/cool_box/0degC/tdac_scan/module_0/chip_09/'

    # Run for files 
    directory_results_no_0tdacs = os.path.join(directory, 'results_init_tdac_high/')
    if not os.path.exists(directory_results_no_0tdacs):
        os.makedirs(directory_results_no_0tdacs)
    latest_results_files_no_0tdacs = find_latest_results_files(directory, '*global_threshold_tuning*_tdac_*.txt')
    # print(latest_results_files_no_0tdacs)
    analyze_and_plot(directory_results_no_0tdacs, latest_results_files_no_0tdacs)



if __name__ == "__main__":
    main()
