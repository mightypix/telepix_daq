import zmq
import json
import threading
import dash
from dash import dcc, html
from dash.dependencies import Input, Output
import plotly.graph_objs as go
from collections import deque
from online_monitor.utils import utils
import numpy as np
from datetime import datetime
import telepix_daq.analysis.analysis_utils as au
from telepix_daq.analysis.interpreter_clean import RawDataInterpreter


NCOL = 120
NROW = 400


class DataReceiver:
    """Simulate receiving and processing data without Qt."""

    def __init__(self, zmq_address, max_len=50):
        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.SUB)
        self.socket.connect(zmq_address)
        self.socket.setsockopt_string(zmq.SUBSCRIBE, "")
        self.data = deque(maxlen=max_len)
        self.is_running = True
        self.interpreter = RawDataInterpreter()

    def receive_data(self):
        """Receives and processes data from the socket."""
        
        while self.is_running:
            try:
                # msg = self.socket.recv_string(zmq.NOBLOCK)

                # data_serialized = self.socket.recv(flags=zmq.NOBLOCK)
                data_serialized = self.socket.recv()
                data = self.deserialize_data(data_serialized)
                self.data.append(data)
            except zmq.Again as e:
                if "Resource temporarily unavailable" in e:
                    print(f"Error receiving data: {e}")
                pass  # No message received, continue
            except Exception as e:
                print(f"Error receiving data: {e}")

    def start_receiving(self):
        """Starts the receiving thread."""
        self.thread = threading.Thread(target=self.receive_data)
        self.thread.start()

    def stop_receiving(self):
        """Stops the receiving thread."""
        self.is_running = False
        self.thread.join()

    def get_data(self):
        """Returns the received data for plotting."""
        # print([ (i[0][0]<<24) | (i[1][0]<<16) | (i[2][0]<<8) | i[3][0]  for i in  self.data])
        
        if len(self.data) == 0:
            return (
                np.zeros((NCOL, NROW, 1), dtype=np.uint32),
                np.zeros((NCOL, NROW, 1), dtype=np.uint32),
                dict(
                    name="",
                    timestamp_start=datetime.now().timestamp(),
                    error=0,
                    scan_par_id=-1,
                    timestamp_stop=datetime.now().timestamp(),
                )
            )
        chunks = [i[0] for i in self.data]
        meta_data = [i[1] for i in self.data]
        self.data.clear()
        
        #arrange some metadata:
        output_meta_data = dict(
            name=meta_data[0]["name"],
            timestamp_start=meta_data[0]["timestamp_start"],
            error=meta_data[-1]["error"],
            scan_par_id=meta_data[-1]["scan_par_id"],
            timestamp_stop=meta_data[-1]["timestamp_stop"],
        )




        #need to add the data into 32bit integer again
        # raw_data = np.array(
        #     [
        #         (np.uint32(i[0]) << 24)
        #         | (np.uint32(i[1]) << 16)
        #         | (np.uint32(i[2]) << 8)
        #         | np.uint32(i[3])
        #         for i in chunks
        #     ],
        #     dtype=np.uint32,
        # )
        raw_data=np.concatenate(chunks,dtype=np.uint32)
        # if len(chunks)>1:
            
        #     # raw_data = (np.array([chunk[0] for chunk in chunks], dtype=np.uint32) << 24) \
        #     # | (np.array([chunk[1] for chunk in chunks], dtype=np.uint32) << 16) \
        #     # | (np.array([chunk[2] for chunk in chunks], dtype=np.uint32) << 8) \
        #     # | np.array([chunk[3] for chunk in chunks], dtype=np.uint32)
        # else:
        #     print(f"Could not divide data into chunks! {chunks}")
        #     return (
        #         np.zeros((NCOL, NROW, 1), dtype=np.uint32),
        #         np.zeros((NCOL, NROW, 1), dtype=np.uint32),
        #         dict(
        #             name="",
        #             timestamp_start=datetime.now().timestamp(),
        #             error=0,
        #             scan_par_id=-1,
        #             timestamp_stop=datetime.now().timestamp(),
        #         )
        #     )
        
        #this should could be speed up?
        hit_buffer = np.zeros(shape=len(raw_data) + 1, dtype=au.hit_dtype)
        ts_buffer = np.zeros(shape=len(raw_data) + 1, dtype=au.ts_dtype)
        trigger_buffer = np.zeros(shape=len(raw_data) + 1, dtype=au.trigger_dtype)
        self.interpreter.hit_index = 0
        self.interpreter.t_i = 0
        self.interpreter.trigger_i = 0
        self.interpreter.reset_hist()
        hit_dat, ts_dat, trigger_data = self.interpreter.interpret(
            raw_data, hit_buffer, ts_buffer, trigger_buffer, 0
        )
        hist_occ, hist_tot = self.interpreter.get_histograms()
        return (hist_occ, hist_tot, output_meta_data)

    def deserialize_data(self, data):
        """Has to convert the data do a python dict"""
        return utils.simple_dec(data)


# Initialize Dash app
app = dash.Dash(__name__)

# Initialize data receiver
zmq_address = "tcp://127.0.0.1:5500"  # Replace with actual address
# zmq_address = "tcp://127.0.0.1:5500"  # Replace with actual address
data_receiver = DataReceiver(zmq_address)
data_receiver.start_receiving()
# Note the axis are swapped because this is for the visual matrix
hist_occ = np.zeros((NROW, NCOL), dtype=np.uint32)
hist_tot = np.zeros(1024, dtype=np.uint16)
oldest_start = datetime.now()

app.layout = html.Div(
    [
        html.H1("Web Monitor"),
        
        html.Div(id="live-stats", style={"fontSize": 20, "padding": "10px 0"}),
        html.Div(id="other-stats", style={"fontSize": 20, "padding": "10px 0"}),
        dcc.Graph(id="live-2d-histogram", animate=True),
        dcc.Graph(id="live-1d-histogram", animate=True),  # New 1D plot
        html.Button("Reset Histogram", id="reset-button", n_clicks=0),
        html.P(
            "Please note that the z-axis and color scale will be updated, if reloaded"
        ),
        dcc.Interval(id="data-update", interval=1 * 1000),  # Update every second
        
    ]
)


@app.callback(
    [
        Output("live-2d-histogram", "figure"),
        Output("live-1d-histogram", "figure"),
        Output("live-stats", "children"),
        Output("other-stats", "children"),
    ],
    [Input("data-update", "n_intervals"), Input("reset-button", "n_clicks")],
)
def update_histogram(n_intervals, n_clicks):
    global hist_occ  # Ensure to modify the global variable
    global hist_tot  # Ensure to modify the global variable
    global oldest_start
    
    new_occ, new_tot, metadata = data_receiver.get_data()
    
    # Reset hist_occ if the button was clicked
    if n_clicks > 0:
        hist_occ = np.zeros((NROW, NCOL), dtype=np.uint32)
        hist_tot = np.zeros(1024, dtype=np.uint16)
        oldest_start = datetime.fromtimestamp(metadata["timestamp_start"])
    new_occ = (new_occ[:].sum(axis=2)).T
    new_tot = (new_tot[:].sum(axis=(0, 1, 2))).T
    start = datetime.fromtimestamp(metadata["timestamp_start"])
    if start < oldest_start:
        oldest_start = start

    if new_occ.shape[0] == hist_occ.shape[0] and new_occ.shape[1] == hist_occ.shape[1]:
        hist_occ += new_occ
        hist_tot += new_tot
    else:
        print(f"Data shape mismatch: {new_occ.shape} vs {hist_occ.shape}")
    hist_show = hist_occ

    zmin = np.min(hist_show)
    zmax = np.max(hist_show)

    fig_2d = go.Figure(
        go.Heatmap(
            z=hist_show,
            colorscale="Blues",
            colorbar=dict(title="Count"),
            zmin=zmin,  # Set min
            zmax=zmax,  # Set max
        )
    )

    fig_2d.update_layout(
        xaxis=dict(title="columns"),
        yaxis=dict(title="rows"),
        title="Sensor Heatmap",
    )
    
    # 1D Histogram of sums along the X-axis
    fig_1d = go.Figure(
        data=go.Bar(x=list(range(len(hist_tot))), y=hist_tot, marker=dict(color="blue"))
    )

    fig_1d.update_layout(
        xaxis=dict(title="TOT"),
        yaxis=dict(title="Count", range=[0, np.max(hist_tot) * 1.1]),
        title="TOT histogram",
    )

    # Updating statistics
    total_counts = int(hist_occ.sum())
    stats_text = f" {metadata['name']} Total Counts: {total_counts}, Max Count in Histogram: {zmax}   Error: {metadata['error']}"

    stop = datetime.fromtimestamp(metadata["timestamp_stop"]).strftime("%H:%M:%S")
    stats_text2 = f"considered time {oldest_start.strftime('%H:%M:%S')} - {stop}       scan_par_id: {metadata['scan_par_id']}"

    return fig_2d, fig_1d, stats_text, stats_text2


if __name__ == "__main__":
    try:
        app.run_server(host='127.0.0.1',debug=True)
    finally:
        data_receiver.stop_receiving()
