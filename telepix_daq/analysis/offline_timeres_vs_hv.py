import os 
import re
import matplotlib.pyplot as plt
import numpy as np
import argparse


def extract_voltage(directory_path):
    pattern = r"/(\d+)V/"
    match = re.search(pattern, directory_path)
    if match:
        voltage = match.group(1)
        return voltage
    else:
        return None



def main(default_input_dir=None):
    
    target_thr = '142'

    default_dir = '/home/lab/daq/DOCKER_TEST/telepix_daq/telepix_daq/output_data/module_0/chip_9/2024_April/subchip/'
    HV_chip_130 = '/home/lab/daq/DOCKER_TEST/telepix_daq/telepix_daq/output_data/module_0/chip_9/2024_April/subchip/130V/scan_source/Sr90/module_0/chip_9/20240612_165113_source_scan_interpreted_time_resolution/'
    HV_chip_120 = '/home/lab/daq/DOCKER_TEST/telepix_daq/telepix_daq/output_data/module_0/chip_9/2024_April/subchip/120V/scan_source/Sr90/module_0/chip_9/20240612_155157_source_scan_interpreted_time_resolution/'
    HV_chip_100 = '/home/lab/daq/DOCKER_TEST/telepix_daq/telepix_daq/output_data/module_0/chip_9/2024_April/subchip/100V/scan_source/Sr90/module_0/chip_9/20240613_112105_source_scan_interpreted_time_resolution/'
    HV_chip_80 = '/home/lab/daq/DOCKER_TEST/telepix_daq/telepix_daq/output_data/module_0/chip_9/2024_April/subchip/80V/scan_source/Sr90/module_0/chip_9/20240613_120612_source_scan_interpreted_time_resolution/'
    HV_chip_70 = '/home/lab/daq/DOCKER_TEST/telepix_daq/telepix_daq/output_data/module_0/chip_9/2024_April/subchip/70V/scan_source/Sr90/module_0/chip_9/20240613_161009_source_scan_interpreted_time_resolution/'
    HV_chip_60 = '/home/lab/daq/DOCKER_TEST/telepix_daq/telepix_daq/output_data/module_0/chip_9/2024_April/subchip/60V/scan_source/Sr90/module_0/chip_9/20240613_165136_source_scan_interpreted_time_resolution/'

    HV_pix_60 = '/home/lab/daq/DOCKER_TEST/telepix_daq/telepix_daq/output_data/module_0/chip_9/2024_April/subpix/60V/scan_source/Sr90/module_0/chip_9/20240613_135500_source_scan_interpreted_time_resolution/'
    HV_pix_70 = '/home/lab/daq/DOCKER_TEST/telepix_daq/telepix_daq/output_data/module_0/chip_9/2024_April/subpix/70V/scan_source/Sr90/module_0/chip_9/20240613_143546_source_scan_interpreted_time_resolution/'
    HV_pix_80 = '/home/lab/daq/DOCKER_TEST/telepix_daq/telepix_daq/output_data/module_0/chip_9/2024_April/subpix/80V/scan_source/Sr90/module_0/chip_9/20240613_151637_source_scan_interpreted_time_resolution/'
   
    values_chip = [HV_chip_130, HV_chip_120, HV_chip_100, HV_chip_80, HV_chip_70, HV_chip_60]
    values_pix = [HV_pix_60, HV_pix_70, HV_pix_80]


    results_time_res = {}
    results_time_res_uncorrected = {}

    for hv_i in values_chip:
        for root, dirs, files in os.walk(hv_i):
            voltage = extract_voltage(root)
            for dir_name in dirs:
                if target_thr in dir_name:
                    subdir_path = os.path.join(root, dir_name)
                    for subroot, subdirs, subfiles in os.walk(subdir_path):
                        for file in subfiles:
                            if "sigma" in file:
                                file_path = os.path.join(subroot, file)
                                with open(file_path, 'r') as f:
                                    for line in f:
                                        if "sigma_tot" in line:
                                            parts = line.split()
                                            if len(parts) >= 2:
                                                res = float(parts[1])
                                                res_err = float(parts[2])
                                                int_time_eff = float(parts[3])
                                                results_time_res[voltage] = [res, res_err, int_time_eff]
                                        elif "sigma_offset" in line:
                                            parts = line.split()
                                            if len(parts) >= 2:
                                                res = float(parts[1])
                                                res_err = float(parts[2])
                                                int_time_eff = float(parts[3])
                                                results_time_res_uncorrected[voltage] = [res, res_err, int_time_eff]


    sorted_items = sorted(results_time_res.items(), key=lambda x: int(x[0]))
    sorted_items_uncorrected = sorted(results_time_res_uncorrected.items(), key=lambda x: int(x[0]))

    hv_values = [item[0] for item in sorted_items]
    time_res_cor = [item[1][0] for item in sorted_items]
    time_res_values_uncorrected = [item[1][0] for item in sorted_items_uncorrected]
    time_res_err = [item[1][1] for item in sorted_items]

    time_res = []
    for y_i in time_res_cor:
        time_res_corr_tl =  np.sqrt(y_i**2-5**2/12-2**2/12)
        time_res.append(time_res_corr_tl)


    if 'subchip' in default_dir:
        bias = "chip"
    else:
        bias = "pix"



    plt.errorbar(hv_values, time_res, time_res_err, marker ="o", markersize=1, label = f"Corrected")
    plt.errorbar(hv_values, time_res_values_uncorrected, time_res_err, marker ="o", markersize=1, label = f"Uncorrected")
    # plt.plot(hv_values, time_res, marker ="o", markersize=1, label = f"Corrected")
    # plt.plot(hv_values, time_res_values_uncorrected, marker ="o", markersize=1, label = f"Uncorrected")
    plt.xlabel(f'Bias Voltage [-V]')
    plt.ylabel('$\\sigma_{corr}$ [ns]')
    plt.grid(True)
    plt.text(0.5, 0.5, 'Preliminary', color='gray', alpha=0.5, fontsize=30,
            horizontalalignment='center', verticalalignment='center', transform=plt.gca().transAxes)
    plt.tight_layout()
    plt.legend() 
    plt.xticks(rotation=45) 
    plt.savefig(f"{default_dir}time_res_vs_HV.pdf")
    plt.savefig(f"{default_dir}time_res_vs_HV.png")



   


if __name__ == "__main__":
    main()