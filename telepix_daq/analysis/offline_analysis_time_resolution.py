import os 
import re
import matplotlib.pyplot as plt
import numpy as np
import argparse

def main(default_input_dir=None):

    # input_dir = "/home/lab/daq/DOCKER_TEST/telepix_daq/telepix_daq/output_data/module_0/chip_9/2024_April/subchip/120V/scan_source/Sr90/module_0/chip_9/20240607_150600_source_scan20240607_173944_interpreted_time_resolution/"
    # input_dir = '/home/lab/daq/DOCKER_TEST/telepix_daq/telepix_daq/output_data/module_0/chip_9/2024_April/subchip/120V/scan_source/Sr90/module_0/chip_9/20240607_150600_source_scan20240610_152247_interpreted_time_resolution/'
    # input_dir = '/home/lab/daq/DOCKER_TEST/telepix_daq/telepix_daq/output_data/module_0/chip_9/2024_April/subchip/120V/scan_source/Sr90/module_0/chip_9/20240607_150600_source_scan20240610_170113_interpreted_time_resolution/'
    error_threshold = 0.1
    input_dir = default_input_dir
    par = "parameter"
    if default_input_dir==None:
        default_input_dir = '/home/lab/daq/DOCKER_TEST/telepix_daq/telepix_daq/output_data/module_0/chip_9/2024_April/subchip/120V/scan_source/Sr90/module_0/chip_9/20240610_154358_source_scan_dac20240610_172630_interpreted_time_resolution/'

        parser = argparse.ArgumentParser(description='Process and plot sigma files from a directory.')
        parser.add_argument('input_dir', type=str, nargs='?', default=default_input_dir, help='The input directory to search for sigma files.')
        parser.add_argument('par', type=str, nargs='?', default=par, help='Parameter that was scaned.')
        args = parser.parse_args()

        input_dir = args.input_dir
        par = args.par

    ##Find HV value
    index_v = input_dir.rfind('V')
    index_start_hv = input_dir[:index_v].rfind('/')
    index_end_hv = index_v
    hv_value = input_dir[index_start_hv + 1:index_end_hv]

    ## change directory & find all time resolutions
    dir = os.chdir(input_dir)
    results_time_res_uncorrected = {}
    results_time_res = {}
    results_in_time_eff = {}
    for root, dirs, files in os.walk(input_dir):
        for file in files:
            if "sigma" in file:
                directory_components = root.split('/')
                last_folder = directory_components[-1]

                file_path = os.path.join(root, file)
                with open(file_path, 'r') as f:
                    for line in f:
                        if "sigma_tot" in line:
                            parts = line.split()
                            if len(parts) >= 2:
                                res = float(parts[1])
                                res_err = float(parts[2])
                                int_time_eff = float(parts[3])
                                results_time_res[last_folder] = [res, res_err, int_time_eff]
                        elif "sigma_offset" in line:
                            parts = line.split()
                            if len(parts) >= 2:
                                res = float(parts[1])
                                res_err = float(parts[2])
                                int_time_eff = float(parts[3])
                                results_time_res_uncorrected[last_folder] = [res, res_err, int_time_eff]


    # print(results_time_res.items())
    sorted_items = sorted(results_time_res.items(), key=lambda x: int(x[0]))
    sorted_items_uncorrected = sorted(results_time_res_uncorrected.items(), key=lambda x: int(x[0]))
    # sorted_items = sorted(results_time_res.items(), key=lambda x: (x[0]))
    # sorted_items_uncorrected = sorted(results_time_res_uncorrected.items(), key=lambda x: (x[0]))
    # sorted_items=results_time_res.items()
    # sorted_items_uncorrected=results_time_res_uncorrected.items()
    if par == "ThPix":
        x_values = [item[0] for item in sorted_items][:-1]
        time_res_values_uncorrected = [item[1][0] for item in sorted_items_uncorrected][:-1]
        time_res_values_error_uncorrected = [item[1][1] for item in sorted_items_uncorrected][:-1]
        time_res_values = [item[1][0] for item in sorted_items][:-1]
        time_res_values_error = [item[1][1] for item in sorted_items][:-1]
        in_time_eff_values = [item[1][2] for item in sorted_items][:-1]
    else:
        # x_values = [item[0] for item in sorted_items]
        x_values = [f"{round(float(item[0]), 3)}" for item in sorted_items]
        time_res_values_uncorrected = [item[1][0] for item in sorted_items_uncorrected]
        time_res_values_error_uncorrected = [item[1][1] for item in sorted_items_uncorrected]
        time_res_values = [item[1][0] for item in sorted_items]
        time_res_values_error = [item[1][1] for item in sorted_items]
        in_time_eff_values = [item[1][2] for item in sorted_items]

    # Apply the error threshold filter
    filtered_x_values = []
    filtered_time_res_values = []
    filtered_time_res_values_error = []
    filtered_time_res_values_uncorrected = []
    filtered_time_res_values_error_uncorrected = []
    filtered_in_time_eff_values = []

    for i in range(len(x_values)):
        if time_res_values_error[i] < error_threshold:
            # print(time_res_values_error[i])
            filtered_x_values.append(x_values[i])
            filtered_time_res_values.append(time_res_values[i])
            filtered_time_res_values_error.append(time_res_values_error[i])
            filtered_time_res_values_uncorrected.append(time_res_values_uncorrected[i])
            filtered_time_res_values_error_uncorrected.append(time_res_values_error_uncorrected[i])
            filtered_in_time_eff_values.append(in_time_eff_values[i])


    time_res_values_cor_negedge = []
    time_res_values_cor_negedge_tl = []
    ### correct time resolution: binning 
    for y_i in filtered_time_res_values:
        # print(y_i)
        # time_res_corr =  np.sqrt(y_i**2-2/12)
        time_res_corr =  np.sqrt(y_i**2-5**2/12)
        time_res_corr_tl =  np.sqrt(y_i**2-5**2/12-2**2/12)
        time_res_values_cor_negedge.append(time_res_corr)
        time_res_values_cor_negedge_tl.append(time_res_corr_tl)

    plt.errorbar(filtered_x_values, filtered_time_res_values_uncorrected, filtered_time_res_values_error_uncorrected, marker ="o", markersize=1, label = "Uncorrected")
    plt.errorbar(filtered_x_values, filtered_time_res_values, filtered_time_res_values_error, marker ="o", markersize=1, label = "ToT + Row + Col")
    plt.errorbar(filtered_x_values, time_res_values_cor_negedge, filtered_time_res_values_error, marker ="o", markersize=1, label = "ToT + Row + Col + 5ns chip binning")
    plt.errorbar(filtered_x_values, time_res_values_cor_negedge_tl, filtered_time_res_values_error, marker ="o", markersize=1, label = "ToT + Row + Col + 5ns chip  + 2ns TL binning")
    plt.xlabel(f'{par} [DACs]')
    plt.ylabel('$\\Delta$T$_{corr}$ [ns]')
    plt.grid(True)
    plt.text(0.5, 0.5, 'Preliminary', color='gray', alpha=0.5, fontsize=30,
            horizontalalignment='center', verticalalignment='center', transform=plt.gca().transAxes)



    # Add custom text to legend
    sigma_value = time_res_values_cor_negedge_tl[0]
    sigma_value_err = filtered_time_res_values_error[0]
    formatted_sigma_value = f"{sigma_value:.2f}"  
    formatted_sigma_value_err = f"{sigma_value_err:.2f}"  
    additional_text = [f"$\\sigma$ = {formatted_sigma_value} $\\pm$ {formatted_sigma_value_err} ns"]
 
    handles, labels = plt.gca().get_legend_handles_labels()
    custom_handles = [plt.Line2D([], [], color='none', label=text) for text in additional_text]
    handles.extend(custom_handles)
    labels.extend(additional_text)

    plt.tight_layout()
    plt.legend(handles=handles, labels=labels, loc='best') 
    plt.xticks(rotation=45) 
    plt.savefig(f"{input_dir}time_res_vs_{par}_zoom.pdf")
    plt.savefig(f"{input_dir}time_res_vs_{par}_zoom.png")



    ### In-time efficiency plot
    fig, ax = plt.subplots() 
    ax.set_xlabel(f'{par} [DACs]') 
    ax.set_ylabel('In-time efficiency') 
    
    ax.plot(x_values,in_time_eff_values ) 
    plt.grid(True)
    plt.text(0.5, 0.5, 'Preliminary', color='gray', alpha=0.5, fontsize=30,
            horizontalalignment='center', verticalalignment='center', transform=plt.gca().transAxes)
    plt.tight_layout()
    # plt.legend()
    plt.xticks(rotation=45) 
    plt.savefig(f"{input_dir}in_time_{par}_zoom.pdf")
    plt.savefig(f"{input_dir}in_time_{par}_zoom.png")
   


if __name__ == "__main__":
    main()