import ROOT 
import os
ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetOptFit(111)
ROOT.TH1.SetDefaultSumw2()
latex = ROOT.TLatex()
ROOT.EnableImplicitMT(12)
import multiprocessing as mp
import copy



def add_text_hist():
    latex.SetTextSize(0.06)
    latex.SetTextAlign(16)  
    latex.DrawLatexNDC(0.12, 0.8, "Preliminary")

def correction_formula_prof(df,var,nbin,bin_min,bin_max,threshold,value_to_corr):

    h1f = df.Profile1D((f"{var}_tmp", f"{var}_tmp", nbin,bin_min,bin_max), var,value_to_corr)
    bin_edges=[]
    # print(f"__________________{var},{value_to_corr}_________________")
    func=f"{value_to_corr} - ("
    for ivalue in range(1,h1f.GetNbinsX()+1):
        corr=h1f.GetBinContent(ivalue)
        val_low=h1f.GetBinLowEdge(ivalue)
        val_high=h1f.GetBinLowEdge(ivalue)+h1f.GetBinWidth(ivalue)
        func+=f"(({var}>={val_low})&& ({var}<{val_high}))*({corr}) +"
    func+="0)"
    return func


def do_fit_par(hist,q):
    res=hist[0].Fit("gaus","SQ")
    q.put(res.Parameter(1))

def correction_formula(df,var,nbin,bin_min,bin_max,threshold,value_to_corr):
    h1f=df.Histo1D((f"{var}_tmp",f"{var}_tmp", nbin,bin_min,bin_max),var)
    h1f.Scale(1./h1f.Integral())
    last_value=0
    bin_edges=[]
    for bin in range(1,h1f.GetNbinsX()):
        if last_value==bin:
            continue
        current_int=h1f.Integral(last_value,bin)
        if current_int>threshold or bin==h1f.GetNbinsX()-1:
            bin_edges.append((h1f.GetBinLowEdge(last_value),h1f.GetBinLowEdge(bin+1)))
            last_value=bin+1

    ### Take row corrected value & correct ToT via Gaus fit for given bins in tot
    mean_correction=[]
    hists=[]
    for tot_min,tot_max in bin_edges:
        df_f=df.Filter(f"{tot_min}<{var} && {var}<{tot_max} &&  fabs({value_to_corr})<30")
        hist=df_f.Histo1D(value_to_corr)
        hist.SetName(f"{hist.GetName()}_{id(hist)}")
        ROOT.SetOwnership( hist, False )
        hists.append([hist,tot_min,tot_max])
    processes=[]
    
    for hist,tot_min,tot_max in hists:
        q = mp.Queue()
        p = mp.Process(target=do_fit_par, args=([hist],q,))
        p.start()
        processes.append([p,tot_min,tot_max,q])
    for p in processes:
        mean=p[3].get()
        p[0].join()
        mean_correction.append((mean,p[1],p[2]))
        # print("wfnwkefnde  ",tot_min,tot_max)
        # ROOT.gPad.Update()
        # input()

    func=f"{value_to_corr} - ("
    for corr,tot_low,tot_high in mean_correction:
        func+=f"(({var}>={tot_low})&& ({var}<{tot_high}))*({corr}) +"
    func+="0)"
    return func


def save_plot(scan_param_id,df,name,nbin,bin_min, bin_max,var,c1,dir,do_fit=False,prof_var=None):
    c1.cd()
    h1_1=df.Histo1D((f"{scan_param_id}_{name}",name,nbin,bin_min,bin_max),var)
    ###Fit Gaus & calculate in-time efficiency
    if do_fit:
        res_gaus=h1_1.Fit("gaus","SQ")
        t_window_low = ROOT.TLine(-12.5, 0, -12.5, h1_1.GetMaximum())
        t_window_high = ROOT.TLine(12.5, 0, 12.5, h1_1.GetMaximum())
        t_window_low.SetLineColor(ROOT.kBlue)
        t_window_low.SetLineWidth(2)
        t_window_low.SetLineStyle(2)
        t_window_high.SetLineColor(ROOT.kBlue)
        t_window_high.SetLineWidth(2)
        t_window_high.SetLineStyle(2)
        t_window_low.Draw("SAME")
        t_window_high.Draw("SAME")
        mean = res_gaus.Parameter(1)
        # print(mean, res_gaus.Parameter(2))
        # input()
        df_narrow=df.Filter(f"({mean}-12.5)>={var} &&({mean}+12.5)<={var} ")
        count=df_narrow.Count()
        count_total=df.Count()
        entries_in_t_window=count.GetValue()
        entries_total=count_total.GetValue()
        # low_bin = h1_1.FindBin(mean-12.5)
        # high_bin = h1_1.FindBin(mean+12.5)+1
        # entries_in_t_window = h1_1.Integral(low_bin, high_bin)
        # entries_total = h1_1.Integral()
        in_time_eff = entries_in_t_window/entries_total
    h1_1.Draw("SAME")
    if "tdiff" in var: 
        h1_1.GetXaxis().SetTitle(f"{var}/ns")
    else:
        h1_1.GetXaxis().SetTitle(f"{var}")
    h1_1.GetYaxis().SetTitle("Entries")
    h1_1.Write()
    add_text_hist()
    c1.Update()
    # root_file.Write()
    c1.SaveAs(f"{dir}{name}.png")
    c1.SaveAs(f"{dir}{name}.pdf")
    if prof_var is not None:
        hprof1d = df.Profile1D((f"{scan_param_id}_{name}_prof_{prof_var}", f"{name}_prof_{prof_var}", nbin,bin_min,bin_max), var, prof_var)
        hprof1d.Draw()
        if "tdiff" in prof_var: 
            hprof1d.GetYaxis().SetTitle(f"{prof_var}/ns")
        else:
            hprof1d.GetYaxis().SetTitle(f"{prof_var}")
        hprof1d.GetXaxis().SetTitle(f"{var}")
        res=hprof1d.Fit("pol1","S")
        hprof1d.Write()
        c1.Update()
        # root_file.Write()
        c1.SaveAs(f"{dir}{name}_prof_{prof_var}.png")
        c1.SaveAs(f"{dir}{name}_prof_{prof_var}.pdf")
        return res.Parameter(1)
    if do_fit:
        return res_gaus.Parameter(2), res_gaus.ParError(2), in_time_eff

def time_res_analysis(input_file, scan_params):

    ### Create RDataframe of input root file
    root_file = ROOT.TFile.Open(f"{input_file}", "UPDATE")

    
    for scan_param_id_i,scan_param_id  in enumerate(scan_params):
        if "/home/lab/daq/DOCKER_TEST/telepix_daq/telepix_daq" in root_file.GetName():
            dir = f"{(root_file.GetName()).replace('.root', '')}_time_resolution/{scan_param_id}/"
        else:
            dir = f"/home/lab/daq/DOCKER_TEST/telepix_daq/telepix_daq/{(root_file.GetName()).replace('.root', '')}_time_resolution/{scan_param_id}/"
        if not os.path.exists(dir):
            os.makedirs(dir)
    
        df = ROOT.RDataFrame(f"tree_{scan_param_id}", f"{input_file}")

        df=df.Filter("tdiff_trigger>-100 && tdiff_trigger<50 && tot>55")
        hist=df.Histo1D("tdiff_trigger")
        df=df.Define("tdiff_offset_corr",f"tdiff_trigger-({hist.GetMean()})")
        hist=df.Histo1D("tdiff_offset_corr")
        df = df.Filter(f"fabs(tdiff_offset_corr)<{hist.GetStdDev()*5}")

        c1 = ROOT.TCanvas("c1", "", 200, 10, 700, 500)

        ###Uncorrected banana plot & time resolution
        h2=df.Histo2D((f"Time_resolution_vs_tot_{scan_param_id}","Time resolution vs ToT",75,-50,50, 1024,0,1024,),"tdiff_offset_corr","tot")
        h2.Draw("COLZ")
        add_text_hist()
        h2.GetXaxis().SetTitle("#DeltaT_{offset_cor} [ns]")
        h2.GetYaxis().SetTitle(f"ToT[10ns]")
        h2.Write()
        c1.Update()
        root_file.Write()
        c1.SaveAs(f"{dir}/tdiff_tot_2d.png")
        c1.SaveAs(f"{dir}/tdiff_tot_2d.pdf")


        save_plot(scan_param_id, df,"tdiff",50,-50,50,"tdiff_offset_corr",c1,dir,do_fit=True)

    
        solpe=save_plot(scan_param_id, df,"row",400,0,400,"row",c1,dir,False,"tdiff_offset_corr")
        # df=df.Define("tdiff_row_cor",f"tdiff_trigger-row*{slope}")

        # ### Row correction via linear regression
        # pram=hprof1d.Fit("pol1","S")
        # slope=pram.Parameter(1)
        # 


        row_formula=correction_formula_prof(df,"row",400,0,400,0.001,"tdiff_offset_corr")

        # print(row_formula)
        df=df.Define("tdiff_row_cor",row_formula)
        


        col_formula=correction_formula_prof(df,"col",120,0,120,0.001,"tdiff_row_cor")
        df=df.Define("tdiff_col_cor",col_formula)
        
        # df = df.Filter("tdiff_row_cor<50 && tdiff_row_cor>-50")


        # tot_formula=correction_formula(df,"tot",1024,0,1024,0.02,"tdiff_col_cor")
        tot_formula=correction_formula(df,"tot",1024,0,1024,0.02,"tdiff_col_cor")
        # # h2f=df.Histo2D(("wv","tdiff_row_cor",75,-50,50, 1024,0,1024),"tdiff_row_cor","tot")
        # h1f=df.Histo1D(("tot_row_corr","tot_row_corr", 1024,0,1024),"tot")
        # h1f.Scale(1./h1f.Integral())
        # last_value=0
        # threshold=0.02
        # bin_edges=[]
        # for bin in range(1,h1f.GetNbinsX()):
        #     if last_value==bin:
        #         continue
        #     current_int=h1f.Integral(last_value,bin)
        #     if current_int>threshold or bin==h1f.GetNbinsX()-1:
        #         bin_edges.append((h1f.GetBinLowEdge(last_value),h1f.GetBinLowEdge(bin+1)))
        #         last_value=bin+1
            
        # print(bin_edges)

        # ### Take row corrected value & correct ToT via Gaus fit for given bins in tot
        # mean_correction=[]
        # for tot_min,tot_max in bin_edges:
        #     df_f=df.Filter(f"{tot_min}<tot && tot<{tot_max}")
        #     hist=df_f.Histo1D("tdiff_row_cor")
        #     res=hist.Fit("gaus","SQ")
        #     mean=res.Parameter(1)
        #     mean_correction.append((mean,tot_min,tot_max))

        # func="tdiff_row_cor - ("
        # for corr,tot_low,tot_high in mean_correction:
        #     func+=f"((tot>{tot_low})&& (tot<={tot_high}))*({corr}) +"
        # func+="0)"

        df=df.Define("tdiff_tot_cor",tot_formula)

        save_plot(scan_param_id, df,"row",400,0,400,"row",c1,dir,False,"tdiff_row_cor")
        save_plot(scan_param_id, df,"row",400,0,400,"row",c1,dir,False,"tdiff_tot_cor")

        save_plot(scan_param_id, df,"row",400,0,400,"row",c1,dir,False,"tdiff_row_cor")
        save_plot(scan_param_id, df,"row",400,0,400,"row",c1,dir,False,"tdiff_trigger")
        save_plot(scan_param_id, df,"row",400,0,400,"row",c1,dir,False,"tdiff_tot_cor")
        
        save_plot(scan_param_id, df,"col",120,0,120,"col",c1,dir,False,"tdiff_col_cor")
        save_plot(scan_param_id, df,"col",120,0,120,"col",c1,dir,False,"tdiff_row_cor")
        save_plot(scan_param_id, df,"col",120,0,120,"col",c1,dir,False,"tdiff_trigger")
        save_plot(scan_param_id, df,"col",120,0,120,"col",c1,dir,False,"tdiff_tot_cor")

        save_plot(scan_param_id, df,"tot",1024,0,1024,"tot",c1,dir,False,"tdiff_trigger")
        save_plot(scan_param_id, df,"tot",1024,0,1024,"tot",c1,dir,False,"tdiff_row_cor")
        save_plot(scan_param_id, df,"tot",1024,0,1024,"tot",c1,dir,False,"tdiff_tot_cor")




        save_plot(scan_param_id, df,"tdiff_row_cor",100,-50,50,"tdiff_row_cor",c1,dir,True)


        c1.cd()
        h2=df.Histo2D((f"tdiff_tot_cor_{scan_param_id}","tdiff_tot_cor",75,-50,50, 1024,0,1024),"tdiff_tot_cor","tot")
        h2.Draw("COLZ")
        add_text_hist()
        h2.GetXaxis().SetTitle("#DeltaT_{totcor} [ns]")
        h2.GetYaxis().SetTitle(f"ToT[10ns]")
        h2.Write()
        c1.Update()
        root_file.Write()
        c1.SaveAs(f"{dir}tdiff_tot_cor_2d.png")
        c1.SaveAs(f"{dir}tdiff_tot_cor_2d.pdf")
        

        c1.cd()
        h2f=df.Histo2D((f"tdiff_col_cor_{scan_param_id}","tdiff_col_cor",75,-50,50, 1024,0,1024),"tdiff_col_cor","tot")
        h2f.Draw("COLZ")
        add_text_hist()
        h2f.GetXaxis().SetTitle("#DeltaT_{colcor} [ns]")
        h2f.GetYaxis().SetTitle(f"ToT[10ns]")
        h2f.Write()
        c1.Update()
        root_file.Write()
        c1.SaveAs(f"{dir}tdiff_col_cor_2d.png")
        c1.SaveAs(f"{dir}tdiff_col_cor_2d.pdf")


        c1.cd()
        h2f=df.Histo2D((f"tdiff_row_cor_{scan_param_id}","tdiff_row_cor",75,-50,50, 1024,0,1024),"tdiff_row_cor","tot")
        h2f.Draw("COLZ")
        add_text_hist()
        h2f.GetXaxis().SetTitle("#DeltaT_{rowcor} [ns]")
        h2.GetYaxis().SetTitle(f"ToT [10ns]")
        h2f.Write()
        c1.Update()
        root_file.Write()
        c1.SaveAs(f"{dir}tdiff_row_cor_2d.png")
        c1.SaveAs(f"{dir}tdiff_row_cor_2d.pdf")


        c1.cd()
        h2f=df.Histo2D((f"tdiff_col_{scan_param_id}","tdiff_col",75,-50,50, 120,0,120),"tdiff_tot_cor","col")
        h2f.Draw("COLZ")
        add_text_hist()
        h2f.GetXaxis().SetTitle("#DeltaT_{totcor} [ns]")
        h2f.GetYaxis().SetTitle(f"col")
        h2f.Write()
        c1.Update()
        root_file.Write()
        c1.SaveAs(f"{dir}tdiff_col.png")
        c1.SaveAs(f"{dir}tdiff_col.pdf")
        
        c1.cd()
        h2f=df.Histo2D((f"tdiff_row_{scan_param_id}","tdiff_row",75,-50,50, 400,0,400),"tdiff_trigger","row")
        h2f.Draw("COLZ")
        add_text_hist()
        h2f.GetXaxis().SetTitle("#DeltaT [ns]")
        h2f.GetYaxis().SetTitle(f"row")
        h2f.Write()
        c1.Update()
        root_file.Write()
        c1.SaveAs(f"{dir}tdiff_row.png")
        c1.SaveAs(f"{dir}tdiff_row.pdf")


        sigma_offset, sigma_offset_error, in_time_eff_offset =save_plot(scan_param_id, df,"tdiff_offset_corr",150,-50,50,"tdiff_offset_corr",c1,dir,do_fit=True)
        sigma_col, sigma_col_error,in_time_eff_col    =save_plot(scan_param_id, df,"tdiff_col_cor",150,-50,50,"tdiff_col_cor",c1,dir,do_fit=True)
        sigma_row, sigma_row_error, in_time_eff_row    =save_plot(scan_param_id, df,"tdiff_row_cor",150,-50,50,"tdiff_row_cor",c1,dir,do_fit=True)
        sigma_tot, sigma_tot_error, in_time_eff_tot    =save_plot(scan_param_id, df,"tdiff_tot_cor",150,-50,50,"tdiff_tot_cor",c1,dir,do_fit=True)

        with open(f"{dir}sigma.txt","w+") as f:
            f.write(f"sigma_offset_{scan_param_id_i}_{scan_param_id}DAC  {sigma_offset} {sigma_offset_error} {in_time_eff_offset}\n")
            f.write(f"sigma_row_{scan_param_id_i}_{scan_param_id}DAC     {sigma_row} {sigma_row_error} {in_time_eff_col}\n")
            f.write(f"sigma_col_{scan_param_id_i}_{scan_param_id}DAC     {sigma_col} {sigma_col_error} {in_time_eff_row}\n")
            f.write(f"sigma_tot_{scan_param_id_i}_{scan_param_id}DAC     {sigma_tot} {sigma_tot_error} {in_time_eff_tot}\n")

        root_file.Write()
    root_file.Close()

    return sigma_tot

