import sys
import time
import os
import numpy as np
import numba
from tqdm import tqdm
import tables as tb

class_spec = [
    ("chunk_size", numba.uint32),
    ("mightypix_data_flag", numba.uint8),
    ("mightypix_token_timestamp", numba.int64),
    ("rx_timestamp_flag", numba.uint8),
    ("rx_timestamp", numba.int64),
    ("error_cnt", numba.uint32),
    ("error_cnt_order", numba.uint32),
    ("col", numba.uint8),
    ("row", numba.uint8),
    ("ts_bin_cnt", numba.uint16),
    ("ts_gray", numba.uint8),
    ("ts_decoded", numba.uint8),
    ("ts3", numba.uint8),
    ("ts", numba.uint32),
    ("ts2", numba.uint16),
    ("tot", numba.uint16),
    ("meta_idx", numba.uint32),
    ("raw_idx", numba.uint32),
    ("last32bits", numba.uint32),
    ("lastcommas", numba.uint8),
    ("used_info", numba.uint8),
    ("raw4x32", numba.uint32[:]),
]

"""
First we separate the data into the dataword, and the comma information to check for errors:
"""

hit_dtype = [
        ("det_col", "<u1"),
        ("det_row", "<u2"),
        ("col", "<u1"),
        ("row", "<u2"),
        ("ts_bin_cnt", "<u2"),
        ("ts_gray", "<u1"),
        ("ts_decoded", "<u1"),
        ("ts3", "<u1"),
        ("ts", "<u4"),
        ("ts2", "<u2"),
        ("rx_timestamp", "<u4"),
        ("scan_param_id", "<u4"),
        ("cnt", "<u4"),
        ("cnt_order", "<u4"),
        ("used_info", "<u1"),
        ("rawlp2", "<u4"),
        ("rawrc2", "<u4"),
        ("rawrc4", "<u4"),
        ("rawlsc1", "<u4"),
        ("tot","<u2")
    ]

@numba.njit
def swap_bit_order(value, n_bits):
    return_value = numba.uint8(0)
    count = 0
    while n_bits > 0:
        n_bits = n_bits - 1
        if n_bits >= count:
            return_value = return_value | ((value & 1 << n_bits) >> (n_bits - count))
        else:
            return_value = return_value | ((value & 1 << n_bits) << (count - n_bits))
        count += 1
    return return_value


@numba.njit
def get_raw_dataword(word, iraw):
    # get either the first or the second dataword
    # print("This is the raw data word", 0xFF & (word >> 8 * iraw))
    return 0xFF & (word >> 8 * iraw)


@numba.njit
def get_raw_commaword(word, iraw):
    # get the comma words just to check
    return ((word >> 16) & 0x3 >> iraw) & 0x1


"""
There are states where the chip sends out data:
lc2 (Load column 2)
lp2 (Load pixel 2)
rc2 (Read column 2)
rc4 (Read column 4)
"""


@numba.njit
def check_last_commas(comma):
    # this is mainly in a function to use njit!
    return comma != 0x0


@numba.njit
def is_mightypix_data_lc2(word):
    # Load column 2
    # this should be filtered out by the fpga!
    return (word >> 24 & 0xFF) == 0x1C


@numba.njit
def is_mightypix_data_lp2(word):
    # Load pixel 2
    return (word >> 24 & 0xFF) == 0xC0


@numba.njit
def is_mightypix_data_rc2(word):
    # Read column 2
    # this is not 8'hC1 because of the comma for the 8b10b decoding/encoding
    # return (word >> 22 & 0x3FF) == 0x304
    return (word >> 22 & 0x3FF) == 0xC1


@numba.njit
def is_mightypix_data_rc4(word):
    # Read column 4
    # Read column 4 DataOut <= {2'd0, TSFromDet[19:0], TS2FromDet[9:0]};
    #     CommaOut <= {1'b0,1'b0,1'b0,1'b0};
    return (word >> 30) & 0x3 == 0x0


@numba.njit
def is_mightypix_data_sc1(word):
    # Send Counter 1
    return (word >> 24) & 0xFF == 0x3C


@numba.njit
def get_ts_bin_cnt(word):
    return (word >> 8) & 0xFFFF


@numba.njit
def get_ts_bin_cnt_sc1(word):
    word = get_ts_bin_cnt(word)
    return (word >> 8) & 0xFF + (word & 0xFF << 8)


@numba.njit
def get_ts_gray(word):
    return word & 0xFF


@numba.njit
def get_col(word):
    return (word >> 17) & 0x1F


@numba.njit
def get_row(word):
    # Read column 2 DataOut <= {8'hC1,2'd0, ColAddFromDet[4:0], RowAddFromDet[9:0], TS3FromDet[6:0]};
    # from the mupix code the row seems to be the inverted signal
    return ~(word >> 7) & 0x7F
    # return swap_bit_order((word >> 7) & 0x7F,7)  # just use the 7 used bits?


@numba.njit
def get_ts3(word):
    return word & 0x7F


@numba.njit
def get_ts(word):
    return (word >> 10) & 0xFFFFF


@numba.njit
def get_ts2(word):
    return word & 0x3FF


@numba.njit
def get_tot(word):
    return (((word >> 9) & 0x3F) - ((word >> 15) & 0x3F)) & 0x3F


@numba.njit
def get_noise_flag(word):
    return (word) & 0x3


@numba.njit
def get_token_TS_right(word):
    return (word) & 0x1FFFFFC


@numba.njit
def get_token_TS_left(word):
    return (word) & 0x1FFFFFF


"""
Functions to identify metadata
"""


@numba.njit
def is_rx_timestamp(word):
    return (word >> 23) == 0x1


@numba.njit
def get_timestamp_22bits(word):
    return word & 0x3FFFFF


@numba.njit(numba.uint8(numba.uint8))
def get_physical_row(row_in):
    # take care of double and quadrupel pixel sizes
    row = row_in / 2
    if row > 30:
        row = (row - 31) / 2 + 31
    return row


# @numba.njit
# def get_timestamp640_8bits(word):
#     return word & 0x00000000000000FF
# @numba.njit
# def get_timestamp640_tot(word):
#     return word & 0x0000000000FFFF00
@numba.njit
def is_tlu(word):
    return (word) & 0x80000000 == 0x80000000


@numba.njit
def get_tlu_number(word):
    return word & 0xFFFF


@numba.njit
def get_tlu_timestamp(word):
    return word & 0x7FFF0000


@numba.njit
def gray_decode(i):
    ret = numba.uint8(0)
    while i != 0:
        ret = ret ^ i
        i = i >> 1
    return ret


class Interpreter(object):
    def __init(self):
        self.reset()
        self.reset_hist()
        self.hit_data = np.array([], dtype=hit_dtype) #stores the hits returned by the interpreter function
        self.debug_out = []
        self.error_cnt_obj = 0 #stores all the error counts returned from raw interpreter object
        

    # creating a custom interpret_data function that interprets only a single raw data piece individually and displays it on the online monitor
    def interpret_data_one_unit(self, readout, meta_data=None):
        '''
        readout : readout data of a single readout
        '''

        data_interpreter = RawDataInterpreter()
        #return an empty array if the raw_data is null as well
        if len(readout) == 0:
            pass#print("!!!!!!!!!!!!!!!!!!!!!!!!!!There is zero raw data present!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        else:
            hit_buffer = np.zeros(shape=len(readout), dtype=hit_dtype)
            hit_data_, debug_out_ = data_interpreter.interpret(readout, meta_data, hit_buffer)
            self.hit_data = np.append(self.hit_data, hit_data_)
            self.debug_out += debug_out_
            self.error_cnt_obj += data_interpreter.get_error_count()
    
    def reset_hist(self):
        '''
        reset the histograms in which the hit data is being saved
        '''
        self.hit_data = np.array([], dtype=hit_dtype)
        self.debug_out = []
        self.error_cnt_obj = 0 




    def interpret_data(self, raw_data, meta_data=None, chunk_size=1000000):
        # this is the hit that we want to output
        # note to self maybe we want some additional information in the hits
        
        #data_interpreter = RawDataInterpreter()
        self.reset_hist()
        
        #self.hit_data = np.array([], dtype=hit_dtype)
        #return an empty array if the raw_data is null as well
        if len(raw_data) == 0:
            self.hit_data = np.array([], dtype=hit_dtype)
            print("!!!!!!!!!!!!!!!!!!!!!!!!!!There is zero raw data present!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        else:
            pbar = tqdm(total=len(raw_data))
            start = 0 
            while start < len(raw_data):
                tmpend = start + chunk_size
                self.interpret_data_one_unit(raw_data[start:tmpend], meta_data)
                print(f"interpret_data: length of hit data function evaluated {len(self.hit_data)} and the input raw data till now {len(raw_data[:tmpend])}")
                # hit_buffer = np.zeros(shape=chunk_size, dtype=hit_dtype)
                # hit_data_, debug_out_ = data_interpreter.interpret(raw_data[start:tmpend], meta_data, hit_buffer)
                
                #self.hit_data = np.append(self.hit_data, hit_data_)
                #debug_out += debug_out_
                start = tmpend
                pbar.update(chunk_size)
            pbar.close()

        return self.hit_data, self.error_cnt_obj, self.debug_out



    # def interpret_data_(self, raw_data, interpret_by_readout = False, interpret_by_chunk = False, n_readouts = None, chunk_size = 1000000, meta_data=None):
    #     '''
    #     Analyze raw data to return the number of hits
        
    #     Parameters:
    #     ----------
    #     raw_data: FIFO readout data to be interpreted
    #     interpret_by_readout: interpret data and add to the hits list readout by readout
    #     interpret_by_chunk: interpret one chunk_size data and add to the hits list 
    #     n_readouts: if interpret_by_readout is true, total number of readouts should be given
    #     chunk_size: if interpret_by_chunk is true, chunk_size should be given, default is 1000000
    #     meta_data: contains meta_data for the raw_data
        
    #     '''
    #     # this is the hit that we want to output
    #     # note to self maybe we want some additional information in the hits?
    #     hit_dtype = [
    #         ("det_col", "<u1"),
    #         ("det_row", "<u2"),
    #         ("col", "<u1"),
    #         ("row", "<u2"),
    #         ("ts_bin_cnt", "<u2"),
    #         ("ts_gray", "<u1"),
    #         ("ts_decoded", "<u1"),
    #         ("ts3", "<u1"),
    #         ("ts", "<u4"),
    #         ("ts2", "<u2"),
    #         ("rx_timestamp", "<u4"),
    #         ("scan_param_id", "<u4"),
    #         ("cnt", "<u4"),
    #         ("cnt_order", "<u4"),
    #         ("used_info", "<u1"),
    #         ("rawlp2", "<u4"),
    #         ("rawrc2", "<u4"),
    #         ("rawrc4", "<u4"),
    #         ("rawlsc1", "<u4"),
    #     ]

    #     pbar = tqdm(total=len(raw_data))
    #     start = 0
    #     data_interpreter = RawDataInterpreter()
    #     debug_out = []
    #     actual_readout = 0
    #     readout_word_indeces = np.column_stack((meta_data['index_start'], meta_data['index_stop']))

    #     hit_data = np.array([], dtype=hit_dtype)
    #     raw_data_list_readout = []
    #     meta_data_list_readout = []
        
    #     #return an empty array if the raw_data is null as well
    #     if len(raw_data) == 0:
    #         hit_data = np.array([], dtype=hit_dtype)
    #         print("!!!!!!!!!!!!!!!!!!!!!!!!!!There is zero raw data present")
        
    #     while(actual_readout < n_readouts):
    #         print(f"Number of readout: {actual_readout}")
    #         index_start, index_stop = readout_word_indeces[actual_readout]
    #         raw_data_list_readout.append(raw_data[index_start:index_stop])
    #         meta_data_list_readout.extend((float(meta_data[actual_readout]['timestamp_start']), float(meta_data[actual_readout]['timestamp_stop']), int(meta_data[actual_readout]['error'])))
            
    #         hit_buffer = np.zeros(shape=len(range(index_start,index_stop+1)), dtype=hit_dtype)
    #         hit_data_, debug_out_ = data_interpreter.interpret(raw_data[index_start:index_stop], meta_data, hit_buffer)
    #         hit_data = np.append(hit_data, hit_data_)
    #         debug_out += debug_out_
    #         pbar.update(len(range(index_start,index_stop+1)))
    #         actual_readout += 1
    #     pbar.close()

    #     return hit_data, data_interpreter.get_error_count(), debug_out



# @numba.experimental.jitclass(class_spec)
class RawDataInterpreter(object):
    def __init__(self):
        self.reset()
        self.error_cnt = 0
        self.error_cnt_order = 0
        self.raw_idx = 0
        self.meta_idx = 0
        self.raw4x32 = np.zeros(4, dtype=np.uint32)
        

    def reset(self):
        """
        Reset all values that are computed from multiple data words
        """
        # these are the temporary storrage binaryies to build that 32 bits that we need
        self.last32bits = 0
        self.lastcommas = 0

        # Data from the chip
        self.mightypix_data_flag = 0
        self.mightypix_token_timestamp = 0

        # Timestamp from RX1 sampled in FPGA
        self.rx_timestamp_flag = 0
        self.rx_timestamp = 0
        self.lastcommas = 0xF
        self.used_info = 0

    def get_error_count(self):
        return self.error_cnt

    def interpret(self, raw_data, meta_data, hit_data):
        """
        The data comes from the fpga in two 32bit words that contain one 32bit data word and additional information:
        Original data send:
        Sync DataOut <= {8'hBC, 8'hBC, 8'hBC, 8'hBC};
        CommaOut <= {1'b1,1'b1,1'b1,1'b1};
        Send counter 1 DataOut <= {K_28_1,timestamp_bin_counter[7:0] , timestamp_bin_counter[15:8], 8'd0};
        CommaOut <= 4'b1_0_0_0;
        Load column 2 DataOut <= {8'h1C, 8'hAA, 8'h1C, 8'hAA};
        CommaOut <= {1'b1,1'b0,1'b1,1'b0};
        Load pixel 2 DataOut <= {8'hC0, timestamp_bin_counter[15:8], timestamp_bin_counter[7:0], timestamp_gray_value[7:0]};
        CommaOut <= 4'b0_0_0_0;
        Read column 2 DataOut <= {8'hC1,2'd0, ColAddFromDet[4:0], RowAddFromDet[9:0], TS3FromDet[6:0]};
        CommaOut <= {1'b0,1'b0,1'b0,1'b0};
        Read column 4 DataOut <= {2'd0, TSFromDet[19:0], TS2FromDet[9:0];
        CommaOut <= {1'b0,1'b0,1'b0,1'b0};
        In all other cases, the data out register is filled with following pattern
        DataOut <= {8'hBC, 8'hBC, 8'hBC, 8'hBC};
        CommaOut <= {1'b1,1'b1,1'b1,1'b1};

        this will be send in chunks:
        {5'b0, last_dw_lc2, data_dec_k_in[0], data_dec_k_in[1], data_dec_in[0], data_dec_in[1]};
        or
        {8'b meta, 8'b dw0 , 8'bdw1}
        the 8b meta data is: 5'b0, bit if last dw was a load column 2, comma word for dw0, comma word for dw1

        or it will send a 23bit timesamp from the fpga:
        {1'b1,23'b fpga_clock_counter_buf}
        """

        hit_index = 0
        debug_out = []

        for raw_data_word in raw_data:

            ##################
            # mightypix DATA #
            ##################

            if is_rx_timestamp(raw_data_word):
                if self.rx_timestamp_flag != 0:
                    self.reset()
                    self.error_cnt += 1
                    debug_out.append(("reset: ", 5))
                    continue

                self.rx_timestamp = get_timestamp_22bits(raw_data_word)
                self.rx_timestamp_flag = 1
                self.last32bits = 0
                self.lastcommas = 0xF
                debug_out.append(("timestamp: ", raw_data_word))
            else:
                # we have a dataword
                # since the important data can start either at the first or second dataword lets make a for loop for that.
                # This might be not good for timing, but something for the future to invesigate
                # debug_out.append(("raw_dw: ",raw_data_word))
                for iraw in range(1, -1, -1):
                    #this line shifts the variable last32bits by 8 places and adds the new data word by bitwise OR
                    self.last32bits = 0xFFFF & (get_raw_dataword(raw_data_word, iraw) | self.last32bits << 8)
                    # print("raw data", hex(self.last32bits))
                    self.lastcommas = 0xF & (get_raw_commaword(raw_data_word, iraw) | self.lastcommas << 1)
                    # print(f"last32bits {self.last32bits:x}, lastcommas {self.lastcommas:x}, iraw: {iraw:x}")
                    # print("last32bits {%x}, lastcommas {%x}, iraw: {%x}".format(self.last32bits,self.lastcommas,iraw))
                    if not check_last_commas(self.lastcommas):
                        debug_out.append(("last32bits: ", self.last32bits))
                        # debug_out.append(("lastcommas: ",self.lastcommas))
                        # debug_out.append(("iraw: ",iraw))
                    if check_last_commas(self.lastcommas):
                        # debug_out.append(("last commas_continue: ",0))
                        self.error_cnt_order += 1
                        continue
                    elif is_mightypix_data_sc1(self.last32bits):
                        debug_out.append(("lsc1: ", self.mightypix_data_flag))
                        # Load pixel 2 DataOut <= {8'hC0, timestamp_bin_counter[15:8], timestamp_bin_counter[7:0], timestamp_gray_value[7:0]};
                        # CommaOut <= 4'b0_0_0_0;
                        self.ts_bin_cnt = get_ts_bin_cnt_sc1(self.last32bits)
                        self.used_info = self.used_info | 0x8
                        self.raw4x32[3] = self.last32bits
                    elif is_mightypix_data_lp2(self.last32bits):
                        print("lp2")
                        debug_out.append(("lp2: ", self.mightypix_data_flag))
                        if self.mightypix_data_flag != 0 and self.mightypix_data_flag != 3:
                            self.reset()
                            debug_out.append(("reset: ", 0))
                            self.error_cnt += 1
                            continue
                        # Load pixel 2 DataOut <= {8'hC0, timestamp_bin_counter[15:8], timestamp_bin_counter[7:0], timestamp_gray_value[7:0]};
                        # CommaOut <= 4'b0_0_0_0;
                        self.ts_bin_cnt = get_ts_bin_cnt(self.last32bits)
                        self.ts_gray = get_ts_gray(self.last32bits)
                        self.ts_decoded = gray_decode(self.ts_gray)
                        # TODO add decoding here!
                        self.mightypix_data_flag = 1
                        self.rx_timestamp_flag = 0
                        self.lastcommas = 0xF
                        self.raw4x32[0] = self.last32bits
                        self.used_info = self.used_info | 0x1

                    elif is_mightypix_data_rc2(self.last32bits):
                        print("rc2")
                        debug_out.append(("rc2: ", self.mightypix_data_flag))
                        if self.mightypix_data_flag != 1 and self.mightypix_data_flag != 3:
                            self.reset()
                            debug_out.append(("reset: ", 2))
                            self.error_cnt += 1
                            continue
                        # Read column 2 DataOut <= {8'hC1,2'd0, ColAddFromDet[4:0], RowAddFromDet[9:0], TS3FromDet[6:0]};
                        # CommaOut <= {1'b0,1'b0,1'b0,1'b0};
                        # one thing to keep in mind here is that the physical pixel numbering differs from the digital implementaitons of the Run2020 pixel
                        self.col = get_col(self.last32bits)
                        self.row = get_row(self.last32bits)
                        self.ts3 = get_ts3(self.last32bits)
                        self.mightypix_data_flag = 2
                        self.rx_timestamp_flag = 0
                        self.lastcommas = 0xF
                        self.raw4x32[1] = self.last32bits
                        self.used_info = self.used_info | 0x2

                    elif is_mightypix_data_rc4(self.last32bits):
                        # print("rc4")
                        # this
                        debug_out.append(("rc4: ", self.mightypix_data_flag))
                        if self.mightypix_data_flag != 2:
                            self.reset()
                            debug_out.append(("reset: ", 3))
                            self.error_cnt += 1
                            continue

                        # Read column 4 DataOut <= {2'd0, TSFromDet[19:0], TS2FromDet[9:0]}
                        # CommaOut <= {1'b0,1'b0,1'b0,1'b0};

                        self.ts = get_ts(self.last32bits)
                        self.ts2 = get_ts2(self.last32bits)
                        self.used_info = self.used_info | 0x4
                        self.tot = get_tot(self.last32bits)
                        self.raw4x32[2] = self.last32bits
                        # Fill the information for the hit
                        # This is what we need to fill:
                        # TODO put a variable that switches the chip version!!
                        hit_data[hit_index]["det_row"] = self.row
                        # hit_data[hit_index]["det_row"] = get_physical_row(self.row)
                        hit_data[hit_index]["det_col"] = self.col - 2  # the first col has the address 2
                        hit_data[hit_index]["row"] = self.row
                        hit_data[hit_index]["col"] = self.col
                        hit_data[hit_index]["ts_bin_cnt"] = self.ts_bin_cnt
                        hit_data[hit_index]["ts_gray"] = self.ts_gray
                        hit_data[hit_index]["ts_decoded"] = self.ts_decoded
                        hit_data[hit_index]["ts3"] = self.ts3
                        hit_data[hit_index]["ts2"] = self.ts2
                        hit_data[hit_index]["ts"] = self.ts
                        hit_data[hit_index]["rx_timestamp"] = self.rx_timestamp
                        hit_data[hit_index]["scan_param_id"] = self.raw_idx
                        hit_data[hit_index]["cnt"] = self.error_cnt
                        hit_data[hit_index]["cnt_order"] = self.error_cnt_order
                        hit_data[hit_index]["used_info"] = self.used_info
                        hit_data[hit_index]["rawlp2"] = self.raw4x32[0]
                        hit_data[hit_index]["rawrc2"] = self.raw4x32[1]
                        hit_data[hit_index]["rawrc4"] = self.raw4x32[2]
                        hit_data[hit_index]["rawlsc1"] = self.raw4x32[3]
                        hit_data[hit_index]["tot"] = self.tot #the method of subtraction of values directly i.e. ts - ts2 to get the tot does not give consistent results.  
                        # Prepare for next hit: Increase hit index and reset mightypix_data_flag
                        hit_index += 1
                        self.used_info = 0x0
                        self.raw4x32[:] = 0
                        self.mightypix_data_flag = 3
                        self.rx_timestamp_flag = 0
                        self.lastcommas = 0xF

            self.raw_idx += 1

        # Trim hit_data buffer to interpreted data hits
        hit_data = hit_data[:hit_index]

        # Find correct scan_param_id in meta data and attach to hit.
        if meta_data is not None:
            for scan_idx, param_id in enumerate(hit_data["scan_param_id"]):
                while self.meta_idx < len(meta_data):
                    if param_id >= meta_data[self.meta_idx]["index_start"] and param_id < meta_data[self.meta_idx]["index_stop"]:
                        hit_data[scan_idx]["scan_param_id"] = meta_data[self.meta_idx]["scan_param_id"]
                        break
                    elif param_id >= meta_data[self.meta_idx]["index_stop"]:
                        self.meta_idx += 1
        else:
            pass
        return hit_data, debug_out
