import sys
import time
import os
import numpy as np
import numba
from tqdm import tqdm
import tables as tb



## not used at all
class_spec = [
    ("chunk_size", numba.uint32),
    ("wcnt", numba.uint8),
    ("used_cnt", numba.int32),
    ("mightypix_data_flag", numba.uint8),
    ("mightypix_token_timestamp", numba.int64),
    ("rx_timestamp_flag", numba.uint8),
    ("rx_timestamp", numba.int64),
    ("error_cnt", numba.uint32),
    ("error_cnt_order", numba.uint32),
    ("col", numba.uint8),
    ("row", numba.uint8),
    ("ts", numba.uint32),
    ("ts_gray", numba.uint16),
    ("ts_decoded", numba.uint16),
    ("ts2", numba.uint16),
    ("ts3", numba.uint16),
    ("ts3_gray", numba.uint16),
    ("ts11", numba.uint16),
    ("ts12_19b", numba.uint16),
    ("ts12_20b", numba.uint16),
    ("tot", numba.uint16),
    ("ts_bin_cnt", numba.uint16),
    ("meta_idx", numba.uint32),
    ("raw_idx", numba.uint32),
    ("last32bits", numba.uint32),
    ("last32bits_old", numba.uint32),
    ("hit_index", numba.int64),
    ("lastcommas", numba.uint8),
    ("used_info", numba.uint8),
    ("raw4x32", numba.uint32[:]),
    ("event_number", numba.int64),
]


# data fromat from FPGA
#  0    1    2    3   4   5   6    7
# C0, cnt, cnt, cnt,
# C1, col, row, ts3, ts, ts, ts, ts2
# More precisely,
# {K_28_0, 8'hAA, K_28_0, 8'hAA}
# {8'hC0, timestamp_bin_counter[15:8], timestamp_bin_counter[7:0], timestamp_gray_value[7:0]};
# {8'hC1,2'd0, ColAddFromDet[4:0], RowAddFromDet[9:0], TS3FromDet[6:0]};
# {2'd0, TS12FromDet[10:0],TS11FromDet[10:0], TS2FromDet[9:0]};

"""
First we separate the data into the dataword, and the comma information to check for errors:
"""

ts_dtype = [("event_number", "<i8"), 
            ("timestamp", "<i8"), 
            ('err', '<u1')]

hit_dtype =  [("event_number", "<i8"), 
                ("col", "<u1"),
                ("row", "<u2"), 
                ("ts11", "<u2"),
                ("ts12", "<u2"), 
                ("ts2", "<u2"), 
                ("ts3", "<u1"),
                ("timestamp", "<u2"), 
                ('err', '<u1'),
                ('scan_param_id', '<u4')
                ]


# [
#         ("det_col", "<u1"),
#         ("det_row", "<u2"),
#         ("col", "<u1"),
#         ("row", "<u2"),
#         ("ts_bin_cnt", "<u2"),
#         ("ts_gray", "<u1"),
#         ("ts_decoded", "<u1"),
#         ("ts3", "<u1"),
#         ("ts", "<u4"),
#         ("ts2", "<u2"),
#         ("rx_timestamp", "<u4"),
#         ("scan_param_id", "<u4"),
#         ("cnt", "<u4"),
#         ("cnt_order", "<u4"),
#         ("used_info", "<u1"),
#         ("rawlp2", "<u4"),
#         ("rawrc2", "<u4"),
#         ("rawrc4", "<u4"),
#         ("rawlsc1", "<u4"),
#         ("tot","<u2")
#     ]

@numba.njit
def swap_bit_order(value, n_bits):
    return_value = numba.uint8(0)
    count = 0
    while n_bits > 0:
        n_bits = n_bits - 1
        if n_bits >= count:
            return_value = return_value | ((value & 1 << n_bits) >> (n_bits - count))
        else:
            return_value = return_value | ((value & 1 << n_bits) << (count - n_bits))
        count += 1
    return return_value


@numba.njit
def get_raw_dataword(word, iraw):
    # get either the first or the second dataword
    # print("This is the raw data word", 0xFF & (word >> 8 * iraw))
    return 0xFF & (word >> 8 * iraw)




@numba.njit
def get_raw_dataword_8b(word):
    # get 8b data words out of 32 if word looks like this x = 0xa1a2a3a4
    a4 = word & 0xff
    a3 = (word >> 8) & 0xff
    a2 = (word >> 16) & 0xff
    a1 = word >> 24
    not_in_table_err=(word >> 18) & 0x3
    disp_err=(word >> 20) & 0x3
    # print(hex(word), 0xFF & a1)
    return (a1,a2,a3,a4,disp_err,not_in_table_err)

@numba.njit
def get_raw_commaword(word, iraw):
    # get the comma words just to check
    return ( ((word >> 16) & 0x3) >> iraw) & 0x1


@numba.njit
def get_wcnt(word):
    # get the wcnt for the current dw
    return ((word >> 24) & 0xF)
"""
There are states where the chip sends out data:
lc2 (Load column 2)
lp2 (Load pixel 2)
rc2 (Read column 2)
rc4 (Read column 4)
"""


@numba.njit
def check_last_commas(comma):
    # this is mainly in a function to use njit!
    return comma != 0x0


@numba.njit
def check_last_commas_first_bit(comma):
    # this is mainly in a function to use njit!
    return comma == 0x8


@numba.njit
def is_mightypix_data_lc2(word):
    # Load column 2
    # this should be filtered out by the fpga!
    return (word >> 24 & 0xFF) == 0x1C


@numba.njit
def is_mightypix_data_lp2(word):
    # Load pixel 2
    return (word >> 24 & 0xFF) == 0xC0


@numba.njit
def is_mightypix_data_rc2(word):
    # Read column 2
    # this is not 8'hC1 because of the comma for the 8b10b decoding/encoding
    # return (word >> 22 & 0x3FF) == 0x304
    # return (word >> 22 & 0x3FF) == 0x3C1
    # print("read_col 2", (word >> 24 & 0xFF))
    # print(word)
    return (word >> 24 & 0xFF) == 0xC1


@numba.njit
def is_mightypix_data_rc4(word):
    # Read column 4
    # Read column 4 DataOut <= {2'd0, TSFromDet[19:0], TS2FromDet[9:0]};
    #     CommaOut <= {1'b0,1'b0,1'b0,1'b0};
    # print("read col 4",(word >> 30) & 0x3  )
    return (word >> 30) & 0x3 == 0x0


@numba.njit
def is_mightypix_data_sc1(word):
    # Send Counter 1
    return (word >> 24) & 0xFF == 0x3C


@numba.njit
def get_ts_bin_cnt(word):
    return (word >> 8) & 0xFFFF


@numba.njit
def get_ts_bin_cnt_sc1(word):
    word = get_ts_bin_cnt(word)
    return (word >> 8) & 0xFF + (word & 0xFF << 8)


@numba.njit
def get_ts_gray(word):
    return word & 0xFF


@numba.njit
def get_col(word):
    return ~((word >> 17) & 0x7F)


@numba.njit
def get_row(word):
    # Read column 2 DataOut <= {8'hC1 ColAddFromDet[7:0], RowAddFromDet[9:0], TS3FromDet[6:0]};
    row = (word >> 7) & 0x3FF
    return ~row


@numba.njit
def get_ts3(word):
    return word & 0x7F


@numba.njit
def get_ts11(word):
    return (word >> 10) & 0x3FF

@numba.njit
def get_ts12_19b(word):
    return (word >> 20) & 0x2FF


@numba.njit
def get_ts12_20b(word):
    return (word >> 29) & 0x1


@numba.njit
def get_ts2(word):
    return word & 0x3FF


@numba.njit
def get_tot(word):
    return (((word >> 9) & 0x3F) - ((word >> 15) & 0x3F)) & 0x3F


@numba.njit
def get_noise_flag(word):
    return (word) & 0x3


@numba.njit
def get_token_TS_right(word):
    return (word) & 0x1FFFFFC


@numba.njit
def get_token_TS_left(word):
    return (word) & 0x1FFFFFF


"""
Functions to identify metadata
"""


@numba.njit
def is_FPGA_timestamp(word):
    return (word & 0xF000_0000) == 0x8000_0000
    # return (word >> 23) == 0x1


@numba.njit
def get_timestamp_22bits(word):
    return word & 0x3FFFFF



@numba.njit
def is_tlu(word):
    return (word) & 0x80000000 == 0x80000000


@numba.njit
def get_tlu_number(word):
    return word & 0xFFFF


@numba.njit
def get_tlu_timestamp(word):
    return word & 0x7FFF0000


@numba.njit
def gray_decode(i):
    ret = numba.uint8(0)
    while i != 0:
        ret = ret ^ i
        i = i >> 1
    return ret


class Interpreter(object):
    def __init__(self):
        # self.reset()
        self.reset_hist()
        self.hit_data = np.array([], dtype=hit_dtype) #stores the hits returned by the interpreter function
        self.ts_data = np.array([], dtype=ts_dtype)
        self.debug_out = []
        self.error_cnt_obj = 0 #stores all the error counts returned from raw interpreter object
        

    # creating a custom interpret_data function that interprets only a single raw data piece individually and displays it on the online monitor
    def interpret_data_one_unit(self, readout, meta_data=None):
        '''
        readout : readout data of a single readout
        '''
        data_interpreter = RawDataInterpreter()
        self.reset_hist()
        #return an empty array if the raw_data is null as well
        if len(readout) == 0:
            pass#print("!!!!!!!!!!!!!!!!!!!!!!!!!!There is zero raw data present!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        else:
            # print("entered one data unit interpreter"
            # print(readout)
            # print(len(readout))
            hit_buffer = np.empty(shape=len(readout), dtype=hit_dtype)
            # print(hit_dtype)
            # print("hitbuffer", len(hit_buffer))
            # print(hit_buffer["event_number"])
            ts_buffer = np.empty(shape=len(readout), dtype=ts_dtype)
            # print(hit_buffer)
            hit_data_, debug_out_, ts_data_ = data_interpreter.interpret(readout, meta_data, hit_buffer, ts_buffer)
            self.hit_data = np.append(self.hit_data, hit_data_)
            # print(hit_data_[-1]["scan_param_id"])
            # print(hit_buffer)
            # print( self.hit_data)
            self.ts_data = np.append(self.ts_data, ts_data_)
            self.debug_out += debug_out_
            self.error_cnt_obj += data_interpreter.get_error_count()
            # print("length after one data unit interpreter", len(self.hit_data))
    
    def reset_hist(self):
        '''
        reset the histograms in which the hit data is being saved
        '''
        self.hit_data = np.array([], dtype=hit_dtype)
        self.debug_out = []
        self.error_cnt_obj = 0 




    def interpret_data(self, raw_data, meta_data=None, chunk_size=100000):
        # this is the hit that we want to output
        
        #data_interpreter = RawDataInterpreter()
        self.reset_hist()
        
        #self.hit_data = np.array([], dtype=hit_dtype)
        #return an empty array if the raw_data is null as well
        if len(raw_data) == 0:
            self.hit_data = np.array([], dtype=hit_dtype)
            print("!!!!!!!!!!!!!!!!!!!!!!!!!!There is zero raw data present!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        else:
            # print("Start interpreting")
            pbar = tqdm(total=len(raw_data))
            start = 0 
            # print(len(raw_data))
            while start < len(raw_data):
                tmpend = start + chunk_size
                self.interpret_data_one_unit(raw_data[start:tmpend], meta_data)

                # print(f"interpret_data: length of hit data function evaluated {len(self.hit_data)} and the input raw data till now {len(raw_data[:tmpend])}")
                # hit_buffer = np.zeros(shape=chunk_size, dtype=hit_dtype)
                # hit_data_, debug_out_ = data_interpreter.interpret(raw_data[start:tmpend], meta_data, hit_buffer)
                
                #self.hit_data = np.append(self.hit_data, hit_data_)
                #debug_out += debug_out_
                start = tmpend
                pbar.update(chunk_size)
            pbar.close()
        # print("interpreter data length", len(self.hit_data))
        # print("check",self.hit_data)
        return self.hit_data, self.error_cnt_obj, self.debug_out



@numba.experimental.jitclass(class_spec)
class RawDataInterpreter(object):
    def __init__(self):
        self.reset()
        self.error_cnt = 0
        self.error_cnt_order = 0
        self.raw_idx = -1
        self.meta_idx = 0
        self.raw4x32 = np.zeros(4, dtype=np.uint32)
        self.used_cnt=0
        

    def reset(self):
        """
        Reset all values that are computed from multiple data words
        """
        # these are the temporary storage binaries to build that 32 bits that we need
        self.last32bits = 0

        # Data from the chip
        self.mightypix_data_flag = 0
        self.mightypix_token_timestamp = 0

        # Timestamp from RX1 sampled in FPGA
        self.rx_timestamp_flag = 0
        self.rx_timestamp = 0
        self.used_info = 0
        self.used_cnt=0

    def get_error_count(self):
        return self.error_cnt

    def interpret(self, raw_data, meta_data, hit_data, ts_data):
        """
        The data comes from the fpga in two 32bit words that contain one 32bit data word and additional information:
        Original data send:
        Sync DataOut <= {8'hBC, 8'hBC, 8'hBC, 8'hBC};
        CommaOut <= {1'b1,1'b1,1'b1,1'b1};
        Send counter 1 DataOut <= {K_28_1,timestamp_bin_counter[7:0] , timestamp_bin_counter[15:8], 8'd0};
        CommaOut <= 4'b1_0_0_0;
        Load column 2 DataOut <= {8'h1C, 8'hAA, 8'h1C, 8'hAA};
        CommaOut <= {1'b1,1'b0,1'b1,1'b0};
        Load pixel 2 DataOut <= {8'hC0, timestamp_bin_counter[15:8], timestamp_bin_counter[7:0], timestamp_gray_value[7:0]};
        CommaOut <= 4'b0_0_0_0;
        Read column 2 DataOut <= {8'hC1,2'd0, ColAddFromDet[4:0], RowAddFromDet[9:0], TS3FromDet[6:0]};
        CommaOut <= {1'b0,1'b0,1'b0,1'b0};
        Read column 4 DataOut <= {2'd0, TSFromDet[19:0], TS2FromDet[9:0];
        CommaOut <= {1'b0,1'b0,1'b0,1'b0};
        In all other cases, the data out register is filled with following pattern
        DataOut <= {8'hBC, 8'hBC, 8'hBC, 8'hBC};
        CommaOut <= {1'b1,1'b1,1'b1,1'b1};

        this will be send in chunks:
        {5'b0, last_dw_lc2, data_dec_k_in[0], data_dec_k_in[1], data_dec_in[0], data_dec_in[1]};
        or
        {8'b meta, 8'b dw0 , 8'bdw1}
        the 8b meta data is: 5'b0, bit if last dw was a load column 2, comma word for dw0, comma word for dw1

        or it will send a 23bit timesamp from the fpga:
        {1'b1,23'b fpga_clock_counter_buf}
        """

        # hit_index = np.dtype('<u8')
        self.hit_index = 0
        # print("type", type(hit_index))
        debug_out = []
        t_i = -1
        self.used_cnt=0
        # print("len data", len(raw_data))
        for raw_data_word in raw_data:
            # print("raw data word", hex(raw_data_word))
            self.raw_idx += 1
            ##################
            # FPGA timestamp DATA #
            ##################
            if is_FPGA_timestamp(raw_data_word):
                if raw_data_word & 0x0F00_0000 == 0x300_0000:
                    t_i = t_i + 1
                    # if t_i >= len(ts_data):
                    #     break
                        # return hit_index
                    ts_data[t_i]['timestamp'] = np.int64(raw_data_word & 0x0000_7FFF) << 48
                    ts_data[t_i]['err'] = 0x3
                    ts_data[t_i]['event_number'] = self.hit_index
                elif ts_data[t_i]['err'] == 0x3:
                    if raw_data_word & 0x0F00_0000 == 0x200_0000:
                        ts_data[t_i]['timestamp'] = ts_data[t_i]['timestamp'] | (np.int64(raw_data_word & 0x00FF_FFFF) << 24)
                        ts_data[t_i]['err'] = 0x2
                    else:
                        ts_data[t_i]['timestamp'] = np.int64(raw_data_word)
                elif ts_data[t_i]['err'] == 0x2:
                    if raw_data_word & 0x0F00_0000 == 0x100_0000:
                        ts_data[t_i]['timestamp'] = ts_data[t_i]['timestamp'] | np.int64(raw_data_word& 0x00FF_FFFF)
                        ts_data[t_i]['err'] = 0x0
                    else:
                        ts_data[t_i]['timestamp'] = np.int64(raw_data_word)


            ##################
            # DUT DATA #
            ##################
            else:
                
                # since the important data can start either at the first or second dataword lets make a for loop for that.
                # This might be not good for timing, but something for the future to investigate
                # 32b contain 4b info, 24b data word (3x8b), 4b data identifier
                self.wcnt=get_wcnt(raw_data_word)
                self.last32bits_old  = 0
                
                for iraw in range(2, -1, -1):
                    #this line shifts the variable last32bits by 8 places and adds the new data word by bitwise OR
                    self.last32bits = 0xFFFFFFFF & ( get_raw_dataword(raw_data_word, iraw) | self.last32bits << 8 )
                    self.used_cnt=self.used_cnt-1
                    if self.used_cnt>0:
                        continue
                    if self.last32bits_old == self.last32bits:
                        # print(self.last32bits_old ,self.last32bits)
                        continue
                    else:
                        # check word cnter
                        info = get_wcnt(raw_data_word)
                        if info == 0xE:
                            info == 0
                        
                        # check counter mode   
                        if is_mightypix_data_sc1(self.last32bits):
                            # print("Sc1")
                            debug_out.append(("lsc1: ", self.mightypix_data_flag))
                            self.ts_bin_cnt = get_ts_bin_cnt_sc1(self.last32bits)
                            self.used_info = self.used_info | 0x8
                            self.raw4x32[3] = self.last32bits
                            self.used_cnt=4
                        
                        if is_mightypix_data_lp2(self.last32bits):
                            # Load pixel 2 DataOut <= {8'hC0, timestamp_bin_counter[15:8], timestamp_bin_counter[7:0], timestamp_gray_value[7:0]};
                            # CommaOut <= 4'b0_0_0_0;
                            debug_out.append(("lp2: ", self.mightypix_data_flag))
                            if self.mightypix_data_flag != 0 and self.mightypix_data_flag != 3:
                                self.reset()
                                debug_out.append(("reset: ", 0))
                                self.error_cnt += 1
                                continue
                            self.ts_bin_cnt = get_ts_bin_cnt(self.last32bits)
                            self.ts_gray = get_ts_gray(self.last32bits)
                            self.ts_decoded = gray_decode(self.ts_gray)
                            self.mightypix_data_flag = 1
                            self.rx_timestamp_flag = 0
                            self.raw4x32[0] = self.last32bits
                            self.used_info = self.used_info | 0x1
                            self.used_cnt=4

                        elif is_mightypix_data_rc2(self.last32bits):
                            # print("Rc2")
                            # Read column 2 DataOut <= {8'hC1,2'd0, ColAddFromDet[4:0], RowAddFromDet[9:0], TS3FromDet[6:0]};
                            # CommaOut <= {1'b0,1'b0,1'b0,1'b0};
                            if self.mightypix_data_flag != 1 and self.mightypix_data_flag != 3:
                                self.reset()
                                debug_out.append(("reset: ", 2))
                                self.error_cnt += 1
                                continue
                            self.col = get_col(self.last32bits) 
                            self.row = get_row(self.last32bits)
                            self.ts3_gray = get_ts3(self.last32bits)
                            self.ts3 = self.ts3_gray
                            self.mightypix_data_flag = 2
                            self.rx_timestamp_flag = 0
                            self.raw4x32[1] = self.last32bits
                            self.used_info = self.used_info | 0x2
                            self.used_cnt=4

                        # elif is_mightypix_data_rc4(self.last32bits) and self.wcnt >= 0x4:
                        elif is_mightypix_data_rc4(self.last32bits):
                            # print("here")
                            # Read column 4 DataOut <= {2'd0, TSFromDet[19:0], TS2FromDet[9:0]}
                            # CommaOut <= {1'b0,1'b0,1'b0,1'b0};
                            debug_out.append(("rc4: ", self.mightypix_data_flag))
                            if self.mightypix_data_flag != 2:
                                self.reset()
                                debug_out.append(("reset: ", 3))
                                self.error_cnt += 1
                                continue
                            
                            print("check")
                            self.ts11 = gray_decode(get_ts11(self.last32bits))
                            self.ts12_19b = gray_decode(get_ts12_19b(self.last32bits))
                            self.ts12_20b = gray_decode(get_ts12_20b(self.last32bits))
                            self.ts2 = get_ts2(self.last32bits)
                            self.used_info = self.used_info | 0x4
                            self.raw4x32[2] = self.last32bits
                            # self.event_number = self.hit_index
                            debug_out.append(("used_info: ", self.used_info))
                            debug_out.append(("raw4x32: ",self.raw4x32[2]))
                            print("timestamp red col4", self.ts11, self.ts12_19b, self.ts12_20b, self.ts2 )

                            if self.row == 0 and self.col == 0 and self.event_number == 0:
                                print( "zeros detected", self.event_number, self.col, self.row)
                        
                        
                            # print( "evn, col, row", self.event_number, self.col, self.row)
                            hit_data[self.hit_index]["event_number"] = self.hit_index
                            hit_data[self.hit_index]["col"] = self.col
                            hit_data[self.hit_index]["row"] = self.row
                            # hit_data[self.hit_index]["phys_row"] = get_physical_row(self.row)
                            # hit_data[self.hit_index]["is_phys_row"] = is_physical_row(self.row)
                            hit_data[self.hit_index]["ts11"] = self.ts11
                            hit_data[self.hit_index]["ts12"] = self.ts12_19b
                            hit_data[self.hit_index]["ts2"] = self.ts2
                            hit_data[self.hit_index]["ts3"] = self.ts3
                            hit_data[self.hit_index]['timestamp'] = ts_data[t_i]['timestamp']
                            hit_data[self.hit_index]['err'] = info 
                            hit_data[self.hit_index]["scan_param_id"] = self.raw_idx
                        
                            print("rc4",  hit_data[self.hit_index]["event_number"])
                            # print(hit_data[self.hit_index])
                            # print("rc4",self.hit_index,"event",  hit_data[self.hit_index]["event_number"],"raw4x32: ",hex(self.raw4x32[0]), hex(self.raw4x32[1]),hex(self.raw4x32[2]),"id", hit_data[self.hit_index]["scan_param_id"] )
                            
                            # print("col", hit_data[self.hit_index]["col"], hit_data[self.hit_index]["row"])
                            self.hit_index += 1
                            self.used_info = 0x0
                        
                           
                            self.mightypix_data_flag = 3
                            self.rx_timestamp_flag = 0
                            self.used_cnt=4
                            
                        
                        self.last32bits_old = self.last32bits

        
        # print(hit_data)
        # print("length of hit data in interpret function() step1", len(hit_data))
        # Trim hit_data buffer to interpreted data hits
        # print("hit index", hit_data[:self.hit_index])
        # 
        hit_data = hit_data[:self.hit_index]
        # print("length of hit data in interpret function() step2", len(hit_data))
        # Find correct scan_param_id in meta data and attach to hit.
        # print(self.meta_idx , len(meta_data),hit_data["scan_param_id"])
        if meta_data is not None:
            for scan_idx, param_id in enumerate(hit_data["scan_param_id"]):
                while self.meta_idx < len(meta_data):
                    # print("nwnfln", param_id, meta_data[self.meta_idx]["index_stop"])
                    if param_id >= meta_data[self.meta_idx]["index_start"] and param_id < meta_data[self.meta_idx]["index_stop"]:
                        hit_data[scan_idx]["scan_param_id"] = meta_data[self.meta_idx]["scan_param_id"]
                        break
                    elif param_id >= meta_data[self.meta_idx]["index_stop"]:
                        self.meta_idx += 1
        else:
            pass
        
        # print(hit_index)
        # print(hit_data[hit_index]["col"], hit_data[hit_index]["row"], hit_data[hit_index]["event_number"])
        
        return hit_data, debug_out, ts_data
