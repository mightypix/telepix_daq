#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import os
import sys

import numpy as np
import tables as tb
from telepix_daq.analysis import analysis_utils as au
from pixel_clusterizer.clusterizer import HitClusterizer
from telepix_daq.analysis.interpreter_clean import RawDataInterpreter
from telepix_daq.system import logger
from tqdm import tqdm
from telepix_daq.analysis.events import build_events
from telepix_daq.analysis import analysis_power as ap
from telepix_daq.analysis import time_resolution_analysis as tra
import time
import numba
# import tracemalloc

import gc
# from memory_profiler import profile

import pixel_clusterizer
# print(pixel_clusterizer.__file__)



FILTER_TABLES = tb.Filters(complib='zlib', complevel=5, fletcher32=False)


class PowerTable(tb.IsDescription):
    scan_param_id = tb.UInt32Col(pos=1)
    current_channel_1 = tb.Float32Col(pos=2)
    voltage_channel_1 = tb.Float32Col(pos=3)
    current_channel_2 = tb.Float32Col(pos=4)
    voltage_channel_2 = tb.Float32Col(pos=5)
    current_channel_3 = tb.Float32Col(pos=6)
    voltage_channel_3 = tb.Float32Col(pos=7)
    current_channel_4 = tb.Float32Col(pos=8)
    voltage_channel_4 = tb.Float32Col(pos=9)
    currentHV_channel_1 = tb.Float32Col(pos=10)
    voltageHV_channel_1 = tb.Float32Col(pos=11)
    currentHV_channel_2 = tb.Float32Col(pos=12)
    voltageHV_channel_2 = tb.Float32Col(pos=13)
    temperature_sensor_1 = tb.Float32Col(pos=14)
    temperature_sensor_2 = tb.Float32Col(pos=15)
    temperature_sensor_3 = tb.Float32Col(pos=16)
    temperature_sensor_4 = tb.Float32Col(pos=17)


class Analysis(object):
    def __init__(self, raw_data_file=None, analyzed_data_file=None,
                 store_hits=True, cluster_hits=False, analyze_tdc=False, use_tdc_trigger_dist=False,
                 build_events=False, chunk_size=100000000, analysis_timestamp = False, file_name_add_on = '',**_):
        self.log = logger.setup_derived_logger('Analysis')

        self.raw_data_file = raw_data_file
        self.analyzed_data_file = analyzed_data_file
        self.store_hits = store_hits
        self.cluster_hits = cluster_hits
        self.build_events = build_events
        self.chunk_size = chunk_size
        self.analyze_tdc = analyze_tdc
        self.use_tdc_trigger_dist = use_tdc_trigger_dist
        self.tot_calib_file=False

        if self.build_events:
            self.cluster_hits = True

        if not os.path.isfile(raw_data_file):
            raise IOError('Raw data file %s does not exist.', raw_data_file)
        
        if analysis_timestamp:
            analysis_time = time.strftime("%Y%m%d_%H%M%S")
        else:
            analysis_time= ""

        if not self.analyzed_data_file:
            self.analyzed_data_file = raw_data_file[:-3] + analysis_time + file_name_add_on +'_interpreted.h5'

        self.last_chunk = False

        self._get_configs()

        self.columns, self.rows = 120, 400

        self.threshold_map = np.ones(shape=(self.columns, self.rows)) * -1
        self.noise_map = np.ones_like(self.threshold_map) * -1
        self.chi2_map = np.zeros_like(self.threshold_map) * -1

        # Setup clusterizer
        self._setup_clusterizer()

    def _get_configs(self):
        ''' Load run config to allow analysis routines to access these info '''
        with tb.open_file(self.raw_data_file, 'r') as in_file:
            self.run_config = au.ConfigDict(in_file.root.configuration_in.scan.run_config[:])
            self.scan_config = au.ConfigDict(in_file.root.configuration_in.scan.scan_config[:])
            self.chip_settings = au.ConfigDict(in_file.root.configuration_in.chip.settings[:])

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if exc_type is not None:
            self.log.exception('Exception during analysis', exc_info=(exc_type, exc_value, traceback))

    def get_scan_param_values(self, scan_param_index=None, scan_parameter=None):
        ''' Return the scan parameter value(s)

            scan_param_index: slicing notation of the scan parameter indeces
            scan_parameter: string
                Name of the scan parameter. If not defined all are returned.
        '''
        with tb.open_file(self.raw_data_file, 'r') as in_file:
            scan_param_table = in_file.root.configuration_out.scan.scan_params[:]
            if scan_param_index:
                scan_param_table = scan_param_table[scan_param_index]
            if scan_parameter:
                scan_param_table = scan_param_table[:][scan_parameter]
        return scan_param_table

    def _range_of_parameter(self, meta_data):
        ''' Calculate the raw data word indeces of each scan parameter id
        '''
        index = np.append(np.array([0]), (np.where(np.diff(meta_data['scan_param_id']) != 0)[0] + 1))
        # print(index)

        expected_values = np.arange(np.max(meta_data['scan_param_id']) + 1)

        # Check for scan parameter IDs with no data
        sel = np.isin(expected_values, meta_data['scan_param_id'])
        if not np.all(sel):
            self.log.warning('No words for scan parameter IDs: %s', str(expected_values[~sel]))

        start = meta_data[index]['index_start']
        stop = np.append(start[:-1] + np.diff(start), meta_data[-1]['index_stop'])
        # print(start, stop)

        return np.column_stack((meta_data['scan_param_id'][index], start, stop))

    def _words_of_parameter(self, par_range, data):
        ''' Yield all raw data words of a scan parameter

            Do not exceed chunk_size. Use a global offset
            parameter.
        '''

        for scan_par_id, start, stop in par_range:
            for i in range(start, stop, self.chunk_size):
                # Shift chunk index to not split events. The offset is determined from previous analyzed chunk.
                # Restrict maximum offset, can happen for readouts without a single event, issue #171
                chunk_offset = max(self.chunk_offset, -self.chunk_size)
                start_chunk = i + chunk_offset

                # Limit maximum read words by chunk size
                stop_limited = min(i + self.chunk_size, stop)
                yield scan_par_id, data[start_chunk:stop_limited]

        # Remaining data of last chunk
        self.last_chunk = True  # Set flag for special treatmend
        if self.chunk_offset == 0:
            return
        yield scan_par_id, data[stop + self.chunk_offset:stop]

    def _create_hit_table(self, out_file, dtype):
        ''' Create hit table node for storage in out_file.
            Copy configuration nodes from raw data file.
        '''
        # print(self.chunk_size)
        # print("Data Array Dtype:",dtype)

        hit_table = out_file.create_table(out_file.root, name='Dut',
                                          description=dtype,
                                          title='hit_data',
                                          expectedrows=self.chunk_size,
                                          filters=tb.Filters(complib='blosc',
                                                             complevel=5,
                                                             fletcher32=False))

        return hit_table
    
    def _create_ts_table(self, out_file, dtype):
        ''' Create ts table node for storage in out_file.
            Copy configuration nodes from raw data file.
        '''
        ts_table = out_file.create_table(out_file.root, name='Dut_TS',
                                          description=dtype,
                                          title='ts_data',
                                          expectedrows=self.chunk_size,
                                          filters=tb.Filters(complib='blosc',
                                                             complevel=5,
                                                             fletcher32=False))

        return ts_table
    
    def _create_trigger_table(self, out_file, dtype):
        ''' Create ts table node for storage in out_file.
            Copy configuration nodes from raw data file.
        '''
        trigger_table = out_file.create_table(out_file.root, name='Dut_TRIGGER',
                                          description=dtype,
                                          title='trigger_data',
                                          expectedrows=self.chunk_size,
                                          filters=tb.Filters(complib='blosc',
                                                             complevel=5,
                                                             fletcher32=False))
        return trigger_table
    
    def _create_matched_hit_table(self, out_file, dtype):
        ''' Create ts table node for storage in out_file.
            Copy configuration nodes from raw data file.
        '''
        matched_hit_table = out_file.create_table(out_file.root, name='Dut_matched_hit',
                                          description=dtype,
                                          title='matched_hit',
                                          expectedrows=self.chunk_size,
                                          filters=tb.Filters(complib='blosc',
                                                             complevel=5,
                                                             fletcher32=False))
        
        return matched_hit_table
    
    def _setup_clusterizer(self):
        ''' Define data structure and settings for hit clusterizer package '''
        # Define all field names and data types
        hit_fields = {'event_number': 'event_number',
                      'trigger_number': 'trigger_number',
                    #   'frame': 'frame',
                      'column': 'column',
                      'row': 'row',
                      'charge': 'charge',
                      'timestamp': 'timestamp'
                      }
        hit_description = [('event_number', 'u8'),
                           ('trigger_number', 'u4'),
                        #    ('frame', 'u1'),
                           ('column', 'u2'),
                           ('row', 'u2'),
                           ('charge', 'u2'),
                           ('timestamp', 'i8')]
        cluster_fields = {'event_number': 'event_number',
                          'column': 'column',
                          'row': 'row',
                          'size': 'n_hits',
                          'id': 'ID',
                          'tot': 'charge',
                          'scan_param_id': 'scan_param_id',
                          'seed_col': 'seed_column',
                          'seed_row': 'seed_row',
                          'mean_col': 'mean_column',
                          'mean_row': 'mean_row'}
        
        cluster_description = [('event_number', 'u8'),
                               ('id', '<u2'),
                               ('size', '<u2'),
                               ('tot', '<u4'),
                               ('seed_col', '<u2'),
                               ('seed_row', '<u2'),
                               ('mean_col', '<f4'),
                               ('mean_row', '<f4'),
                               ('dist_col', '<u4'),
                               ('dist_row', '<u4'),
                               ('cluster_shape', '<i8'),
                               ('scan_param_id', 'u4'),
                            #    ('real_event_number','u4'),
                               ]

        hit_dtype = np.dtype(hit_description)
        self.cluster_dtype = np.dtype(cluster_description)


        if self.cluster_hits:  # Allow analysis without clusterizer installed
            # Define end of cluster function to calculate cluster shape
            # and cluster distance in column and row direction
            @numba.njit
            def _end_of_cluster_function(hits, clusters, cluster_size,
                                         cluster_hit_indices, cluster_index,
                                         cluster_id, charge_correction,
                                         noisy_pixels, disabled_pixels,
                                         seed_hit_index):
                hit_arr = np.zeros((15, 15), dtype=np.bool_)
                center_col = hits[cluster_hit_indices[0]].column
                center_row = hits[cluster_hit_indices[0]].row
                hit_arr[7, 7] = 1
                min_col = hits[cluster_hit_indices[0]].column
                max_col = hits[cluster_hit_indices[0]].column
                min_row = hits[cluster_hit_indices[0]].row
                max_row = hits[cluster_hit_indices[0]].row
                for i in cluster_hit_indices[1:]:
                    if i < 0:  # Not used indeces = -1
                        break
                    diff_col = np.int32(hits[i].column - center_col)
                    diff_row = np.int32(hits[i].row - center_row)
                    if np.abs(diff_col) < 8 and np.abs(diff_row) < 8:
                        hit_arr[7 + hits[i].column - center_col,
                                7 + hits[i].row - center_row] = 1
                    if hits[i].column < min_col:
                        min_col = hits[i].column
                    if hits[i].column > max_col:
                        max_col = hits[i].column
                    if hits[i].row < min_row:
                        min_row = hits[i].row
                    if hits[i].row > max_row:
                        max_row = hits[i].row

                if max_col - min_col < 8 and max_row - min_row < 8:
                    # Make 8x8 array
                    col_base = 7 + min_col - center_col
                    row_base = 7 + min_row - center_row
                    cluster_arr = hit_arr[col_base:col_base + 8,
                                          row_base:row_base + 8]
                    # Finally calculate cluster shape
                    # uint64 desired, but numexpr and others limited to int64
                    if cluster_arr[7, 7] == 1:
                        cluster_shape = np.int64(-1)
                    else:
                        cluster_shape = np.int64(
                            au.calc_cluster_shape(cluster_arr))
                else:
                    # Cluster is exceeding 8x8 array
                    cluster_shape = np.int64(-1)

                clusters[cluster_index].cluster_shape = cluster_shape
                clusters[cluster_index].dist_col = max_col - min_col + 1
                clusters[cluster_index].dist_row = max_row - min_row + 1

            def end_of_cluster_function(hits, clusters, cluster_size,
                                        cluster_hit_indices, cluster_index,
                                        cluster_id, charge_correction,
                                        noisy_pixels, disabled_pixels,
                                        seed_hit_index):
                _end_of_cluster_function(hits, clusters, cluster_size,
                                         cluster_hit_indices, cluster_index,
                                         cluster_id, charge_correction,
                                         noisy_pixels, disabled_pixels,
                                         seed_hit_index)

            if self.tot_calib_file:
                max_hit_charge = 2048
            else:
                max_hit_charge = 1024
            # Initialize clusterizer with custom hit/cluster fields
            self.clz = HitClusterizer(
                hit_fields=hit_fields,
                hit_dtype=hit_dtype,
                cluster_fields=cluster_fields,
                cluster_dtype=self.cluster_dtype,
                min_hit_charge=1,
                max_hit_charge=max_hit_charge,
                column_cluster_distance=5,
                row_cluster_distance=5,
                frame_cluster_distance=1,
                ignore_same_hits=True)

            # Set end_of_cluster function for shape and distance calculation
            self.clz.set_end_of_cluster_function(end_of_cluster_function)



    # @profile
    def analyze_data(self):
        self.log.info('Analyzing data...')
        self.chunk_offset = 0
        self.tdiff = 0
        
        with tb.open_file(self.raw_data_file) as in_file:
            n_words = in_file.root.raw_data.shape[0]
            meta_data = in_file.root.meta_data[:]
            meta_data_all = in_file.root.meta_data

            if meta_data.shape[0] == 0:
                self.log.warning('Data is empty. Skip analysis!')
                return

            n_scan_params = np.max(meta_data['scan_param_id']) + 1

            par_range = self._range_of_parameter(meta_data)
            compression = tb.Filters(complevel=2, complib='zlib')
            with tb.open_file(self.analyzed_data_file, 'w', title=in_file.title, filters=compression) as out_file:
                out_file.create_group(out_file.root, name='configuration_in', title='Configuration after scan step')
                out_file.copy_children(in_file.root.configuration_out, out_file.root.configuration_in, recursive=True)


                if self.store_hits:
                    hit_table = self._create_hit_table(out_file, dtype=au.hit_dtype)
                    ts_table = self._create_ts_table(out_file, dtype=au.ts_dtype)
                    trigger_table = self._create_trigger_table(out_file, dtype=au.trigger_dtype)
                    # power_table = self._create_power_dac_table(out_file)
                
                    new_dtype = meta_data_all.dtype 
                    temp_table = out_file.create_table(out_file.root, name='meta_table_interpretr', description=new_dtype,
                                                    title='Meta Table Interpreter', filters=FILTER_TABLES)
                    temp_table.append(meta_data_all[:])
                    temp_table.flush()
                
                if self.build_events:
                    trigger_n, trigger_ts, event_n = 0, 0, 0
                    event_table = self._create_table(out_file, name='Hits', title='event_data', dtype=au.event_dtype)
                # if self.tot_calib_file is not None:
                #     with tb.open_file(self.tot_calib_file, 'r') as calib_file:
                #         self.tot_calib = calib_file.root.InjTotCalibration[:]
                if self.cluster_hits:
                    cluster_table = out_file.create_table(
                        out_file.root, name='Cluster',
                        description=self.cluster_dtype,
                        title='Cluster',
                        filters=tb.Filters(complib='blosc',
                                           complevel=5,
                                           fletcher32=False))
                    if self.tot_calib_file:
                        cs_tot_size = 2048
                    else:
                        cs_tot_size = 1024
                    hist_cs_size = np.zeros(shape=(30, ), dtype=np.uint32)
                    hist_cs_tot = np.zeros(shape=(cs_tot_size, ), dtype=np.uint32)
                    hist_cs_shape = np.zeros(shape=(10, ), dtype=np.int32)

                    matched_hit_table= self._create_matched_hit_table(out_file, dtype=au.matched_hit_dtype)


                interpreter = RawDataInterpreter(n_scan_params=n_scan_params)
                self.last_chunk = False
                pbar = tqdm(total=n_words, unit=' Words', unit_scale=True)
                upd = 0
                for scan_param_id, words in self._words_of_parameter(par_range, in_file.root.raw_data):
                    hit_buffer = np.zeros(shape=4 * self.chunk_size, dtype=au.hit_dtype)
                    # print(words)
                    ts_buffer = np.zeros(shape=4 * self.chunk_size, dtype=au.ts_dtype)
                    trigger_buffer = np.zeros(shape=4 * self.chunk_size, dtype=au.trigger_dtype)
                    hit_dat, ts_dat, trigger_dat = interpreter.interpret(
                        words,
                        hit_buffer,
                        ts_buffer,
                        trigger_buffer,
                        scan_param_id
                    )
                    upd = words.shape[0]
                    # print(hit_dat)

                    if self.store_hits:
                        hit_dat.sort(order="timestamp")
                        hit_table.append(hit_dat)
                        hit_table.flush()
                        ts_table.append(ts_dat)
                        ts_table.flush()
                        trigger_table.append(trigger_dat)
                        trigger_table.flush()

                    if self.build_events:
                        if np.count_nonzero(hit_dat["col"] == 1023) > 0:
                            event_buffer = np.zeros(len(hit_dat), dtype=au.event_dtype)
                            event_dat, trigger_n, trigger_ts, event_n = build_events(hit_dat, event_buffer, trigger_n, trigger_ts, event_n)
                            event_table.append(event_dat)
                            event_table.flush()
                        else:
                            self.log.error("No TLU data found in raw data. Check data or disable event building")
                            raise Exception
                    # print(self.cluster_hits)
                    # input()
                    if self.cluster_hits:
                        # print("____________________!!!")
                        if self.build_events:
                            data_to_clusterizer = event_dat
                        else:
                            hit_dat = hit_dat[hit_dat['col'] < 1000]  # Can only call tot_calib for hit data
                            hit_data_cs_fmt = np.zeros(len(hit_dat), dtype=au.event_dtype)
                            hit_data_cs_fmt['event_number'][:] = hit_dat['timestamp'][:]
                            hit_data_cs_fmt['trigger_number'][:] -1
                            hit_data_cs_fmt['frame'][:] = -1
                            hit_data_cs_fmt['column'][:] = hit_dat['col'][:]
                            hit_data_cs_fmt['row'][:] = hit_dat['row'][:]
                            hit_data_cs_fmt['charge'][:] = hit_dat["tot"][:]
                            hit_data_cs_fmt['timestamp'][:] = hit_dat['timestamp'][:]
                            data_to_clusterizer = hit_data_cs_fmt

                        if self.tot_calib_file:
                            data_to_clusterizer['charge'][:] = au._inv_tot_response_func(
                                data_to_clusterizer['charge'][:],
                                self.tot_calib[data_to_clusterizer[:]['column'], data_to_clusterizer[:]['row']][:, 0],
                                self.tot_calib[data_to_clusterizer[:]['column'], data_to_clusterizer[:]['row']][:, 1],
                                self.tot_calib[data_to_clusterizer[:]['column'], data_to_clusterizer[:]['row']][:, 2]
                            )

                        _, cluster = self.clz.cluster_hits(data_to_clusterizer)
                        cluster_table.append(cluster)
                        # Create actual cluster hists
                        sel_size = np.logical_and(cluster['size'] > 0, cluster['size'] < 6)
                        cs_size = np.bincount(cluster['size'][sel_size], minlength=30)[:30]
                        cs_tot = np.bincount(cluster['tot'], minlength=1024)[:1024]
                        sel = np.logical_and(cluster['cluster_shape'] > 0, cluster['cluster_shape'] < 10)
                        cs_shape = np.bincount(cluster['cluster_shape'][sel], minlength=10)[:10]
                        matched_buffer = np.zeros(len(cluster), dtype=au.matched_hit_dtype)
                        matched_hit=au.create_matched_hits(hit_dat,cluster, matched_buffer,trigger_dat)
                        

                        import ROOT
                        import array
                        grouped_data = {}
                        for c in ["event_number", "col", "row", "tot", "timestamp", "timestamp_trigger", "tdiff_trigger", 'scan_param_id']:
                            for idx, val in enumerate(matched_hit["scan_param_id"]):
                                if val not in grouped_data:
                                    grouped_data[val] = {k: [] for k in ["event_number", "col", "row", "tot", "timestamp", "timestamp_trigger", "tdiff_trigger", 'scan_param_id']}
                                grouped_data[val][c].append(matched_hit[c][idx])

                        output_root = self.analyzed_data_file.replace(".h5",".root")
                        # print(output_root)
                        # print("_________________________")
                        root_file = ROOT.TFile(output_root, "UPDATE")

                        scan_id = self.run_config['scan_id']
                        scan_params=[i for i in grouped_data]
                        if scan_id in ['source_scan']:
                            scan_params = np.arange(self.scan_config['thr_high_START'], self.scan_config['thr_high_STOP'], self.scan_config['thr_high_STEP'])
                        if scan_id in ['source_scan_dac']:
                            scan_params = np.arange(self.scan_config['dac_start'], self.scan_config['dac_stop'], self.scan_config['dac_step'])
                        for scan_param_id in grouped_data:

                            data=grouped_data[val]
                            tree_name = f'tree_{scan_params[scan_param_id]}'
                            tree = ROOT.TTree(tree_name, tree_name)

                            branches = {}
                            for k, v in data.items():
                                branches[k] = array.array('d', v)
                                tree.Branch(k, branches[k], f'{k}/D')

                            # Fill the tree
                            for i in range(len(data["event_number"])):
                                for k, v in data.items():
                                    branches[k][0] = v[i]  # Fill each branch with the corresponding value from the data
                                tree.Fill()
                            root_file.WriteTObject(tree)
                        root_file.Close()

                        # import ROOT
                        # content_dict={}
                        # for c in ["event_number", "col", "row", "tot", "timestamp", "timestamp_trigger", "tdiff_trigger", 'scan_param_id']:
                        #     content_dict[c]=matched_hit[c].copy()
                        # df = ROOT.RDF.FromNumpy(content_dict)
                        # output_root = self.analyzed_data_file.replace(".h5",".root")
                        # df.Snapshot('tree', output_root)
                        
                        matched_hit_table.append(matched_hit)
                        matched_hit_table.flush()
                        
                        # Add to total hists
                        hist_cs_size += cs_size.astype(np.uint32)
                        hist_cs_tot += cs_tot.astype(np.uint32)
                        hist_cs_shape += cs_shape.astype(np.uint32)

                    pbar.update(upd)
                pbar.close()

                
                hist_occ, hist_tot = interpreter.get_histograms()
                power_dac_data = ap.get_results(temp_table)
                power_dac_data_array = np.array(list(power_dac_data.values()))

                scan_id = self.run_config['scan_id']
                if scan_id in ['source_scan']:
                    scan_params = np.arange(self.scan_config['thr_high_START'], self.scan_config['thr_high_STOP'], self.scan_config['thr_high_STEP'])
                    # tra.time_res_analysis(output_root, scan_params)
                    tra.time_res_analysis_main(output_root, scan_params)
                elif scan_id in ['source_scan_dac']:
                    scan_params = np.arange(self.scan_config['dac_start'], self.scan_config['dac_stop'], self.scan_config['dac_step'])
                    tra.time_res_analysis_main(output_root, scan_params)

        gc.collect()


        self._create_additional_hit_data(hist_occ, hist_tot, power_dac_data_array,self.tdiff)
        if self.cluster_hits:
            self._create_additional_cluster_data(hist_cs_size, hist_cs_tot, hist_cs_shape)

    
    def _create_additional_hit_data(self, hist_occ, hist_tot,power_dac_data_array, tdiff):
        # print(self.analyzed_data_file)
        # print("__________________")
        with tb.open_file(self.analyzed_data_file, 'r+') as out_file:
            scan_id = self.run_config['scan_id']
            analogue = self.scan_config['analogue']

            out_file.create_carray(out_file.root,
                                   name='HistOcc',
                                   title='Occupancy Histogram',
                                   obj=hist_occ,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))
            try:
                out_file.create_carray(out_file.root,
                                    name='HistTot',
                                    title='ToT Histogram',
                                    obj=hist_tot,
                                    filters=tb.Filters(complib='blosc',
                                                        complevel=5,
                                                        fletcher32=False))
            except:
                self.log.error("Will skip the tot for now")

            # try:
            #     out_file.create_carray(out_file.root,
            #                         name='HistTDiff',
            #                         title='Tdiff Histogram',
            #                         obj=hist_tdiff,
            #                         filters=tb.Filters(complib='blosc',
            #                                             complevel=5,
            #                                             fletcher32=False))
            # except Exception as e:
            #     self.log.error(f"Will skip the tdiff for now: {e}")


            out_file.create_carray(out_file.root,
                                   name='PowerDAC',
                                   title='Data Power-DACs',
                                   obj=power_dac_data_array,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))
            
            # hist_scurve = au.scurve_hist3d(hits, scan_param_range)
            # out_file.create_carray(out_file.root,
            #                         name="HistSCurve",
            #                         title="Scurve Data",
            #                         obj=hist_scurve,
            #                         filters=tb.Filters(complib='blosc',
            #                                             complevel=5,
            #                                             fletcher32=False))

            # if self.analyze_tdc:  # Only store if TDC analysis is used.
            #     out_file.create_carray(out_file.root,
            #                            name='HistTdcStatus',
            #                            title='Tdc status Histogram',
            #                            obj=hist_tdc_status,
            #                            filters=tb.Filters(complib='blosc',
            #                                               complevel=5,
            #                                               fletcher32=False))

            if scan_id in ['threshold_scan', 'pixel_thr_scan', 'global_threshold_tuning_inj', 'global_threshold_tuning']:
                n_injections = self.scan_config['n_injections']
       

                hist_scurve = hist_occ.reshape((self.rows * self.columns, -1))
                # hist_scurve =np.ma.masked_array(hist_occ.reshape((self.rows * self.columns, -1)), ~self.enable_mask.reshape(self.rows * self.columns, -1))


                if scan_id in ['threshold_scan', 'global_threshold_tuning_inj']:
                    # if analogue == True:
                    scan_params = [self.scan_config['VCAL_MED'] - v for v in np.arange(self.scan_config['VCAL_LOW_start'],
                                                                                    self.scan_config['VCAL_LOW_stop'], self.scan_config['VCAL_LOW_step'])]
                    scan_params_cor = [2*v for v in scan_params]
                    # print(scan_params_cor)
                    self.amplitude_map, self.threshold_map, self.noise_map, self.bkgnd_map, self.chi2_map = au.fit_scurves_multithread(hist_scurve, scan_params_cor, n_injections, optimize_fit_range=False)
                    # # else:
                    # scan_params = np.arange(self.scan_config['vdigital_start'], self.scan_config['vdigital_stop'], self.scan_config['vdigital_step'])
                    # self.amplitude_map, self.threshold_map, self.noise_map, self.bkgnd_map, self.chi2_map = au.fit_scurves_multithread(hist_scurve, scan_params, n_injections, optimize_fit_range=False)

                elif scan_id in ['pixel_thr_scan', "global_threshold_tuning"]:
                    scan_params = np.arange(self.scan_config['thr_high_START'], self.scan_config['thr_high_STOP'], self.scan_config['thr_high_STEP'])
                    # print(f"Analysis axis {scan_params}")
                    self.amplitude_map, self.threshold_map, self.noise_map, self.bkgnd_map, self.chi2_map = au.fit_scurves_multithread(hist_scurve, scan_params, n_injections, optimize_fit_range=False)


                elif scan_id == 'autorange_threshold_scan':
                    scan_params = self.get_scan_param_values(scan_parameter='vcal_high') - self.get_scan_param_values(scan_parameter='vcal_med')
                    self.amplitude_map, self.threshold_map, self.noise_map, self.bkgnd_map, self.chi2_map = au.fit_scurves_multithread(hist_scurve, scan_params, n_injections, optimize_fit_range=False)



                out_file.create_carray(out_file.root, name='AmplitudeMap', title='Amplitude Map', obj=self.amplitude_map,
                                       filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                out_file.create_carray(out_file.root, name='ThresholdMap', title='Threshold Map', obj=self.threshold_map,
                                       filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                out_file.create_carray(out_file.root, name='NoiseMap', title='Noise Map', obj=self.noise_map,
                                       filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                out_file.create_carray(out_file.root, name='BkgndMap', title='Bkgnd Map', obj=self.bkgnd_map,
                                       filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                out_file.create_carray(out_file.root, name='Chi2Map', title='Chi2 / ndf Map', obj=self.chi2_map,
                                       filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                
                out_file.create_carray(out_file.root, name='TimeDifferenceAll', title='Time Difference All', obj=self.amplitude_map,
                                       filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
            elif scan_id in ['source_scan', 'source_scan_dac']:         
                # event_hit = np.array(out_file.root['Dut'][:]['event_number'])
                # event_fpga = np.array(out_file.root['Dut_TS'][:]['event_number'])
                # event_trigger = np.array(out_file.root['Dut_TRIGGER'][:]['event_number'])
                # ts_fpga = np.array(out_file.root['Dut_TS'][:]['timestamp'])
                # ts_trigger = np.array(out_file.root['Dut_TRIGGER'][:]['timestamp'])
                # ts_hit = np.array(out_file.root['Dut'][:]['ts1'])
                # out_file.create_carray(out_file.root, name='event_hit', title='event_hit', obj=event_hit,
                #                    filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                # out_file.create_carray(out_file.root, name='event_fpga', title='event_fpga', obj=event_fpga,
                #                    filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                # out_file.create_carray(out_file.root, name='event_trigger', title='event_trigger', obj=event_trigger,
                #                    filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                # out_file.create_carray(out_file.root, name='ts_fpga', title='ts_fpga', obj=ts_fpga,
                #                    filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                # out_file.create_carray(out_file.root, name='ts_trigger', title='ts_trigger', obj=ts_trigger,
                #                    filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                # out_file.create_carray(out_file.root, name='ts_hit', title='ts_hit', obj=ts_hit,
                #                    filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                event_number = np.array(out_file.root['Dut'][:]['event_number'])
                ts_fpga = np.array(out_file.root['Dut'][:]['timestamp'])
                ts_fpga_raw = np.array(out_file.root['Dut'][:]['fpga'])
                ts_trigger = np.array(out_file.root['Dut'][:]['timestamp_trigger'])
                ts_hit = np.array(out_file.root['Dut'][:]['ts1'])
                ts_diff = np.array(out_file.root['Dut_matched_hit'][:]['tdiff_trigger'])
                # print(ts_diff)
                out_file.create_carray(out_file.root, name='event_number', title='event_number', obj=event_number,
                                    filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                out_file.create_carray(out_file.root, name='ts_fpga', title='ts_fpga', obj=ts_fpga,
                                    filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                out_file.create_carray(out_file.root, name='ts_fpga_raw', title='ts_fpga_raw', obj=ts_fpga_raw,
                                    filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                out_file.create_carray(out_file.root, name='ts_trigger', title='ts_trigger', obj=ts_trigger,
                                    filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                out_file.create_carray(out_file.root, name='ts_hit', title='ts_hit', obj=ts_hit,
                                    filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                out_file.create_carray(out_file.root, name='ts_diff', title='ts_diff', obj=ts_diff,
                                    filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                try:
                    # self.nbins, self.bin_centers, self.amplitude_tr, self.mu_tr, self.sigma_tr, self.k_l, self.k_h, self.fwhm, self.err_fwhm, self.reduced_chi_sq, self.binwidth, self.mu_tr_err, self.sigma_tr_err, self.k_l_err, self.k_h_err = au.fit_fancy_time_resolution(self, ts_diff)
                
                    out_file.create_group(out_file.root, 'time_resolution_fit', 'Fit Params for Time Resolution')
                    out_file.create_carray(out_file.root.time_resolution_fit, name='nbins', title='nbins', obj=self.nbins,
                                        filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                    out_file.create_carray(out_file.root.time_resolution_fit, name='bin_centers', title='bin_centers', obj=self.bin_centers,
                                        filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                    out_file.create_carray(out_file.root.time_resolution_fit, name='amplitude_tr', title='amplitude_tr', obj=self.amplitude_tr,
                                        filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                    out_file.create_carray(out_file.root.time_resolution_fit, name='mu_tr', title='mu_tr', obj=self.mu_tr,
                                        filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                    out_file.create_carray(out_file.root.time_resolution_fit, name='sigma_tr', title='sigma_tr', obj=self.sigma_tr,
                                        filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                    out_file.create_carray(out_file.root.time_resolution_fit, name='k_l', title='k_l', obj=self.k_l,
                                        filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                    out_file.create_carray(out_file.root.time_resolution_fit, name='k_h', title='k_h', obj=self.k_h,
                                        filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                    out_file.create_carray(out_file.root.time_resolution_fit, name='fwhm', title='fwhm', obj=self.fwhm,
                                        filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                    out_file.create_carray(out_file.root.time_resolution_fit, name='err_fwhm', title='err_fwhm', obj=self.err_fwhm,
                                        filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                    out_file.create_carray(out_file.root.time_resolution_fit, name='reduced_chi_sq', title='reduced_chi_sq', obj=self.reduced_chi_sq,
                                        filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                    out_file.create_carray(out_file.root.time_resolution_fit, name='tdiff', title='tdiff', obj=self.tdiff,
                                        filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                    out_file.create_carray(out_file.root.time_resolution_fit, name='binwidth', title='binwidth', obj=self.binwidth,
                                        filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                    out_file.create_carray(out_file.root.time_resolution_fit, name='mu_tr_err', title='mu_tr_err', obj=self.mu_tr_err,
                                        filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                    out_file.create_carray(out_file.root.time_resolution_fit, name='sigma_tr_err', title='sigma_tr_err', obj=self.sigma_tr_err,
                                        filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                    out_file.create_carray(out_file.root.time_resolution_fit, name='k_l_err', title='k_l_err', obj=self.k_l_err,
                                        filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                    out_file.create_carray(out_file.root.time_resolution_fit, name='k_h_err', title='k_h_err', obj=self.k_h_err,
                                        filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
                except:
                     self.log.exception('Exception during analysis: No fancy time resolution...')

    
    def _create_additional_cluster_data(self, hist_cs_size, hist_cs_tot, hist_cs_shape):
        '''
            Store cluster histograms in analyzed data file
        '''
        with tb.open_file(self.analyzed_data_file, 'r+') as out_file:
            out_file.create_carray(out_file.root,
                                   name='HistClusterSize',
                                   title='Cluster Size Histogram',
                                   obj=hist_cs_size,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))
            out_file.create_carray(out_file.root,
                                   name='HistClusterTot',
                                   title='Cluster ToT Histogram',
                                   obj=hist_cs_tot,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))
            out_file.create_carray(out_file.root,
                                   name='HistClusterShape',
                                   title='Cluster Shape Histogram',
                                   obj=hist_cs_shape,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))