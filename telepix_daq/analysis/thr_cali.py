import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import linregress




cali_value = "BL"
variable = "Baseline"
# cali_value = "Thr"
# variable = "Threshold"

excel_file = f'/home/lab/daq/DOCKER_TEST/Calibration_measurements/Hannah_Calibration_measurements_{cali_value}.ods'  
data = pd.read_excel(excel_file)

x = data[f'{cali_value}/DACs'].values
y = data[f'{cali_value}/V'].values
y_error = data[f'del_{cali_value}/V'].values


slope, intercept, r_value, p_value, std_err = linregress(x, y)
y_pred = slope * x + intercept

slope_error = std_err
intercept_error = std_err * x.mean()

fit_function = f'y = {slope:.5f} $\pm$ {slope_error:.5f} * x + {intercept:.4f} $\pm$ {intercept_error:.4f}'


plt.figure(figsize=(8, 6))
plt.scatter(x.flatten(), y, color='green', label=f'{variable}')
plt.plot(x.flatten(), y_pred, color='red', label=f'{fit_function}')
plt.xlabel(f'{cali_value}/DAC')
plt.ylabel(f'{cali_value}/V')


plt.errorbar(x.flatten(), y, yerr=y_error, fmt='o', color='blue', label='Error: $\pm$ 0.001V')


plt.grid(True)
plt.legend()
plt.savefig(f"/home/lab/daq/DOCKER_TEST/Calibration_measurements/{cali_value}_voltage_cali.pdf")
plt.savefig(f"/home/lab/daq/DOCKER_TEST/Calibration_measurements/{cali_value}_voltage_cali.png")
