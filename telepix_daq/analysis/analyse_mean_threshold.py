#!/bin/env python

import os 
import re
import matplotlib.pyplot as plt
import numpy as np
import glob

'''
    This analysis script can be used to get the mean threshold & sigma of an scurve distribution
'''



def analyze(directory_results, input_file):
    # print(input_file)
    pattern_tdac = r'_tdac_(\d+)_'
    match_tdac = re.search(pattern_tdac, input_file)
    if match_tdac:
        tdac_value = float(match_tdac.group(1))
        # print(tdac_value)
    
    latest_files = {}
    # Extract vminus, vndac
    pattern_vminus_vndac = r'vndac([+-]?\d*\.\d+|\d+)_vminus_([+-]?\d*\.\d+|\d+)'
    match_vminus_vndac = re.search(pattern_vminus_vndac, input_file)
    if match_vminus_vndac:
        # print(match_vminus_vndac)
        vndac = float(match_vminus_vndac.group(1))
        vminus = float(match_vminus_vndac.group(2))
        key = (vminus, vndac)

        

        with open(input_file, 'r') as file:
            lines = file.readlines()
            for line in lines:
                if "mu" in line:
                    mu_match = re.search(r'mu=([\d\.\+-]+)', line)
                    sigma_match = re.search(r'sigma=([\d\.\+-]+)', line)
                    
                    if mu_match:
                        mu = float(mu_match.group(1))
                    if sigma_match:
                        sigma = float(sigma_match.group(1))
                


    return vndac, vminus, mu, sigma




def main():
    # directory_results = '/home/lab/daq/run2020/mightypix_daq/output_data/scan_tuning/70V/pmos/short_cable/module_0/chip_26/thr_results/'
    directory_results = '/home/lhcb/daq/telepix_daq/telepix_daq/output_data/module_0/chip_09/2024_September/120V/cool_box/0degC/tdac_scan/module_0/chip_09/thr_results/'    
    input_file = ''
    if not os.path.exists(directory_results):
        os.makedirs(directory_results)
    vndac, vminus, thr, thr_err = analyze(directory_results, input_file)

if __name__ == "__main__":
    main()
