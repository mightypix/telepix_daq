#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Finds the optimum global threshold value for target threshold using binary search.
'''

import numpy as np

from telepix_daq.analysis import analysis
from telepix_daq.system.scan_base2 import ScanBase
from telepix_daq.analysis import plotting
from telepix_daq.scans.shift_and_inject import shift_and_inject
import time


scan_configuration = {
  
    "start_row": 0, 
    "stop_row": 400, 
    "start_column": 0, 
    "stop_column": 120, 

    'thr':  145,
    'TDAC': 0,

    'thr_high_START':  200,
    'thr_high_STOP': 150,
    'thr_high_STEP': -1,

    "analogue": False, 

    'vndac': 0,
    "vminus": 0.8,

    'tdac_start': 7,
    'tdac_stop': -1,
    'tdac_step': -3,

}



class TDACTuning(ScanBase):
    scan_id = 'global_threshold_tuning'

    def _configure(self, start_column=0, stop_column=14, start_row=0, stop_row=124, analogue= False, **_):
        '''
        Parameters
        ----------
        start_column : int [0:120]
            First column to scan
        stop_column : int [0:120]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:400]
            First row to scan
        stop_row : int [0:400]
            Row to stop the scan. This row is excluded from the scan.

        VCAL_LOW : int
            Injection DAC low value.
        VCAL_HIGH : int
            Injection DAC high value.
        '''
        
        self.data.start_column, self.data.stop_column, self.data.start_row, self.data.stop_row = start_column, stop_column, start_row, stop_row
        self.chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['injection'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['tdac'][start_column:stop_column, start_row:stop_row] = tdac_i

        self.chip.masks.apply_disable_mask()
        self.chip.masks.update()

        if not analogue:
            self.chip.set_inj_low(1.8/2)
            self.chip.set_inj_high(0.01)


    def _scan(self, n_injections=100, vndac = 0, vminus = 0.75, thr_high_START=1.2, thr_high_STOP=1.1, thr_high_STEP=-0.1, analogue= False, **_):
        '''
        Global threshold tuning main loop
        '''

        ## Possibility to change vndac & vminus
        # self.daq['DAC_SR']['VNDAC'] = vndac
        # self.daq['DAC_SR']['IPDAC'] = vndac    
        # self.chip.set_sr_on(sr = "LD_Dac")
        # self.daq['DAC_SR'].write()
        # self.chip.sr_ready('DAC_SR')
        # self.chip.set_sr_off(sr = "LD_Dac")
        # self.chip.set_global_voltage(VMinus_Pix=vminus)
        # self.chip.set_tdac(tdac_i)

        # Only check pixel that can respond
        sel_pixel = np.zeros([self.chip.COL_SIZE, self.chip.ROW_SIZE],dtype=np.bool_)
        sel_pixel[self.data.start_column:self.data.stop_column, self.data.start_row:self.data.stop_row] = True

        ## measure scurve for fixed injection
        thr_range = np.arange(thr_high_START, thr_high_STOP, thr_high_STEP)

        for scan_param_id, vcal_thr in enumerate(thr_range):
            if not self.fifo_readout.force_stop.is_set():
                 
                self.daq['VDAC_SR'].set_size(self.daq["VDAC_SR"]._conf['size'])
                self.daq['VDAC_SR']["ThPix"] = int(vcal_thr)
                self.chip.set_sr_on(sr = "LD_VDac")
                self.daq['VDAC_SR'].write()
                for i in range(10000):
                    if self.daq['VDAC_SR'].is_ready:
                        break
                    else:
                        time.sleep(0.001)
                if self.daq['VDAC_SR'].is_done():
                    self.chip.logger.info(f"Loading of threshold dac to {vcal_thr} done")
                self.chip.set_sr_off(sr = "LD_VDac")
                
                with self.readout(scan_param_id=scan_param_id, readout_interval=0.000001):
                    # pass
                    shift_and_inject(chip=self.chip, n_injections=n_injections, scan_param_id=scan_param_id, injection=True, analogue=analogue)
            else:
                break


    def _analyze(self):
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5',file_name_add_on = f'_tdac_{tdac_i}_vndac{vndac}_vminus_{vminus}', **self.configuration['bench']['analysis']) as a:
            a.analyze_data()

        if self.configuration['bench']['analysis']['create_pdf']:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file, mask_noisy_pixels=120) as p:
                p.create_standard_plots()


if __name__ == '__main__':
    with TDACTuning(scan_config=scan_configuration) as tuning:
        vndac = scan_configuration['vndac']
        vminus = scan_configuration['vminus']

        tdac_range = np.arange(scan_configuration['tdac_start'], scan_configuration['tdac_stop'], scan_configuration['tdac_step'])

        for tdac_i in tdac_range:
            tuning.log.info(f'Starting scan for TDAC value: {tdac_i}')
            tuning.start(reconfigure=True)
            tuning.log.info(f'Finished analysis for TDAC value: {tdac_i}')
            # tuning.close() #TODO: FixMe!
