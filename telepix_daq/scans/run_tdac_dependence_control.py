#!/bin/env python

'''
    Use this to run TDAC dependence automaized
'''

import argparse
import re
import subprocess
import tune_global_threshold_tdac_dependence
from telepix_daq.analysis import analyse_thr_vs_tdac

def main():

    vndac_values = [5, 10, 20]
    vminus_values = [0.8, 0.9]

    parser = argparse.ArgumentParser(description="Threshold tuning script with TDAC dependence.")

    # Define arguments for tuning
    parser.add_argument('--start_column', type=int, default=1, help='Start column for the scan')
    parser.add_argument('--stop_column', type=int, default=10, help='Stop column for the scan')
    parser.add_argument('--start_row', type=int, default=1, help='Start row for the scan')
    parser.add_argument('--stop_row', type=int, default=10, help='Stop row for the scan')
    parser.add_argument('--thr_high_START', type=float, default=190, help='Starting value for high threshold scan')
    parser.add_argument('--thr_high_STOP', type=float, default=140, help='Stopping value for high threshold scan')
    parser.add_argument('--thr_high_STEP', type=float, default=-1, help='Step value for high threshold scan')
    parser.add_argument('--vndac', type=int, default=0, help='vndac value')
    parser.add_argument('--vminus', type=float, default=0.8, help='vminus value')

    args = parser.parse_args()

    for vminus_i in vminus_values:
        for vndac_i in vndac_values:
            # Update arguments for the current iteration
            args.vndac = vndac_i
            args.vminus = vminus_i

                # Call tune_global_threshold_tdac_dependence with updated arguments and capture the output
            command = [
                'python', 'scans/tune_global_threshold_tdac_dependence.py', 
                '--start_column', str(args.start_column),
                '--stop_column', str(args.stop_column),
                '--start_row', str(args.start_row),
                '--stop_row', str(args.stop_row),
                '--thr_high_START', str(args.thr_high_START),
                '--thr_high_STOP', str(args.thr_high_STOP),
                '--thr_high_STEP', str(args.thr_high_STEP),
                '--vndac', str(args.vndac),
                '--vminus', str(args.vminus)
            ]

            print(f"Running command: {' '.join(command)}")
            
            process = subprocess.Popen(
                command,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                text=True
            )
            stdout, stderr = process.communicate()
            
            # Print stdout and stderr for debugging
            print("STDOUT:")
            print(stdout)
            # print("STDERR:")
            # print(stderr)
            
            ## Analyse data after each scan
            process = subprocess.Popen(
                ['python', 'analysis/analyse_thr_vs_tdac.py'],
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                text=True
            )
            stdout, stderr = process.communicate()



if __name__ == "__main__":
    main()
