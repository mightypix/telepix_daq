#!/bin/env python
import sys
import numpy as np
from tqdm import tqdm
import time
import h5py


from telepix_daq.analysis import analysis
from telepix_daq.system.scan_base2 import ScanBase
from telepix_daq.analysis import plotting
from telepix_daq.scans.shift_and_inject import shift_and_inject,get_scan_loop_mask_steps
import telepix_daq.analysis.analysis_utils as au

np.set_printoptions(threshold=sys.maxsize, linewidth=500)

'''
    This script is used to analyse a scan offline.
'''






def offline_analysis():
    input_filename =  "output_data/module_0/chip_13/2024_November/tests/module_0/chip_13/20241128_121342_eudaq_scan.h5" 
    input_filename_analysed = "output_data/module_0/chip_8/2024_April/subchip/130V/scan_thr/module_0/chip_8/20240511_183442_pixel_thr_scan.h5"
    analysed = False
    only_analysis = False

    
    if not analysed and not only_analysis:
        with analysis.Analysis(raw_data_file=input_filename, analysis_timestamp = False, cluster_hits = False) as a:
            a.analyze_data()

        with h5py.File(input_filename, 'r') as f:
            configuration = f["configuration_out"]
            create_pdf=False
            for i,ival in configuration['bench']['analysis']:
                if i == b'create_pdf':
                    create_pdf=(ival==b'True')
            if create_pdf:
                with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
                    p.create_standard_plots()

    elif analysed and not only_analysis: 
        with h5py.File(input_filename, 'r') as f:
            configuration = f["configuration_out"]
            create_pdf=False
            for i,ival in configuration['bench']['analysis']:
                if i == b'create_pdf':
                    create_pdf=(ival==b'True')
            if create_pdf:
                with plotting.Plotting(analyzed_data_file=input_filename_analysed) as p:
                    p.create_standard_plots()

    if only_analysis:
        with analysis.Analysis(raw_data_file=input_filename, analysis_timestamp = True) as a:
            a.analyze_data()




if __name__ == "__main__":
   offline_analysis()