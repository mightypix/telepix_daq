#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    First a global noise dependent threshold tuning is done. Start with a high threshold a lower until you have
    approx 10 hits per pixel. We aim for 1Hz/pixel noise rate -> 1kHz for all pixel -> 0.01ms readout time
    Finds the optimum global threshold value for target threshold using binary search.
'''

import numpy as np
from tqdm import tqdm
import copy
import threading
import os

from telepix_daq.analysis import analysis
from telepix_daq.system.scan_base2 import ScanBase
from telepix_daq.analysis import plotting
import telepix_daq.analysis.analysis_utils as au
from telepix_daq.analysis import online as oa
import time
from telepix_daq.analysis.interpreter_clean import RawDataInterpreter
import matplotlib.pyplot as plt
from matplotlib import colors, cm


scan_configuration = {
  
    "start_row": 0, 
    "stop_row": 400, 
    "start_column": 0, 
    "stop_column": 120, 


    # Target threshold
    'VCAL_LOW': 127,
    'VCAL_HIGH': 165,
    'VCAL_STEP': -1,

    'analogue': False,

    "vndac":5,

    'min_rate': 10,
    'max_hit_rate': 1000,

    # Stop conditions (choose one)
    'scan_timeout': False,             # Timeout for scan after which the scan will be stopped, in seconds; if False no limit on scan time
    'max_triggers': False,          # Number of maximum received triggers after stopping readout, if False no limit on received trigger
    'min_spec_occupancy': False,    # Minimum hits for each pixel above which the scan will be stopped; only a fraction of all pixels needs to reach this limit (see below)

    # For stop condition 'min_spec_occupancy' only
    'fraction': 0.99,   # Fraction of enabled pixels that need to reach the minimum occupancy (no hits in dead/disconnected pixels!)

    'trigger_latency': 100,     # Latency of trigger in units of 25 ns (BCs)
    'trigger_delay': 57,        # Trigger delay in units of 25 ns (BCs)
    'trigger_length': 32,       # Length of trigger command (amount of consecutive BCs are read out)
    'veto_length': 500,         # Length of TLU veto in units of 25 ns (BCs). This vetos new triggers while not all data is revieved. Increase by factor of number of connected chips. Should also be adjusted for longer trigger length.

    # Trigger configuration
    'bench': {'TLU': {
        'TRIGGER_MODE': 3,      # Selecting trigger mode: Use trigger inputs/trigger select (0), TLU no handshake (1), TLU simple handshake (2), TLU data handshake (3)
        'TRIGGER_SELECT': 0     # HitOR [DP_ML_5 and mDP] (3), HitOR [mDP only] (2), HitOR [DP_ML_5 only] (1), disabled (0)
    }
    }

}


class NoiseTuningExternal(ScanBase):
    scan_id = 'ext_trigger_scan'

    is_parallel_scan = True     # Parallel readout of ExtTrigger-type scans

    stop_scan = threading.Event()

    def _configure(self, scan_timeout=10, max_triggers=False, min_spec_occupancy=False, trigger_length=32, trigger_delay=57, veto_length=500,  trigger_latency=100, start_column=0, stop_column=120, start_row=0, stop_row=400,  VCAL_HIGH=172, VCAL_STEP = -1, **_):
        '''
        Parameters
        ----------
        max_triggers : int / False
            Maximum amount of triggers to record. Set to False for no limit.
        trigger_length : int
            Amount of BCIDs to read out on every trigger.
        trigger_delay : int
            Delay the trigger command by this amount in units of 25ns.
        veto_length : int
            Length of TLU veto in units of 25ns.
        trigger_latency : int
            Latency of trigger in units 
        start_column : int [0:120]
            First column to scan
        stop_column : int [0:120]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:400]
            First row to scan
        stop_row : int [0:400]
            Row to stop the scan. This row is excluded from the scan.

        VCAL_LOW : int
            Injection DAC low value.
        VCAL_HIGH : int
            Injection DAC high value.
        '''
        if (scan_timeout and max_triggers) or (scan_timeout and min_spec_occupancy) or (max_triggers and min_spec_occupancy):
            self.log.warning("You should only use one of the stop conditions at a time.")

        self.data.n_trigger = 0  
        self.n_trigger = 0   # Init total trigger number already here to prevent misleading, additional error when scan crashes

        self.data.start_column, self.data.stop_column, self.data.start_row, self.data.stop_row = start_column, stop_column, start_row, stop_row  
        self.data.tdac_range = [0,1,2,3,4,5,6,7]
        self.data.scan_range = ((start_column, stop_column), (start_row, stop_row))

        self.chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['injection'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['tdac'][start_column:stop_column, start_row:stop_row] = 0
        
        self.chip.masks.apply_disable_mask()
        self.chip.masks.update()

        self.min_spec_occupancy = min_spec_occupancy

        self.interpreter = RawDataInterpreter()


    def _get_histogram(self,data_tuple):
        self.interpreter.reset_hist()
        
        if len(data_tuple)==0:
            hist_occ = np.zeros((self.chip.COL_SIZE, self.chip.ROW_SIZE), dtype=np.uint32)
            return hist_occ
        raw_data =  np.concatenate([i[0] for i in  data_tuple])
        hit_buffer = np.zeros(shape=len(raw_data), dtype=au.hit_dtype)
        ts_buffer = np.zeros(shape=len(raw_data), dtype=au.ts_dtype)
        trigger_buffer = np.zeros(shape=len(raw_data), dtype=au.trigger_dtype)
        hit_dat, ts_dat, trigger_data = self.interpreter.interpret(
            raw_data,
            hit_buffer,
            ts_buffer,
            trigger_buffer,
            0
        )
        hist_occ, _ = self.interpreter.get_histograms()
        return hist_occ


    def _scan(self, vndac=0, start_column=0, stop_column=120, scan_timeout=10, max_triggers=False, min_spec_occupancy=False, fraction=0.99, VCAL_LOW=165, VCAL_HIGH=172, VCAL_STEP = -1, min_rate=10, max_hit_rate =1000,**_):
        '''
        Noise trimming loop with external trigger

        Parameters
        ----------
        scan_timeout : int / False
            Number of seconds to records triggers. Set to False for no limit.
        max_triggers : int / False
            Maximum amount of triggers to record. Set to False for no limit.
        tdac_value_bits : iterable
            Bits to toggle during tuning. Should be monotone.
        min_occupancy : int
            Minimum number of hits to count a pixel as noisy.

        '''

        def timed_out():
            if scan_timeout:
                current_time = time.time()
                if current_time - start_time > scan_timeout:
                    self.log.info('Scan timeout was reached')
                    return True
            return False
        
        ## Possibility to change vndac & vminus
        self.daq['DAC_SR']['VNDAC'] = vndac
        self.chip.write_register("LD_Dac","DAC_SR")

        thr_range = np.arange(VCAL_HIGH, VCAL_LOW, VCAL_STEP)
        pbar = tqdm(total=len(thr_range), unit=' Mask steps')

        ### first find lowest global threshold
        scan_param_id = 0
        which_thr=0
        iteration=0
        readout_interval=0.001

        ### main scan loop
        while True:
            iteration+=1
            if abs(which_thr)>= len(thr_range):
                break
            vcal_thr=thr_range[which_thr]
            self.data.vcal_thr = vcal_thr

            if not self.fifo_readout.force_stop.is_set() or not (self.stop_scan.is_set() or timed_out()):
                error = False
                self.daq['VDAC_SR']["ThPix"] = int(vcal_thr)
                self.chip.write_register("LD_VDac","VDAC_SR")
                self.chip.logger.info(f"Loading of threshold dac to {vcal_thr} done")

                last_time=self.fifo_readout.get_float_time()
                with self.readout(scan_param_id=scan_param_id, readout_interval=readout_interval, fill_buffer=True, clear_buffer=True, callback=self.analyze_data_online):
            
                    self.stop_scan.clear()
                    curr_time=self.fifo_readout.get_float_time()

                    # Scan loop
                    try:
                        ## check for errors
                        if self.daq['data_rx'].get_lost_count():
                            error = True
                            self.daq.rx_channels[self.chip.receiver].RESET = 1

                        ## check occupancy
                        hist_occ = self._get_histogram(list(self.fifo_readout.data))
                        occ_max = np.sum(hist_occ)
                        measured_readout_time=curr_time-last_time
                        hit_rate = occ_max/measured_readout_time
                        self.chip.logger.info(f"Occupancy for threshold {vcal_thr} is {occ_max} with hitrate of {hit_rate} hits/s for all pixels!")
                        
                        found_noisy_pix=False
                        disable_pix = False
                        for col in range(*self.data.scan_range[0]):
                            for row in range(*self.data.scan_range[1]):
                                if hist_occ[col, row] >= min_rate*measured_readout_time and self.chip.masks.disable_mask[col, row]!= False:
                                    self.chip.masks.disable_mask[col, row] = False
                                    self.log.warning(f"disabled {col} {row}, iteration {iteration}")
                                    found_noisy_pix=True
                                    thr_last_index = which_thr
                                    disable_pix = True
                                    self.chip.send_reset()
                
                
                        which_thr+=1


                        # cmap =plt.cm.plasma
                        # cmap.set_bad('w')   # white for None pixels
                        # cmap.set_over('r')  # Make noisy pixels red
                        # cmap.set_under('g')
                        # fig, axs = plt.subplots(1,1)  
                        # axs.set_xlabel('Column')
                        # axs.set_ylabel('Row')
                        # axs.set_title(f'Occupancy map for threshold {vcal_thr} DAC and iteration: {iteration}')
                        # im = axs.imshow((hist_occ[:].sum(axis=2)).T, interpolation='none',aspect='auto', cmap=cmap)  # TODO: use pcolor or pcolormesh #, aspect='equal'
                        # cbar = fig.colorbar(im, ax=axs)
                        # cbar.set_label('Occupancy')

                        # out_dir=f"{self.output_dir}/noise_tuning"
                        # if not os.path.exists(out_dir):
                        #     os.makedirs(out_dir)
                        # plt.savefig(f"{out_dir}/{self.timestamp}_global_tuning_thr_{vcal_thr}_{iteration}.pdf")
                        # plt.savefig(f"{out_dir}/{self.timestamp}_global_tuning_thr_{vcal_thr}_{iteration}.png")
                        if disable_pix:
                            self.chip.masks.apply_disable_mask()
                            self.chip.masks.update(disable_pix=disable_pix)
                        scan_param_id += 1

                        if (hit_rate >= max_hit_rate ):
                            self.log.info(f'Scan was stopped because maximal hit rate was reached with {hit_rate}Hits/s')
                            break

                        # Stop scan if reached trigger limit
                        if max_triggers and triggers >= max_triggers:
                            self.stop_scan.set()
                            self.log.info('Trigger limit was reached: {0}'.format(max_triggers))


                    except KeyboardInterrupt:  # React on keyboard interupt
                        self.stop_scan.set()
                        self.log.info('Scan was stopped due to keyboard interrupt')

            else:
                break


        self.log.info('Reset chip!')
        self.chip.send_reset()

        self.data.vcal_thr = thr_range[thr_last_index]
        self.data.vcal_thr += 1  # Increase global threshold -> To be adjusted
        self.log.success('Lowest stable global threshold value: {0}'.format(self.data.vcal_thr))
        self.log.success(f'Found {(~(self.chip.masks.disable_mask)).sum()} noisy pixels before tuning')
        self.daq['VDAC_SR']["ThPix"] = int(self.data.vcal_thr)
        self.chip.write_register("LD_VDac","VDAC_SR")


        ## second step: tune local threshold. Start with high value and go as low as possible. If noise, increase threshold
        self.log.info('Starting TDAC scan. Finding lowest TDAC threshold...')


        ### first check all noisy pixels from disable_mask and set tdac to 7 (111). If some are still noisy,
        ### we will disable them.
        noisy_pixel_mask = self.chip.masks.disable_mask[:]
        noisy_pixels = []
        retune=False
        scan_param_id = 0 ##reset to 0

        for col in range(*self.data.scan_range[0]):
            for row in range(*self.data.scan_range[1]):
                    if noisy_pixel_mask[col, row] == False and (self.chip.masks['tdac'][col, row] < 7):
                        self.chip.masks['tdac'][col, row] = 0
                        noisy_pixels.append([col,row])
                        retune=True
                    else:
                        self.chip.masks.disable_mask[col,row] = True
        if retune:
            self.chip.masks.update()

            ### check if pixels are still noisy!
            ### sometimes the readout has issues...
            # self.fifo_readout.start()
            # self.fifo_readout.stop()
            last_time=self.fifo_readout.get_float_time()
            with self.readout(scan_param_id=scan_param_id, readout_interval=readout_interval, fill_buffer=True, clear_buffer=True, callback=self.analyze_data_online):
                pass
            curr_time=self.fifo_readout.get_float_time()
            measured_readout_time=curr_time-last_time
            # hist_occ = self._get_histogram(list(self.fifo_readout.data))

            for pix_i in noisy_pixels:
                self.log.info(f"Noisy pix {pix_i} with occ of {np.max(hist_occ[pix_i[0], pix_i[1]])} at max occ of {min_rate*measured_readout_time}")
                if np.max(hist_occ[pix_i[0], pix_i[1]]) >= min_rate*measured_readout_time:
                    self.chip.masks.disable_mask[pix_i[0], pix_i[1]] = False

                    ##TODO further tuning here needed!!! Check if this works and is enough
                    for trim_value in self.data.tdac_range:
                        self.log.info(f'For pixel {pix_i} trim bit is set to {trim_value}')
                        self.chip.masks['tdac'][pix_i[0], pix_i[1]] = trim_value
                        self.chip.masks.update() 
                        last_time=self.fifo_readout.get_float_time()
                        with self.readout(scan_param_id=scan_param_id, readout_interval=readout_interval, fill_buffer=True, clear_buffer=True, callback=self.analyze_data_online):
                            pass
                        curr_time=self.fifo_readout.get_float_time()
                        measured_readout_time=curr_time-last_time
                        hist_occ = self._get_histogram(list(self.fifo_readout.data))
                        # cmap =plt.cm.plasma
                        # cmap.set_bad('w')   # white for None pixels
                        # cmap.set_over('r')  # Make noisy pixels red
                        # cmap.set_under('g')
                        # fig, axs = plt.subplots(1,1)  
                        # axs.set_xlabel('Column')
                        # axs.set_ylabel('Row')
                        # # axs.set_title(f'Occupancy map for threshold {vcal_thr} DAC and iteration: {iteration}')
                        # im = axs.imshow((hist_occ[:].sum(axis=2)).T, interpolation='none',aspect='auto', cmap=cmap)  # TODO: use pcolor or pcolormesh #, aspect='equal'
                        # cbar = fig.colorbar(im, ax=axs)
                        # cbar.set_label('Occupancy')

                        # plt.savefig(f"output_data/module_0/chip_0/noise_tuning/global_tuning_thr_{vcal_thr}_{iteration}.pdf")
                        # out_dir=f"{self.output_dir}/noise_tuning"
                        # if not os.path.exists(out_dir):
                        #     os.makedirs(out_dir)
                        # plt.savefig(f"{out_dir}/{self.timestamp}_global_{pix_i[0]}_{pix_i[1]}_{trim_value}_tuning_thr_{vcal_thr}_{iteration}.pdf")
                        # plt.savefig(f"{out_dir}/{self.timestamp}_global_{pix_i[0]}_{pix_i[1]}_{trim_value}_tuning_thr_{vcal_thr}_{iteration}.png")

                        self.log.info(f"Pixel {pix_i} has an occupancy of {np.max(hist_occ[pix_i[0], pix_i[1]])} at trim value {trim_value}")
                        if np.max(hist_occ[pix_i[0], pix_i[1]]) <= min_rate*measured_readout_time:
                            self.chip.masks.disable_mask[pix_i[0], pix_i[1]] = True
                            break
                        else:
                            self.chip.masks.disable_mask[pix_i[0], pix_i[1]] = False
                else:
                    self.chip.masks.disable_mask[pix_i[0], pix_i[1]] = True


        pbar.close()
        self.chip.power_down()

        n_disabled_pixels = (~(self.chip.masks.disable_mask)).sum()
        self.log.success(f'Found {n_disabled_pixels} noisy pixels after tuning')
       
        # self.chip.masks.apply_disable_mask()
        # self.chip.masks.update()
        # with self.readout(scan_param_id=scan_param_id, readout_interval=readout_interval, fill_buffer=True, clear_buffer=True, callback=self.analyze_data_online):
        #             pass
        
        self.stop_scan.set()
        self.log.success('Scan finished')


    # Used to overwrite data storing function: self.readout.handle_data
    def analyze_data_online(self, data_tuple):
        super(NoiseTuningExternal, self).handle_data(data_tuple)
        

    def handle_data(self, data_tuple, receiver=None):
        ''' Check recorded trigger number '''
        super(NoiseTuningExternal, self).handle_data(data_tuple)

        raw_data = data_tuple[0]

        self.data.hist_occ.add(raw_data)

        # if self.min_spec_occupancy:
        #     self.data.hist_occ.add(raw_data)

        sel = np.bitwise_and(raw_data, au.TRIGGER_HEADER) > 0
        trigger_words = raw_data[sel]
        trigger_number = np.bitwise_and(trigger_words, au.TRG_MASK)
        trigger_inc = np.diff(trigger_number)
        trigger_issues = np.logical_and(trigger_inc != 1, trigger_inc != -2**31)

        self.data.n_trigger += trigger_number.shape[0]

        if np.any(trigger_issues):
            self.log.warning('Trigger numbers not strictly increasing')
            if np.count_nonzero(trigger_issues > 0) <= 10:
                self.log.warning('Trigger delta(s): {0}'.format(str(trigger_inc[trigger_issues > 0])))
            else:
                self.log.warning('More than 10 trigger numbers not strictly increasing in this readout!')


if __name__ == '__main__':
    with NoiseTuningExternal(scan_config=scan_configuration) as scan:
        try:
            scan.configure()
            scan.scan()
            scan.notify('Noise scan has finished!')
            scan.analyze()
        except Exception as e:
            scan.log.error(e)
            scan.notify('ERROR: Noise scan has failed!')
