#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    First a global noise dependent threshold tuning is done. Start with a high threshold a lower until you have
    approx 10 hits per pixel. We aim for 1Hz/pixel noise rate -> 1kHz for all pixel -> 0.01ms readout time
    Finds the optimum global threshold value for target threshold using binary search.
'''

import numpy as np
from tqdm import tqdm
import copy
import threading

from telepix_daq.analysis import analysis
from telepix_daq.system.scan_base2 import ScanBase
from telepix_daq.analysis import plotting
import telepix_daq.analysis.analysis_utils as au
from telepix_daq.analysis import online as oa
import time
from telepix_daq.analysis.interpreter_clean import RawDataInterpreter
import matplotlib.pyplot as plt
from matplotlib import colors, cm


scan_configuration = {
  
    "start_row": 0, 
    "stop_row": 400, 
    "start_column": 0, 
    "stop_column": 120, 


    # Target threshold
    'VCAL_LOW': 127,
    'VCAL_HIGH': 165,
    'VCAL_STEP': -1,

    'min_occupancy': 10,

    # Stop conditions (choose one)
    'scan_timeout': False,             # Timeout for scan after which the scan will be stopped, in seconds; if False no limit on scan time
    'max_triggers': False,          # Number of maximum received triggers after stopping readout, if False no limit on received trigger
    'min_spec_occupancy': False,    # Minimum hits for each pixel above which the scan will be stopped; only a fraction of all pixels needs to reach this limit (see below)

    # For stop condition 'min_spec_occupancy' only
    'fraction': 0.99,   # Fraction of enabled pixels that need to reach the minimum occupancy (no hits in dead/disconnected pixels!)

    'trigger_latency': 100,     # Latency of trigger in units of 25 ns (BCs)
    'trigger_delay': 57,        # Trigger delay in units of 25 ns (BCs)
    'trigger_length': 32,       # Length of trigger command (amount of consecutive BCs are read out)
    'veto_length': 500,         # Length of TLU veto in units of 25 ns (BCs). This vetos new triggers while not all data is revieved. Increase by factor of number of connected chips. Should also be adjusted for longer trigger length.

    # Trigger configuration
    'bench': {'TLU': {
        'TRIGGER_MODE': 3,      # Selecting trigger mode: Use trigger inputs/trigger select (0), TLU no handshake (1), TLU simple handshake (2), TLU data handshake (3)
        'TRIGGER_SELECT': 0     # HitOR [DP_ML_5 and mDP] (3), HitOR [mDP only] (2), HitOR [DP_ML_5 only] (1), disabled (0)
    }
    }

}


class ScanExternal(ScanBase):
    scan_id = 'ext_trigger_scan'

    is_parallel_scan = True     # Parallel readout of ExtTrigger-type scans

    stop_scan = threading.Event()

    def _configure(self, scan_timeout=10, max_triggers=False, min_spec_occupancy=False, trigger_length=32, trigger_delay=57, veto_length=500,  trigger_latency=100, start_column=0, stop_column=120, start_row=0, stop_row=400,  VCAL_HIGH=172, VCAL_STEP = -1, **_):
        '''
        Parameters
        ----------
        max_triggers : int / False
            Maximum amount of triggers to record. Set to False for no limit.
        trigger_length : int
            Amount of BCIDs to read out on every trigger.
        trigger_delay : int
            Delay the trigger command by this amount in units of 25ns.
        veto_length : int
            Length of TLU veto in units of 25ns.
        trigger_latency : int
            Latency of trigger in units 
        start_column : int [0:120]
            First column to scan
        stop_column : int [0:120]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:400]
            First row to scan
        stop_row : int [0:400]
            Row to stop the scan. This row is excluded from the scan.

        VCAL_LOW : int
            Injection DAC low value.
        VCAL_HIGH : int
            Injection DAC high value.
        '''
        if (scan_timeout and max_triggers) or (scan_timeout and min_spec_occupancy) or (max_triggers and min_spec_occupancy):
            self.log.warning("You should only use one of the stop conditions at a time.")

        self.daq.configure_tlu_module(max_triggers=max_triggers)  # Configure tlu module using trigger configuration 
        # in use in original, might be needed...
        #self.daq.configure_trigger_cmd_pulse(trigger_length=trigger_length, trigger_delay=trigger_delay)   # Configure trigger command pulse
        # self.daq.configure_tlu_veto_pulse(veto_length=veto_length) 

        self.data.n_trigger = 0  
        self.n_trigger = 0   # Init total trigger number already here to prevent misleading, additional error when scan crashes

        self.data.start_column, self.data.stop_column, self.data.start_row, self.data.stop_row = start_column, stop_column, start_row, stop_row  
        self.data.tdac_range = [0,1,3,7]
        self.data.scan_range = ((start_column, stop_column), (start_row, stop_row))

        mask=self.chip.masks['enable'].copy()
        self.chip.masks['enable'][:]=False
        self.chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['injection'][start_column:stop_column, start_row:stop_row] = False
        self.chip.masks['tdac'][start_column:stop_column, start_row:stop_row] = 0b000
        
        self.chip.masks.apply_disable_mask()
        self.chip.masks.update()

        self.min_spec_occupancy = min_spec_occupancy
        if min_spec_occupancy:
            pass
            # self.data.hist_occ = oa.OccupancyHistogramming(chip_type=self.chip.chip_type.lower(), rx_id=int(self.chip.receiver[-1]))
            # self.data.occupancy = np.zeros(shape=self.chip.masks['enable'].shape)
            # self.data.enabled_pixels = np.logical_and(self.chip.masks['enable'], self.chip.masks.disable_mask)

        self.daq['VDAC_SR'].set_size(self.daq["VDAC_SR"]._conf['size'])
        self.daq['VDAC_SR']["ThPix"] = int(VCAL_HIGH)
        self.chip.set_sr_on(sr = "LD_VDac")
        self.daq['VDAC_SR'].write()
        for i in range(10000):
            if self.daq['VDAC_SR'].is_ready:
                break
            else:
                time.sleep(0.001)
        if self.daq['VDAC_SR'].is_done():
            self.chip.logger.info(f"Loading of threshold dac to {VCAL_HIGH} done")
        self.chip.set_sr_off(sr = "LD_VDac")

        self.interpreter = RawDataInterpreter()



    def _get_histogram(self,data_tuple):
        self.interpreter.reset_hist()
        
        if len(data_tuple)==0:
            hist_occ = np.zeros((self.chip.COL_SIZE, self.chip.ROW_SIZE), dtype=np.uint32)
            return hist_occ
        raw_data =  np.concatenate([i[0] for i in  data_tuple])
        hit_buffer = np.zeros(shape=len(raw_data), dtype=au.hit_dtype)
        ts_buffer = np.zeros(shape=len(raw_data), dtype=au.ts_dtype)
        hit_dat, ts_dat = self.interpreter.interpret(
            raw_data,
            hit_buffer,
            ts_buffer,
            0
        )
        hist_occ, _ = self.interpreter.get_histograms()
        return hist_occ


    def _scan(self, start_column=0, stop_column=120, scan_timeout=10, max_triggers=False, min_spec_occupancy=False, fraction=0.99, VCAL_LOW=165, VCAL_HIGH=172, VCAL_STEP = -1, min_occupancy=10,**_):
        '''
        Noise trimming loop with external trigger

        Parameters
        ----------
        scan_timeout : int / False
            Number of seconds to records triggers. Set to False for no limit.
        max_triggers : int / False
            Maximum amount of triggers to record. Set to False for no limit.
        tdac_value_bits : iterable
            Bits to toggle during tuning. Should be monotone.
        min_occupancy : int
            Minimum number of hits to count a pixel as noisy.

        '''

        def timed_out():
            if scan_timeout:
                current_time = time.time()
                if current_time - start_time > scan_timeout:
                    self.log.info('Scan timeout was reached')
                    return True
            return False
        
        if scan_timeout:
            self.pbar = tqdm(total=scan_timeout, unit='')  # [s]
        elif max_triggers:
            self.pbar = tqdm(total=max_triggers, unit=' Triggers')
        elif min_spec_occupancy:
            self.pbar = tqdm(total=100, unit=' % Hits')
        else:  # EUDAQ scan
            self.pbar = tqdm(total=scan_timeout, unit='')  # [s]


        thr_range = np.arange(VCAL_HIGH, VCAL_LOW, VCAL_STEP)
        pbar = tqdm(total=len(thr_range), unit=' Mask steps')

        ### first find lowest global threshold
        scan_param_id = 0
        which_thr=0
        iteration=0

        ### main scan loop
        while True:
            iteration+=1
            if abs(which_thr)>= len(thr_range):
                break
            vcal_thr=thr_range[which_thr]
            self.data.vcal_thr = vcal_thr
            if not self.fifo_readout.force_stop.is_set() or not (self.stop_scan.is_set() or timed_out()):
                error = False
                self.daq['VDAC_SR']["ThPix"] = int(vcal_thr)
                self.write_register("LD_Config","CONF_SR")
                self.chip.logger.info(f"Loading of threshold dac to {vcal_thr} done")

                readout_interval=0.01/10000

                with self.readout(scan_param_id=scan_param_id, readout_interval=readout_interval, fill_buffer=True, clear_buffer=True, callback=self.analyze_data_online):
                # with self.readout():
                    self.stop_scan.clear()

                    # self.daq.enable_ext_trigger()  # Enable external trigger
                    self.daq.enable_tlu_module()   # Enable TLU module

                    start_time = time.time()

                    # Scan loop
                    try:
                        triggers = self.daq.get_trigger_counter()

                        # Read tlu error counters
                        trig_low_timeout_errors, trig_accept_errors = self.daq.get_tlu_erros()
                        if trig_low_timeout_errors != 0 or trig_accept_errors != 0:
                            self.log.warning('TLU errors detected! TRIGGER_LOW_TIMEOUT_ERROR_COUNTER: {0}, TLU_TRIGGER_ACCEPT_ERROR_COUNTER: {1}'.format(trig_low_timeout_errors, trig_accept_errors))

                        time.sleep(1)
                        # Update progress bar
                        if scan_timeout:
                            self.pbar.n = int(time.time() - start_time)
                        elif max_triggers:
                            self.pbar.n = triggers
                        self.pbar.refresh()


                        ## check for errors
                        if self.daq['data_rx'].get_lost_count():
                            error = True
                            self.daq.rx_channels[self.chip.receiver].RESET = 1

                        ## check occupancy
                        hist_occ = self._get_histogram(list(self.fifo_readout.data))
                        occ_max = np.sum(hist_occ)
                        last_time, curr_time = self.fifo_readout.update_timestamp()
                        hit_rate = occ_max/(curr_time-last_time)
                        self.chip.logger.info(f"Occupancy for threshold {vcal_thr} is {occ_max} with hitrate of {hit_rate}")
                        
                        found_noisy_pix=False
                        disable_pix = False
                        for col in range(*self.data.scan_range[0]):
                            for row in range(*self.data.scan_range[1]):
                                # if hist_occ[col, row] >= min_occupancy and self.chip.masks.disable_mask[col, row]!= False:
                                if hist_occ[col, row] >= min_occupancy:
                                    print(col, row, hist_occ[col, row])
                                    self.chip.masks.disable_mask[col, row] = False
                                    self.log.warning(f"disabled {col} {row}, iteration {iteration}")
                                    found_noisy_pix=True
                                    thr_last_index = which_thr
                                    disable_pix = True
                                    self.chip.send_reset()
                        if found_noisy_pix:
                            which_thr-=1
                        else:
                            which_thr+=1

                        ## stop scan step if too many noisy pixels
                        mask = hist_occ >= min_occupancy
                        cmap =plt.cm.plasma
                        cmap.set_bad('w')   # white for None pixels
                        cmap.set_over('r')  # Make noisy pixels red
                        cmap.set_under('g')
                        fig, axs = plt.subplots(1,1)  
                        masked_hist_occ = np.ma.masked_where(mask, hist_occ)
                        # im = axs.imshow((hist_occ[:].sum(axis=2)).T, interpolation='none',aspect='auto', cmap=cmap)  # TODO: use pcolor or pcolormesh #, aspect='equal'
                        im = axs.imshow(masked_hist_occ, interpolation='none',aspect='auto', cmap=cmap)  # TODO: use pcolor or pcolormesh #, aspect='equal'
                        cbar = fig.colorbar(im, ax=axs)
                        plt.savefig(f"output_data/module_0/chip_0/noise_tuning/global_tuning_thr_{vcal_thr}_{iteration}.pdf")
                        if disable_pix:
                            self.chip.masks.apply_disable_mask()
                            self.chip.masks.update(disable_pix=disable_pix)
                        scan_param_id += 1
                        if (np.max(hist_occ) >= min_occupancy and not found_noisy_pix) or error:
                            break

                        # Stop scan if fraction of pixels reached minimum hits per pixel
                        #if min_spec_occupancy and np.count_nonzero(self.data.occupancy >= min_spec_occupancy) >= fraction * num_enabled_pixels:
                            #self.stop_scan.set()
                            #self.log.info('Reached required minimal number of hits per pixel ({0})'.format(min_spec_occupancy))

                        # Stop scan if reached trigger limit
                        if max_triggers and triggers >= max_triggers:
                            self.stop_scan.set()
                            self.log.info('Trigger limit was reached: {0}'.format(max_triggers))


                    except KeyboardInterrupt:  # React on keyboard interupt
                        self.stop_scan.set()
                        self.log.info('Scan was stopped due to keyboard interrupt')

            else:
                break

        self.pbar.close()

        self.log.info('Reset chip!')
        self.chip.send_reset()

        self.data.vcal_thr = thr_range[thr_last_index]
        self.data.vcal_thr += 1  # Increase global threshold -> To be adjusted
        self.log.success('Lowest stable global threshold value: {0}'.format(self.data.vcal_thr))
        self.daq['VDAC_SR']["ThPix"] = int(self.data.vcal_thr)
        self.write_register("LD_Config","CONF_SR")
       

        n_disabled_pixels = (~(self.chip.masks.disable_mask[self.data.scan_range[0][0]:self.data.scan_range[0][1], self.data.scan_range[1][0]:self.data.scan_range[1][1]])).sum()
        mean_tdac = np.mean(self.chip.masks['tdac'][self.data.scan_range[0][0]:self.data.scan_range[0][1], self.data.scan_range[1][0]:self.data.scan_range[1][1]])
        self.log.success('Found optimal TDAC settings with a mean of {0:1.2f} and disabled {1} untunable pixels.'.format(mean_tdac, n_disabled_pixels))
        self.log.success(f'Lowest possible Threshold is {self.data.vth}.')

        self.data.hist_occ.close()  # stop analysis process


        self.daq.disable_tlu_module()      # disable TLU module
        # self.daq.disable_ext_trigger()     # disable external trigger


        # if min_spec_occupancy:              # close online analysis for each chip
        #     for self.data in self._scan_data_containers:
                # self.data.hist_occ.close()

        self.n_trigger = self.data.n_trigger    # Print number of triggers in ScanBase

        # Reset latency
        #self.chip.write_trigger_latency(self.old_trigger_latency)

        self.log.success('Scan finished')





    # Used to overwrite data storing function: self.readout.handle_data
    def analyze_data_online(self, data_tuple):
        super(ScanExternal, self).handle_data(data_tuple)
        

    def handle_data(self, data_tuple, receiver=None):
        ''' Check recorded trigger number '''
        super(ScanExternal, self).handle_data(data_tuple)

        raw_data = data_tuple[0]

        self.data.hist_occ.add(raw_data)

        # if self.min_spec_occupancy:
        #     self.data.hist_occ.add(raw_data)

        sel = np.bitwise_and(raw_data, au.TRIGGER_HEADER) > 0
        trigger_words = raw_data[sel]
        trigger_number = np.bitwise_and(trigger_words, au.TRG_MASK)
        trigger_inc = np.diff(trigger_number)
        trigger_issues = np.logical_and(trigger_inc != 1, trigger_inc != -2**31)

        self.data.n_trigger += trigger_number.shape[0]

        if np.any(trigger_issues):
            self.log.warning('Trigger numbers not strictly increasing')
            if np.count_nonzero(trigger_issues > 0) <= 10:
                self.log.warning('Trigger delta(s): {0}'.format(str(trigger_inc[trigger_issues > 0])))
            else:
                self.log.warning('More than 10 trigger numbers not strictly increasing in this readout!')


if __name__ == '__main__':
    with ScanExternal(scan_config=scan_configuration) as scan:
        try:
            scan.configure()
            scan.scan()
            scan.notify('Noise scan has finished!')
            scan.analyze()
        except Exception as e:
            scan.log.error(e)
            scan.notify('ERROR: Noise scan has failed!')
