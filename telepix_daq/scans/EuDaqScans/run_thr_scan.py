#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Simple threshold scan 
'''

import numpy as np
from tqdm import tqdm
import threading

from telepix_daq.analysis import analysis
from telepix_daq.system.scan_base2 import ScanBase
from telepix_daq.system import logger
from telepix_daq.scans.shift_and_inject import shift_and_inject,get_scan_loop_mask_steps
import time


scan_configuration = {
  
     "start_row": 6, 
    "stop_row": 8, 
    "start_column": 6, 
    "stop_column": 8, 


    'thr_high_START':  175,
    'thr_high_STOP': 145,
    'thr_high_STEP': -1,

    "analogue": False, 

    # Stop conditions (choose one)
    'scan_timeout': True,             # Timeout for scan after which the scan will be stopped, in seconds; if False no limit on scan time
    'max_triggers': False,          # Number of maximum received triggers after stopping readout, if False no limit on received trigger
    

    # For stop condition 'min_spec_occupancy' only
    'fraction': 0.99,   # Fraction of enabled pixels that need to reach the minimum occupancy (no hits in dead/disconnected pixels!)


    'trigger_latency': 100,     # Latency of trigger in units of 25 ns (BCs)
    'trigger_delay': 57,        # Trigger delay in units of 25 ns (BCs)
    'trigger_length': 32,       # Length of trigger command (amount of consecutive BCs are read out)
    'veto_length': 500,         # Length of TLU veto in units of 25 ns (BCs). This vetos new triggers while not all data is revieved. Increase by factor of number of connected chips. Should also be adjusted for longer trigger length.

    # Trigger configuration
    'bench': {'TLU': {
        'TRIGGER_MODE': 3,      # Selecting trigger mode: Use trigger inputs/trigger select (0), TLU no handshake (1), TLU simple handshake (2), TLU data handshake (3)
        'TRIGGER_SELECT': 0     # HitOR [DP_ML_5 and mDP] (3), HitOR [mDP only] (2), HitOR [DP_ML_5 only] (1), disabled (0)
    }
    }

}


class ThrScan(ScanBase):
   
    scan_id = 'ext_trigger_scan'    

    is_parallel_scan = True     # Parallel readout of ExtTrigger-type scans

    stop_scan = threading.Event()

    def __init__(self, daq_conf=None, bench_config=None, scan_config={}, scan_config_per_chip=None, suffix=''):
        super().__init__(daq_conf, bench_config, scan_config, scan_config_per_chip, suffix)
        self.log = logger.setup_derived_logger(self.__class__.__name__)

    def _configure(self, scan_timeout=10, max_triggers=False, min_spec_occupancy=False, veto_length=500, start_column=0, stop_column=120, start_row=0, stop_row=400, **__):
        '''
        Parameters
        ----------
        max_triggers : int / False
            Maximum amount of triggers to record. Set to False for no limit.
        trigger_length : int
            Amount of BCIDs to read out on every trigger.
        trigger_delay : int
            Delay the trigger command by this amount in units of 25ns.
        veto_length : int
            Length of TLU veto in units of 25ns.
        trigger_latency : int
            Latency of trigger in units 
        start_column : int [0:120]
            First column to scan
        stop_column : int [0:120]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:400]
            First row to scan
        stop_row : int [0:400]
            Row to stop the scan. This row is excluded from the scan.

        VCAL_LOW : int
            Injection DAC low value.
        VCAL_HIGH : int
            Injection DAC high value.
        '''

        ### TLU
        if (scan_timeout and max_triggers) or (scan_timeout and min_spec_occupancy) or (max_triggers and min_spec_occupancy):
            self.log.warning("You should only use one of the stop conditions at a time.")
        
        # self.daq.configure_tlu_module(max_triggers=max_triggers)  # Configure tlu module using trigger configuration 
        # self.daq.configure_tlu_veto_pulse(veto_length=veto_length) 
    
        self.data.n_trigger = 0  
        self.n_trigger = 0   # Init total trigger number already here to prevent misleading, additional error when scan crashes

        ### Sensor
        self.data.start_column, self.data.stop_column, self.data.start_row, self.data.stop_row = start_column, stop_column, start_row, stop_row  
        self.data.scan_range = ((start_column, stop_column), (start_row, stop_row))

        self.chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['injection'][start_column:stop_column, start_row:stop_row] = True
   
        self.chip.masks.apply_disable_mask()
        self.chip.masks.update()
        self.stop_scan.clear()




    def _scan(self, scan_timeout=10, max_triggers=False, min_spec_occupancy=False, thr_high_START=150, thr_high_STOP=175, thr_high_STEP=1, analogue= False, **_):
        '''
        Global threshold tuning main loop

        Parameters
        ----------
        scan_timeout : int / False
            Number of seconds to records triggers. Set to False for no limit.
        n_injections : int
            Number of injections.

        '''


        def timed_out():
            if scan_timeout:
                current_time = time.time()
                if current_time - start_time > scan_timeout:
                    self.log.info('Scan timeout was reached')
                    return True
            return False

        # time.sleep(20)

        if scan_timeout:
            self.pbar = tqdm(total=scan_timeout, unit='')  # [s]
        elif max_triggers:
            self.pbar = tqdm(total=max_triggers, unit=' Triggers')
        elif min_spec_occupancy:
            self.pbar = tqdm(total=100, unit=' % Hits')
        else:  # EUDAQ scan
            self.pbar = tqdm(total=scan_timeout, unit='')  # [s]



        thr_range = np.arange(thr_high_START, thr_high_STOP, thr_high_STEP)


        for scan_param_id, vcal_thr in enumerate(thr_range):


            self.daq['VDAC_SR']["ThPix"] = int(vcal_thr)
            self.chip.write_register("LD_VDac","VDAC_SR")
            self.chip.logger.info(f"Loading of threshold dac to {vcal_thr} done")

            ### use for thr scan 
            if not self.fifo_readout.force_stop.is_set():
                with self.readout(scan_param_id=scan_param_id, readout_interval=0.00001):
                    shift_and_inject(chip=self.chip, n_injections=100, scan_param_id=scan_param_id, injection=True, analogue=analogue)
            else:
                break
        else:
            self.log.success('Scan finished')

            ### use for eudaq scan    
            # i_count=0
            # with self.readout(scan_param_id=scan_param_id, readout_interval=0.00001):
            #     while not self.fifo_readout.force_stop.is_set() or not (self.stop_scan.is_set() or timed_out()):
            #     # while not self.fifo_readout.force_stop.is_set() or not (self.stop_scan.is_set()):
            #         i_count+=1
            #         if self.stop_scan.is_set():
            #             break
            #         if i_count>60*0.5:
            #             break
            #         time.sleep(1)
        
        self.stop_scan.set()
        self.log.success('Scan finished')
        


        # self.daq.disable_tlu_module()      # disable TLU module TODO: maybe enable for us???
        # self.n_trigger = self.data.n_trigger    # Print number of triggers in ScanBase
        self.n_trigger +=1


    def _analyze(self):
        print("analyze")
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5', **self.configuration['bench']['analysis']) as a:
            a.analyze_data()


if __name__ == '__main__':
    with ThrScan(scan_config=scan_configuration) as scan:
        scan.start()
        print("done!")
        # scan.analyze()
