#!/bin/env python

'''
    Use this to start the complete tuning process or adapt it to start other scripts where you provide different arguments to each.
    ATTENTION: YAML file & folder structure is definded within testbeanch.yaml!
    TODO: Integrate flavor selection here -> Keep in mind that thr1/2 has to be adjusted in the corresponding script 
    Tuning:
        1. Run global tuning of all pixels to get mean thr:
            Parameters: Col range, row range, thr1/2 range, thr1/2 value (depends on amplifier flavor), mio3 file for testbench
            Return: h5 file
        2. Run analysis to calculate target thr from mean thr
            Input: h5 file
            Return: target thr
        3. Run local tuning for all pixels 
            Parameters: Col range, row range, thr1/2 range, thr1/2 value (depends on amplifier flavor), mio3 file for testbench, target_thr
            Return: h5 file
        4. Run analysis to compare threshold distribution for tuned & untuned scans
            Input: h5 file
            Return: Plots
'''

import argparse
import re
import subprocess
import sys
from telepix_daq.analysis import analyse_mean_threshold  


def extract_h5_file_path(output):
    """
    Extract the .pdf file path from the console output and convert it to .txt and .h5.
    """
    match = re.search(r'(\S+\.pdf)', output)
    if match:
        pdf_file = match.group(1)
        txt_file = pdf_file.replace('pdf', 'txt')
        h5_file = pdf_file.replace('pdf', 'h5')
        print(f"Found file: {pdf_file}, Converted to : {txt_file}, .h5: {h5_file}")
        return txt_file, h5_file
    else:
        raise RuntimeError("No file path found in console output.")


def run_subprocess(command):
    """
    Run the command using subprocess and capture real-time output from stdout and stderr.
    """
    stdout=subprocess.check_output(command,stderr=subprocess.STDOUT)

    stdout_lines = stdout.decode("utf-8").splitlines()

    # Poll the process in real-time
    stdout = "\n".join(stdout_lines)
    # stderr = "\n".join(stderr_lines)

    return stdout


def main():
    parser = argparse.ArgumentParser(description="Threshold tuning script with global and local adjustments.")
    parser.add_argument('--start_column', type=int, default=0)
    parser.add_argument('--stop_column', type=int, default=120)
    parser.add_argument('--start_row', type=int, default=0)
    parser.add_argument('--stop_row', type=int, default=100)
    parser.add_argument('--thr_high_START', type=float, default=170)
    parser.add_argument('--thr_high_STOP', type=float, default=138)
    parser.add_argument('--thr_high_STEP', type=float, default=-1)
    parser.add_argument('--vndac', type=float, default=25)
    
    args = parser.parse_args()

    # Run global threshold tuning
    global_command = [
        'python', 'scans/tune_global_threshold_global.py',
        '--start_column', str(args.start_column),
        '--stop_column', str(args.stop_column),
        '--start_row', str(args.start_row),
        '--stop_row', str(args.stop_row),
        '--thr_high_START', str(args.thr_high_START),
        '--thr_high_STOP', str(args.thr_high_STOP),
        '--thr_high_STEP', str(args.thr_high_STEP),
        '--vndac', str(args.vndac)
    ]

    stdout_global = run_subprocess(global_command)
    # print(stdout_global)
    # Extract the .txt and .h5 file paths from the global output
    try:
        txt_file_interpreted, h5_file_interpreted = extract_h5_file_path(stdout_global)
    except RuntimeError as e:
        print(f"Error extracting file paths: {e}")
        sys.exit(1)

    # Analyse data and calculate the target threshold
    directory_results = "/home/lhcb/daq/telepix_daq/telepix_daq/output_data/module_0/chip_09/2024_September/120V/cool_box/0degC/matrix_tuning/module_0/chip_09/"
    vndac, vminus, thr, thr_err = analyse_mean_threshold.analyze(directory_results=directory_results, input_file=txt_file_interpreted)
    # # target_thr = thr - 3 * thr_err
    target_thr = thr - 1 * thr_err
    # target_thr = 157
    print(f"Threshold goal: {target_thr}")

    # Run local threshold tuning
    local_command = [
        'python', 'scans/tune_global_threshold_local.py',
        '--start_column', str(args.start_column),
        '--stop_column', str(args.stop_column),
        '--start_row', str(args.start_row),
        '--stop_row', str(args.stop_row),
        '--thr_high_START', str(args.thr_high_START),
        '--thr_high_STOP', str(args.thr_high_STOP),
        '--thr_high_STEP', str(args.thr_high_STEP),
        '--target_thr', str(target_thr),
        '--vndac', str(args.vndac)
    ]

    stdout_local = run_subprocess(local_command)
    # Extract the .txt and .h5 file paths from the global output
    try:
        txt_file_interpreted, h5_file_interpreted = extract_h5_file_path(stdout_local)
    except RuntimeError as e:
        print(f"Error extracting file paths: {e}")
        sys.exit(1)

    # Optionally process the local command's output, if needed
    print("Done with local tuning.")


    # Analyse and compare tuning data with untuned data
    # analyse_tune_comparison(h5_file_interpreted, h5_file_interpreted_tune)

if __name__ == "__main__":
    main()
