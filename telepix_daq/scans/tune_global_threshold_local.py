#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Finds the optimum global threshold value for target threshold using binary search.
'''

import numpy as np
from tqdm import tqdm

from telepix_daq.analysis import analysis
from telepix_daq.system.scan_base2 import ScanBase
from telepix_daq.analysis import plotting
from telepix_daq.scans.shift_and_inject import shift_and_inject,get_scan_loop_mask_steps
import telepix_daq.analysis.analysis_utils as au
import time
from telepix_daq.analysis.interpreter_clean import RawDataInterpreter
import matplotlib.pyplot as plt
import tables as tb


scan_configuration = {
  
    "start_row": 1, 
    "stop_row": 20, 
    "start_column": 1, 
    "stop_column": 120, 

    'target_thr':  152,

    'thr_high_START':  190,
    'thr_high_STOP': 140,
    'thr_high_STEP': -1,

    "analogue": False, 

    'vndac': 25,
    "vminus": 0.8,
    
    'n_injections':100
}




class TDACTuning(ScanBase):
    scan_id = 'global_threshold_tuning'

    def _configure(self, start_column=0, stop_column=120, start_row=0, stop_row=400, analogue= False,  **_):
        '''
        Only selected pixels have TDACs different than 7. All remaining pixels have TDACs of 7 to avoid interferences
        '''

        self.data.start_column, self.data.stop_column, self.data.start_row, self.data.stop_row = start_column, stop_column, start_row, stop_row
        self.chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['injection'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['tdac'][start_column:stop_column, start_row:stop_row] =  tdac_mask

        self.chip.masks.apply_disable_mask()
        self.chip.masks.update()

        if not analogue:
            self.chip.set_inj_low(1.8/2)
            self.chip.set_inj_high(0.01)


        self.data.scan_range = ((start_column, stop_column), (start_row, stop_row))


    def _scan(self, n_injections=100, vndac = 0, vminus = 0.75, thr_high_START=1.2, thr_high_STOP=1.1, thr_high_STEP=-0.1, analogue= False, target_thr = None, **_):
        '''
        Global threshold tuning main loop
        Idea:
            1. Select target threshold: Either automized (*global.py) by scurve and mu - 3*sigma as target or set externally -> the automized scan is done externally because we need the mean & have to run the full analysis
            2. Select step strength/size: Select IPDAC properly -> The goal is to use the full range of TDACs but have a low power consumption: IPDAC=2 seems okay
            3. Select scurve steps: In principal only high granularity at transisition points is needed -> For the moment a fix step size is implemented. TODO: Improve this
            4. Matrix tuning via binary search:
                1. 111 -> 7: Start with this and clear MSB -> Take scurve abd check if mean occupancy is at lower or higher target thr. If lower, set MSB again
                2. Repeat with second bit
                3. Repeat with LSB
                4. Now you should have taken 3 scurves. If a pixel has TDACs of 7, take another scurve
                5. Apply best TDACs to all pixels
            5. Comparison of tuned & untuned threshold distribution -> Do this offline
        '''

        ## Possibility to change vndac & vminus
        self.daq['DAC_SR']['VNDAC'] = vndac
        self.chip.write_register("LD_Dac","DAC_SR")
        self.chip.set_global_voltage(VMinus_Pix=vminus)

        # Only check pixel that can respond
        sel_pixel = np.zeros([self.chip.COL_SIZE, self.chip.ROW_SIZE],dtype=np.bool_)
        sel_pixel[self.data.start_column:self.data.stop_column, self.data.start_row:self.data.stop_row] = True

        ## measure scurve for fixed injection
        thr_range = np.arange(thr_high_START, thr_high_STOP, thr_high_STEP)

        for scan_param_id, vcal_thr in enumerate(thr_range):
            if not self.fifo_readout.force_stop.is_set():

                self.daq['VDAC_SR']["ThPix"] = int(vcal_thr)
                self.chip.write_register("LD_VDac","VDAC_SR")
                
                with self.readout(scan_param_id=scan_param_id, readout_interval=0.00001):
                    shift_and_inject(chip=self.chip, n_injections=n_injections, scan_param_id=scan_param_id, injection=True, analogue=analogue)
            else:
                break





    def _analyze(self):

        self.scurve_results = {}  # Initialize a dictionary to store scurves

        self.scan_parameter_range = np.arange(self.scan_config['thr_high_START'],
                                                self.scan_config['thr_high_STOP'] + self.scan_config['thr_high_STEP'],
                                                self.scan_config['thr_high_STEP'])

        with analysis.Analysis(raw_data_file=self.output_filename + '.h5',file_name_add_on = f'local_vndac{vndac}_vminus_{vminus}', **self.configuration['bench']['analysis']) as a:
            a.analyze_data()
            analyzed_data_file=a.analyzed_data_file
            if isinstance(analyzed_data_file, str):
                in_file = tb.open_file(analyzed_data_file, 'r')
                root = in_file.root
            else:
                root = analyzed_data_file
            hist_occ = root.HistOcc[:]


            for col in range(self.data.start_column, self.data.stop_column):
                for row in range(self.data.start_row, self.data.stop_row):
                    scurve= hist_occ[col,row]
                    # self.scurve_results[(col, row)] = scurve 
                    self.scurve_results[(col, row)] = {
                    'scan_parameter_range': self.scan_parameter_range,
                    'scurve': scurve
                }

            if self.configuration['bench']['analysis']['create_pdf']:
                with plotting.Plotting(analyzed_data_file=a.analyzed_data_file, mask_noisy_pixels=120) as p:
                    p.create_standard_plots()




if __name__ == '__main__':   
    import argparse

    parser = argparse.ArgumentParser(description="Global threshold tuning argument parser.")
    # Optional arguments for scan configuration
    parser.add_argument('--start_row', type=int, default=scan_configuration['start_row'], help='Start row for the scan')
    parser.add_argument('--stop_row', type=int, default=scan_configuration['stop_row'], help='Stop row for the scan')
    parser.add_argument('--start_column', type=int, default=scan_configuration['start_column'], help='Start column for the scan')
    parser.add_argument('--stop_column', type=int, default=scan_configuration['stop_column'], help='Stop column for the scan')
    parser.add_argument('--thr_high_START', type=float, default=scan_configuration['thr_high_START'], help='Starting value for high threshold scan')
    parser.add_argument('--thr_high_STOP', type=float, default=scan_configuration['thr_high_STOP'], help='Stopping value for high threshold scan')
    parser.add_argument('--thr_high_STEP', type=float, default=scan_configuration['thr_high_STEP'], help='Step value for high threshold scan')
    parser.add_argument('--analogue', action='store_true', default=scan_configuration['analogue'], help='Use analogue or digital injection')
    parser.add_argument('--vndac', type=int, default=scan_configuration['vndac'], help='VNDAC value')
    parser.add_argument('--vminus', type=float, default=scan_configuration['vminus'], help='Vminus voltage')
    parser.add_argument('--target_thr', type=float, default=scan_configuration['target_thr'], help='Target threshold')

    args = parser.parse_args()

    # Update scan configuration with parsed arguments
    scan_configuration['start_row'] = args.start_row
    scan_configuration['stop_row'] = args.stop_row
    scan_configuration['start_column'] = args.start_column
    scan_configuration['stop_column'] = args.stop_column
    scan_configuration['thr_high_START'] = args.thr_high_START
    scan_configuration['thr_high_STOP'] = args.thr_high_STOP
    scan_configuration['thr_high_STEP'] = args.thr_high_STEP
    scan_configuration['analogue'] = args.analogue
    scan_configuration['vndac'] = args.vndac
    scan_configuration['vminus'] = args.vminus
    scan_configuration['target_thr'] = args.target_thr


    with TDACTuning(scan_config=scan_configuration) as tuning:
        vndac = scan_configuration['vndac']
        vminus = scan_configuration['vminus']
        start_column = scan_configuration['start_column']
        stop_column = scan_configuration['stop_column']
        start_row = scan_configuration['start_row']
        stop_row = scan_configuration['stop_row']
        best_tdac_map = np.full((stop_column - start_column, stop_row - start_row), 0b011, dtype=int)  # Start TDAC at 7 (max value)
      
        # Loop over TDAC bits for all pixels at once
        for bit in range(2, -1, -1):  # 3 bits (MSB to LSB)
            # Prepare TDAC for all pixels in the scan range
            for col in range(start_column, stop_column):
                for row in range(start_row, stop_row):
                    best_tdac_value = best_tdac_map[col - start_column, row - start_row]
                    # Clear the current bit (bitwise AND to clear)
                    current_tdac_value = best_tdac_value & ~(1 << bit)
                    # tuning.log.info(f"Pixel ({col}, {row}): TDAC changed from {format(best_tdac_value, '03b')} to {format(current_tdac_value, '03b')} (clearing bit {bit})")
                    best_tdac_map[col - start_column, row - start_row] = current_tdac_value

            # Perform the scan for all pixels at this TDAC configuration
            tdac_mask = best_tdac_map
            tuning.start(reconfigure=True)  # Perform scan with the updated TDAC values

            # After scan, analyze the results and update TDAC values based on threshold comparison
            for col in range(start_column, stop_column):
                for row in range(start_row, stop_row):
                    result = tuning.scurve_results[(col, row)]
                    scurve = result['scurve']
                    closest_index = np.argmin(np.abs(scurve - scan_configuration['n_injections'] / 2))
                    thr_at_closest_occ = result['scan_parameter_range'][closest_index]
                    target_thr = scan_configuration['target_thr']

                    previous_tdac_value = best_tdac_map[col - start_column, row - start_row]

                    # If threshold is above the target, restore the bit (now we increase TDAC if the threshold is higher)
                    if thr_at_closest_occ > target_thr:
                        tuning.log.info(f"Threshold {thr_at_closest_occ} for pixel ({col},{row}) is larger than {target_thr}")
                        best_tdac_map[col - start_column, row - start_row] |= (1 << bit)
                    else:
                        tuning.log.info(f"Threshold {thr_at_closest_occ} for pixel ({col},{row}) is smaller than {target_thr}")

                    updated_tdac_value = best_tdac_map[col - start_column, row - start_row]

                    # Log the change in TDAC value
                    if previous_tdac_value != updated_tdac_value:
                        tuning.log.info(f"Pixel ({col}, {row}): TDAC changed from {format(previous_tdac_value, '03b')} to {format(updated_tdac_value, '03b')}")


        tdac_mask = best_tdac_map
        tuning.start(reconfigure=True)  # Perform scan with the updated TDAC values

        tuning.log.info("Final best TDAC map has been applied.")
        tuning.log.info(f'Best TDAC map:\n{best_tdac_map}')
        tuning.log.info("TDAC tuning process finished.")