#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Finds the optimum global threshold value for target threshold using binary search.
'''

import numpy as np
from tqdm import tqdm

from telepix_daq.analysis import analysis
from telepix_daq.system.scan_base2 import ScanBase
from telepix_daq.analysis import plotting
from telepix_daq.scans.shift_and_inject import shift_and_inject,get_scan_loop_mask_steps
import telepix_daq.analysis.analysis_utils as au
import time


### ATTENTION: You have to change the name in analysis depending on the initial tdacs. If all are 0 include this in the name. If not, remove it

scan_configuration = {
  
    "start_row": 1, 
    "stop_row": 2, 
    "start_column": 1, 
    "stop_column": 2, 

    'VCAL_MED': 0.9,
    'VCAL_LOW_start': 0.9,
    'VCAL_LOW_stop': 0.55,
    'VCAL_LOW_step': -0.01,
    
    "analogue": False, 

    'ThPix': 143,

    'vndac': 10,
    "vminus": 0.8,

    'tdac_start': 7,
    'tdac_stop': -1,
    'tdac_step': -1,
}



class TDACTuning(ScanBase):
    scan_id = 'global_threshold_tuning_inj'

    def _configure(self, start_column=0, stop_column=120, start_row=0, stop_row=400, analogue= False, **_):
        '''
        Parameters
        ----------
        start_column : int [0:120]
            First column to scan
        stop_column : int [0:120]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:400]
            First row to scan
        stop_row : int [0:400]
            Row to stop the scan. This row is excluded from the scan.

        VCAL_LOW : int
            Injection DAC low value.
        VCAL_HIGH : int
            Injection DAC high value.
        '''

        self.data.start_column, self.data.stop_column, self.data.start_row, self.data.stop_row = start_column, stop_column, start_row, stop_row
        self.chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['injection'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['tdac'][start_column:stop_column, start_row:stop_row] = tdac_i
        
        self.chip.masks.apply_disable_mask()
        self.chip.masks.update()
        
        # self.chip.set_tdac(tdac_i)

        if not analogue:
            self.chip.set_inj_low(1.8/2)
            self.chip.set_inj_high(0.01)


    def _scan(self, n_injections=100, vndac = 0, vminus = 0.75,VCAL_MED=0.9, VCAL_LOW_start=0.5, VCAL_LOW_stop=0.15, VCAL_LOW_step=-0.01, ThPix=175, analogue= False, **_):
        '''
        Global threshold tuning main loop
        '''

        ## Possibility to change vndac & vminus
        self.daq['DAC_SR']['VNDAC'] = vndac
        self.chip.write_register("LD_Dac","DAC_SR")
        self.chip.set_global_voltage(VMinus_Pix=vminus)
        self.daq['VDAC_SR']["ThPix"] = int(ThPix)
        self.chip.write_register("LD_VDac","VDAC_SR")

        # Only check pixel that can respond
        sel_pixel = np.zeros([self.chip.COL_SIZE, self.chip.ROW_SIZE],dtype=np.bool_)
        sel_pixel[self.data.start_column:self.data.stop_column, self.data.start_row:self.data.stop_row] = True

        ## measure scurve for fixed injection
        thr_range = np.arange(VCAL_LOW_start, VCAL_LOW_stop, VCAL_LOW_step)
        for scan_param_id, vcal_low in enumerate(thr_range):

            if not self.fifo_readout.force_stop.is_set():
                with self.readout(scan_param_id=scan_param_id, readout_interval=0.00001):
                    # pass
                    shift_and_inject(chip=self.chip, n_injections=n_injections, scan_param_id=scan_param_id,  inj_high=VCAL_MED, inj_low=vcal_low, injection=True, analogue = analogue)
            else:
                break


    def _analyze(self):
        # with analysis.Analysis(raw_data_file=self.output_filename + '.h5',file_name_add_on = f'_tdac_{tdac_i}_vndac{vndac}_vminus_{vminus}_ipdac{ipdac}_0tdacs', **self.configuration['bench']['analysis']) as a:
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5',file_name_add_on = f'_tdac_{tdac_i}_vndac{vndac}_vminus_{vminus}', **self.configuration['bench']['analysis']) as a:
            a.analyze_data()

        if self.configuration['bench']['analysis']['create_pdf']:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file, mask_noisy_pixels=120) as p:
                p.create_standard_plots()


if __name__ == '__main__':

    import argparse

    parser = argparse.ArgumentParser(description="Global threshold tuning argument parser.")
    # Optional arguments for scan configuration
    parser.add_argument('--start_row', type=int, default=scan_configuration['start_row'], help='Start row for the scan')
    parser.add_argument('--stop_row', type=int, default=scan_configuration['stop_row'], help='Stop row for the scan')
    parser.add_argument('--start_column', type=int, default=scan_configuration['start_column'], help='Start column for the scan')
    parser.add_argument('--stop_column', type=int, default=scan_configuration['stop_column'], help='Stop column for the scan')
    parser.add_argument('--thr_high_START', type=float, default=scan_configuration['VCAL_LOW_start'], help='Starting value for high threshold scan')
    parser.add_argument('--thr_high_STOP', type=float, default=scan_configuration['VCAL_LOW_stop'], help='Stopping value for high threshold scan')
    parser.add_argument('--thr_high_STEP', type=float, default=scan_configuration['VCAL_LOW_step'], help='Step value for high threshold scan')
    parser.add_argument('--analogue', action='store_true', default=scan_configuration['analogue'], help='Use analogue or digital injection')
    parser.add_argument('--vndac', type=int, default=scan_configuration['vndac'], help='VNDAC value')
    parser.add_argument('--vminus', type=float, default=scan_configuration['vminus'], help='Vminus voltage')

    args = parser.parse_args()

    # Update scan configuration with parsed arguments
    scan_configuration['start_row'] = args.start_row
    scan_configuration['stop_row'] = args.stop_row
    scan_configuration['start_column'] = args.start_column
    scan_configuration['stop_column'] = args.stop_column
    scan_configuration['VCAL_LOW_start'] = args.thr_high_START
    scan_configuration['VCAL_LOW_stop'] = args.thr_high_STOP
    scan_configuration['VCAL_LOW_step'] = args.thr_high_STEP
    scan_configuration['analogue'] = args.analogue
    scan_configuration['vndac'] = args.vndac
    scan_configuration['vminus'] = args.vminus



    with TDACTuning(scan_config=scan_configuration) as tuning:
        vndac = scan_configuration['vndac']
        vminus = scan_configuration['vminus']
        tdac_range = np.arange(scan_configuration['tdac_start'], scan_configuration['tdac_stop'], scan_configuration['tdac_step'])

        for tdac_i in tdac_range:
            tuning.log.info(f'Starting scan for TDAC value: {tdac_i}')
            tuning.start(reconfigure=True)
            tuning.log.info(f'Finished analysis for TDAC value: {tdac_i}')
            # tuning.close() #TODO: FixMe! Configuration_out in h5 is only set for last step and not for each. Reconfigure forces new config of sensor to write masks properly
