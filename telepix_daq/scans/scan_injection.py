#!/bin/env python
import sys
import numpy as np
from tqdm import tqdm

from telepix_daq.analysis import analysis
from telepix_daq.system.scan_base2 import ScanBase
from telepix_daq.analysis import plotting
from telepix_daq.scans.shift_and_inject import shift_and_inject,get_scan_loop_mask_steps
import telepix_daq.analysis.analysis_utils as au

np.set_printoptions(threshold=sys.maxsize, linewidth=500)

"""
       This scurve scan measures the #injections in dependence  of the injected charge in volts. 
       You have to change a jumper on the PCB for analogue injection  & change analogue to TRUE.
       Vice versa for digital.
"""

scan_configuration = {

    "start_row": 0, 
    "stop_row": 1, 
    "start_column": 0, 
    "stop_column": 1, 

    'n_injections': 100,

    'VCAL_MED': 0.9,
    'VCAL_LOW_start': 0.9,
    'VCAL_LOW_stop': 0.55,
    'VCAL_LOW_step': -0.01,
    
    "analogue": False, 

    'ThPix': 145

}




class ScanPixel(ScanBase):
    scan_id = "threshold_scan"


    def _configure(self, start_column=0, stop_column=120, start_row=0, stop_row=400, TDAC=None, analogue=False, **_):
        '''
        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        TDAC : int / np.ndarray
            If int: TDAC value to use for all enabled pixels, overwriting any existing mask.
            If np.ndarray: TDAC mask to use for enabled pixels. Has to have shape to match start_column:stop_column, start_row:stop_row
        '''

    
        mask=self.chip.masks['enable'].copy()
        self.chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['injection'][start_column:stop_column, start_row:stop_row] = True
        # self.chip.masks['tdac'][start_column:stop_column, start_row:stop_row] = True

        if TDAC is not None:
            if type(TDAC) == int:
                self.chip.masks['tdac'][start_column:stop_column, start_row:stop_row] = TDAC
            elif type(TDAC) == np.ndarray:
                self.chip.masks['tdac'][start_column:stop_column, start_row:stop_row] = TDAC[start_column:stop_column, start_row:stop_row]

        
        self.chip.masks.apply_disable_mask()
        self.chip.masks.update()

        if not analogue:
            self.chip.set_inj_low(1.8/2)
            self.chip.set_inj_high(0.01)



    def _scan(self, n_injections=100, VCAL_MED=0.9, VCAL_LOW_start=0.5, VCAL_LOW_stop=0.15, VCAL_LOW_step=-0.01, ThPix=175, analogue= False, **_):
        """
        Threshold scan starting with high value -> low value for ThPix
        """

        thr_range = np.arange(VCAL_LOW_start, VCAL_LOW_stop, VCAL_LOW_step)

        pbar = tqdm(total=get_scan_loop_mask_steps(chip=self.chip) * len(thr_range), unit=' Mask steps')


        self.daq['VDAC_SR'].set_size(self.daq["VDAC_SR"]._conf['size'])
        self.daq['VDAC_SR']["ThPix"] = int(ThPix)
        self.chip.set_sr_on(sr = "LD_VDac")
        self.daq['VDAC_SR'].write()
        self.chip.sr_ready('VDAC_SR')
        if self.daq['VDAC_SR'].is_done():
            self.chip.logger.info(f"Loading of ThPix to {ThPix} is done!")
        self.chip.set_sr_off(sr = "LD_VDac")


        for scan_param_id, vcal_low in enumerate(thr_range):

            if not self.fifo_readout.force_stop.is_set():

                with self.readout(scan_param_id=scan_param_id, readout_interval=0.00001):
                    # pass
                    shift_and_inject(chip=self.chip, n_injections=n_injections, pbar=pbar, scan_param_id=scan_param_id,  inj_high=VCAL_MED, inj_low=vcal_low, injection=True, analogue = analogue)
            else:
                break
        else:
            self.log.success('Scan finished')
        pbar.close()
        self.chip.power_down()


    def _analyze(self):
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5', **self.configuration['bench']['analysis']) as a:
            a.analyze_data()

        if self.configuration['bench']['analysis']['create_pdf']:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file, mask_noisy_pixels=120) as p:
                p.create_standard_plots()



if __name__ == "__main__":
   with ScanPixel(scan_config = scan_configuration) as scan:
       scan.start()