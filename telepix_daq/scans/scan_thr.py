#!/bin/env python
import sys
import numpy as np
from tqdm import tqdm
import time
from random import randint

from telepix_daq.analysis import analysis
from telepix_daq.system.scan_base2 import ScanBase
from telepix_daq.analysis import plotting
from telepix_daq.scans.shift_and_inject import shift_and_inject,get_scan_loop_mask_steps
import telepix_daq.analysis.analysis_utils as au

np.set_printoptions(threshold=sys.maxsize, linewidth=500)

"""
    This scurve scan measures the #injections in dependence  of the comparator threshold.
    You have to change a jumper on the PCB for analogue injection  & change analogue to TRUE.
    Vice versa for digital.
"""

scan_configuration = {

    "start_row": 6, 
    "stop_row": 8, 
    "start_column": 6, 
    "stop_column":  8, 

    'thr_high_START':  175,
    'thr_high_STOP': 150,
    'thr_high_STEP': -2,

    "analogue": False 
}



class ScanPixel(ScanBase):
    scan_id = "pixel_thr_scan"
    
    def _configure(self, start_column=0, stop_column=120, start_row=0, stop_row=400, TDAC=None, analogue= False, **_):
        '''
        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        TDAC : int / np.ndarray
            If int: TDAC value to use for all enabled pixels, overwriting any existing mask.
            If np.ndarray: TDAC mask to use for enabled pixels. Has to have shape to match start_column:stop_column, start_row:stop_row
        enable:
            enables all pixels once
        injection:
            mask shifts vor given pixel range -> inject always in one specific pixel for each step
        '''
        

        self.chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['injection'][start_column:stop_column, start_row:stop_row] = True
        # self.chip.masks['tdac'][start_column:stop_column, start_row:stop_row] = True

        # if TDAC is not None:
        #     if type(TDAC) == int:
        #         self.chip.masks['tdac'][start_column:stop_column, start_row:stop_row] = TDAC
        #     elif type(TDAC) == np.ndarray:
        #         self.chip.masks['tdac'][start_column:stop_column, start_row:stop_row] = TDAC[start_column:stop_column, start_row:stop_row]


        
        self.chip.masks.apply_disable_mask()
        self.chip.masks.update()

        if not analogue:
            self.chip.set_inj_low(1.8/2)
            self.chip.set_inj_high(0.01)



    def _scan(self, n_injections=100, thr_high_START=150, thr_high_STOP=175, thr_high_STEP=1, analogue= False,**_):
        """
        Threshold scan
        """
        # self.chip.setup_analog_injection()
        thr_range = np.arange(thr_high_START, thr_high_STOP, thr_high_STEP)
        pbar = tqdm(total=get_scan_loop_mask_steps(chip=self.chip) * len(thr_range), unit=' Mask steps')
        # self.chip.set_en_ampout([0])
        # self.chip.set_en_hitbus([0])

        for scan_param_id, vcal_thr in enumerate(thr_range):
            # print(scan_param_id)
            
            if not self.fifo_readout.force_stop.is_set():

                self.daq['VDAC_SR'].set_size(self.daq["VDAC_SR"]._conf['size'])
                self.daq['VDAC_SR']["ThPix"] = int(vcal_thr)
                self.chip.set_sr_on(sr = "LD_VDac")
                self.daq['VDAC_SR'].write()
                for i in range(10000):
                    if self.daq['VDAC_SR'].is_ready:
                        break
                    else:
                        time.sleep(0.001)
                if self.daq['VDAC_SR'].is_done():
                    self.chip.logger.info(f"Loading of threshold dac to {vcal_thr} done")
                self.chip.set_sr_off(sr = "LD_VDac")
                
                with self.readout(scan_param_id=scan_param_id, readout_interval=0.00001):
                    shift_and_inject(chip=self.chip, n_injections=n_injections, pbar=pbar, scan_param_id=scan_param_id, injection=True, analogue=analogue)
            else:
                break
        else:
            self.log.success('Scan finished')
        pbar.close()
        self.chip.power_down()


    def _analyze(self):
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5', **self.configuration['bench']['analysis']) as a:
            a.analyze_data()

        if self.configuration['bench']['analysis']['create_pdf']:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file, mask_noisy_pixels=120) as p:
                p.create_standard_plots()


if __name__ == "__main__":
    import argparse


    parser = argparse.ArgumentParser(description="Global threshold tuning argument parser.")
    # Optional arguments for scan configuration
    parser.add_argument('--start_row', type=int, default=scan_configuration['start_row'], help='Start row for the scan')
    parser.add_argument('--stop_row', type=int, default=scan_configuration['stop_row'], help='Stop row for the scan')
    parser.add_argument('--start_column', type=int, default=scan_configuration['start_column'], help='Start column for the scan')
    parser.add_argument('--stop_column', type=int, default=scan_configuration['stop_column'], help='Stop column for the scan')
    parser.add_argument('--thr_high_START', type=float, default=scan_configuration['thr_high_START'], help='Starting value for high threshold scan')
    parser.add_argument('--thr_high_STOP', type=float, default=scan_configuration['thr_high_STOP'], help='Stopping value for high threshold scan')
    parser.add_argument('--thr_high_STEP', type=float, default=scan_configuration['thr_high_STEP'], help='Step value for high threshold scan')
    parser.add_argument('--analogue', action='store_true', default=scan_configuration['analogue'], help='Use analogue or digital injection')

    args = parser.parse_args()

    # Update scan configuration with parsed arguments
    scan_configuration['start_row'] = args.start_row
    scan_configuration['stop_row'] = args.stop_row
    scan_configuration['start_column'] = args.start_column
    scan_configuration['stop_column'] = args.stop_column
    scan_configuration['thr_high_START'] = args.thr_high_START
    scan_configuration['thr_high_STOP'] = args.thr_high_STOP
    scan_configuration['thr_high_STEP'] = args.thr_high_STEP
    scan_configuration['analogue'] = args.analogue

    with ScanPixel(scan_config = scan_configuration) as scan:
       scan.start()
