#!/bin/env python

'''
    Use this to run thr scan over all pixels
'''

import argparse
import subprocess
import math

def main():

    # Total rows and columns for the full scan
    total_rows = 400
    total_cols = 120

    # Define package size (number of rows and columns per package)
    package_rows = 20
    package_cols = 60

    parser = argparse.ArgumentParser(description="Threshold scan script.")

    # Define arguments for thr scans
    parser.add_argument('--thr_high_START', type=float, default=190, help='Starting value for high threshold scan')
    parser.add_argument('--thr_high_STOP', type=float, default=140, help='Stopping value for high threshold scan')
    parser.add_argument('--thr_high_STEP', type=float, default=-1, help='Step value for high threshold scan')

    args = parser.parse_args()

    # Calculate number of row and column packages
    num_row_packages = math.ceil(total_rows / package_rows)
    num_col_packages = math.ceil(total_cols / package_cols)


    # Start scanning in packages
    for row_package in range(num_row_packages):
        for col_package in range(num_col_packages):
            # Calculate the start and stop rows for the current package
            start_row = row_package * package_rows
            stop_row = min((row_package + 1) * package_rows, total_rows)

            # Calculate the start and stop columns for the current package
            start_col = col_package * package_cols
            stop_col = min((col_package + 1) * package_cols, total_cols)

            print(f"Scanning package - Rows: {start_row} to {stop_row}, Columns: {start_col} to {stop_col}")


            ##Call tune_global_threshold_tdac_dependence with updated arguments and capture the output
            command = [
                'python', 'scans/scan_thr.py', 
                '--start_column', str(start_col),
                '--stop_column', str(stop_col),
                '--start_row', str(start_row),
                '--stop_row', str(stop_row),
                '--thr_high_START', str(args.thr_high_START),
                '--thr_high_STOP', str(args.thr_high_STOP),
                '--thr_high_STEP', str(args.thr_high_STEP),
            ]

            print(f"Running command: {' '.join(command)}")
            
            process = subprocess.Popen(
                command,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                text=True
            )
            stdout, stderr = process.communicate()
            
            ##Print stdout and stderr for debugging
            print("STDOUT:")
            print(stdout)
            print("STDERR:")
            print(stderr)




if __name__ == "__main__":
    main()
