#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# HISKP Bonn
# ------------------------------------------------------------
#

import numpy as np
import time
'''
    This module takes care of the mask shifting and injection of the supported chips
    in order to keep actual scans cleaner.
'''


def shift_and_inject(chip, n_injections, pbar=None, scan_param_id=0, masks=['injection'], pattern='default', cache=False, skip_empty=True, inj_high=0.9, inj_low=0.4, v_digital = 160, injection = True, analogue = False):
    ''' Regular mask shift and analog injection function.
    
    Parameters:
    ----------
        scan : scan object
            Scan object
        n_injections : int
            Number of injections per loop.
        pbar : tqdm progressbar
            Tqdm progressbar
        scan_param_id : int
            Scan parameter id of actual scan loop
        masks : list
            List of masks ('injection') which should be shifted during scan loop.
        pattern : string
            Injection pattern ('default', 'hitbus', ...)
        cache : boolean
            If True use mask caching for speedup. Default is False.
        skip_empty : boolean
            If True skip empty mask steps for speedup. Default is True.
        inj_high: float
            Hardcoded to 1.8V/2
        inj_low: float
            If external injection is used this is the one you want to change! Do not go above 0.9!!!
            Injection voltage is (high-low)*2
    '''


    for fe, active_pixels in chip.masks.shift(masks=masks, pattern=pattern, cache=cache, skip_empty=skip_empty):
        if not fe == 'skipped':
            if injection:
                ### only used for debug 
                # chip.set_en_ampout(active_pixels[0])
                # chip.set_en_hitbus(active_pixels[0])
                enabled_pixels=chip.masks["enable"]
                if enabled_pixels[active_pixels].sum():
                    # print(f"Injection {active_pixels}")
                    if analogue:
                        chip.set_inj_all(inj_high=inj_high, inj_low=inj_low, inj_n=n_injections, inj_width = 50, delay=1500)
                        chip.start_inj()
                    else:
                        chip.set_inj_param(inj_n=n_injections, inj_width = 50, delay=2000)
                        chip.setup_digital_injection(vcal_high=inj_high, vcal_med=inj_low, v_ref = 1.9,  unit="V")
                        chip.start_inj()
                    if chip.daq["data_rx"]["LOST_ERR"]:
                        chip.logger.info(f"We did see this lost data: {chip.daq['data_rx'].get_lost_count()}")
                        # chip.logger.info(f"We did see this rc2: {chip.daq['data_rx'].get_read_col2_count()}")
                        # chip.logger.info(f"We did see this decoder error: {chip.daq['data_rx'].get_decoder_err_count()}")
                        # chip.send_chip_reset()
                        chip.daq['data_rx'].RESET_CNT_SYNC = 1
                        chip.daq['data_rx'].RESET_CNT_SYNC = 0
                        time.sleep(0.001)
                        chip.logger.warning("We lost data reset send!")
                        # chip.logger.warning("Repeat injection!")
                        # time.sleep(0.001)
                        # # chip.logger.info(f"After reset we did see this lost data: {chip.daq['data_rx'].get_lost_count()}")
                        # # chip.logger.info(f"After reset we did see rc2: {chip.daq['data_rx'].get_read_col2_count()}")
                        # # chip.logger.info(f"After reset we did see this decoder error: {chip.daq['data_rx'].get_decoder_err_count()}")
                        # chip.set_inj_param(inj_n=n_injections, inj_width = 100, delay=1500)
                        # chip.setup_digital_injection(vcal_high=inj_high, vcal_med=inj_low, v_ref = 1.9,  unit="V")
                        # chip.start_inj()
                else:
                    continue
            else:
                pass
        if pbar is not None:
            pbar.update(1)



def get_scan_loop_mask_steps(chip, pattern='default'):
    ''' Returns total number of mask steps for specific pattern

    Parameters:
    ----------
        scan : scan object
            Scan object
        pattern : string
            Injection pattern ('default', 'hitbus', ...)
    '''

    return chip.masks.get_mask_steps(pattern=pattern)
