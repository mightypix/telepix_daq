#!/bin/env python
import sys
import numpy as np
import time
from tqdm import tqdm
from bitarray.util import ba2int

from telepix_daq.analysis import analysis
from telepix_daq.system.scan_base2 import ScanBase
from telepix_daq.analysis import plotting
from telepix_daq.scans.shift_and_inject import shift_and_inject,get_scan_loop_mask_steps
import telepix_daq.analysis.analysis_utils as au


np.set_printoptions(threshold=sys.maxsize, linewidth=500)

"""
    This scan tries to find the lowest achievable global thresholds at certain initial TRIM settings for every enabled CSA. 
"""

scan_configuration = {

    "start_row": 1, 
    "stop_row": 3, 
    "start_column": 1, 
    "stop_column": 3,

    "dac_register" : "DAC_SR",
    "load_dac_register" : "LD_Dac",
    "dacs_to_scan": ["VP"], #IPLoad, VP, VNFoll

    "dac_start": 0,
    "dac_stop":  10,
    "dac_step": 1,

    "channels": ["VDDA","VDDD"], 

    'n_injections': 100,

    "analogue": False 

}



class ScanDac(ScanBase):
    scan_id = "pixel_dac_scan"
    
    scan_param_id_plot = []
    power_channel1 = []
    power_channel2 = []
    dac_ranges =[]


    def _configure(self, start_column=0, stop_column=120, start_row=0, stop_row=400, TDAC=None, analogue= False, **_):
        '''
        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        TDAC : int / np.ndarray
            If int: TDAC value to use for all enabled pixels, overwriting any existing mask.
            If np.ndarray: TDAC mask to use for enabled pixels. Has to have shape to match start_column:stop_column, start_row:stop_row
        '''
        
        self.chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['injection'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['tdac'][start_column:stop_column, start_row:stop_row] = True

        if TDAC is not None:
            if type(TDAC) == int:
                self.chip.masks['tdac'][start_column:stop_column, start_row:stop_row] = TDAC
            elif type(TDAC) == np.ndarray:
                self.chip.masks['tdac'][start_column:stop_column, start_row:stop_row] = TDAC[start_column:stop_column, start_row:stop_row]

        self.chip.masks.apply_disable_mask()
        self.chip.masks.update()

        if not analogue:
            self.chip.set_inj_low(1.8/2)
            self.chip.set_inj_high(0.01)



    def _dac_scan_range(self, dac_register = None, dac = None, dac_start = 0, dac_stop = 1, dac_step =1):
        # dac_value = ba2int(self.daq[dac_register][dac])
        # dac_START = dac_value -1
        # if dac_START <0:
        #     dac_START = 0
        # dac_scan_range = np.arange(dac_START - dac_start, dac_value + dac_stop, 1)
        dac_scan_range = np.arange(dac_start, dac_stop, dac_step)
        return dac_scan_range



    def _set_dac(self,load_dac_register = None, dac_register = None, dac = None, dac_value =None):
        self.daq[dac_register][dac] = dac_value
        self.chip.write_register(load_dac_register,dac_register)



    def _scan(self, n_injections=100, dacs_to_scan = None, dac_register = None, load_dac_register = None,dac_start = 0, dac_stop = 1, dac_step = 0, **_): #set proper start stop step
        """
        Dac scan: You have to provide of DACs to scan into a list. Each Dac will be scanned within +-10 around its default value
        in steps of 1. This can be changed in _dac_scan_range().
        """

        scan_param_id_plot = []

        for dac_i in dacs_to_scan:
            self.chip.logger.info(f"Start to scan dac {dac_i}")
            dac_range = self._dac_scan_range(dac_register, dac_i, dac_start, dac_stop, dac_step)
            self.dac_ranges.append(dac_range)
            pbar = tqdm(total=get_scan_loop_mask_steps(chip=self.chip) * len(dac_range), unit=' Mask steps')
            scan_param_id_plot.append(dac_i)

            for scan_param_id, dac_value_i in enumerate(dac_range):
                dac_value =int(dac_value_i)
                if not self.fifo_readout.force_stop.is_set():
                    self._set_dac(load_dac_register, dac_register, dac_i, dac_value)
                    
                    with self.readout(scan_param_id=scan_param_id, readout_interval=0.00001):
                        if self.daq["data_rx"].READY == 1:
                            with open(self.output_filename + "_DACs.txt", 'a') as file:
                                file.write(f"Dac:   {dac_i}, dac value: {dac_value_i}")
                        else:
                            continue
                            
                        # shift_and_inject(chip=self.chip, n_injections=n_injections, pbar=pbar, scan_param_id=scan_param_id)
                        # self.chip.start_inj()
                        
                else:
                    break
            else:
                self.log.success(f'Scan of dac {dac_i} finished')
        pbar.close()
        self.chip.power_down()

    
    def _analyze(self): 
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5', **self.configuration['bench']['analysis']) as a:
            a.analyze_data()

        if self.configuration['bench']['analysis']['create_pdf']:
             with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
                 p.create_standard_plots() 


if __name__ == "__main__":
   with ScanDac(scan_config = scan_configuration) as scan:
       scan.start()