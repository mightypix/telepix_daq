#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    First a global noise dependent threshold tuning is done. Start with a high threshold and lower until you have
    approx 10 hits per pixel. We aim for 1Hz/pixel noise rate -> 1kHz for all pixel -> 0.01ms readout time
    Finds the optimum global threshold value for target threshold using binary search.
'''


import numpy as np
from tqdm import tqdm
import copy
import threading
import os

from telepix_daq.analysis import analysis
from telepix_daq.system.scan_base2 import ScanBase
from telepix_daq.analysis import plotting
from telepix_daq.scans.shift_and_inject import shift_and_inject,get_scan_loop_mask_steps
import telepix_daq.analysis.analysis_utils as au
from telepix_daq.analysis import online as oa
import time
from telepix_daq.analysis.interpreter_clean import RawDataInterpreter
import matplotlib.pyplot as plt
from matplotlib import colors, cm



scan_configuration = {
  
    "start_row": 0, 
    "stop_row": 400, 
    "start_column": 0, 
    "stop_column": 120, 

    'n_injections': 100,

    'vndac': 20,
    
    "analogue": False, 

    'min_rate': 100,
    'max_hit_rate': 1000,

    # Target threshold
    'VCAL_LOW': 137,
    'VCAL_HIGH': 145,
    'VCAL_STEP': -1


}


class NoiseTuning(ScanBase):
    scan_id = 'global_threshold_tuning'


    is_parallel_scan = True     # Parallel readout of ExtTrigger-type scans

    stop_scan = threading.Event()


    def _configure(self, start_column=0, stop_column=120, start_row=0, stop_row=400, VCAL_LOW=165, VCAL_HIGH=172, VCAL_STEP = -1, analogue = False, **_):
        '''
        Parameters
        ----------
        start_column : int [0:120]
            First column to scan
        stop_column : int [0:120]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:400]
            First row to scan
        stop_row : int [0:400]
            Row to stop the scan. This row is excluded from the scan.

        VCAL_LOW : int
            Injection DAC low value.
        VCAL_HIGH : int
            Injection DAC high value.
        '''

        self.data.start_column, self.data.stop_column, self.data.start_row, self.data.stop_row = start_column, stop_column, start_row, stop_row  
        self.data.tdac_range = [0,1,2,3,4,5,6,7]
        self.data.scan_range = ((start_column, stop_column), (start_row, stop_row))

        self.chip.masks['enable'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['injection'][start_column:stop_column, start_row:stop_row] = True
        self.chip.masks['tdac'][start_column:stop_column, start_row:stop_row] = 0
        
        self.chip.masks.apply_disable_mask()
        self.chip.masks.update()

        self.interpreter = RawDataInterpreter()


        if not analogue:
            self.chip.set_inj_low(1.8/2)
            self.chip.set_inj_high(0.01)



    def _get_histogram(self,data_tuple):
        self.interpreter.reset_hist()
        
        if len(data_tuple)==0:
            hist_occ = np.zeros((self.chip.COL_SIZE, self.chip.ROW_SIZE), dtype=np.uint32)
            return hist_occ
        raw_data =  np.concatenate([i[0] for i in  data_tuple])
        hit_buffer = np.zeros(shape=len(raw_data), dtype=au.hit_dtype)
        ts_buffer = np.zeros(shape=len(raw_data), dtype=au.ts_dtype)
        trigger_buffer = np.zeros(shape=len(raw_data), dtype=au.trigger_dtype)
        hit_dat, ts_dat, trigger_data = self.interpreter.interpret(
            raw_data,
            hit_buffer,
            ts_buffer,
            trigger_buffer,
            0
        )
        hist_occ, _ = self.interpreter.get_histograms()
        return hist_occ


    def _scan(self,vndac = 0, start_column=0, stop_column=120, scan_timeout=10, max_triggers=False, min_spec_occupancy=False, fraction=0.99, VCAL_LOW=165, VCAL_HIGH=172, VCAL_STEP = -1, min_rate=10, max_hit_rate =1000,**_):
        '''
        Global threshold tuning main loop

        Parameters
        ----------
        n_injections : int
            Number of injections.
        tdac_value_bits : iterable
            Bits to toggle during tuning. Should be monotone.

        min_occupancy : int
            Minimum number of hits to count a pixel as noisy.

        '''

        ## Possibility to change vndac & vminus
        self.daq['DAC_SR']['VNDAC'] = vndac
        self.chip.write_register("LD_Dac","DAC_SR")


        thr_range = np.arange(VCAL_HIGH, VCAL_LOW, VCAL_STEP)
        pbar = tqdm(total=len(thr_range), unit=' Mask steps')

        # ### first find lowest global threshold
        scan_param_id = 0
        which_thr=0
        iteration=0
        readout_interval=0.001

        while True:
            iteration+=1
            if abs(which_thr)>= len(thr_range):
                break
            vcal_thr=thr_range[which_thr]
            self.data.vcal_thr = vcal_thr


            if not self.fifo_readout.force_stop.is_set():

                try:
                    error = False
                    self.daq['VDAC_SR']["ThPix"] = int(vcal_thr)
                    self.chip.write_register("LD_VDac","VDAC_SR")
                    self.chip.logger.info(f"Loading of threshold dac to {vcal_thr} done")

                    
                    last_time=self.fifo_readout.get_float_time()
                    with self.readout(scan_param_id=scan_param_id, readout_interval=readout_interval, fill_buffer=True, clear_buffer=True, callback=self.analyze_data_online):
                        pass
                    curr_time=self.fifo_readout.get_float_time()
                    pbar.update(1)

                    ## Read sensor error counters
                    if self.daq['data_rx'].get_lost_count():
                        error = True
                        self.daq.rx_channels[self.chip.receiver].RESET = 1

                    
                    # print(list(self.fifo_readout.data))
                    hist_occ = self._get_histogram(list(self.fifo_readout.data))
                    # data_tmp = list(self.fifo_readout.data)
                    occ_max = np.sum(hist_occ)
                    measured_readout_time=curr_time-last_time
                    # print(occ_max, measured_readout_time)
                    hit_rate = occ_max/measured_readout_time
                    self.chip.logger.info(f"Occupancy for threshold {vcal_thr} is {occ_max} with hitrate of {hit_rate} hits/s for all pixels!")
                    

                    found_noisy_pix=False
                    disable_pix = False
                    for col in range(*self.data.scan_range[0]):
                        for row in range(*self.data.scan_range[1]):
                             if hist_occ[col, row] >= min_rate*measured_readout_time and self.chip.masks.disable_mask[col, row]!= False:
                                self.chip.masks.disable_mask[col, row] = False
                                self.log.warning(f"disabled {col} {row}, iteration {iteration}")
                                found_noisy_pix=True
                                thr_last_index = which_thr
                                disable_pix = True
                                self.chip.send_reset()
            
                    which_thr+=1


                    cmap =plt.cm.plasma
                    cmap.set_bad('w')   # white for None pixels
                    cmap.set_over('r')  # Make noisy pixels red
                    cmap.set_under('g')
                    fig, axs = plt.subplots(1,1)  
                    axs.set_xlabel('Column')
                    axs.set_ylabel('Row')
                    axs.set_title(f'Occupancy map for threshold {vcal_thr} DAC and iteration: {iteration}')
                    im = axs.imshow((hist_occ[:].sum(axis=2)).T, interpolation='none',aspect='auto', cmap=cmap)  # TODO: use pcolor or pcolormesh #, aspect='equal'
                    cbar = fig.colorbar(im, ax=axs)
                    cbar.set_label('Occupancy')

                    out_dir=f"{self.output_dir}/noise_tuning"
                    if not os.path.exists(out_dir):
                        os.makedirs(out_dir)
                    plt.savefig(f"{out_dir}/{self.timestamp}_global_tuning_thr_{vcal_thr}_{iteration}.pdf")
                    plt.savefig(f"{out_dir}/{self.timestamp}_global_tuning_thr_{vcal_thr}_{iteration}.png")
                    if disable_pix:
                        self.chip.masks.apply_disable_mask()
                        self.chip.masks.update(disable_pix=disable_pix)
                    scan_param_id += 1

                    if (hit_rate >= max_hit_rate ):
                        self.log.info(f'Scan was stopped because maximal hit rate was reached with {hit_rate}Hits/s')
                        break


                except KeyboardInterrupt:  # React on keyboard interupt
                    self.stop_scan.set()
                    self.log.info('Scan was stopped due to keyboard interrupt')
                    break

            else:
                self.stop_scan.set()
                self.log.info('Scan done & stopped!')
                break  
   
        self.log.info('Reset chip')
        self.chip.send_reset()
        self.data.vcal_thr = thr_range[thr_last_index]
        self.data.vcal_thr += 1  # Increase global threshold -> To be adjusted
        self.log.success('Lowest stable global threshold value: {0}'.format(self.data.vcal_thr))
        self.daq['VDAC_SR']["ThPix"] = int(self.data.vcal_thr)
        self.chip.write_register("LD_VDac","VDAC_SR")
       

        ## second step: tune local threshold. Start with high value and go as low as possible. If noise, increase threshold
        self.log.info('Starting TDAC scan. Finding lowest TDAC threshold...')


        ### first check all noisy pixels from disable_mask and set tdac to 7 (111). If some are still noisy,
        ### we will disable them.
        noisy_pixel_mask = self.chip.masks.disable_mask[:]
        noisy_pixels = []
        retune=False
        scan_param_id = 0 ##reset to 0

        for col in range(*self.data.scan_range[0]):
            for row in range(*self.data.scan_range[1]):
                    if noisy_pixel_mask[col, row] == False and (self.chip.masks['tdac'][col, row] < 7):
                        self.chip.masks['tdac'][col, row] = 0
                        noisy_pixels.append([col,row])
                        retune=True
                    else:
                        self.chip.masks.disable_mask[col,row] = False
        if retune:
            self.chip.masks.update()

            ### check if pixels are still noisy!
            ### sometimes the readout has issues...
            # self.fifo_readout.start()
            # self.fifo_readout.stop()
            last_time=self.fifo_readout.get_float_time()
            with self.readout(scan_param_id=scan_param_id, readout_interval=readout_interval, fill_buffer=True, clear_buffer=True, callback=self.analyze_data_online):
                pass
            curr_time=self.fifo_readout.get_float_time()
            measured_readout_time=curr_time-last_time
            # hist_occ = self._get_histogram(list(self.fifo_readout.data))

            for pix_i in noisy_pixels:
                if np.max(hist_occ[pix_i[0], pix_i[1]]) >= min_rate*measured_readout_time:
                    self.chip.masks.disable_mask[pix_i[0], pix_i[1]] = False
                    ##TODO further tuning here needed!!! Check if this works and is enough
                    for trim_value in self.data.tdac_range:
                        self.log.info(f'For pixel {pix_i} trim bit is set to {trim_value}')
                        self.chip.masks['tdac'][pix_i[0], pix_i[1]] = trim_value
                        self.chip.masks.update() 
                        last_time=self.fifo_readout.get_float_time()
                        with self.readout(scan_param_id=scan_param_id, readout_interval=readout_interval, fill_buffer=True, clear_buffer=True, callback=self.analyze_data_online):
                            pass
                        curr_time=self.fifo_readout.get_float_time()
                        measured_readout_time=curr_time-last_time
                        hist_occ = self._get_histogram(list(self.fifo_readout.data))
                        cmap =plt.cm.plasma
                        cmap.set_bad('w')   # white for None pixels
                        cmap.set_over('r')  # Make noisy pixels red
                        cmap.set_under('g')
                        fig, axs = plt.subplots(1,1)  
                        axs.set_xlabel('Column')
                        axs.set_ylabel('Row')
                        # axs.set_title(f'Occupancy map for threshold {vcal_thr} DAC and iteration: {iteration}')
                        im = axs.imshow((hist_occ[:].sum(axis=2)).T, interpolation='none',aspect='auto', cmap=cmap)  # TODO: use pcolor or pcolormesh #, aspect='equal'
                        cbar = fig.colorbar(im, ax=axs)
                        cbar.set_label('Occupancy')

                        # plt.savefig(f"output_data/module_0/chip_0/noise_tuning/global_tuning_thr_{vcal_thr}_{iteration}.pdf")
                        out_dir=f"{self.output_dir}/noise_tuning"
                        if not os.path.exists(out_dir):
                            os.makedirs(out_dir)
                        plt.savefig(f"{out_dir}/{self.timestamp}_global_{pix_i[0]}_{pix_i[1]}_{trim_value}_tuning_thr_{vcal_thr}_{iteration}.pdf")
                        plt.savefig(f"{out_dir}/{self.timestamp}_global_{pix_i[0]}_{pix_i[1]}_{trim_value}_tuning_thr_{vcal_thr}_{iteration}.png")

                        self.log.info(f"Pixel {pix_i} has an occupancy of {np.max(hist_occ[pix_i[0], pix_i[1]])} at trim value {trim_value}")
                        if np.max(hist_occ[pix_i[0], pix_i[1]]) <= min_rate*measured_readout_time:
                            continue
                        else:
                            self.chip.masks.disable_mask[pix_i[0], pix_i[1]] = True
                else:
                    self.chip.masks.disable_mask[pix_i[0], pix_i[1]] = True


        pbar.close()
        self.chip.power_down()

        n_disabled_pixels = ((self.chip.masks.disable_mask[self.data.scan_range[0][0]:self.data.scan_range[0][1], self.data.scan_range[1][0]:self.data.scan_range[1][1]])).sum()
        mean_tdac = np.mean(self.chip.masks['tdac'][self.data.scan_range[0][0]:self.data.scan_range[0][1], self.data.scan_range[1][0]:self.data.scan_range[1][1]])
        self.log.success('Found optimal TDAC settings with a mean of {0:1.2f} and disabled {1} untunable pixels.'.format(mean_tdac, n_disabled_pixels))
        # self.log.success(f'Lowest possible Threshold is {vcal_thr}.')

        # self.data.hist_occ.close()  # stop analysis process
        self.stop_scan.set()
        self.log.success('Scan finished')


    # Used to overwrite data storing function: self.readout.handle_data
    def analyze_data_online(self, data_tuple):
        super(NoiseTuning, self).handle_data(data_tuple)

if __name__ == '__main__':
    with NoiseTuning(scan_config=scan_configuration) as tuning:
        tuning.configure()
        tuning.start()
