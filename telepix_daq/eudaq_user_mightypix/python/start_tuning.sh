#!/usr/bin/env sh
export QT_PLUGIN_PATH=/usr/lib/qt/plugins


export EUDAQ_DIR=/daq/eudaq
# export EUDAQ_DIR=/../../../daq/eudaq
export PYTHONPATH=${EUDAQ_DIR}/lib:${PYTHONPATH}

export LD_LIBRARY_PATH=${EUDAQ_DIR}/lib:${LD_LIBRARY_PATH}
# BINPATH=../eudaqFolder/eudaq/bin
BINPATH=../../../../daq/eudaq/bin
# BINPATH=/daq/eudaq/bin
THIS_DIR=/telepix_daq/telepix_daq/eudaq_user_mightypix/python

$BINPATH/euRun &
sleep 1
$BINPATH/euLog &
sleep 1


# IP variables: use them if Producer & RunControl computer are not the same!
# RUNCONTROLIP=192.168.22.1 ##testbeam
RUNCONTROLIP=127.0.0.1 ##lab
RPCPORT=44000
# export TLUIP=127.0.0.1
# export NIIP=127.0.0.1

echo "Check pathes in telescope.ini and in the conf-files"


## for AIDA TLU
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/cactus/lib
# start controlhub for stable AIDA TLU TCP/IP communication
/opt/cactus/bin/controlhub_start
/opt/cactus/bin/controlhub_status

## for AIDA TLU
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/cactus/lib
# start controlhub for stable AIDA TLU TCP/IP communication
# /opt/cactus/bin/controlhub_start
# /opt/cactus/bin/controlhub_status



# Start Run Control
# xterm -T "Run Control" -e '$BINPATH/euRun' &
# "$BINPATH/euRun" &
# sleep 2

# Start Logger
# xterm -T "Log Collector" -e '$BINPATH/euLog -r tcp://${RUNCONTROLIP}' &
# $BINPATH/euLog  &
# sleep 1


# Start TLU Producer
xterm -T "AidaTluProducer" -e $BINPATH/euCliProducer -n AidaTluProducer -t aida_tlu -r tcp://${RUNCONTROLIP}:${RPCPORT} & 
# xterm -T "AidaTluProducer" -e $BINPATH/euCliProducer -n AidaTluProducer -t aida_tlu &
# $BINPATH/euCliProducer -n AidaTluProducer -t aida_tlu & 
# "$BINPATH/euCliProducer -n AidaTluProducer -t aida_tlu -r tcp://${RUNCONTROLIP}:${RPCPORT}' " & 
sleep 1


## needed at testbeam
# python3 eudaqproducer_mars_ext_trimming.py -r "192.168.22.1:44000" &
## needed at lab
python3 eudaqproducer_mars_trimming.py