#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

from basil.HL.RegisterHardwareLayer import RegisterHardwareLayer
import time


class mightypix_data_rx(RegisterHardwareLayer):
    """ """

    _registers = {
        "RESET": {"descr": {"addr": 0, "size": 8, "properties": ["writeonly"]}},
        "RX_RESET": {"descr": {"addr": 1, "size": 8, "properties": ["writeonly"]}},
        "VERSION": {"descr": {"addr": 0, "size": 8, "properties": ["ro"]}},
        'LOST_ERR': {'descr': {'addr': 1, 'size': 1, 'offset': 0, 'properties': ['ro']}},
        'READ_COL2': {'descr': {'addr': 1, 'size': 1, 'offset': 1, 'properties': ['ro']}},
        'DECODER_ERR': {'descr': {'addr': 1, 'size': 1, 'offset': 2, 'properties': ['ro']}},
        'READY': {'descr': {'addr': 1, 'size': 1, 'offset': 3, 'properties': ['ro']}},
        'ENABLE': {'descr': {'addr': 1, 'size': 1, 'offset': 4}},
        'INVERT_RX': {'descr': {'addr': 1, 'size': 1, 'offset': 5, "properties": ["writeonly"]}},
        'RAW_DATA_WR': {'descr': {'addr': 1, 'size': 1, 'offset': 6}},
        'DUMP_ALL': {'descr': {'addr': 1, 'size': 1, 'offset': 7}},
        'SAMPLING_EDGE': {'descr': {'addr': 1, 'size': 1, 'offset': 6}},
        # 'STATUS': {'descr': {'addr': 1, 'size': 4, 'offset': 0, 'properties': ['ro']}},
        "ENABLE_RX": {"descr": {"addr": 2, "size": 1, "offset": 2}},
        "RESET_CNT_SYNC": {"descr": {"addr": 2, "size": 1, "offset": 0}},
        # "NOT_FILTER_COLUMN2": {"descr": {"addr": 2, "size": 1, "offset": 3}},
        'DATA_DELAY': {'descr': {'addr': 7, 'size': 5}},
        "FIFO_SIZE": {    "default": 0, "descr": {"addr": 3, "size": 16, "properties": ["ro"]}},
        "DECODER_ERROR_COUNTER": {"default": 0,"descr": {"addr": 5, "size": 8, "properties": ["ro"]}},
        # "SAMPLING_EDGE": {"descr": {"addr": 7, "size": 1, "offset": 5}},
        'READ_COL2_CNT': {'descr': {'addr': 7, 'size': 8, 'properties': ['ro']}},
        "LOST_DATA_COUNTER": {"descr": {"addr": 8, "size": 8, "properties": ["ro"]}},
        "LOAD_COLUMN2_COUNTER": {"default": 0, "descr": {"addr": 9, "size": 8, "properties": ["ro"]}},
        "LOAD_PIXEL2_COUNTER": {"default": 0, "descr": {"addr": 10, "size": 8, "properties": ["ro"]}},
        'RAW_DATA0': {'descr': {'addr': 11, 'size': 10, 'properties': ['ro']}},
        'RAW_DATA1': {'descr': {'addr': 12, 'size': 10, 'offset':2, 'properties': ['ro']}},
                

    }

    _require_version = "==1"

    def __init__(self, intf, conf):
        super(mightypix_data_rx, self).__init__(intf, conf)

    def reset(self):
        """Soft reset the module."""
        self.RESET = 0

    def set_en(self, value):
        self.ENABLE = value
        self.ENABLE_RX = value
        # print("enablr", self.ENABLE)
    
    def reset_counters(self):
        self.RESET_CNT_SYNC=1
        self.RESET_CNT_SYNC=0

    def get_reset_counters(self):
        return self.RESET_CNT_SYNC

    def get_en(self):
        return self.ENABLE

    def get_lost_count(self):
        return self.LOST_DATA_COUNTER

    def get_decoder_err_count(self):
        return self.DECODER_ERROR_COUNTER
    
    def get_read_col2_count(self):
        return self.READ_COL2_CNT

    def get_fifo_size(self):
        return self.FIFO_SIZE
