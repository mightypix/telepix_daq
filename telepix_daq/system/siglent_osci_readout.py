import pyvisa
import matplotlib.pyplot as plt
import struct
import math
import gc
import time
import numpy as np
from datetime import datetime
import os

"""Modify the following global variables according to the model"""
# SDS_RSC = 'TCPIP::192.168.218.132::inst0::INSTR'
SDS_RSC = 'TCPIP::131.220.218.132::inst0::INSTR'
CHANNEL = ["C1","C2","C3","C4"]
HORI_NUM = 10 #default 10
# vertical_divisions = 8
vertical_divisions = 8
tdiv_enum = [100e-12, 200e-12, 500e-12, 1e-9,
# tdiv_enum = [ 200e-12, 500e-12, 1e-9,
2e-9, 5e-9, 10e-9, 20e-9, 50e-9, 100e-9, 200e-9, 500e-9, \
1e-6, 2e-6, 5e-6, 10e-6, 20e-6, 50e-6, 100e-6, 200e-6, 500e-6, \
1e-3, 2e-3, 5e-3, 10e-3, 20e-3, 50e-3, 100e-3, 200e-3, 500e-3, \
1, 2, 5, 10, 20, 50, 100, 200, 500, 1000]#remove 100e-12, for slow osci and add for fast

"""The following code realizes the process of waveform reconstruction with slice"""
#establishing connection to osci and setting length of received data
_rm = pyvisa.ResourceManager() 
sds = _rm.open_resource(SDS_RSC)
sds.timeout = 30000 # default value is 2000(2s)
sds.chunk_size = 20 * 1024 * 1024 # default value is 20*1024(20k bytes)

#this function gets the settings of the osci contained in the preamble
class siglent_osci():
    def main_desc(self,recv):

    #receiving raw data from osci
        WAVE_ARRAY_1 = recv[0x3c:0x3f + 1]
        wave_array_count = recv[0x74:0x77 + 1]
        first_point = recv[0x84:0x87 + 1]
        sp = recv[0x88:0x8b + 1]
        v_scale = recv[0x9c:0x9f + 1]
        v_offset = recv[0xa0:0xa3 + 1]
        interval = recv[0xb0:0xb3 + 1]
        code_per_div = recv[0xa4:0Xa7 + 1]#
        adc_bit = recv[0xac:0Xad + 1]
        delay = recv[0xb4:0xbb + 1]
        tdiv = recv[0x144:0x145 + 1]
        probe = recv[0x148:0x14b + 1]

    #converting raw data into values
        data_bytes = struct.unpack('i', WAVE_ARRAY_1)[0]
        point_num = struct.unpack('i', wave_array_count)[0]
        fp = struct.unpack('i', first_point)[0]
        sp = struct.unpack('i', sp)[0]
        interval = struct.unpack('f', interval)[0]
        delay = struct.unpack('d', delay)[0]
        tdiv_index = struct.unpack('h', tdiv)[0]
        probe = struct.unpack('f', probe)[0]
        vdiv = struct.unpack('f', v_scale)[0] * probe
        offset = struct.unpack('f', v_offset)[0] * probe
        code = struct.unpack('f', code_per_div)[0]
        adc_bit = struct.unpack('h', adc_bit)[0]
        tdiv = tdiv_enum[tdiv_index]

        return vdiv, offset, interval, delay, tdiv, code, adc_bit

    #this function gets waveforms

    def main_wf_data(self,number_ch):
        #setting up to read waveform data
        
        sds.write(":WAVeform:STARt 0")
        sds.write("WAV:SOUR {}".format(CHANNEL[number_ch]))
        sds.write("WAV:PREamble?")
        recv_all = sds.read_raw()
        recv = recv_all[recv_all.find(b'#') + 11:]
        vdiv, ofst, interval, trdl, tdiv, vcode_per, adc_bit = self.main_desc(recv)
        points = sds.query(":ACQuire:POINts?").strip()
        points = float(sds.query(":ACQuire:POINts?").strip())
        one_piece_num = float(sds.query(":WAVeform:MAXPoint?").strip())
        if points > one_piece_num:
            sds.write(":WAVeform:POINt {}".format(one_piece_num))
        if adc_bit > 8:
            sds.write(":WAVeform:WIDTh WORD")
        read_times = math.ceil(points / one_piece_num)
        recv_all = []

        #reading raw waveform data for a single channel
        for i in range(0, read_times):
            start = i * one_piece_num
            sds.write(":WAVeform:STARt {}".format(start))
            sds.write("WAV:DATA?")
            recv_rtn = sds.read_raw().rstrip()
            block_start = recv_rtn.find(b'#')
            data_digit = int(recv_rtn[block_start + 1:block_start + 2])
            data_start = block_start + 2 + data_digit
            recv = list(recv_rtn[data_start:])
            recv_all += recv

        convert_data = []
        if adc_bit > 8:
            for i in range(0, int(len(recv_all) / 2)):
                data = recv_all[2 * i + 1] * 256 + recv_all[2 * i]
                convert_data.append(data)
        else:
            convert_data = recv_all
        volt_value = []
        for data in convert_data:
            if data > pow(2, adc_bit - 1) - 1:
                data = data - pow(2, adc_bit)
            else:
                pass
            volt_value.append(data)
        del recv, recv_all, convert_data
        gc.collect()

        #converting raw data into values with units
        time_value = []
        for idx in range(0, len(volt_value)):
            volt_value[idx] = volt_value[idx] / vcode_per * float(vdiv) *vertical_divisions/2 - float(ofst) #add *vertical_divisions/2 for slow osci !!!!!!!!! problem with original code: full measurement range is from -vcode_per to + vcode_per -> need to scale voltage per division with total number of divisions
            #volt_value[idx] = volt_value[idx] / vcode_per * float(vdiv) - float(ofst) #this for fast osci !!!!!!!!! problem with original code: full measurement range is from -vcode_per to + vcode_per -> need to scale voltage per division with total number of divisions
            time_data = -(float(tdiv) * HORI_NUM / 2) + idx * interval + float(trdl) #interval should be divided by 2? not anymore????????????? was maybe broken because of incorrect query for trigger status? trdl had wrong sign
            time_value.append(time_data)
        return time_value, volt_value, vcode_per, vdiv
    

    #sets trigger to single mode and reads all waveforms after trigger
    def meassure_single_trigger(self,filename = None, plotting = True, title = 'measurement', dac_value = ''):
        start_time = time.time()
        dt_object = datetime.fromtimestamp(start_time)
        
        if not filename:
            filename = dt_object.strftime("output_data/osci_pictures/measurement_%d-%m-%Y_%H-%M-%S")
        directory = os.path.dirname(filename)
        if not os.path.exists(directory):
            os.makedirs(directory)


        num_channels = 4
        wait_time = 0
        sds.write("TRIG:MODE SING")
        while True:
            time.sleep(0.001)
            wait_time+=1
            trigger_stat = sds.query('TRIG:STAT?')
            if trigger_stat == "Stop\n":
                print("Trigger detected! Waited {:.3f} second(s) for trigger.".format(wait_time/1000))
                break
        for i in range(num_channels):
            time_data, volt_data, vcode_per, vdiv = self.main_wf_data(i)
            # print(volt_data)
            volt_maxima = np.amax(volt_data)
            volt_minima = np.amin(volt_data)
            self.write_data(filename, time_data, volt_data, title +f" channel {i}")
            self.plot_waveform(time_data, volt_data, i)
        if plotting:
            plt.savefig(filename+".pdf")
            plt.savefig(filename+".png")
            plt.clf()
        return volt_maxima-volt_minima
    
    def meassure_single_trigger_test(self,channel=3):
        start_time = time.time()
        dt_object = datetime.fromtimestamp(start_time)
        wait_time = 0
        sds.write("TRIG:MODE SING")
        while True:
            time.sleep(0.001)
            wait_time+=1
            trigger_stat = sds.query('TRIG:STAT?')
            if trigger_stat == "Stop\n":
                print("Trigger detected! Waited {:.3f} second(s) for trigger.".format(wait_time/1000))
                break
        time_data, volt_data, vcode_per, vdiv = self.main_wf_data(channel)
        volt_maxima = np.amax(volt_data)
        volt_minima = np.amin(volt_data)
        return volt_maxima-volt_minima
    
    def meassure_single_trigger_test_2(self,channel=3):
        start_time = time.time()
        dt_object = datetime.fromtimestamp(start_time)
        wait_time = 0
        sds.write("TRIG:MODE SING")
        while True:
            time.sleep(0.001)
            wait_time+=1
            trigger_stat = sds.query('TRIG:STAT?')
            if trigger_stat == "Stop\n":
                print("Trigger detected! Waited {:.3f} second(s) for trigger.".format(wait_time/1000))
                break
        time_data, volt_data, vcode_per, vdiv = self.main_wf_data(channel)
        volt_maxima = np.amax(volt_data)
        volt_minima = np.amin(volt_data)
        return volt_data,time_data



    def plot_waveform(self,time_data, volt_data, label):
        plt.scatter(np.array(time_data), volt_data, label='Channel ' + str(label), s=.5)
        plt.grid(True)
        plt.xlabel('Time [s]')
        plt.ylabel('Voltage [V]')

    def write_data(self,filename, time_data, volt_data, title):
        # Open the file in append mode, creating it if it doesn't exist\
        with open(filename+".txt", 'a') as file:
            # Write array1 to the file
            file.write(title + '\n')
            np.savetxt(file, time_data, delimiter=' ')
            file.write('\n')
            np.savetxt(file, volt_data, delimiter=' ')
            file.write('\n\n')
    
    def write_data_dac(self,filename, dac_data, volt_peak):
        # Open the file in append mode, creating it if it doesn't exist\
        with open(filename+"max_data.txt", 'a') as file:
            # Write array1 to the file
            file.write("DAC_value/amplitude" + '\n')
            np.savetxt(file, dac_data, delimiter=' ')
            file.write('\n')
            np.savetxt(file, volt_peak, delimiter=' ')
            file.write('\n\n')


#running the measurement: setting up output file, waiting for trigger of osci, extracting waveforms of all channels, fitting functions to data, determening intersections, time over threshold and writting them to output file
# if __name__=='__main__':
#     siglent_osci.meassure_single_trigger()

if __name__=='__main__':
    oscilloscope = siglent_osci()
    oscilloscope.meassure_single_trigger()
