#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import logging
from struct import pack, unpack_from, calcsize
from array import array
from collections import OrderedDict
from time import sleep
import string

from basil.HL.HardwareLayer import HardwareLayer
from basil.HL.FEI4AdapterCard import Eeprom24Lc128


logger = logging.getLogger(__name__)


class DacDac7578(HardwareLayer):
    '''DAC 7578

    Set voltage and current (GPAC).
    '''
    DAC7578_CMD_WRITE_CH = 0x00  # write DAC input register channel
    DAC7578_CMD_UPDATE_CH = 0x10  # write DAC input register channel
    DAC7578_CMD_WRITE_UPDATE_CH = 0x30  # write and update DAC input register channel
    DAC7578_CMD_POWER = 0x40  # Power down/on DAC
    DAC7578_CMD_POWER = 0x50  # Software reset DAC

    def __init__(self, intf, conf):
        super(DacDac7578, self).__init__(intf, conf)
        self._base_addr = conf['base_addr']

    def _set_dac_value(self, address, dac_ch, value):
        msb = (value >> 4)  # MSB first
        lsb = (value << 4) & 0xff  # LSB left aligned
        # print(value, msb, lsb, dac_ch, self.DAC7578_CMD_WRITE_UPDATE_CH )
        data = array('B', pack('BBB', self.DAC7578_CMD_WRITE_UPDATE_CH | dac_ch, msb, lsb))
        self._intf.write(self._base_addr + address, data)


class I2cAnalogChannel(DacDac7578):

    def _set_dac_value(self, address, dac_ch, value):
        DacDac7578._set_dac_value(self, address=address, dac_ch=dac_ch, value=value)



class GPAC_MightyPix(I2cAnalogChannel):
    '''GPAC interface

    Note:
    - Solder 100 Ohm resistor parallel to C55
    - Replace R14 by 50 Ohm resistor (0 Ohm on some cards)
    - Close JP2 (2-3, SENSE-GND)
    - Close JP3 (2-3, REF_2.048V-VREF_1)
    - Outputs are clamped to potential of PWR0, keep potential of PWR0 close to supply voltage
    '''
    # EEPROM data V1
    HEADER_GPAC = 0xa101
    HEADER_ADDR = 0
    HEADER_FORMAT = '>H'  # Version of EEPROM data
    ID_ADDR = HEADER_ADDR + calcsize(HEADER_FORMAT)
    ID_FORMAT = '>H'  # Adapter Card ID
    CAL_DATA_CH_GPAC_FORMAT = '64sdddddddddd'
    CAL_DATA_ADDR = ID_ADDR + calcsize(ID_FORMAT)
    CAL_DATA_GPAC_FORMAT = '<' + 22 * CAL_DATA_CH_GPAC_FORMAT

    # DAC
    # DAC7578_3_ADD = 0x94 #1001 0100
    # DAC7578_3_ADD = 0x90
    DAC7578_3_ADD = 0x98
    # DAC7578_3_ADD = 1

    # Channel mappings
    _ch_map = {
        'VOUTA': {
            'DAC': {'address': DAC7578_3_ADD, 'dac_ch': 0},
        },
        'VOUTB': {
            'DAC': {'address': DAC7578_3_ADD, 'dac_ch': 1},
        },
        'VOUTC': {
            'DAC': {'address': DAC7578_3_ADD, 'dac_ch': 2},
        },

        'VOUTD': {
            'DAC': {'address': DAC7578_3_ADD, 'dac_ch': 3},
        },
        'VOUTE': {
            'DAC': {'address': DAC7578_3_ADD, 'dac_ch': 4},
        },
        'VOUTF': {
            'DAC': {'address': DAC7578_3_ADD, 'dac_ch': 5},
        },
        'VOUTG': {
            'DAC': {'address': DAC7578_3_ADD, 'dac_ch': 6},
        },
        'VOUTH': {
            'DAC': {'address': DAC7578_3_ADD, 'dac_ch': 7},
        },
    }

    def __init__(self, intf, conf):
        super(GPAC_MightyPix, self).__init__(intf, conf)
        self._base_addr = conf['base_addr']

        # Channel calibrations
        self._ch_cal = OrderedDict([
            ('VOUTA', {
                'name': '',
                'default': 0.0,
                'min': 0.0,
                'max': 1.0,
                'limit': 0.0,  # PWR only
                'Vref': float('nan'),
                'DAC': {'offset': 0.0, 'gain': 0.5}
            }),
            ('VOUTB', {
                'name': '',
                'default': 0.0,
                'min': 0.0,
                'max': 1.0,
                'limit': 0.0,  # PWR only
                'Vref': float('nan'),
                'DAC': {'offset': 0.0, 'gain': 0.5}
            }),
            ('VOUTC', {
                'name': '',
                'default': 0.0,
                'min': 0.0,
                'max': 1.0,
                'limit': 0.0,  # PWR only
                'Vref': float('nan'),
                'DAC': {'offset': 0.0, 'gain': 0.5}
            }),
            ('VOUTD', {
                'name': '',
                'default': 0.0,
                'min': 0.0,
                'max': 1.0,
                'limit': 0.0,  # PWR only
                'Vref': float('nan'),
                'DAC': {'offset': 0.0, 'gain': 0.5}
            }),
            ('VOUTE', {
                'name': '',
                'default': 0.0,
                'min': 0.0,
                'max': 1.0,
                'limit': 0.0,  # PWR only
                'Vref': float('nan'),
                'DAC': {'offset': 0.0, 'gain': 0.5}
            }),
            ('VOUTF', {
                'name': '',
                'default': 0.0,
                'min': 0.0,
                'max': 1.0,
                'limit': 0.0,  # PWR only
                'Vref': float('nan'),
                'DAC': {'offset': 0.0, 'gain': 0.5}
            }),
            ('VOUTG', { # inj low
                'name': '',
                'default': 0.0,
                'min': 0.0,
                'max': 1.0,
                'limit': 0.0,  # PWR only
                'Vref': float('nan'),
                'DAC': {'offset': 0.0, 'gain': 0.5}
            }),
            ('VOUTH', { # inj high
                'name': '',
                'default': 0.0,
                'min': 0.0,
                'max': 1.0,
                'limit': 0.0,  # PWR only
                'Vref': float('nan'),
                'DAC': {'offset': 0.0, 'gain': 0.5}
            }),
        ])

    def init(self):
        # init DACs and ADCs
        super(GPAC_MightyPix, self).init()
        self._init.setdefault('no_calibration', False)
        # read calibration
        if not self._init['no_calibration']:
            self.read_eeprom_calibration()
            logger.info('Found adapter card: {} with ID {}'.format('General Purpose Analog Card (GPAC)', self.get_id()))
        else:
            logger.info('General Purpose Analog Card (GPAC): skip reading calibration parameters from EEPROM')

  

    def _set_dac_value(self, channel, value):
        # due to small offset and gain variations value can be slightly negative
        # print(value)
        logger.debug("set_dac_value {}{}".format(value, channel))
        # print("set_dac_value {}{}".format(value, channel))
        if -2 < value < 0:
            value = 0
        # if value is greater than maximum dac value, set it to maximum and output an error message
        if value > 4095:
            value = 4095
            logger.warning('%s DAC value reached maximum value', channel)
        I2cAnalogChannel._set_dac_value(self, value=value, **self._ch_map[channel]['DAC'])

    def set_voltage(self, channel, value, unit='raw', ref_volt=1.8):
        '''Setting voltage
        '''
        # dac_offset = self._ch_cal[channel]['DAC']['offset']
        # dac_gain = self._ch_cal[channel]['DAC']['gain']
        # print("set_voltage_initial, channel, value", channel, value, unit)
        if unit == 'V':
            # value = value
            value = int((value*(2**12))/ref_volt) # conversion volts->dac
        elif unit == 'raw':
            value = value
        elif unit == 'mV':
            value = int((value*1000*(2**12))/ref_volt) # conversion volts->dac

        else:
            raise TypeError("Invalid unit type.")

        # print("set_voltage_final, channel, value", channel, value, unit)
        logger.debug("set_voltage_final, channel {}, value {}".format(channel, value))
        self._set_dac_value(channel=channel, value=value)
        # I2cAnalogChannel._set_dac_value(self, value=value, **self._ch_map[channel]['DAC'])