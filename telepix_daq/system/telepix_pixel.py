"""
This is the main class for the TelePix daq that controls the chip.
This is based on basil code from SiLab most of the credit goes to them and all the bugs are my fault!


Author: LHCb Bonn
contact: Klaas Padeken
"""
import sys
import time
import os
import bitarray
from bitarray.util import ba2int
from basil.dut import Dut
import tables as tb
import yaml

import numpy as np
import pkg_resources

from telepix_daq.system import logger

FLAVOR_COLS = {'TelePix': range(0, 120),
               }


def get_flavor(col):
    for fe, cols in FLAVOR_COLS.items():
        if col in cols:
            return fe


sys.path = [os.path.dirname(os.path.abspath(__file__))] + sys.path
OUTPUT_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), "output_data")
VERSION = pkg_resources.get_distribution("telepix_daq").version




def mk_fname(ext="data.npy", dirname=None):
    """Generate a filename in in the output folder."""
    if dirname is None:
        prefix = ext.split(".")[0]
        dirname = os.path.join(OUTPUT_DIR, prefix)
    if not os.path.exists(dirname):
        os.makedirs(dirname)
    return os.path.join(dirname, time.strftime("%Y%m%d_%H%M%S_") + ext)




class MaskObject(dict):
    def __init__(self, chip, masks, dimensions):
        self.chip = chip
        self.masks = masks
        self.dimensions = dimensions
        self.defaults = {}
        self.disable_mask = np.ones(self.dimensions, bool)
        self.to_write = np.zeros(self.dimensions, bool)
        self.og_enable_mask = np.zeros(self.dimensions, bool)
        self.og_tdac_mask = np.ones(self.dimensions, int)
        self.was = {}
        # self.disable_pix = False

        for name, props in masks.items():
            self.defaults[name] = props['default']
            dtype=np.dtype(bool)
            if type(props['default'])!=type(True):
                dtype=type(props['default'])
            self.was[name] = np.full(dimensions, props['default'],dtype)
            self[name] = np.full(dimensions, props['default'],dtype)

        self.supported_mask_patterns = ['default']

        # Initialize mask patterns to None and create pattern only on demand to save time
        self.shift_patterns = dict.fromkeys(self.supported_mask_patterns)


        super(MaskObject, self).__init__()

    def _create_shift_pattern(self, pattern):
        ''' Called when pattern is required '''
        if pattern not in self.supported_mask_patterns:
            raise NotImplementedError('Mask pattern not supported:', pattern)
        if not self.shift_patterns[pattern]:  # check if pattern is already created
            if pattern == 'double':
                self.shift_patterns['double'] = DoubleShiftPattern(self.dimensions, mask_step=2)
            elif pattern == 'default':
                self.shift_patterns['default'] = DoubleShiftPattern(self.dimensions, mask_step=4)  # default is double shift pattern

    def get_mask_steps(self, pattern='default'):
        fe_multiplier = 0
        for _, cols in self.chip.flavor_cols.items():
            fe_mask = np.zeros(self.dimensions, bool)
            fe_mask[cols[0]:cols[-1] + 1, :] = True

            if np.any(np.logical_and(fe_mask, self['enable'])):
                fe_multiplier += 1
        self._create_shift_pattern(pattern)
        return self.shift_patterns[pattern]._get_mask_steps() * fe_multiplier

    def shift(self, masks=['enable'], pattern='default', cache=False, skip_empty=True, enable_mask = True):
        '''
            This function is called from scan loops to loop over 1 FE at a time and
            over the shifting masks as defined by the mask shift pattern
        '''

        original_masks = {name: mask for name, mask in self.items()}
        # self.og_enable_mask = original_masks["enable"]
        active_pixels = []

        self._create_shift_pattern(pattern)
       

        # # Cache calculation to speed up repeated use
        # if cache and self.mask_cache:
        #     for fe, data in self.mask_cache:
        #         for d in data:
        #             self.chip.write_command(d)
        #         if fe != 'reset':
        #             yield fe, active_pixels

        #     for name, mask in original_masks.items():
        #         self[name] = mask
        #     return

        for fe, cols in self.chip.flavor_cols.items():
            fe_mask = np.zeros(self.dimensions, bool)
            fe_mask[cols[0]:cols[-1] + 1, :] = True
            if not np.any(np.logical_and(fe_mask, original_masks['enable'])):   # Only loop over frontends that have enabled columns
                continue

            # data = [self.chip.enable_core_col_clock(range(int(cols[0] / 8), int((cols[-1] - 1) / 8 + 1)), write=True)]   # Enable only one frontend at a time
            data = []

            self.shift_patterns[pattern].reset()
            for pat in self.shift_patterns[pattern]:
                if isinstance(pat, np.ndarray):  # If DoubleShiftPattern or ClassicShiftPattern is used
                    for mask in masks:
                        self[mask] = np.logical_and(np.logical_and(original_masks[mask], pat), fe_mask)

                    if not np.any(self['enable'][:]) and skip_empty:   # Skip empty steps for speedup
                        # if cache:
                        #     self.mask_cache.append(('skipped', []))
                        yield 'skipped', active_pixels
                        continue
                else:  # If CrosstalkShiftPattern is used
                    for name, mask in pat.items():
                        self[name] = np.logical_and(np.logical_and(original_masks[name], mask), fe_mask)
                #This is what writes the changes:
                update_return = self.update()
                if not update_return:
                    active_pixels = np.where(self[mask][0:self.dimensions[0], 0:self.dimensions[1]])
                    # print("no new injection update")
                    yield fe, active_pixels
                    # continue
                    # break
                # if cache:
                #     self.mask_cache.append((fe, data))
                #     data = []
                active_pixels = np.where(self[mask][0:self.dimensions[0], 0:self.dimensions[1]])
                yield fe, active_pixels

        for name, mask in original_masks.items():
            self[name] = mask
        # self.mask_cache.append(('reset', self.update()))

    def reset_all(self):
        for name, _ in self.items():
            self[name] = np.full(self.dimensions, self.defaults[name])
        self.apply_disable_mask()

    def apply_disable_mask(self):
        ### output of this mask are enabled pixels = True & disabled = False!
        self['enable'] = np.logical_and(self['enable'], self.disable_mask)

    def _find_changes(self):
        '''
            Find out which values have changed in any mask compared to last update()
            TODO: implement a smarter shift for enabled sensors
        '''
        return_update_pix=False
        return_update_inj=False
        return_update_tdac=False
        self.pix_to_write = np.zeros(self.dimensions, bool)
        self.inj_to_write = np.zeros(self.dimensions, bool)
        self.tdac_to_write = np.ones(self.dimensions, int)

        for name, mask in self.items():
            if 'injection' == name:
                self.inj_to_write = np.logical_and(self['enable'] ,mask[:])
                if not np.array_equal(self.inj_to_write , self.was[name]):
                    return_update_inj=True
            elif 'enable' == name:
                self.pix_to_write = np.logical_or(self.pix_to_write, np.not_equal(mask, self.was[name]))
                # print(self.pix_to_write)
                # print("find changes stop")
                # self.pix_to_write = self.og_enable_mask
                # if not np.array_equal(self.pix_to_write, self.was[name]):
                # differences = np.where(mask != self.was[name])
                # for index in zip(*differences):
                    # print(f"Element at index {index} is different: mask[{index}] = {mask[index]}, self.was[name][{index}] = {self.was[name][index]}")
                if not np.array_equal(mask, self.was[name]):
                    # print("update enable")
                    return_update_pix=True
            elif "tdac" == name:
                self.tdac_to_write = np.not_equal(self.tdac_to_write, mask)
                self.tdac_to_write = mask[:]
                if not np.array_equal(self.tdac_to_write, self.was[name]):
                    return_update_tdac=True

            else:
                pass

        return return_update_pix, return_update_inj, return_update_tdac
        # return self.pix_to_write, self.inj_to_write
    
        # print("changes", self.pix_to_write)
    # def get_pixel_data(self, col, row):
    #     tdac = str(bin(self['tdac'][col, row]))[2:].zfill(3)

    #     return '0' + tdac if self['enable'][col, row] else '0000'

    # def get_pixel_portal_data(self, colgroup, row):
    #     return int(
    #         '0b'
    #         + self.get_pixel_data(colgroup * 4 + 3, row)
    #         + self.get_pixel_data(colgroup * 4 + 2, row)
    #         + self.get_pixel_data(colgroup * 4 + 1, row)
    #         + self.get_pixel_data(colgroup * 4, row), 2
    #     )

    # def get_column_group_data(self, mask, colgroup):
    #     inj = np.logical_or.reduce(self[mask], axis=1)[colgroup * 16: (colgroup + 1) * 16]
    #     return np.packbits(inj, bitorder='little').view(np.uint16)[0]

    # def get_row_group_data(self, mask, rowgroup):
    #     inj = np.logical_or.reduce(self[mask], axis=0)[rowgroup * 16: (rowgroup + 1) * 16]
    #     return np.packbits(inj, bitorder='little').view(np.uint16)[0]

    def update(self, force=False, disable_pix = False):

        ''' Write the actual pixel register configuration

            Only write changes or the complete matrix
        '''

        if force and self.chip.daq.board_version == 'SIMULATION':
            return []

        update_pix, update_inj, update_tdac =self._find_changes()

        if force:
            inj_write_mask = np.ones(self.dimensions, bool)
            pix_write_mask = np.ones(self.dimensions, bool)
            tdac_write_mask = np.ones(self.dimensions, int)
        else:   # Find out which pixels need to be updated
            inj_write_mask = self.inj_to_write
            pix_write_mask = self.pix_to_write
            tdac_write_mask = self.tdac_to_write
        inj_to_write = np.column_stack((np.where(inj_write_mask)))
        pix_to_write = np.column_stack((np.where((pix_write_mask))))
        tdac_to_write = tdac_write_mask
        # print("_______")
        # tdac_to_write = np.column_stack((np.where((tdac_write_mask))))
        
        if update_pix or force:
            if disable_pix:
                self.chip.set_pix_colum(pix_to_write)
            else:
                self.chip.init_pix(pix_to_write, disable =disable_pix)
                self.chip.init_tdac()
        return_value=True
        if update_inj:
            if len(inj_to_write)>0:
                self.chip.set_en_inj(inj_to_write)
            else:
                return_value=False
        else:
            return_value=False
        if update_tdac:
            self.chip.set_tdac(tdac_to_write)
        
        # Set this mask as last mask to be able to find changes in next update()
        for name, mask in self.items():
            if "enable" in name:
                # self.was[name][:] = self.og_enable_mask
                self.was[name][:] = mask[:]
            else:
                self.was[name][:] = mask[:]
        return return_value


class ShiftPatternBase(object):
    '''
        Base class for shift patterns
    '''

    def __init__(self, dimensions, mask_step):
        self.dimensions = dimensions
        self.mask_step = mask_step
        self.current_step = -1
        self.base_mask = self.make_first_mask()

    def __iter__(self):
        return self

    def reset(self):
        self.current_step = -1

    def _get_mask_steps(self):
        return self.dimensions[0] * self.mask_step

    def make_first_mask(self):
        raise NotImplementedError('You have to define the initial mask')

    def make_mask_for_step(self, step):
        raise NotImplementedError('You have to define the mask at a given step')

    def __next__(self):
        if self.current_step >= (self.dimensions[1] * self.mask_step) - 1:
            raise StopIteration
        else:
            self.current_step += 1
            # print(self.current_step)
            return self.make_mask_for_step(self.current_step)


class DoubleShiftPattern(ShiftPatternBase):
    '''
        Enables pixels along one column with specified distance (mask_step)
    '''

    def make_first_mask(self):
        mask = np.zeros(self.dimensions, bool)

        for col in range(0, self.dimensions[0], self.mask_step):
            # self.chip.set_pix()
            mask[col, 0] = True
        # print("first mask")
        # print(mask[0])
        # print(mask[1])
        # print(mask[2])
        return mask

    def make_mask_for_step(self, step):
        # test = np.roll(np.roll(self.base_mask, step // self.dimensions[1], 0), step % self.dimensions[1], 1)
        # print(test[0])
        # print(test[1])
        # print(test[2])
        return np.roll(np.roll(self.base_mask, step // self.dimensions[1], 0), step % self.dimensions[1], 1)


class TelePix(Dut):
    """
    This is the main class for the control of the Telepix.
    The default __init__() will load all necessary software inits, but the HW will be initialized with the init() method.
    """

    flavor_cols = FLAVOR_COLS

    def __init__(self, daq, conf=None, chip_sn='W00R00', receiver="rx0", **kwargs):
        """Initialize the software components of the program."""

        self.proj_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


        if conf is None or len(conf) == 0:
            self.log.warning("No explicit configuration supplied. Using 'default.cfg.yaml'!")
            with open(os.path.join(os.path.dirname(__file__), 'default.cfg.yaml'), 'r') as f:
                self.conf = yaml.full_load(f)
        elif isinstance(conf, dict):
            self.conf = conf
        elif isinstance(conf, str) and os.path.isfile(conf):
            with open(conf) as f:
                self.conf = yaml.full_load(f)
        else:
            raise TypeError('Supplied config has unknown format!')


        self.daq = daq
        self.chip_sn = chip_sn
        self.receiver = receiver


        # Initialize logger.
        self.logger = logger.setup_derived_logger('TelePix - Chip' + chip_sn)
        super(TelePix, self).__init__(conf=self.conf)
        self.COL_SIZE = 120
        self.ROW_SIZE = 400

        self.mask = self._create_mask("none")
        masks = {'enable': {'default': False},
            'injection': {'default': False},
            'tdac': {'default': 0b000}
        }
        self.masks = MaskObject(self, masks, (self.COL_SIZE, self.ROW_SIZE))
        self.injection = self._create_mask("none")
        self.trim = np.zeros([self.COL_SIZE, self.ROW_SIZE], np.uint8)
        self.isInitialized = False
        self.debug = 1
        self.conf_buf = []
        
        self.ncols = self.COL_SIZE
        self.nrows = self.ROW_SIZE
        self.inj_device = "GPAC_MightyPix"
        self.gpio_expander = "GPIO_Expander"
        
        self.PixelConf = {'en_pix': np.zeros([self.ncols, self.nrows], dtype='u1') * 0xFF,
                            'tdac0': np.ones([self.ncols, self.nrows], dtype=np.uint8) * 0xFF
                            }

        self.SET_VALUE = {}

        
    def init(self, dut=None, no_power_reset=True):
        """Initialize the HW part of the chip."""

        for reg in self.daq._conf["registers"]:
            # if reg["name"] in ["INJ_HIGH", "INJ_LOW"] and "init" in reg:
            if reg["name"] in ["INJ_HI", "INJ_LO"] and "init" in reg:
                self.daq.logger.info("modify %s: %s" % (reg["name"], str(reg["init"])))
                self.daq[reg["name"]]._ch_cal[reg["arg_add"]["channel"]].update(reg["init"])
        
        
        ## for TLU sync pulser
        self.daq["CONF"]["ENABLE_RST_TLU"]=1
        self.daq["CONF"].write()
        # self.daq["pulse_tlu_sync"]["WIDTH"]=100
        self.daq["pulse_tlu_sync"]["WIDTH"]=1000000
        self.daq["pulse_tlu_sync"]["REPEAT"]=1
        self.daq["pulse_tlu_sync"]["DELAY"]=1
        self.daq["pulse_tlu_sync"].set_en(True) #note this should work with the external trigger!
        ######
        
        
        self.send_reset()
        self.daq["inj"].reset()

        


        self.daq['CONF_SR'].set_size(self.daq["CONF_SR"]._conf['size'])
        self.daq['DAC_SR'].set_size(self.daq["DAC_SR"]._conf['size'])
        self.daq['VDAC_SR'].set_size(self.daq["VDAC_SR"]._conf['size'])
        self.daq['PixelConf_row'].set_size(self.daq["PixelConf_row"]._conf['size'])
        self.daq['PixelConf_tdac'].set_size(self.daq["PixelConf_tdac"]._conf['size'])

        self.power_up()
        time.sleep(0.01)
        self.set_conf(init=True)
        self.isInitialized = True
        self.send_chip_reset()
        # self.daq['data_rx']["DUMP_ALL"] = 1


    def sr_ready(self, sr):
        while not self.daq[sr].is_ready:
            pass


    def write_register(self, LD, register):
        self.set_sr_on(sr = LD)
        self.daq[register].set_size(self.daq[register]._conf['size'])
        self.daq[register].write()
        self.sr_ready(register)
        self.set_sr_off(sr = LD)


    def get_sn(self):
        return self.chip_sn
    def mask_rx(self, mask=True):
        self.daq['data_rx'].set_en(not mask)
        if not mask:
            trash = self.daq['fifo'].FIFO_SIZE
            if trash != 0:
                self.daq['fifo'].get_data()
                self.logger.warn('mask_rx: discard trash ({0} {1})'.format(trash, self.daq['fifo'].FIFO_SIZE))



    def send_reset(self):
        """Send a reset to the FPGA (not the chip)."""
        self.daq["CONF"]["RESET"] = 1
        self.daq["CONF"].write()
        self.daq["CONF"]["RESET"] = 0
        self.daq["CONF"].write()
        self.send_chip_reset()

    def send_chip_reset(self):
        # self.daq["CONF"].write()
        self.daq["CONF"]["RESET_CHIP"] = 0
        self.daq["CONF"].write()
        self.daq["CONF"]["RESET_CHIP"] = 1
        self.daq["CONF"].write()
        self.send_sync()
        self.daq['data_rx'].reset_counters()

    def send_sync(self):
        self.daq["CONF"]["SYNC"] = 0
        self.daq["CONF"].write()
        self.daq["CONF"]["SYNC"] = 1
        self.daq["CONF"].write()


    ### TP has 5 register with different loads. They are controlled by an I2C 
    ### The communication with the IC is serial and done via firmware similar to SPI 
    ### channel info:
    ### 0: toggle for clks: switching channel 0 on and off -> 0: CCkx high & 1: Ckx high (CCk only for config register needed)
    ### 1: C_LD -> hopefully config register load
    ### 2: Row
    ### 3: TDAC
    ### 4: DAC
    ### 5: VDAC
    ### 6: C_Rb 
    ### 7: Output for TDC Shift register: CSOUT

    ### This selects the correct clock
    def set_sr_on(self, sr = None):
        if isinstance(self.gpio_expander, str) and self.gpio_expander == "GPIO_Expander":
            # This decides if pin 0 should be on or not
            self.daq[sr].set_clk(channel = sr, value = 0x1)

    ### This sets the right load signal and turns it off
    def set_sr_off(self, sr = None):
        if isinstance(self.gpio_expander, str) and self.gpio_expander == "GPIO_Expander":
            self.daq[sr].set_bit(channel = sr, value = 0x1)
            self.daq[sr].set_clk(channel = sr, value = 0X0)
            

    ### this programs the shift register: 
    ## take care of DAC multilpexing because of external dacs
    ## order: voltage, config, dacs, row, tdac
    def set_conf(self, init=False, write=True, **kwargs):
        self.conf_buf.append('set_conf:')
        if init:

            self.daq['VDAC_SR'].set_size(self.daq["VDAC_SR"]._conf['size'])
            self.daq['VDAC_SR'].set_configuration(self.daq['VDAC_SR']._init)
            self.vdac_default()
            self.set_sr_on(sr = "LD_VDac")
            self.daq['VDAC_SR'].write()
            self.sr_ready('VDAC_SR')
            if self.daq['VDAC_SR'].is_done():
                self.logger.info(f"Loading of default voltage dacs is done!")
            self.set_sr_off(sr = "LD_VDac")
            # time.sleep(0.01)
            # input()

            self.daq['DAC_SR'].set_size(self.daq["DAC_SR"]._conf['size'])
            self.daq['DAC_SR'].set_configuration(self.daq['DAC_SR']._init)
            self.set_sr_on(sr = "LD_Dac")
            self.daq['DAC_SR'].write()
            self.sr_ready('DAC_SR')
            self.set_sr_off(sr = "LD_Dac")
            # time.sleep(0.01)
        
            self.daq['CONF_SR'].set_size(self.daq["CONF_SR"]._conf['size'])
            self.daq['CONF_SR'].set_configuration(self.daq['CONF_SR']._init)
            self.daq['CONF_SR']['InjEn'] = 1  
            self.set_sr_on(sr = "LD_Config")
            self.daq['CONF_SR'].write()
            self.sr_ready('CONF_SR')
            self.set_sr_off(sr = "LD_Config")
            # time.sleep(0.01)
            
        for k in kwargs:
            self.daq['VDAC_SR'][k] = kwargs[k]
            self.daq['CONF_SR'][k] = kwargs[k]
            self.daq['DAC_SR'][k] = kwargs[k]
            self.daq['PixelConf_row'][k] = kwargs[k]
            self.conf_buf[-1] = self.conf_buf[-1] + '{}={}'.format(k, kwargs[k])
          
    
    def set_en_ampout(self, cols, write=True):
        self.daq['PixelConf_row'].set_size(self.daq["PixelConf_row"]._conf['size'])
        # self.daq['PixelConf_row'].set_configuration(self.daq['PixelConf_row']._init)

        if isinstance(cols, int):
            cols = [cols]
        elif isinstance(cols, str): 
            if cols == 'none':
                cols = []
            else:   # if 'all', enable all
                cols = range(self.ncols)
    
        for c in range(self.ncols):
            if c in cols:
                self.daq['PixelConf_row']["PixelConf_row"][self.ncols -1 - c]['en_ampout_col'] = 1
            else:
                self.daq['PixelConf_row']["PixelConf_row"][self.ncols -1 - c]['en_ampout_col'] = 0

        self.set_sr_on(sr = "LD_Row")
        self.daq['PixelConf_row'].write()
        self.sr_ready("PixelConf_row")

        self.set_sr_off(sr = "LD_Row")

        # log
        en_ampout_col = 0
        for c in range(self.ncols):
            en_ampout_col = (en_ampout_col << 1) | self.daq['PixelConf_row']["PixelConf_row"][c]['en_ampout_col'].tovalue()
        # self.logger.info('set_en_ampout: EnAO=0x{0:032x}'.format(en_ampout_col))
   

    def set_en_hitbus(self, cols, write=True):
        if isinstance(cols, int):
            cols = [cols]
        elif isinstance(cols, str):
            if cols == 'none':
                cols = []
            else:   # if 'all', enable all
                cols = range(self.ncols)
                
        for col in range(self.ncols):
            if col in cols:
                self.daq['PixelConf_row']["PixelConf_row"][self.ncols -col -1]['en_hitbus_col'] = 1
            else:
                self.daq['PixelConf_row']["PixelConf_row"][self.ncols - col -1]['en_hitbus_col'] = 0


        self.daq['PixelConf_row'].set_size(self.daq["PixelConf_row"]._conf['size'])
        self.set_sr_on(sr = "LD_Row")
        self.daq['PixelConf_row'].write()
        self.sr_ready("PixelConf_row")
        self.set_sr_off(sr = "LD_Row")

        # log
        en_hitbus_col = 0
        for c in range(self.ncols):
            en_hitbus_col = (en_hitbus_col << 1) | self.daq['PixelConf_row']["PixelConf_row"][c]['en_hitbus_col'].tovalue()
        # self.logger.info('set_en_hitbus: EnHB=0x{0:032x}'.format(en_hitbus_col))
        
     

    def set_ts_inj(self, en=True, reset=False):
        if en:
            self.daq['ts_external'].RESET = reset
        self.daq['ts_external'].ENABLE_RISING = en


    def enable_mask(self, col=0, row=0):
        """
        Set the disable mask.

        This will sent the mask for pixel col, row.
        This will disable this pixel (sorry for the name).
        It mainly tries to avoid x,y confusion with the numpy array.
        """
        self.mask[col, row] = 1
            


    def set_en_inj(self,pix = "none",cols=None, rows=None, write=True):
        if rows is None and cols is None:
            # print(pix)
            if pix is None:
                pix = np.zeros([2, 0], dtype=int)
            elif isinstance(pix, str):  # if none
                pix = np.zeros([2, 0], dtype=int)
            elif len(pix) == 0:
                pix = np.zeros([2, 0], dtype=int)
            elif isinstance(pix[0], int):
                pix = [pix]
            
            if type(pix) == np.ndarray:
                # print("pixel all", pix)
                sorted_pairs = sorted(pix, key=lambda x: x[1])
                cols = [pair[0] for pair in sorted_pairs]
                rows = [pair[1] for pair in sorted_pairs]
                # pix = np.array(pix)
                # cols = np.unique(pix[0, :])
                # rows = np.unique(pix[1, :])
            
            else:
                pix = np.array(pix)
                cols = np.unique(pix[0])
                rows = np.unique(pix[1])
        # print(pix, cols, rows)
        if cols is not None:        
            for c in range(self.ncols):
                if c in cols:
                    self.daq['PixelConf_row']["PixelConf_row"][self.ncols -1 - c ]['en_injection_col'] = 1
                else:
                    self.daq['PixelConf_row']["PixelConf_row"][self.ncols -1- c]['en_injection_col'] = 0
        if rows is not None:
            for r in range(self.nrows):
                div_row, mod_row = divmod(r, 4)
                if r in rows:
                    self.daq['PixelConf_row']["PixelConf_row"][self.ncols -div_row -1]['en_injection_row'][mod_row] = "1"
                else:
                    self.daq['PixelConf_row']["PixelConf_row"][self.ncols - div_row -1]['en_injection_row'][mod_row] = "0"

            # self.logger.info('set_inj: row={0} en_inj=0x{1}'.format(
            #     r, self.daq['PixelConf_row']['PixelConf_row'][:]['en_injection_row']))
    
        ## enable pixel for injection
        self.daq['PixelConf_row'].set_size(self.daq["PixelConf_row"]._conf['size'])
        self.set_sr_on(sr = "LD_Row")
        self.daq['PixelConf_row'].write()
        self.sr_ready("PixelConf_row")
        self.set_sr_off(sr = "LD_Row")
        for c in range(self.ncols):
            self.daq['PixelConf_row']["PixelConf_row"][c]['en_injection_col'] = "0"
            for r in range(4):
                self.daq['PixelConf_row']["PixelConf_row"][c]['en_injection_row'][r] = "0"

        self.daq['CONF_SR'].set_size(self.daq["CONF_SR"]._conf['size'])
        self.daq['CONF_SR'].set_configuration(self.daq['CONF_SR']._init)
        # write to the EnInj switch in CONF
        if cols is None or len(cols) == 0:
            self.daq['CONF_SR']['InjEn'] = 0
        elif rows is None or len(rows) == 0:
            self.daq['CONF_SR']['InjEn'] = 0
        else:
            self.daq['CONF_SR']['InjEn'] = 1
        self.set_sr_on(sr = "LD_Config")
        self.daq['CONF_SR'].write()
        self.sr_ready('CONF_SR')
        self.set_sr_off(sr = "LD_Config")


  
    
    def get_en_inj(self):
        cols = []
        rows = []
        for col in range(self.ncols):
            if self.daq['PixelConf_row']["PixelConf_row"][self.ncols -1 -col]['en_injection_col'].tovalue() == 1:
                cols.append(col)
        for row in range(self.nrows):
            row_pac = (row // 4)
            row_pac_i = (row % 4)
            if self.daq['PixelConf_row']["PixelConf_row"][self.ncols -1 -row_pac]['en_injection_row'].tovalue() == 1:
                rows.append(row_pac)
        return cols, rows
    

    def set_pix_colum(self, pix):
        # return
        self.logger.info(f"Disabling pixels {pix}!")
        if isinstance(pix, str):
            if pix == 'all':
                pix = np.ones([self.ncols, self.nrows], dtype='u1')
            elif pix == 'none':
                pix = np.zeros([self.ncols, self.nrows], dtype='u1')
        elif isinstance(pix,np.ndarray):
            if len(pix[0])==2:
                pix_temp = np.zeros([self.ncols, self.nrows], dtype='u1')
                for i,j in pix:
                    pix_temp[i,j]=1
                pix=pix_temp
        elif len(pix[0])!=chip.nrows:
            pix_temp = np.zeros([self.ncols, self.nrows], dtype='u1')
            for i,j in pix:
                pix_temp[i,j]=1
            pix=pix_temp

        disable_mask_=self.masks.disable_mask
        rows = np.unique(np.argwhere(disable_mask_ == False)[:, 1])
        columns = np.unique(np.argwhere(disable_mask_ == False)[:, 0])
        tmp_InjEn = self.daq['CONF_SR']['InjEn'].tovalue()

        self.daq['CONF_SR']['RAMWrEnable'] = 1
        self.daq['CONF_SR']['InjEn'] = 0
        self.daq['CONF_SR']['PCH'] = 0
        self.write_register("LD_Config","CONF_SR")

        # save Inj rows
        tmp_EnRow = []
        for c in range(self.ncols):
            tmp_EnRow.append(self.daq['PixelConf_row']['PixelConf_row'][self.ncols -1 -c]['en_injection_row'].tovalue())


        ## mask all pixels
        self.daq['CONF_SR']['WrRAM_Comp'] =  1
        self.daq['CONF_SR']['WrRAM_TDAC'] = "111"
        self.write_register("LD_Config","CONF_SR")
        # print(rows)
        # print(columns)

        ## write pixel & TDAC register
        for r in range(self.nrows):
            for c in range(self.ncols):
                self.daq['PixelConf_row']['PixelConf_row'][self.ncols -1 - c]['en_injection_row'] = 0
            div_row, mod_row = divmod(r, 4)
            self.daq['PixelConf_row']['PixelConf_row'][self.ncols -1 - div_row]['en_injection_row'][mod_row] = '0' if (r in rows) else "1"
            # self.daq['PixelConf_row']['PixelConf_row'][self.ncols -1 - div_row]['en_injection_row'][mod_row] = '1' 
            for col in range(self.ncols):
                self.daq['PixelConf_tdac']['TDAC'][col]="1" if (col in columns) else "0"
            
            self.PixelConf['en_pix'][:, r] = pix[:, r]

            self.write_register("LD_Row","PixelConf_row")
            self.write_register("LD_TDac","PixelConf_tdac")

        # # restore En Inj
        self.daq['CONF_SR']['InjEn'] = tmp_InjEn
        self.daq['CONF_SR']['RAMWrEnable'] = 0
        self.write_register("LD_Config","CONF_SR")

        self.daq['CONF_SR']['RAMWrEnable'] = 0
        self.write_register("LD_Config","CONF_SR")


        # restore EnRow
        for c in range(self.ncols):
            self['PixelConf_row']['PixelConf_row'][self.ncols - c - 1]['en_injection_row'] = tmp_EnRow[c]
        self.write_register("LD_Row","PixelConf_row")
        self.logger.info("Done disabling pixels!")




    def init_pix(self, pix, disable=False):
        """
        Write the same TDAC and enable for pixels.
        """
        self.logger.info("Start initializing pixels. This will take some time!")
        if isinstance(pix, str):
            if pix == 'all':
                pix = np.ones([self.ncols, self.nrows], dtype='u1')
            elif pix == 'none':
                pix = np.zeros([self.ncols, self.nrows], dtype='u1')
        elif isinstance(pix,np.ndarray):
            if len(pix[0])==2:
                pix_temp = np.zeros([self.ncols, self.nrows], dtype='u1')
                for i,j in pix:
                    pix_temp[i,j]=1
                pix=pix_temp
        elif len(pix[0])!=chip.nrows:
            pix_temp = np.zeros([self.ncols, self.nrows], dtype='u1')
            for i,j in pix:
                pix_temp[i,j]=1
            pix=pix_temp
        enable_mask=self.masks["enable"]
        rows = np.unique(np.argwhere(pix & enable_mask)[:, 1])
        columns = np.unique(np.argwhere(pix & enable_mask)[:, 0])

        disable_mask=np.invert(enable_mask)
        pixels=np.where(enable_mask==0)
        pixels =[ (row,col)  for row,col in zip(pixels[0],pixels[1])]
    
        self.logger.debug(f"pixels that will be disabled: {pixels}")

        tmp_InjEn = self.daq['CONF_SR']['InjEn'].tovalue()
        tmp_EnRow = []
        for c in range(self.ncols):
            tmp_EnRow.append(self.daq['PixelConf_row']['PixelConf_row'][self.ncols - c - 1]['en_injection_row'].tovalue())

        self.daq['CONF_SR']['RAMWrEnable'] = 0
        self.daq['CONF_SR']['InjEn'] = 0
        self.daq['CONF_SR']['PCH'] = 0
        self.daq['CONF_SR']['WrRAM_Comp'] =  0
        # self.daq['CONF_SR']['WrRAM_TDAC'] = "111"
        # self.daq['CONF_SR']['WrRAM_TDAC'] = "111"
        self.write_register("LD_Config","CONF_SR")

        self.daq['CONF_SR']['RAMWrEnable'] = 1
        self.daq['CONF_SR']['WrRAM_Comp'] =  1
        # self.daq['CONF_SR']['WrRAM_TDAC'] = "111"
        self.write_register("LD_Config","CONF_SR")

        ## write pixel & TDAC register
        for r in range(self.nrows):
            for c in range(self.ncols):
                self.daq['PixelConf_row']['PixelConf_row'][self.ncols -1 - c]['en_injection_row'] = 0
            div_row, mod_row = divmod(r, 4)
            self.daq['PixelConf_row']['PixelConf_row'][self.ncols -1 - div_row]['en_injection_row'][mod_row] = '1' 
            self.write_register("LD_Row","PixelConf_row")
            for col in range(self.ncols):
                self.daq['PixelConf_tdac']['TDAC'][col]="0" if (col in columns) else "1"
                # self.daq['PixelConf_tdac']['TDAC'][col]="1" if disable_mask[col][r] else "0"
            self.write_register("LD_TDac","PixelConf_tdac")
            # self.write_register("LD_Row","PixelConf_row")

        self.daq['CONF_SR']['RAMWrEnable'] = 0
        self.daq['CONF_SR']['WrRAM_Comp'] =  0
        self.write_register("LD_Config","CONF_SR")

        self.logger.info("Done initializing pixels!")


        # if len(pixels) != 0:
        #     self.logger.info("Start disabling single pixels!")
        #     # # reduced_pixels=[]
        #     # for row,col in pixels:
        #     #     if row not in reduced_pixels[:][0]:
        #     #         reduced_pixels.append((row,col))
        #     # self.logger.info("try to reduce pixels: ",reduced_pixels)
        #     self.daq['CONF_SR']['RAMWrEnable'] = 1
        #     self.daq['CONF_SR']['WrRAM_Comp'] =  1
        #     self.write_register("LD_Config","CONF_SR")
                
        #     for row in range(self.nrows):
        #         for c in range(self.ncols):
        #             self.daq['PixelConf_row']['PixelConf_row'][self.ncols -1 - c]['en_injection_row'] = 0
        #         div_row, mod_row = divmod(row, 4)
        #         self.daq['PixelConf_row']['PixelConf_row'][self.ncols -1 - div_row]['en_injection_row'][mod_row] = '1' 
        #         self.write_register("LD_Row","PixelConf_row")
        #         for c in range(self.ncols):
        #             self.daq['PixelConf_tdac']['TDAC'][c]="1" if disable_mask[c][row] else "0"
        #         self.write_register("LD_TDac","PixelConf_tdac")
        #     self.daq['CONF_SR']['RAMWrEnable'] = 0
        #     self.daq['CONF_SR']['WrRAM_Comp'] =  0
        #     self.write_register("LD_Config","CONF_SR")

        #     self.logger.info("Done disbaling single pixels!")


        # # restore En Inj
        self.daq['CONF_SR']['InjEn'] = tmp_InjEn
        self.write_register("LD_Config","CONF_SR")


    def init_tdac(self, tdac_init = 0):
        # enable Inj, enable RAMWr
        self.logger.info(f"Init TDACs to {tdac_init}!") 
        self.daq['CONF_SR']['RAMWrEnable'] = 1
        self.write_register("LD_Config","CONF_SR")

        for i, ival in enumerate([1, 2, 4]):
            for c in range(self.ncols):
                self.daq['PixelConf_tdac']['TDAC'][c] = tdac_init
            self.write_register("LD_TDac","PixelConf_tdac")
            for c in range(self.ncols):
                self.daq['PixelConf_row']['PixelConf_row'][self.ncols -1 - c]['en_injection_row']= '1111' 
            self.write_register("LD_Row","PixelConf_row")        
            self.daq['CONF_SR']['WrRAM_TDAC'] = 1 << i
            # self.daq['CONF_SR']['WrRAM_TDAC'] = 0x1
            self.write_register("LD_Config","CONF_SR")
            self.daq['CONF_SR']['WrRAM_TDAC'] = 0
            self.write_register("LD_Config","CONF_SR")
        self.daq['CONF_SR']['RAMWrEnable'] = 0
        self.write_register("LD_Config","CONF_SR")


    def set_tdac(self, tdac):
        # return
        self.logger.info("Start writing tune bits. This will take some time!") 
      
        if isinstance(tdac, int) or isinstance(tdac, np.int64):
            # tdac = np.ones(self.PixelConf['tdac0'].shape, dtype=np.uint8) * tdac
            tdac = np.ones([self.ncols, self.nrows], dtype='u1') * tdac 

        enable_mask=self.masks["enable"]
        tdac_mask=self.masks["tdac"]
        columns = np.argwhere(tdac_mask)[:, 0]
        rows = np.argwhere(tdac_mask)[:, 1]

        # Enable RAMWr
        self.daq['CONF_SR']['RAMWrEnable'] = 1
        self.write_register("LD_Config", "CONF_SR")

        # # Initialize TDAC to 0
        # for c in range(self.ncols):
        #     self.daq['PixelConf_tdac']['TDAC'][c] = 0
        # self.write_register("LD_TDac", "PixelConf_tdac")

        # Iterate through the bits of the tdac value (binary representation)
        for bit_pos in range(0, 3, 1):  # assuming 3 bits are required 
            for r in rows:
                for c in range(self.ncols):
                    reversed_tdac = int('{:03b}'.format(tdac[c, r])[::-1], 2)
                    bit_value = (reversed_tdac >> bit_pos) & 1
                    self.daq['PixelConf_tdac']['TDAC'][c] = bit_value
                    # print(r, c, bit_value)
                self.write_register("LD_TDac", "PixelConf_tdac")

                # Select the right row
                for c in range(self.ncols):
                    self.daq['PixelConf_row']['PixelConf_row'][self.ncols -1 - c]['en_injection_row'] = 0 ##this masks all pixels 
                div_row, mod_row = divmod(r, 4)
                self.daq['PixelConf_row']['PixelConf_row'][self.ncols -1 - div_row]['en_injection_row'][mod_row] = '1' 

                # Set WrRAM_TDAC at the current bit position
                self.daq['CONF_SR']['WrRAM_TDAC'] = 1 << bit_pos
                self.write_register("LD_Config", "CONF_SR")

                # Reset WrRAM_TDAC after writing
                self.daq['CONF_SR']['WrRAM_TDAC'] = 0
                self.write_register("LD_Config", "CONF_SR")

        # Disable RAMWr
        self.daq['CONF_SR']['RAMWrEnable'] = 0
        self.write_register("LD_Config", "CONF_SR")

        self.logger.info("Done setting TDACs!")
        

  

    ## threshold defaults for vdac in dacs (try voltages but maybe dacs are needed)
    def vdac_default(        
        self,
        ThPix = 150,# BL + 50mV
        BlPix=127, 
        VCASC2=150,
        ThFine = 195   #thr for TDC : 1.4V
        ):
        self.daq['VDAC_SR']["ThPix"] = ThPix
        # self.daq['VDAC_SR']["BlPix"] = BlPix
        self.daq['VDAC_SR']["VCASC2"] = VCASC2
        self.daq['VDAC_SR']["ThFine"] = ThFine
        
    
    # power
    ## only VMinus is set externally. The others can be set but are not connected at the moment -> they are set internally via vdac
    def power_up(
        self,
        VMinus_Pix = 0.8, 
        VDD_PLL = 1.8,
        VDD_LVDS = 1.8,
<<<<<<< HEAD
        BlPix = 0.94
=======
        BlPix = 0.97
>>>>>>> development_v2
    ):
        """Power up everything with the default settings. Note that setting the power manually might not work, because there is some high currents in the GPAC_MightyPix startup."""
        self.set_global_voltage(
            VMinus_Pix = VMinus_Pix,
            VDD_PLL = 1.8,
            VDD_LVDS = 1.8,
            BL_Pix = BlPix
            # TH_Pix = 1.01
        )

    def power_down(self):
        """Disables the power."""
        for pwr in [
            "VMinus_Pix",
            "VDD_PLL",
            "VDD_LVDS",
        ]:
            self.daq[pwr].set_voltage(0, unit="V")
            self.SET_VALUE[pwr] = 0

    def power_cycle(self, wait=0.1):
        """Switch off and on."""
        self.power_down()
        self.power_up()

    def power_status(self):
        """Loop over all power units and displays the status."""
        status = {}
        full_power = 0.0
        analogue_power = 0.0
        digital_power = 0.0
        for pwr in [
            "VMinus_Pix",
            "VDD_PLL",
            "VDD_LVDS"
        ]:
            # If this a line is failing in your code you probably did not init() the chip!
            # Remember: this chip initializes the sw on __init__ and the hw on init!
            # status[pwr + "[V]"] = self[pwr].get_voltage(unit="V")
            status[pwr + "set"] = self.SET_VALUE[pwr]
            status["Power [mW]"] = 0
        return status

    def _create_mask(self, pix):
        """
        Create a mask of the same dimensions as the chip's dimensions.

        Parameters
        ----------
        pix: string OR list OR tuple OR np.ndarray
            - valid strings as input: "all" (all 1s) or "none" (all 0s)
            - list, tuple or np.ndarray:
                - Single pixel: e.g. pix=[20,60]
                - Matrix of the same dimensions as the chip
                - A list of pixels: e.g. pix=[[20,60],[20,61],[20,62]]
        """
        mask = np.empty([self.COL_SIZE, self.ROW_SIZE])
        mask.fill(np.NaN)
        # A string as input: "all" (all 1s) or "none" (all 0s)
        if isinstance(pix, str):
            if pix == "all":
                mask.fill(1)
            elif pix == "none":
                mask.fill(0)
            else:  # shouldnt here be an error raise saying that only "all" or "none" string is allowed
                pass
        # A list, tuple or np.ndarray as input
        elif isinstance(pix, (list, tuple, np.ndarray)):
            mask.fill(0)
            # print(len(mask))
            if len(pix) > 0:
                # Single pixel format: e.g. pix=[20,60]
                if isinstance(pix[0], int):
                    mask[pix[0], pix[1]] = 1
                # A matrix of the same dimensions as the chip
                elif len(pix) == self.COL_SIZE and len(pix[0]) == self.ROW_SIZE:
                    mask[:, :] = np.array(pix, np.bool)
                # A list of pixels: e.g. pix=[[20,60],[20,61],[20,62]]
                else:
                    for p in pix:
                        if len(p) == 2 and isinstance(p[0], int) and isinstance(p[1], int):
                            mask[p[0], p[1]] = 1
                        else:
                            self.logger.info("The listed item {0:s} does not correspond to a valid pixel format.".format(p))
                            mask.fill(np.NaN)
                            break
            else:
                self.logger.info("No pixels given as input for mask creation.")
        else:
            self.logger.info("You have not specified a valid input for mask creation. Please check the code documentation.")
        return mask



    def get_data(self, wait=0.01, no_inject=False):
        """
        Return data on the FIFO generated by injected pulses.

        Parameters
        ----------
        wait: float
            Period of time (in seconds) between the last injection and read-out of the last batch of data.

        Return
        ----------
        raw: numpy.ndarray
            Read-out data
        """

        count = 0
        while not self.daq["data_rx"]["READY"]:
            time.sleep(0.01)
            count += 1
            if count > 10:
                break
        if count > 10:
            self.logger.warn("Could not get a synchronization lock")
            return np.empty(0, dtype="uint32")
        if not no_inject:
            self.logger.debug("injecting: ")
            self.daq["inj"].start()
        else:
            self.logger.debug("WTF??")
        i = 0
        raw = np.empty(0, dtype="uint32")
        time.sleep(0.01)
        while self.daq["inj"].is_done() != 1:
            time.sleep(0.01)
            raw = np.append(raw, self.daq['fifo'].get_data())
            i = i + 1
            if i > 10:
                break
        time.sleep(wait)
        raw = np.append(raw, self.daq['fifo'].get_data())

        return raw

    def set_inj_high(self, inj_high):
        """Note that the injection voltage set here is half the real voltage, if the GPAC_MightyPix is used."""
        if isinstance(self.inj_device, str) and self.inj_device == "GPAC_MightyPix":
            self.daq["INJ_HI"].set_voltage(inj_high, unit="V")
        else:
            self.inj_device.set_voltage(inj_high, high=True)
        self.SET_VALUE["INJ_HI"] = inj_high

    def set_inj_low(self, inj_low):
        """Note that the injection voltage set here is half the real voltage, if the GPAC_MightyPix is used."""
        if isinstance(self.inj_device, str) and self.inj_device == "GPAC_MightyPix":
            self.daq["INJ_LO"].set_voltage(inj_low, unit="V")
        else:
            self.inj_device.set_voltage(inj_low, high=False)
        self.SET_VALUE["INJ_LO"] = inj_low

    def set_inj_all(self, inj_high=0.9, inj_low=0.3, inj_n=1, inj_width=10, delay=100):
        """Note that the injection voltage set here is half the real voltage, if the GPAC_MightyPix is used."""
        self.set_inj_high(inj_high)
        self.set_inj_low(inj_low)
        self.daq["inj"].reset()
        self.daq["inj"]["REPEAT"] = inj_n
        self.daq["inj"]["DELAY"] = delay
        self.daq["inj"]["WIDTH"] = inj_width
        self.daq["inj"]["EN"] = True

    def setup_analog_injection(self, vcal_high=0.9, vcal_med=0.3):
        self.set_inj_high(vcal_high)
        self.set_inj_low(vcal_med)
        self.logger.info(f"inj: {vcal_high} (high) {vcal_med} (low) -> {(vcal_high-vcal_med)*2}V injection")

    def setup_digital_injection(self, vcal_high=0.9, vcal_med=0.5, v_ref = 1.9, unit="V"):
        # self.set_inj_low(v_ref/2)
        # self.set_inj_high(0.01)
        inj_amp = (vcal_high-vcal_med)*2
        if unit =='V':
            inj_amp = int(inj_amp * 255 / v_ref)
        self.daq['VDAC_SR'].set_size(self.daq["VDAC_SR"]._conf['size'])
        self.daq['VDAC_SR']["VCASC2"] = int(inj_amp)
        self.set_sr_on(sr = "LD_VDac")
        self.daq['VDAC_SR'].write()
        self.sr_ready('VDAC_SR')
        # if self.daq['VDAC_SR'].is_done():
        #     self.logger.info(f"Loading of Injection to {inj_amp} DAC counts is done!")
        self.set_sr_off(sr = "LD_VDac")


    def set_inj_param(self, inj_n=1, inj_width=500000, delay=700):
        """Note that the injection voltage set here is half the real voltage, if the GPAC_MightyPix is used."""
        self.daq["inj"].reset()
        self.daq["inj"]["REPEAT"] = inj_n
        self.daq["inj"]["DELAY"] = delay
        self.daq["inj"]["WIDTH"] = inj_width
        self.daq["inj"]["EN"] = True




    def start_inj(self, inj_high=None, wait=False):
        """Start the injection."""
        if inj_high is not None:
            self.set_inj_high(inj_high)
        self.daq["inj"].start()

        while self.daq["inj"].is_done() != 1 and wait is True:
            # time.sleep(0.0001)
            pass




    def get_rx_status(self):
        """Report the error status counters."""
        return_counters = {}

        for prop in [
            "READ_COL2_CNT",
            "LOST_DATA_COUNTER",
            "DECODER_ERROR_COUNTER",
            "FIFO_SIZE",
            "ENABLE",
            "READY",
            "LOAD_COLUMN2_COUNTER",
            "LOAD_PIXEL2_COUNTER",
            "RAW_DATA0",
            "RAW_DATA1"
        ]:  return_counters[prop] = getattr(self.daq["data_rx"], prop)
        if self.isMIO3:
            return_counters["FIFO"] = self.daq["fifo"].get_FIFO_SIZE()
        else:
            return_counters["FIFO"] = self.daq["FIFO"].get_FIFO_INT_SIZE()
        return return_counters

    def dac_status(self):
        # pass
        """Return the current DAC stettings"""
        current_dac_settings = {}
        for dac in self.daq["CONF_SR"]._fields:
            if isinstance(self.daq["CONF_SR"][dac],bitarray.bitarray) :
                current_dac_settings[dac] = ba2int(self.daq["CONF_SR"][dac])
            else:
                # print(self["CONF_SR"][dac])
                for i in self.daq["CONF_SR"][dac]:
                    for dac_i in i._fields:
                        current_dac_settings[f"dac_{dac_i}"]=ba2int(i[dac_i])


        return current_dac_settings

    # Above function only returns the "CONF_SR" settings, we need the "CONF" settings as well
    def dac_status_CONF(self):
        """Returns the current DAC CONF settings"""
        current_dac_CONF_settings = {}
        for dac in self.daq["CONF"]._fields:
            current_dac_CONF_settings[dac] = ba2int(self.daq["CONF"][dac])
        return current_dac_CONF_settings

    def get_configuration(self):
        ret = super(TelePix, self).get_configuration()
        # ret['PixelConf'] = self.PixelConf
        # add some statuses
        # ret['status'] = self.get_rx_status()
        # ret = {
        #     "mask": self.mask,
        #     "injection": self.injection,
        #     "dacs": np.asarray(list(self.dac_status().values()), dtype=np.dtype("U")),
        # }
        ret={}
        ret.update(self.dac_status())
        # this is the same as in the daq settings but maybe a bit more readable
        for i_conf, value in self.PixelConf.items():
            ret[i_conf] = str([i.tobytes() for i in value])
        # print(ret)
        return ret

    def get_firmware_configuration(self):
        ret={}
        # print(super().get_configuration())
        for i_conf, value in super().get_configuration().items():
            if isinstance(value, dict):
                for ii_conf,ivalue in value.items():
                    if isinstance(ivalue, list):
                        for iii_conf,iivalue in enumerate(ivalue):
                            if isinstance(iivalue, bitarray.bitarray):
                                ret[f"{i_conf}_{ii_conf}_{iii_conf}"]=ba2int(iivalue)
                            else:
                                ret[f"{i_conf}_{ii_conf}_{iii_conf}"]=str(iivalue)
                    else:
                        if isinstance(ivalue, bitarray.bitarray):
                            ret[f"{i_conf}_{ii_conf}"]=ba2int(ivalue)
                        elif isinstance(ivalue,str):
                            try:
                                ret[f"{i_conf}_{ii_conf}"]=int(ivalue,2)
                            except:
                                ret[f"{i_conf}_{ii_conf}"]=ivalue
                        else:
                            ret[f"{i_conf}_{ii_conf}"]=str(ivalue)
            else:
                ret[i_conf] = str(value)

            return ret

    def reset_read(self, wait=0.0001):
        """
        Reset the read-out of the chip.

        Parameters
        ----------
        wait: float
            Period of time (in seconds) that the function waits before changing back the state of the reset.
        """
        self.daq["CONF"]["RESET"] = 1
        self.daq["CONF"].write()

        time.sleep(wait)
        self.daq["CONF"]["RESET"] = 0
        self.daq["CONF"].write()


    def set_read(self):
        """
        Enable the read-out of the chip.

        Parameters
        ----------
        """
        self.daq["data_rx"].set_en(False)
        self.daq["data_rx"].reset()
        self.reset_read(wait=0.001)
        self.daq["fifo"]["RESET"]
        i=0
        while self.daq["fifo"].get_FIFO_SIZE()>0:
            self.daq["fifo"]["RESET"]
            i+=1
            if i>10:
                self.logger.warning(f"Exceeded  max number of resets!!   {self.daq['fifo'].get_FIFO_SIZE()}")
                break
            time.sleep(0.001)
        self.daq["data_rx"].set_en(True)


        fifo_size = self.daq["fifo"].get_FIFO_SIZE()
        if fifo_size >0:
            self.logger.warning(
                "Setting TelePix Read-out: reset_fifo={}".format(
                    fifo_size,
                )
            )
        
        self.daq["fifo"]["RESET"]  # mio3
        fifo_size = self.daq["fifo"].get_FIFO_SIZE()
        if fifo_size >0:
            self.logger.warning(
                "Setting TelePix Read-out: reset_fifo={}".format(
                    fifo_size,
                )
            )



       

    def stop_read(self):
        """
        Stop the read-out of the chip.

        Returns
        ----------
        lost_cnt: int
            Number of hits not read-out when the read-out was stopped.
        """
        self.daq["data_rx"].set_en(False)
        # lost_cnt = self.daq["data_rx"]["LOST_DATA_COUNTER"]
        # if lost_cnt != 0:
        #     self.logger.warn("stop_read: error cnt={0:d}".format(lost_cnt))
        # return lost_cnt

    def set_th(self, th_id=None, th_value=None):
        """
        Set a global threshold.

        Parameters
        ----------
        th_id: int or list of int
            Single integer or list of integers corresponding to valid Threshold IDs (i.e. 1, 2 or 3).
        th_value: float or list of floats
            Single float or list of floats corresponding to valid Threshold values (in Volts).
        """
        if isinstance(th_id, int):
            if th_id > 0 and th_id < 4:
                th_string = "TH" + str(th_id)
                th_dict = {th_string: th_value}
                self.set_global_voltage(**th_dict)
            else:
                self.logger.info("*{0}* is not a valid Threshold ID. (Only 1, 2 or 3 are valid)".format(th_id))
        elif isinstance(th_id, (list, tuple, np.ndarray)) and len(th_id) > 0:
            if len(th_id) == len(th_value):
                for th_pos, th_iter in enumerate(th_id):
                    if isinstance(th_iter, int) and th_iter > 0 and th_iter < 4:
                        th_string = "TH" + str(th_iter)
                        th_dict = {th_string: th_value[th_pos]}
                        self.set_global_voltage(**th_dict)
                    else:
                        self.logger.info("*{0}* is not a valid Threshold ID. (Only 1, 2 or 3 are valid)".format(th_iter))
            else:
                self.logger.info("The number of threshold values does not match the number of threshold IDs.")
        else:
            self.logger.info("The input was incorrect. It must be either: 1. An integer TH ID and a value, or 2. A list of TH IDs and list of values.")

    def set_global_voltage(self, **kwarg):
        """
        Set a global voltage to a specific value (in V).

        Parameters
        ----------
        kwarg:
            Any name on the list of global voltages as parameter name, followed by the value expected to be set as parameter value (in V).
            e.g. self.set_global_voltage(TH1=1.5)
        """
        for k in kwarg.keys():
            if k == "setting":
                if "value" not in kwarg:
                    raise ValueError("set_global_voltage needs a value if 'setting' is used!")

                # removed for simulation only put it back!!!!!!!!!!!!!!!!!
                self[kwarg[k]].set_voltage(kwarg["value"], unit="V")
                self.SET_VALUE[kwarg[k]] = kwarg["value"]
                self.logger.info(f"Setting {kwarg[k]} to {kwarg['value']}")
                break
            self.logger.info(f"Setting {k} to {kwarg[k]}")
            # removed for simulation only put it back!!!!!!!!!!!!!!!!!
            self.daq[k].set_voltage(kwarg[k], unit="V")
            self.SET_VALUE[k] = kwarg[k]

  
    def convert_physical_to_dig_pixel(self, col, row):
        """The conversion is mainly for the address that is returned, if you want to have physical pixel."""
        if row < 31:
            dig_row = row * 2
        else:
            dig_row = (row - 31) * 4 + 64
        self.logger.info(f"input {col},{row}")
        self.logger.info(f"output {col},{dig_row}")
        return (col, dig_row)

    def read_config_file(self, filename, config_type):
        """
        Read pixel configuratrion from a file.

        There are two types of files supported:
        .h5 file, where the pixel info has to be in the root directory by the name config_type
        .conf file, where the pixel info is saved as example_mask.conf

        The config type should be one of the matrices: mask,injection,trim.
        """
        # FIXME add more comments here
        if "h5" == filename.split()[-1]:
            with tb.open_file(filename, "r") as in_file:
                try:
                    setattr(self, config_type, getattr(in_file.root, config_type)[:])
                except:
                    self.logger.error(f"Could not get {config_type} from file {filename}.")
        elif "conf" == filename.split()[-1]:
            rows_inv = []
            try:
                with open(filename) as in_file:
                    for line in in_file:
                        if line[0] == "#":
                            continue
                        row_inv.append([int(i) for i in line])
                row_col_array = np.asarray(row_inv[::-1])
                setattr(self, config_type, row_col_array.T())
            except Exception as e:
                self.logger.error(f"Could not get {config_type} from file {filename}. \n {str(e)}")
        else:
            self.logger.error(f"Could not get {config_type} from file {filename}.")
    
    def show(self, config_id="all", show_plot=False):
        """
        Log general information about the current status of the chip.

        Parameters
        ----------
        pix_mask_id: string
            - "power": Logs Power status
            - "dac": Logs DAC status
            Things that need to be added:
            - "Trim": Logs TDAC mask.
            - "EnHB": Logs Hit Buffer mask.
            - "EnInj": Logs Injection mask.
            - "EnAmpout": Logs Ampout mask.
        """
        if config_id == "all" or config_id == "power":
            ret = self.power_status()
            self.logger.info("Power" + ": " + str(ret))
        if config_id == "all" or config_id == "dac":
            ret = self.dac_status()
            self.logger.info("DACs" + ": " + str(ret))
        # if config_id=="all" or config_id=="Trim":
        #     pix_conf_list = np.array(self.PIXEL_CONF["Trim"],int)
        #     self.logger.info("Trim"+": "+str(pix_conf_list.tolist()))
        # if config_id=="all" or config_id=="EnPre":
        #     pix_conf_list = np.array(self.PIXEL_CONF["EnPre"],int)
        #     self.logger.info("EnPre"+": "+str(pix_conf_list.tolist()))
        # if config_id=="all" or config_id=="EnInj":
        #     pix_conf_list = np.array(self.PIXEL_CONF["EnInj"],int)
        #     self.logger.info("EnInj"+": "+str(pix_conf_list.tolist()))
        # if config_id=="all" or config_id=="EnMonitor":
        #     pix_conf_list = np.array(self.PIXEL_CONF["EnMonitor"],int)
        #     self.logger.info("EnMonitor"+": "+str(pix_conf_list.tolist()))
    
    def close(self):
        """Make sure the power is out."""
        # self.power_down()


if __name__ == "__main__":
    """This is just a simple main to test that the chip is connected and nothing huge is wrong."""
    # Create the Pixel object
    chip = TelePix()
    chip.init()
    input()
    chip.reset_read()
    raw = chip.get_data()
    chip.dut["intf"].close()





    
 # def set_pix(self, pix, disable=False):
    #     return
    #     self.logger.info("Start initializing columns. This will take some time!")
    #     if isinstance(pix, str):
    #         if pix == 'all':
    #             pix = np.ones([self.ncols, self.nrows], dtype='u1')
    #         elif pix == 'none':
    #             pix = np.zeros([self.ncols, self.nrows], dtype='u1')
    #     elif isinstance(pix,np.ndarray):
    #         if len(pix[0])==2:
    #             pix_temp = np.zeros([self.ncols, self.nrows], dtype='u1')
    #             for i,j in pix:
    #                 pix_temp[i,j]=1
    #             pix=pix_temp
    #     elif len(pix[0])!=chip.nrows:
    #         pix_temp = np.zeros([self.ncols, self.nrows], dtype='u1')
    #         for i,j in pix:
    #             pix_temp[i,j]=1
    #         pix=pix_temp
    #     # self.PixelConf['en_pix']
    #     enable_mask=self.masks["enable"]
    #     rows = np.unique(np.argwhere(pix & enable_mask)[:, 1])
    #     columns = np.unique(np.argwhere(pix & enable_mask)[:, 0])

    #     pixels = [i for i in zip(*(np.where(pix == enable_mask)))]
    #     # print(pix)
    #     tmp_InjEn = self.daq['CONF_SR']['InjEn'].tovalue()
    #     tmp_EnRow = []
    #     for c in range(self.ncols):
    #         tmp_EnRow.append(self.daq['PixelConf_row']['PixelConf_row'][self.ncols - c - 1]['en_injection_row'].tovalue())
    #     self.daq['CONF_SR']['RAMWrEnable'] = 1
    #     self.daq['CONF_SR']['InjEn'] = 0
    #     self.daq['CONF_SR']['PCH'] = 0
    #     self.write_register("LD_Config","CONF_SR")


    #     # save Inj rows
    #     tmp_EnRow = []
    #     for c in range(self.ncols):
    #         tmp_EnRow.append(self.daq['PixelConf_row']['PixelConf_row'][self.ncols -1 -c]['en_injection_row'].tovalue())

    #     print("rows", rows)
    #     print("cols", columns)
    #     # print(pix)
    #     # print(f"This is  88,242: {pix[88,242]}")
    #     self.daq['CONF_SR']['WrRAM_Comp'] = 1
    #     self.write_register("LD_Config","CONF_SR")


    #     # for c in range(self.ncols):
    #     #     self.daq['PixelConf_tdac']['TDAC'][c] = "0" if (c in columns) else "1"
    #     # self.write_register("LD_TDac","PixelConf_tdac")


    #     for r in rows:
    #         ## first write TDAC value
    #         for c in range(self.ncols):
    #             self.daq['PixelConf_tdac']['TDAC'][c] = ~pix[c, r]
    #             # self.daq['PixelConf_tdac']['TDAC'][c] = "0" if (c in columns) else "1"
    #         self.write_register("LD_TDac","PixelConf_tdac")


    #         # select row
    #         for c in range(self.ncols):
    #             self.daq['PixelConf_row']['PixelConf_row'][self.ncols -1 - c]['en_injection_row'] = 0
    #         div_row, mod_row = divmod(r, 4)
    #         self.daq['PixelConf_row']['PixelConf_row'][self.ncols -1 - div_row]['en_injection_row'][mod_row] = '1'
    #         self.write_register("LD_Row","PixelConf_row")

    #        # # set WrRAM to 0
    #         self.daq['CONF_SR']['WrRAM_Comp'] = 1
    #         self.write_register("LD_Config","CONF_SR")
    #     #    # write RAM
    #         self.daq['CONF_SR']['WrRAM_Comp'] = 1
    #         self.write_register("LD_Config","CONF_SR")

    #         # # log and update memory
    #         self.PixelConf['en_pix'][:, r] = pix[:, r]

    #     # # restore En Inj
    #     self.daq['CONF_SR']['InjEn'] = tmp_InjEn
    #     self.daq['CONF_SR']['RAMWrEnable'] = 0
    #     self.write_register("LD_Config","CONF_SR")

    #     # # restore EnRow
    #     self.daq['PixelConf_row'].set_size(self.daq["PixelConf_row"]._conf['size'])
    #     # self.daq['PixelConf_row'].set_configuration(self.daq['PixelConf_row']._init)
    #     for c in range(self.ncols):
    #         self.daq['PixelConf_row']['PixelConf_row'][self.ncols - c - 1]['en_injection_row'] = tmp_EnRow[c]
    #     self.set_sr_on(sr = "LD_Row")
    #     self.daq['PixelConf_row'].write()
    #     self.sr_ready("PixelConf_row")
    #     self.set_sr_off(sr = "LD_Row")


    #     self.daq['CONF_SR'].set_size(self.daq["CONF_SR"]._conf['size'])
    #     self.daq['CONF_SR']['RAMWrEnable'] = 0
    #     self.set_sr_on(sr = "LD_Config")
    #     self.daq['CONF_SR'].write()
    #     self.sr_ready("CONF_SR")
    #     self.set_sr_off(sr = "LD_Config")
    #     self.logger.info("Reset Done")
      # def reset_chip(self):
    #     return
    #     self.logger.info("Resetting chip! This will take some time")
    #     tmp_InjEn = self.daq['CONF_SR']['InjEn'].tovalue()
    #     ## first reset the chip 
    #     self.logger.info("Write Config Register")
    #     self.daq['CONF_SR'].set_size(self.daq["CONF_SR"]._conf['size'])
    #     self.daq['CONF_SR'].set_configuration(self.daq['CONF_SR']._init)
    #     self.daq['CONF_SR']['RAMWrEnable'] = 1  
    #     self.daq['CONF_SR']['InjEn'] = 0
    #     self.set_sr_on(sr = "LD_Config")
    #     self.daq['CONF_SR'].write()
    #     self.sr_ready("CONF_SR")
    #     self.set_sr_off(sr = "LD_Config")

    #     self.logger.info("Write TDAC Register")
    #     # save Inj rows
    #     tmp_EnRow = []
    #     for c in range(self.ncols):
    #         tmp_EnRow.append(self.daq['PixelConf_row']['PixelConf_row'][self.ncols - c - 1]['en_injection_row'].tovalue())

    #     self.daq['PixelConf_tdac'].set_size(self.daq["PixelConf_tdac"]._conf['size'])
    #     self.daq['PixelConf_tdac']['TDAC'] = 0x0
    #     self.daq['PixelConf_tdac'].write()
    #     self.set_sr_on(sr = "LD_TDac")
    #     self.daq['PixelConf_tdac'].write()
    #     self.sr_ready("PixelConf_tdac")
    #     self.set_sr_off(sr = "LD_TDac")

    #     self.logger.info("Write Pixel and RAM Register")
    #     for r in range(self.nrows):
    #         # select row
    #         for c in range(self.ncols):
    #             self.daq['PixelConf_row']['PixelConf_row'][self.ncols - c - 1]['en_injection_row'] = 0
    #         #Toko has 12 as divider but 4 worked in the past & should be right because 1col connects 4rows
    #         div_r, mod_r = divmod(r, 4)
    #         self.daq['PixelConf_row']['PixelConf_row'][self.ncols - div_r - 1]['en_injection_row'][mod_r] = '1'

    #         self.daq['PixelConf_row'].set_size(self.daq["PixelConf_row"]._conf['size'])
    #         self.daq['PixelConf_row'].set_configuration(self.daq['PixelConf_row']._init)
    #         self.set_sr_on(sr = "LD_Row")
    #         self.daq['PixelConf_row'].write()
    #         self.sr_ready("PixelConf_row")
    #         self.set_sr_off(sr = "LD_Row")

    #         # write RAM
    #         self.daq['CONF_SR'].set_size(self.daq["CONF_SR"]._conf['size'])
    #         # self.daq['CONF_SR'].set_configuration(self.daq['CONF_SR']._init)
    #         self.daq['CONF_SR']['WrRAM_TDAC'] = 7
    #         self.daq['CONF_SR']['WrRAM_Comp'] = 1
    #         self.set_sr_on(sr = "LD_Config")
    #         self.daq['CONF_SR'].write()
    #         self.sr_ready("CONF_SR")
    #         self.set_sr_off(sr = "LD_Config")


    #         # set WrRAM to 0
    #         self.daq['CONF_SR'].set_size(self.daq["CONF_SR"]._conf['size'])
    #         self.daq['CONF_SR']['WrRAM_TDAC'] = 0
    #         self.daq['CONF_SR']['WrRAM_Comp'] = 0
    #         self.set_sr_on(sr = "LD_Config")
    #         self.daq['CONF_SR'].write()
    #         self.sr_ready("CONF_SR")
    #         self.set_sr_off(sr = "LD_Config")

    #     self.logger.info("Pixel and RAM Register Done. Writing nominal values")

    #     self.PixelConf["en_pix"][:, :] = 1
    #     self.PixelConf["tdac0"][:, :] = 7

    #     # restore En Inj
    #     self.daq['CONF_SR'].set_size(self.daq["CONF_SR"]._conf['size'])
    #     tmp_InjEn = self.daq['CONF_SR']['InjEn'].tovalue()
    #     self.daq['CONF_SR']['InjEn'] = tmp_InjEn
    #     self.daq['CONF_SR']['RAMWrEnable'] = 0
    #     self.set_sr_on(sr = "LD_Config")
    #     self.daq['CONF_SR'].write()
    #     self.sr_ready("CONF_SR")
    #     self.set_sr_off(sr = "LD_Config")

    #     # restore EnRow
    #     self.daq['PixelConf_row'].set_size(self.daq["PixelConf_row"]._conf['size'])
    #     # self.daq['PixelConf_row'].set_configuration(self.daq['PixelConf_row']._init)
    #     for c in range(self.ncols):
    #         self.daq['PixelConf_row']['PixelConf_row'][self.ncols - c - 1]['en_injection_row'] = tmp_EnRow[c]
    #     self.set_sr_on(sr = "LD_Row")
    #     self.daq['PixelConf_row'].write()
    #     self.sr_ready("PixelConf_row")
    #     self.set_sr_off(sr = "LD_Row")


    #     self.daq['CONF_SR'].set_size(self.daq["CONF_SR"]._conf['size'])
    #     self.daq['CONF_SR']['RAMWrEnable'] = 0
    #     self.set_sr_on(sr = "LD_Config")
    #     self.daq['CONF_SR'].write()
    #     self.sr_ready("CONF_SR")
    #     self.set_sr_off(sr = "LD_Config")
    #     self.logger.info("Reset Done")


        # print(rows)
        # print(columns)
        ## diable everything
        # for col in range(self.ncols):
        #     self.daq['PixelConf_tdac']['TDAC'][col]="1"
        
        # for r in range(400):
        # # for r in range(20,120):
        #     self.write_register("LD_TDac","PixelConf_tdac")
        #     for c in range(self.ncols):
        #         self.daq['PixelConf_row']['PixelConf_row'][self.ncols -1 - c]['en_injection_row'] = 0
        #     div_row, mod_row = divmod(r, 4)
        #     self.daq['PixelConf_row']['PixelConf_row'][self.ncols -1 - div_row]['en_injection_row'][mod_row] = '1'
        #     # self.daq['PixelConf_row']['PixelConf_row'][self.ncols -1 -div_row]['en_injection_row'] = '1111'
        #     self.write_register("LD_Row","PixelConf_row")
        #     self.daq['CONF_SR']['WrRAM_Comp'] =  1
        #     # self.daq['CONF_SR']['WrRAM_TDAC'] = "111"
        #     self.write_register("LD_Config","CONF_SR")



        #NOT CLEAR WHY INVERTED AND WHY THIS TEXT IS BIG
        # for column in range(self.ncols):
        #     self.daq['PixelConf_tdac']['TDAC'][column]="0" if (column in columns) else "1"
        # for r in rows:
        #     self.write_register("LD_TDac","PixelConf_tdac")
            
        #     ## select row
        #     for c in range(self.ncols):
        #         self.daq['PixelConf_row']['PixelConf_row'][self.ncols -1 - c]['en_injection_row'] = 0
        #     div_row, mod_row = divmod(r, 4)
        #     self.daq['PixelConf_row']['PixelConf_row'][self.ncols -1 - div_row]['en_injection_row'][mod_row] = '1'
        #     self.write_register("LD_Row","PixelConf_row")

        #     ## set WrRAM to 0 (disable)
        #     self.daq['CONF_SR']['WrRAM_Comp'] = 0
        #     self.write_register("LD_Config","CONF_SR")
        #     ## write RAM 1: enable
        #     self.daq['CONF_SR']['WrRAM_Comp'] = 1
        #     self.write_register("LD_Config","CONF_SR")

        #     # # log and update memory
        #     self.PixelConf['en_pix'][:, r] = pix[:, r]


    # def set_tdac_final(self, tdac):
    #     # return

    #     self.logger.info("Start writing tune bits. This will take some time!")  
    #     # print("tdac is set to", tdac, type(tdac))
    #     # print(self.PixelConf['tdac0'][1])
    #     if isinstance(tdac, int) or isinstance(tdac, np.int64):
    #         # tdac = np.ones(self.PixelConf['tdac0'].shape, dtype=np.uint8) * tdac
    #         tdac = np.ones([self.ncols, self.nrows], dtype='u1') * tdac     
    #         print(tdac)

    #     enable_mask=self.masks["enable"]
    #     columns = np.argwhere(tdac & enable_mask)[:, 0]
    #     # print(columns)


    #     self.daq['CONF_SR']['RAMWrEnable'] = 1
    #     self.write_register("LD_Config","CONF_SR")

    #     for tbit, tmask in enumerate([1, 2, 4]):
    #         a = tdac[:, :] & tmask
    #         b = self.PixelConf['tdac0'][:, :] & tmask 
    #         rows = np.unique(np.argwhere(a != b)[:, 1])      
    #         for r in rows:
    #             for c in range(self.ncols):
    #                 # self.daq['PixelConf_tdac']['TDAC'][c]="0" if (c in columns) else "1"
    #                 # self.daq['PixelConf_tdac']['TDAC'][c]="1" if (c in columns) else "0"
    #                 self.daq['PixelConf_tdac']['TDAC'][c]='{0:d}'.format((tdac[c, r] & tmask) != 0)
    #                 # self.daq['PixelConf_tdac']['TDAC'][c]=1
    #             self.write_register("LD_TDac","PixelConf_tdac")

    #             ## select row 
    #             for c in range(self.ncols):
    #                 self.daq['PixelConf_row']['PixelConf_row'][self.ncols -1 - c]['en_injection_row'] = 0 ##this masks all pixels 
    #             div_row, mod_row = divmod(r, 4)
    #             self.daq['PixelConf_row']['PixelConf_row'][self.ncols -1 - div_row]['en_injection_row'][mod_row] = '1' 
    #             # self.daq['PixelConf_row']['PixelConf_row'][self.ncols -1 - div_row]['en_injection_row'] = '1111' 
    #             self.write_register("LD_Row","PixelConf_row")

    #             ## write RAM: 3 bits to write 1 value (value is stored in TDAC register. So it's always a one that you have to write to the corresponding position)
    #             self.daq['CONF_SR']['WrRAM_TDAC'] = 1 << tbit
    #             self.write_register("LD_Config","CONF_SR")

    #             # log
    #             self.logger.info('set_tdac: row={0} bit={1} tdac=0x{2}'.format(
    #                 r, tbit, self.daq['PixelConf_tdac']['TDAC'].to01()))
    #             ## set it to 0 back
    #             self.daq['CONF_SR']['WrRAM_TDAC'] = 0
    #             self.write_register("LD_Config","CONF_SR")

    #     self.daq['CONF_SR']['RAMWrEnable'] = 0
    #     self.write_register("LD_Config","CONF_SR")

    #     self.PixelConf['tdac0'][:, :] = np.copy(tdac[:, :])

    #     self.logger.info("Done setting TDACs!")
        

         # def set_tdac_hard(self, tdac):
    #     # enable Inj, enable RAMWr
    #     # print("init tdac", self.PixelConf['tdac0'])
    #     self.daq['CONF_SR']['RAMWrEnable'] = 1
    #     self.write_register("LD_Config","CONF_SR")

    #     # for i, ival in enumerate([1, 2, 4]):
    #     for c in range(self.ncols):
    #         self.daq['PixelConf_tdac']['TDAC'][c] = 0
    #     self.write_register("LD_TDac","PixelConf_tdac")
    #     for c in range(self.ncols):
    #         self.daq['PixelConf_row']['PixelConf_row'][self.ncols -1 - c]['en_injection_row']= '1111' 
    #     self.write_register("LD_Row","PixelConf_row")        
    #     self.daq['CONF_SR']['WrRAM_TDAC'] = 1 << 0
    #     # self.daq['CONF_SR']['WrRAM_TDAC'] = 0x1
    #     self.write_register("LD_Config","CONF_SR")
    #     self.daq['CONF_SR']['WrRAM_TDAC'] = 0
    #     self.write_register("LD_Config","CONF_SR")

    #     for c in range(self.ncols):
    #         self.daq['PixelConf_tdac']['TDAC'][c] = 0
    #     self.write_register("LD_TDac","PixelConf_tdac")
    #     for c in range(self.ncols):
    #         self.daq['PixelConf_row']['PixelConf_row'][self.ncols -1 - c]['en_injection_row']= '1111' 
    #     self.write_register("LD_Row","PixelConf_row")        
    #     self.daq['CONF_SR']['WrRAM_TDAC'] = 1 << 1
    #     # self.daq['CONF_SR']['WrRAM_TDAC'] = 0x1
    #     self.write_register("LD_Config","CONF_SR")
    #     self.daq['CONF_SR']['WrRAM_TDAC'] = 0
    #     self.write_register("LD_Config","CONF_SR")

    #     for c in range(self.ncols):
    #         self.daq['PixelConf_tdac']['TDAC'][c] = 1
    #     self.write_register("LD_TDac","PixelConf_tdac")
    #     for c in range(self.ncols):
    #         self.daq['PixelConf_row']['PixelConf_row'][self.ncols -1 - c]['en_injection_row']= '1111' 
    #     self.write_register("LD_Row","PixelConf_row")        
    #     self.daq['CONF_SR']['WrRAM_TDAC'] = 1 << 2
    #     # self.daq['CONF_SR']['WrRAM_TDAC'] = 0x1
    #     self.write_register("LD_Config","CONF_SR")
    #     self.daq['CONF_SR']['WrRAM_TDAC'] = 0
    #     self.write_register("LD_Config","CONF_SR")


    #     self.daq['CONF_SR']['RAMWrEnable'] = 0
    #     self.write_register("LD_Config","CONF_SR")