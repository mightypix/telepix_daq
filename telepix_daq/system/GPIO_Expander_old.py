#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import logging
from struct import pack, unpack_from, calcsize
from array import array
from collections import OrderedDict
from time import sleep
import string

from basil.HL.HardwareLayer import HardwareLayer
from basil.HL.FEI4AdapterCard import Eeprom24Lc128


logger = logging.getLogger(__name__)


class TCA9534(HardwareLayer):
    '''TCA9534
    '''
    CTRL_INPUT =0x00
    CTRL_OUTPUT =0x01
    CTRL_POLARITY_INV =0x02
    CTRL_CONF =0x03

    def __init__(self, intf, conf):
        super(TCA9534, self).__init__(intf, conf)
        self._base_addr = conf['base_addr']

    def _set_clk_on(self, address, ch, value):
        data = array('B', pack('BB', self.CTRL_OUTPUT,  (((value << ch) & 0xFF))))
        self._intf.write(self._base_addr + address, data)
        # sleep(0.1)


    def _set_bit(self, address, ch, value):
        data = array('B', pack('BB', self.CTRL_CONF,  (0 << ch ) & 0xFF))
        self._intf.write(self._base_addr + address,  data)
        # distinguish between Config register (channel 1) and the others because this has a different clk (channel 0)
        if ch == 1:
            data = array('B', pack('BB', self.CTRL_OUTPUT, (value << ch ) & 0xFF))
            self._intf.write(self._base_addr + address, data)
            # sleep(0.1)
            # input()
        else:
            data = array('B', pack('BB', self.CTRL_OUTPUT,  (((value << ch) & 0xFF) | 1)))
            self._intf.write(self._base_addr + address, data)
            # sleep(1)
            # input()


    def _set_bit_off(self, address):
        # data = array('B', pack('BB', self.CTRL_CONF, 0xFF))
        # self._intf.write(self._base_addr + address, data)
        data = array('B', pack('BB', self.CTRL_OUTPUT, 0xFF))
        self._intf.write(self._base_addr + address, data)
        sleep(0.01)



class GPIO_Expander(TCA9534):
    '''TCA9534A Interface. The address is 0x38 but due to a shift by one bit to the right in basil its 0x70 ( thus it is shifted back to its original address)
        '''
    # DAC
    TCA9534_ADD = 0x70

    # Channel mappings
    _ch_map = {
        'VOUTA': {
            'DAC': {'address': TCA9534_ADD, 'ch': 0},
        },
        'VOUTB': {
            'DAC': {'address': TCA9534_ADD, 'ch': 1},
        },
        'VOUTC': {
            'DAC': {'address': TCA9534_ADD, 'ch': 2},
        },

        'VOUTD': {
            'DAC': {'address': TCA9534_ADD, 'ch': 3},
        },
        'VOUTE': {
            'DAC': {'address': TCA9534_ADD, 'ch': 4},
        },
        'VOUTF': {
            'DAC': {'address': TCA9534_ADD, 'ch': 5},
        },
        'VOUTG': {
            'DAC': {'address': TCA9534_ADD, 'ch': 6},
        },
        'VOUTH': {
            'DAC': {'address': TCA9534_ADD, 'ch': 7},
        },
    }

    def __init__(self, intf, conf):
        super(GPIO_Expander, self).__init__(intf, conf)
        self._base_addr = conf['base_addr']

    def init(self):
        # init DACs and ADCs
        super(GPIO_Expander, self).init()
  

    def set_bit(self, channel, value):
        '''switching one channel on and off
        '''
        self._set_bit(**self._ch_map[channel]['DAC'], value = value)

    def set_clk_on(self, channel, value):
        '''need this to constantly set ck1 high except config is written (switch is automatically done in set_bit())
        '''
        self._set_clk_on(**self._ch_map[channel]['DAC'], value = value)

    def set_bit_off(self):
        '''default all outputs to zero
        '''
        self._set_bit_off(address = self.TCA9534_ADD)

"""
first attempts
"""

    # def _set_bit(self, address, ch, value):
    #     sleep(0.1)
    #     data = array('B', pack('BB', self.CTRL_CONF,  (0 << ch ) & 0xFF))
    #     self._intf.write(self._base_addr + address,  data)
    #     # input()
    #     if value == 0x0:
    #         # data = array('B', pack('BB', self.CTRL_CONF,  (0 << ch ) & 0xFF))
    #         # self._intf.write(self._base_addr + address,  data)
    #         data = array('B', pack('BB', self.CTRL_OUTPUT,  (0 << ch ) & 0xFF))
    #         self._intf.write(self._base_addr + address,  data)
    #         sleep(0.1)
    #         data = array('B', pack('BB', self.CTRL_OUTPUT, (value << ch ) & 0xFF))
    #         self._intf.write(self._base_addr + address, data)
    #     else:
    #         # data = array('B', pack('BB', self.CTRL_CONF,  (0 << ch ) & 0xFF))
    #         # self._intf.write(self._base_addr + address,  data)
    #         data = array('B', pack('BB', self.CTRL_OUTPUT, (1 << ch ) & 0xFF))
    #         self._intf.write(self._base_addr + address,  data)
    #         sleep(0.1)
    #         data = array('B', pack('BB', self.CTRL_OUTPUT, (value << ch ) & 0xFF))
    #         self._intf.write(self._base_addr + address, data)
    #     # data = array('B', pack('BB', self.CTRL_OUTPUT, (value << ch ) & 0xFF))
    #     # self._intf.write(self._base_addr + address, data)
    #     # input()
    #     # data = array('B', pack('BB', self.CTRL_OUTPUT, 0xFF))
    #     # self._intf.write(self._base_addr + address, data)

    # def _set_clk_bit(self, address, ch, value):
    #     data = array('B', pack('BB', self.CTRL_CONF, 0xFF))
    #     self._intf.write(self._base_addr + address, data)
    #     self._set_bit_off(address)
    #     # ch = ch +1 
    #     if value == 0x0:
    #         # data = array('B', pack('BB', self.CTRL_CONF,  (0 << ch ) & 0xFF))
    #         # self._intf.write(self._base_addr + address,  data)
    #         data = array('B', pack('BB', self.CTRL_OUTPUT,  (0 << ch ) & 0xFF))
    #         self._intf.write(self._base_addr + address,  data)
    #         sleep(0.1)
    #     else:
    #         # data = array('B', pack('BB', self.CTRL_CONF,  (0 << ch ) & 0xFF))
    #         # self._intf.write(self._base_addr + address,  data)
    #         data = array('B', pack('BB', self.CTRL_OUTPUT, (value << ch ) & 0xFF))
    #         self._intf.write(self._base_addr + address,  data)
    #         sleep(0.1)