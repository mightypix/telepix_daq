import serial.tools.list_ports

ports = serial.tools.list_ports.comports()
i = 1

print('List of Ports:')
for port, desc, hwid in sorted(ports):
    ports_line = "{}: {} [{}] \n".format(port, desc, hwid)
    print(i, ports_line)
    i += 1

