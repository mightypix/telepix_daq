import influxdb_client
from influxdb_client.client.write_api import SYNCHRONOUS
import numpy as np

bucket = "temp_data"
org = "lhcb"
token = "stFpMguo5Y7VxQbTR80oWbXQJE-4RTTgWFmsXxAc5_6e2FpfexTac7p6uf4v__ypbBv3_UdXScQbIMCYT_V0SQ=="
# Store the URL of your InfluxDB instance
url="http://131.220.218.117:8086"
mac_dict = ["0A:F9:E0:4F:F3:BE",
"CA:C9:A3:54:9B:ED",
"CA:C9:A3:5B:BB:92",
"CA:C9:A3:54:A5:3A"]

client = influxdb_client.InfluxDBClient(
    url=url,
    token=token,
    org=org
)

# Query script
query_api = client.query_api()
class temperature_influxdb_query():
    def get_temperature(position):
        mac_address = mac_dict[position-1]
        query = f'from(bucket: "temp_data")\
        |> range(start: -5s)\
        |> filter(fn: (r) => r["_measurement"] == "temp_sensor")\
        |> filter(fn: (r) => r["MAC"] == "{mac_address}")\
        |> filter(fn: (r) => r["_field"] == "temp")'
        result = query_api.query(org=org, query=query)
        # print(result)
        results = []
        for table in result:
            for record in table.records:
                results.append(record.get_value())
        # print(results)
        # print(isinstance(results, list))
        # print(results != [])
        # print(isinstance(results, list) and results != [])
        if isinstance(results, list) and results != []:
            results = float(results[len(results)-1])
            # print("if")
            return results
        else:
            # print("else")
            return -1000 #"no temperature measured"


# derp = temperature_influxdb_query.get_temperature(1)
# print(derp)
# print(results)