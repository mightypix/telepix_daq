#from telepix_daq.system import logger
#import logging
# from telepix_daq.system import logger
import sys
from basil.dut import Dut
from telepix_daq.system import logger
# from basil.HL import iseg_shr

#####
#HV and LV Inputs for PS - care paths, ip and usb port
#to use u have to remove comments here, in class MetaTable and handle_data

#logger = logging.getLogger(__name__)
#log = logger.setup_main_logger()
#log = logger.setup_derived_logger('TelePix - LV Power Supply')


class hmp4040(Dut):
    def init_hmp4040(self):
        #from telepix_daq.system import logger
        log = logger.setup_derived_logger('TelePix - LV Power Supply')
        # print(Dut('/home/lab/daq/basil/basil/HL/rs_hmp4040.yaml'))
        try:
            hmp=Dut('/home/lab/daq/basil/basil/HL/rs_hmp4040.yaml') #change usb port in this file (maybe also pyvisa isn't installed, had to change this in setup.py)
            hmp.init()
            log.info("LV Power Supply:"+hmp["Powersupply"].get_name())
            return hmp
        except Exception as e:
            return None
            # print(e)
            while True:
                self.log.info('Could not connect to the LV power supply.') 
                # lv_ps_skip = input(" Proceed anyways? \n Enter [y/n]:").strip().lower() 
                self.log.error("Could not connect to LV!!!")
                lv_ps_skip='y'
                if lv_ps_skip in ('y', 'n'):
                    break  # Exit the loop if the input is valid
                else:
                    print("Invalid input. Please enter 'y' for yes or 'n' for no.")
            if lv_ps_skip == "y":
                self.log.info("Proceeding without LV power supply!")
                return None
                #logging.info("Proceeding without LV power supply!")
            else:
                self.log.info("Aborting scan!")
                #logging.info("Aborting scan!")
                sys.exit()

class iseg_shr(Dut):
    def init_iseg_shr(self):
        #from telepix_daq.system import logger
        #logger = logger.setup_derived_logger('TelePix - HV Power Supply')
        try:
            import basil.HL.iseg_shr as iseg_shr
            # icsClient = iseg_shr.importFromPath("/home/lab/mambaforge/lib/python3.11/site-packages/icsClientPython_3", "icsClientPython_3") #install icsClientPython_3 maually, not pip (@niclas on your pc the path should be miniconda instead of mambaforge)
            icsClient = iseg_shr.importFromPath("/home/lab/miniconda3/lib/python3.10/site-packages/icsClientPython_3/", "icsClientPython_3") #install icsClientPython_3 maually, not pip (@niclas on your pc the path should be miniconda instead of mambaforge)
            connectionName = "new_iCS_connection"
            # connectionParameters = "ws://131.220.218.112" #ip need to be changed for other ps
            connectionParameters = "ws://192.168.20.101" #ip need to be changed for other ps
            connectionLogin = "user"
            connectionPassword = "X9(;lykwARUXK6`ObNs_iW-2?mXdgVI_" #this is not nice for a public push, change this on the powersupplys too pls
            connection = iseg_shr.establishConnection(icsClient, connectionName, connectionParameters, connectionLogin, connectionPassword)
            # if type(connection) != icsClient.connection:
            #     print("no HV connection")
            #     exit(1)
            iseg = connection.getModules()
            self.log.info("Sucessfully connected to HV Power Supply.")
            return iseg
    
        except Exception as e:
            return None
            while True:
                hv_ps_skip = input("Could not connect to the HV power supply. Proceed anyways? \n Enter [y/n]:").strip().lower()
                if hv_ps_skip in ('y', 'n'):
                    break  # Exit the loop if the input is valid
                else:
                    print("Invalid input. Please enter 'y' for yes or 'n' for no.")
            if hv_ps_skip == "y":
                self.log.info("Proceeding without HV power supply!")
                return None
                #logging.info("Proceeding without HV power supply!")
            else:
                self.log.info("Aborting scan!")
                #logging.info("Aborting scan!")
                sys.exit()