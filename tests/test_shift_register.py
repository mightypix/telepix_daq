import cocotb
from cocotb.regression import TestFactory
from cocotb.triggers import RisingEdge


from basil.utils.sim.utils import cocotb_compile_and_run, cocotb_compile_clean

@cocotb.coroutine
def shift_reg_SIPO_tb(dut):
    # Initialize inputs
    dut.clk <= 0
    dut.serial_in <= 0

    # Wait for a few clock cycles
    for _ in range(5):
        yield RisingEdge(dut.clk)

    # Test case 1: Shift in 0, parallel_out should be 0
    dut.serial_in <= 0
    yield RisingEdge(dut.clk)
    assert dut.parallel_out == 0

    # Test case 2: Shift in 1, parallel_out should be 0
    dut.serial_in <= 1
    yield RisingEdge(dut.clk)
    assert dut.parallel_out == 0

    # Test case 3: Shift in 1, parallel_out should be 1
    dut.serial_in <= 1
    yield RisingEdge(dut.clk)
    assert dut.parallel_out == 1

    # You can add more test cases as needed

# Create a test factory
tf = TestFactory(test_function=shift_reg_SIPO_tb)

# Add the Verilog file containing the DUT (Design Under Test)
# tf.add_option("-sv")
# tf.add_file("shift_reg_SIPO.v")  # Replace with your Verilog module file
tf.extra_args = ["-sv", "shift_reg_SIPO.v"]  # Replace with your Verilog module file

# Define the DUT module name (shift_reg_SIPO in this case)
tf.generate_tests()
  
print(tf)  
# Run the simulation
# tf.run()



# cocotb_compile_and_run(
#         sim_files=["shift_reg_SIPO.v"],
#         extra_defines=extra_defines,
#         sim_bus="basil.utils.sim.BasilSbusDriver",
#         include_dirs=(
#             root_dir,
#             root_dir + "/mightypix_daq/firmware/src",
#             root_dir + "/mightypix_daq/firmware/IP",
#             root_dir + "/mightypix_daq/SiTCP_Netlist_for_Kintex7/",
#             root_dir + "/tests/hdl",
#         ),
#         # extra='EXTRA_ARGS += -g2012'
#     )