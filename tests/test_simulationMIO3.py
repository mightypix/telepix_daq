#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import unittest
import os
import yaml
import time
from bitarray import bitarray
import socketserver

from basil.utils.sim.utils import cocotb_compile_and_run, cocotb_compile_clean
from telepix_daq.system.telepix_pixel import TelePix
from telepix_daq.system.mio3 import MIO3
import basil.dut


class TestSimulation(unittest.TestCase):
    def setUp(self):
        extra_defines = []

        root_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

        simulation_modules = {}
        simulation_modules["Drive100Clock"] = {}
        os.environ["SIMULATION_MODULES"] = yaml.dump(simulation_modules)
        os.environ["SIMULATION_END_ON_DISCONNECT"] = "1"
        os.environ["COCOTB_REDUCED_LOG_FMT"] = "1"
        os.environ["SIMULATION_TRANSACTION_WAIT"] = str(25000 * 300) ## increases simulation time
        # os.environ["SIMULATION_TRANSACTION_WAIT"] = str(25000 * 30) ## increases simulation time
        # os.environ["SIMULATION_BUS_CLOCK"] = "0"

        # Find free port
        with socketserver.TCPServer(("localhost", 0), None) as s:
            free_port = s.server_address[1]

        os.environ["SIMULATION_PORT"] = str(free_port)
        try:
            cocotb_compile_clean()
        except:
            pass
        cocotb_compile_and_run(
            sim_files=[root_dir + "/tests/hdl/tb_mio3.sv"],
            top_level="tb",
            extra_defines=extra_defines,
            sim_bus="basil.utils.sim.BasilSbusDriver",
            # sim_bus="basil.utils.sim.SiLibUsbBusDriver",
            include_dirs=(
                root_dir,
                root_dir + "/telepix_daq/firmware/src",
                # root_dir + "/telepix_daq/firmware/IP",
                root_dir + "/telepix_daq/SiTCP_Netlist_for_Kintex7/",
                root_dir + "/firmware/modules",
                root_dir + "/firmware/modules/utils",
                root_dir + "/tests/hdl",
            ),
            # extra='EXTRA_ARGS += -g2012'
        )


        with open(root_dir + "/telepix_daq/system/mio3.yaml", "r") as f:
            cnfg = yaml.safe_load(f)



        cnfg["transfer_layer"][0]["type"] = "SiSim"
        cnfg["transfer_layer"][0]["init"]["host"] = "localhost"
        cnfg["transfer_layer"][0]["init"]["port"] = free_port
        cnfg["transfer_layer"][0]["init"]["timeout"] = 10000
        cnfg["hw_drivers"][4] = { #TODO make name not int  
             "name": "fifo",
             "type": "bram_fifo",
             "interface": "intf",
             "base_addr": 0x8000,
             "base_data_addr": 0x80000000,
        }
        

        
        daq = MIO3(conf=cnfg)

        daq.init()
        
        daq["ts_external"]["ENABLE"] = 1

        # self.chip = TelePix(daq, conf=daq.configuration)
        # self.chip.init()
        # self.chip.daq["data_rx"].ENABLE = 1


    def wait_for_sim(dut: MIO3, repetitions=8) -> None:
        dut.write_command(dut.write_sync(write=False), repetitions=repetitions)


    def test_configuration(self):
        # self.assertTrue(self.chip.daq["data_rx"].READY)
        # self.chip.daq["data_rx"].ENABLE = 1
#

        # self.assertEqual(data.tolist(), [0x3CA0, 0x500, 0xC0C0, 0xC0AA, 0xC0C0, 0xC0AA, 0xC000, 0x3CA0, 0x500, 0xC0C0, 0xC0AA, 0xC0C0, 0xC0AA, 0xC000, 0x3CA0, 0x500, 0xC0C0, 0xC0AA, 0xC0C0, 0xC0AA])
        a = [0x3CA0, 0x500, 0xC0C0, 0xC0AA, 0xC0C0, 0xC0AA, 0xC000, 0x3CA0, 0x500, 0xC0C0, 0xAAC0, 0xC084, 0x3CA0, 0x500, 0xC0C0, 0xC0AA, 0xC0C0, 0xC0AA]

        # ['0x3ca0', '0x500', '0xc0c0', '0xc0aa', '0x80f6eb', '0xc0c0', '0xc0aa', '0x80f73b', '0xc000', '0x3ca0', '0x500', '0xc0c0', '0xc0aa', '0x80febb', '0xc0c0', '0xc0aa', '0x80ff0b', '0xc000', '0x3ca0', '0x500', '0xc0c0', '0xc0aa', '0x81068b', '0xc0c0', '0xc0aa', '0x8106db', '0xc000']
        # ['0x3ca0', '0x500', '0xc0c0', '0xc0aa', '0x80f6eb', '0xc0c0', '0xc0aa', '0x80f73b', '0xc000', '0x3ca0', '0x500', '0xc0c0', '0xc0aa', '0x80febb', '0xc0c0', '0xc0aa', '0x80ff0b', '0xc000', '0x3ca0', '0x500', '0xc0c0', '0xc0aa', '0x81068b', '0xc0c0', '0xc0aa', '0x8106db', '0xc000']

        # 3ca0,
        # 0500,
        # C0C0,
        # C0AA,
        # C0C0,
        # C0AA,
        # C000,


    # def test_read(self):
    #     self.dut.enable_injection(0, 0)
    #     self.dut.reset_read()
    #     raw = self.dut.get_data()


    def tearDown(self):
        # self.chip.close()
        cocotb_compile_clean()


if __name__ == "__main__":
    unittest.main()
