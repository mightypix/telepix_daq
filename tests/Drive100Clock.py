#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import cocotb
from cocotb.clock import Clock
from cocotb_bus.drivers import BusDriver


class Drive100Clock(BusDriver):

    _signals = ["CLK100"]

    def __init__(self, entity):
        BusDriver.__init__(self, entity, "", entity.CLK100)

    async def run(self):
        cocotb.start_soon(Clock(self.clock, 10, "ns").start())