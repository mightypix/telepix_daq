/**
 * ------------------------------------------------------------
 * Copyright (c) SILAB , Physics Institute of Bonn University 
 * ------------------------------------------------------------
 */

`timescale 1ps / 1ps

`define COCOTB_SIM 1
`include "telepix_daq/firmware/src/mightypix_mio3.v"

// // fpga
`include "utils/clock_multiplier.v"
`include "utils/DCM_sim.v"
`include "bram_fifo/bram_fifo.v"
`include "bram_fifo/bram_fifo_core.v"
`include "utils/OBUFDS_sim.v"
// `include "utils/IBBUFDS_sim.v"

`include "encode_8b10b.v"

module tb (
    input wire FCLK_IN,

    output wire BUS_CLK,
    input wire  BUS_RST,
    input wire [31:0] BUS_DATA_IN,
    output wire [31:0] BUS_DATA_OUT,
    input wire [31:0] BUS_ADD,
    input wire BUS_RD,
    input wire BUS_WR,
    output wire BUS_BYTE_ACCESS,
    input wire TS_IN

);


wire DATA_LVDS_N;
wire DATA_LVDS_P;


// Connect tb internal bus to external split bus
wire [31:0] BUS_DATA;
assign BUS_DATA = BUS_WR ? BUS_DATA_IN : 32'bz;
assign BUS_DATA_OUT = BUS_DATA;
assign BUS_BYTE_ACCESS = BUS_ADD < 32'h8000_0000 ? 1'b1 : 1'b0;


// CLOCKS
wire CLK_RX_OUT_BUF;
wire CLK_RX_BUF;
wire CLK_IN_BUF;
wire CLK400_BUF;
wire CLK_REF_IN_BUF;
wire conf_clk;
wire CLK_REF_IN; 
wire spi_clk;
wire CLK_1GHZ;
wire CLK_142_BUS;

wire CLK100, PLL_VCO;

// ----- Clock (mimics a PLL) -----
  // 100MHZ input
    localparam PLL_MUL                = 10;
    localparam PLL_DIV_MAIN           = 1;
    localparam PLL_DIV_CLK_IN         = 10; // used for internal pll
    localparam PLL_DIV_CLK_RX         = 10; //used as WCLK in receiver  -> data clk
    localparam PLL_DIV_CLK_RX_OUT     = 1; // -> data read clk (2x rx)
    localparam PLL_DIV_CLK_REF_IN     = 2; // used as external PLL
    localparam PLL_DIV_CLK400         = 40; // used for external timestamp: 25 MHz
    // localparam PLL_DIV_CLK_1GHZ         = 1.0; // used for external timestamp: 25 MHz

clock_multiplier #(
    .MULTIPLIER(PLL_MUL)
) i_clock_multiplier(
    .CLK(CLK100),
    .CLOCK(PLL_VCO)
);

clock_divider #(.DIVISOR(PLL_DIV_CLK_IN) ) clock_divider3 ( .CLK(PLL_VCO), .RESET(1'b0), .CE(), .CLOCK(CLK_IN_BUF) );
clock_divider #(.DIVISOR(PLL_DIV_CLK_REF_IN) ) clock_divider5 ( .CLK(PLL_VCO), .RESET(1'b0), .CE(), .CLOCK(CLK_REF_IN_BUF) );
clock_divider #(.DIVISOR(PLL_DIV_CLK_RX) ) clock_divider2 ( .CLK(PLL_VCO), .RESET(1'b0), .CE(), .CLOCK(CLK_RX_BUF) );
// clock_divider #(.DIVISOR(PLL_DIV_CLK_RX_OUT) ) clock_divider1 ( .CLK(PLL_VCO), .RESET(1'b0), .CE(), .CLOCK(CLK_RX_OUT_BUF) );
clock_divider #(.DIVISOR(PLL_DIV_CLK400) ) clock_divider6 ( .CLK(PLL_VCO), .RESET(1'b0), .CE(), .CLOCK(CLK400_BUF) );
clock_divider #(.DIVISOR(700) ) clock_divider7 ( .CLK(CLK_IN_BUF), .RESET(1'b0), .CE(), .CLOCK(spi_clk) );
clock_divider #(.DIVISOR(80)) clock_divider8 (.CLK(CLK_IN_BUF),.RESET(1'b0),.CE(), .CLOCK(conf_clk) );
clock_divider #(.DIVISOR(7)) clock_divider9 (.CLK(PLL_VCO),.RESET(1'b0),.CE(), .CLOCK(CLK_142_BUS) );
// clock_divider #(.DIVISOR(PLL_DIV_CLK_1GHZ)) clock_divider9 (.CLK(PLL_VCO),.RESET(),.CE(), .CLOCK(CLK_1GHZ) );

assign BUS_CLK = CLK_142_BUS;

localparam FIFO_BASEADDR = 32'h8000;
localparam FIFO_HIGHADDR = 32'h9000-1;
localparam FIFO_BASEADDR_DATA = 32'h8000_0000;
localparam FIFO_HIGHADDR_DATA = 32'h9000_0000;

// Firmware core
wire ARB_READY_OUT,ARB_WRITE_OUT;
wire [31:0]ARB_DATA_OUT;
wire SCK, CSB, MOSI, MISO, RST_Ctl_RB, Ctl_Clk_1, Ctl_Clk_2, Ctl_Load, Ctl_SIN, Ctl_SOut;
wire INJECTION, Sync_res, RX_READY, clk_enable;
wire FIFO_FULL, FIFO_NEAR_FULL;
wire I2C_SDA, I2C_SCL;

bram_fifo #(
    .BASEADDR(FIFO_BASEADDR),
    .HIGHADDR(FIFO_HIGHADDR),
    .BASEADDR_DATA(FIFO_BASEADDR_DATA),
    .HIGHADDR_DATA(FIFO_HIGHADDR_DATA),
    .ABUSWIDTH(32)
) i_out_fifo (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD[31:16]), //TODO: this is still strange -> address should 32b insted of 16
    .BUS_DATA(BUS_DATA),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .FIFO_READ_NEXT_OUT(ARB_READY_OUT),
    .FIFO_EMPTY_IN(!ARB_READY_OUT),
    .FIFO_DATA(ARB_DATA_OUT),

    .FIFO_NOT_EMPTY(),
    .FIFO_FULL(FIFO_FULL),
    .FIFO_NEAR_FULL(FIFO_NEAR_FULL),
    .FIFO_READ_ERROR()
);


wire DATA_IN;
// IBUFDS IBUFDS_DATA_IN_inst
//     (
//     .I  (DATA_LVDS_P),
//     .IB (DATA_LVDS_N),
//     .O  (DATA_IN)
//     );


//fast data input wrapper - not usable for run2020 chips
wire recieved_data;
// wire [31:0] recieved_data;
//   wire        soft_reset_rx;
//   wire [ 3:0] rxdisperr_i;
//   wire [ 3:0] rxnotintable_i;
//   wire        rxusrclk_i;
//   wire        rxusrclk2_i;
//   wire [ 3:0] rxcharisk_i;
//   wire        resetdone;
//   wire cpllreset_i;
//   wire gt0_gtrxreset_i;
//   wire gt0_rxslide_i;
//   wire track_data_out;
//   wire cplllocked;



mightypix_core fpga
                 (
                   //local bus
                   .BUS_CLK(BUS_CLK),
                   .BUS_DATA(BUS_DATA[7:0]),
                   .BUS_ADD(BUS_ADD),
                   .BUS_RD(BUS_RD),
                   .BUS_WR(BUS_WR),
                   .BUS_RST(BUS_RST),

                   .ARB_READY_OUT(ARB_READY_OUT),
                   .ARB_WRITE_OUT(ARB_WRITE_OUT),
                   .ARB_DATA_OUT(ARB_DATA_OUT),
                   .FIFO_FULL(FIFO_FULL),
                   .FIFO_NEAR_FULL(FIFO_NEAR_FULL),

                    //TLU
                  .TLU_CLK(),
                  .TLU_BUSY(),
                  .TLU_RESET(),
                  .TLU_TRIGGER(),

                   //clocks
                   .conf_clk(conf_clk),
                   .spi_clk(spi_clk),
                   .CLK_RX_x0p1(conf_clk),
                   .CLK_IN(CLK_IN_BUF),
                   .CLK_RX(CLK_RX_BUF),
                   .CLK_RX_OUT(PLL_VCO),
                   .CLK400(CLK400_BUF),
                   .CLK_1GHZ(PLL_VCO),

                    //RX INFO
                    // .soft_reset_rx(soft_reset_rx),
                    // .rxdisperr(rxdisperr_i),
                    // .rxnotintable(rxnotintable_i),
                    // .rxusrclk(rxusrclk_i),
                    // .rxusrclk2(rxusrclk2_i),
                    // .rxdata(recieved_data),
                    // .rxcharisk(rxcharisk_i),
                    // .resetdone(resetdone),
                    // .track_data_out(track_data_out),
                    // .cplllocked(cplllocked),

                   //data_out here!!!
                   .SDA(I2C_SDA),
                   .SCL(I2C_SCL),
                   .SCK(SCK),    //DIN6
                   .CSB(CSB),    //DIN5
                   .MOSI(MOSI),   //DIN4
                   .MISO(MISO),    //DOUT1
                  // .RST_Ctl_RB(RST_Ctl_RB),   //DIN7
                   .Ctl_Clk_1(Ctl_Clk_1),    //DIN3
                   .Ctl_Clk_2(Ctl_Clk_2),    //DIN2
                   .Ctl_Load(Ctl_Load),     //DIN1
                   .Ctl_SIN(Ctl_SIN),     //DIN0
                   .Ctl_SOut(Ctl_SOut),      //DOUT0

                   .INJECTION(INJECTION),
                   .Sync_res(Sync_res),  
                   .RX_READY(RX_READY),
                   .DATA(recieved_data),
                   .CLK_ENABLE(clk_enable),


                  // external TS
                    .TS_IN(CLK400_BUF)
                 );


wire CLK_SER;
assign CLK_SER = fpga.CLK_1GHZ;

reg [3:0] cnt10 = 0;
always@(posedge CLK_SER)
    if (cnt10 == 9)
        cnt10 <= 0;
    else
        cnt10 <= cnt10 + 1;

wire load;
assign load = (cnt10 == 9);





logic [PSIZE-1:0][8:0] pattern;
localparam PSIZE = 100;

initial begin
    // pattern[0] = {1'b1, 8'hCB}; //K28_5
    pattern[0] = {1'b1, 8'h0}; //K28_5
    for (int i=1; i < 10; i = i + 1 )
    begin
        //  pattern[i] = {1'b1, i};
        // pattern[i] = {1'b1, 8'hCB}; //K28_5
        pattern[i] = {1'b1, 8'hbc}; //K28_5
        // pattern[i] = {1'b1, 8'hbc}; //K28_5
    end
    // for (int i=72; i < 100; i = i + 1 )
    // begin
    //     pattern[i] = {1'b1, 8'hbc}; //K28_5
    // end
    pattern[10] = {1'b1, 8'hbc};    
    pattern[11] = {1'b1, 8'hbc};    
    pattern[12] = {1'b1, 8'hbc};    
    pattern[13] = {1'b1, 8'hbc};    
    pattern[14] = {1'b1, 8'hbc};    
    pattern[15] = {1'b1, 8'hbc};    
    pattern[16] = {1'b1, 8'hbc};    
    pattern[17] = {1'b1, 8'hbc};    
    pattern[18] = {1'b1, 8'hbc};    
    pattern[19] = {1'b1, 8'hbc};    
    pattern[20] = {1'b1, 8'hbc};    
    pattern[21] = {1'b1, 8'hbc};    
    pattern[22] = {1'b1, 8'hbc};    
    pattern[23] = {1'b1, 8'hbc};    
    pattern[24] = {1'b1, 8'hbc};    
    pattern[25] = {1'b1, 8'hbc};    
    pattern[26] = {1'b1, 8'h1c};    
    pattern[27] = {1'b0, 8'haa};    
    pattern[28] = {1'b1, 8'h1c};    
    pattern[29] = {1'b0, 8'haa};    
    pattern[30] = {1'b1, 8'hbc};    
    pattern[31] = {1'b1, 8'hbc};    
    pattern[32] = {1'b1, 8'hbc};    
    pattern[33] = {1'b1, 8'hbc};    
    pattern[34] = {1'b0, 8'hc0};    
    pattern[35] = {1'b0, 8'h01};    
    pattern[36] = {1'b0, 8'he0};    
    pattern[37] = {1'b0, 8'h1f};    
    pattern[38] = {1'b1, 8'hbc};    
    pattern[39] = {1'b1, 8'hbc};    
    pattern[40] = {1'b1, 8'hbc};    
    pattern[41] = {1'b1, 8'hbc};    
    pattern[42] = {1'b0, 8'hc1};    
    pattern[43] = {1'b0, 8'hff};    
    pattern[44] = {1'b0, 8'heb};    
    pattern[45] = {1'b0, 8'h80};    
    pattern[46] = {1'b1, 8'hbc};    
    pattern[47] = {1'b1, 8'hbc};    
    pattern[48] = {1'b1, 8'hbc};    
    pattern[49] = {1'b1, 8'hbc};    
    pattern[50] = {1'b0, 8'h04};    
    pattern[51] = {1'b0, 8'hc0};    
    pattern[52] = {1'b0, 8'hc2};    
    pattern[53] = {1'b0, 8'h1a};    
    pattern[54] = {1'b1, 8'hbc};    
    pattern[55] = {1'b1, 8'hbc};    
    pattern[56] = {1'b1, 8'hbc};    
    pattern[57] = {1'b1, 8'hbc};    
    pattern[58] = {1'b1, 8'hbc};    
    pattern[59] = {1'b1, 8'hbc};    
    pattern[60] = {1'b1, 8'hbc};    
    pattern[61] = {1'b1, 8'hbc};    
    pattern[62] = {1'b1, 8'hbc};    
    pattern[63] = {1'b1, 8'hbc};    
    pattern[64] = {1'b1, 8'hbc};    
    pattern[65] = {1'b1, 8'hbc};    
    pattern[66] = {1'b1, 8'hbc};    
    pattern[67] = {1'b1, 8'hbc};    
    pattern[68] = {1'b1, 8'hbc};    
    pattern[69] = {1'b1, 8'hbc};    
    pattern[70] = {1'b1, 8'hbc};    
    pattern[71] = {1'b1, 8'hbc};    
    pattern[72] = {1'b1, 8'hbc};    
    pattern[73] = {1'b1, 8'hbc};    
    pattern[74] = {1'b1, 8'hbc};    
    pattern[75] = {1'b1, 8'hbc};    
    pattern[76] = {1'b1, 8'hbc};    
    pattern[77] = {1'b1, 8'hbc};    
    pattern[78] = {1'b1, 8'hbc};    
    pattern[79] = {1'b1, 8'hbc};    
    pattern[80] = {1'b1, 8'hbc};    
    pattern[81] = {1'b1, 8'hbc};    
    pattern[82] = {1'b1, 8'hbc};    
    pattern[83] = {1'b1, 8'hbc};    
    pattern[84] = {1'b1, 8'hbc};    
    pattern[85] = {1'b1, 8'hbc};    
    pattern[86] = {1'b1, 8'hbc};    
    pattern[87] = {1'b1, 8'hbc};    
    pattern[88] = {1'b1, 8'hbc};    
    pattern[89] = {1'b1, 8'hbc};    
    pattern[90] = {1'b1, 8'hbc};    
    pattern[91] = {1'b1, 8'hbc};    
    pattern[92] = {1'b1, 8'hbc};    
    pattern[93] = {1'b1, 8'hbc};    
    pattern[94] = {1'b1, 8'hbc};    
    pattern[95] = {1'b1, 8'hbc};    
    pattern[96] = {1'b1, 8'hbc};    
    pattern[97] = {1'b1, 8'hbc};    
    pattern[98] = {1'b1, 8'hbc};    
    pattern[99] = {1'b1, 8'hbc};    
    // pattern[100] = {1'b1, 8'hbc};    
    // pattern[101] = {1'b1, 8'hbc};    
    // pattern[102] = {1'b1, 8'hbc};    
    // pattern[103] = {1'b1, 8'hbc};    

    //  pattern[32] = {1'b0, 8'hc0};
    //  pattern[33] = {1'b0, 8'h37};
    //  pattern[34] = {1'b0, 8'h8b};
    //  pattern[35] = {1'b0, 8'h21};

    //  pattern[36] = {1'b0, 8'hc1};
    //  pattern[37] = {1'b0, 8'h36};
    //  pattern[38] = {1'b0, 8'h8c};
    //  pattern[39] = {1'b0, 8'h12};

    //  pattern[40] = {1'b1, 8'h1c};
    //  pattern[41] = {1'b0, 8'haa};
    //  pattern[42] = {1'b1, 8'h1c};
    //  pattern[43] = {1'b0, 8'haa};
     
    //  pattern[44] = {1'b1, 8'hbc};
    //  pattern[45] = {1'b1, 8'hbc};
    //  pattern[46] = {1'b1, 8'hbc};
    //  pattern[47] = {1'b1, 8'hbc};

    //  pattern[48] = {1'b1, 8'h1c};
    //  pattern[49] = {1'b0, 8'haa};
    //  pattern[50] = {1'b1, 8'h1c};
    //  pattern[51] = {1'b0, 8'haa};
    //  //0xc055a774
    //  pattern[52] = {1'b0, 8'hc0};
    //  pattern[53] = {1'b0, 8'h55};
    //  pattern[54] = {1'b0, 8'ha7};
    //  pattern[55] = {1'b0, 8'h74};
     
    // //  0xc109f5ff
    //  pattern[56] = {1'b0, 8'hc1};
    //  pattern[57] = {1'b0, 8'h09};
    //  pattern[58] = {1'b0, 8'hf5};
    //  pattern[59] = {1'b0, 8'hff};

    // //  0x1e3789ad
    //  pattern[60] = {1'b0, 8'h1e};
    //  pattern[61] = {1'b0, 8'h37};
    //  pattern[62] = {1'b0, 8'h89};
    //  pattern[63] = {1'b0, 8'had};

    //  pattern[64] = {1'b1, 8'h1c};
    //  pattern[65] = {1'b0, 8'haa};
    //  pattern[66] = {1'b1, 8'h1c};
    //  pattern[67] = {1'b0, 8'haa};

    //  pattern[68] = {1'b1, 8'h1c};
    //  pattern[69] = {1'b0, 8'haa};
    //  pattern[70] = {1'b1, 8'h1c};
    //  pattern[71] = {1'b0, 8'haa};


    //  pattern[72] = {1'b0, 8'hc0};
    //  pattern[73] = {1'b0, 8'h37};
    //  pattern[74] = {1'b0, 8'h8b};
    //  pattern[75] = {1'b0, 8'h21};

    //  pattern[76] = {1'b1, 8'hbc};
    //  pattern[77] = {1'b1, 8'hbc};
    //  pattern[78] = {1'b1, 8'hbc};
    //  pattern[79] = {1'b1, 8'hbc};

    //  pattern[80] = {1'b1, 8'h1c};
    //  pattern[81] = {1'b0, 8'haa};
    //  pattern[82] = {1'b1, 8'h1c};
    //  pattern[83] = {1'b0, 8'haa};
     
    //  pattern[84] = {1'b1, 8'hbc};
    //  pattern[85] = {1'b1, 8'hbc};
    //  pattern[86] = {1'b1, 8'hbc};
    //  pattern[87] = {1'b1, 8'hbc};
     


// '0x21caa', '0x21caa', '0x21caa', '0x21caa', '0xc055', '0xa774', '0x80d783', '0xc109', '0xf5ff', '0x1e37', '0x89ad', '0x21caa', '0x21caa', '0x21caa', '0x21caa', '0x21caa', '0x21caa', '0x21caa', '0x21caa', '0xc055', '0xa774', '0x80df53', '0xc109', '0xf5ff', '0x1e37', '0x89ad', '0x21caa', '0x21caa', '0x21caa', '0x21caa', '0x21caa', '0x21caa', '0x21caa', '0x21caa', '0xc055', '0xa774', '0x80e723', '0xc109', '0xf5ff', '0x1e37', '0x89ad', '0x21caa'

    
end

// logic [PSIZE-1:0][35:0] pattern;

// localparam PSIZE = 100;

// initial begin
//     for (int i=0; i < 40; i = i + 1 )
//     begin
//         // pattern[i] = {1'b1, 8'hbc}; //K28_5
//         pattern[i] = {4'b1111, 32'hbcbcbcbc}; //K28_5
//     end
//     for (int i=72; i < 100; i = i + 1 )
//     begin
//         // pattern[i] = {1'b1, 8'hbc}; //K28_5
//         pattern[i] = {4'b1111, 32'hbcbcbcbc}; //K28_5
//     end
    
//      pattern[41] = {4'b1010, 32'h1caa1caa};
//      pattern[42] = {4'b0000, 32'haaaaaaaa};
//      pattern[43] = {4'b1010, 32'h1caa1caa};
//      pattern[44] = {4'b0000, 32'ha7a7a7a7};
//      pattern[45] = {4'b0101, 32'haa1caa1c};
//      pattern[46] = {4'b0000, 32'haaaaaaaa};
//      pattern[47] = {4'b0101, 32'haa1caa1c};
     
//      pattern[48] = {4'b1111, 32'hbcbcbcbc};
//      pattern[49] = {4'b1111, 32'hbcbcbcbc};
//      pattern[50] = {4'b1111, 32'hbcbcbcbc};
//      pattern[51] = {4'b1111, 32'hbcbcbcbc};

//      pattern[52] = {4'b0000, 32'haaaaaaaa};
//      pattern[53] = {4'b0101, 32'haa1caa1c};
//      pattern[54] = {4'b1111, 32'h1c1c1c1c};
//      pattern[55] = {4'b0000, 32'haaaaaaaa};

//      pattern[56] = {4'b0000, 32'hc0c0c0c0};
//      pattern[57] = {4'b0000, 32'h55555555};
//      pattern[58] = {4'b0000, 32'ha7a7a7a7};
//      pattern[59] = {4'b0000, 32'h74747474};
     
//      pattern[60] = {4'b0000, 32'hffffffff};
// end

logic dispout, dispin = 0; 
always@(posedge CLK_SER)
    if (load)
        dispin <= dispout;

integer wcounter = 0;
always@(posedge CLK_SER)
    if (load)
        if (wcounter < PSIZE-1)
            wcounter <= wcounter +1;
        else
            wcounter <= 0;

integer sync_time = 0;
always@(posedge CLK_SER)
    if (load)
        sync_time <= sync_time + 1;



logic [8:0] datain;
// logic [35:0] datain;
assign datain = sync_time > 10 ? pattern[wcounter] : {1'b1, 8'hbc};
// assign datain = pattern[wcounter][];
// assign datain = sync_time > 10 ? pattern[wcounter] : {4'b1111, 32'hbcbcbcbc};

logic [9:0] dataout_enc;
// logic [9:0] dataout_enc;
encode_8b10b enc(.datain(datain[7:0]), .k(datain[8]), .dispin(dispin), .dataout(dataout_enc), .dispout(dispout)) ;


// logic [35:0] ser_out;
logic [9:0] ser_out;
always@(posedge CLK_SER)
    if (load)
        ser_out <= dataout_enc;
    else
        ser_out[9:0] <= {1'bx, ser_out[9:1]};



assign recieved_data = ser_out;

initial begin
    $dumpfile("mightypix.vcd");
    $dumpvars(0,pattern); //Needed ?
    $dumpvars(0);
end

endmodule
