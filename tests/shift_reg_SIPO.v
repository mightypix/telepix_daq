module shift_reg_SIPO(input clk, input serial_in, output reg [7:0] parallel_out);

reg [7:0] shift_register;

  always @(posedge clk)
    begin
      shift_register = {serial_in, shift_reg[7:1]};
      parallel_out = shift_reg;
    end
endmodule



shift_reg_SIPO_TP shift_reg_SIPO(
  .clk(Ctl_Clk_1),  //ck1 on TP
  .serial_in(Ctl_SIN), //
  .parallel_out(Ctl_Load),
);

initial begin
    $dumpfile("test_shift.vcd");
    // $dumpvars(0,pattern);
    $dumpvars(0);
end